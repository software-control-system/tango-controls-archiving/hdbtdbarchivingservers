package org.tango.archiving.collector.infra;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;
import sun.java2d.pipe.SpanIterator;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.withSettings;

@RunWith(MockitoJUnitRunner.class)
public class ArchivingTemporaryFileTest {

    final static Logger logger = LoggerFactory.getLogger(ArchivingTemporaryFileTest.class.getName());
    private final ArchivingDatabase mockArchivingDatabase = Mockito.mock(ArchivingDatabase.class);
    private final FileWriter channel = Mockito.mock(FileWriter.class, withSettings().verboseLogging());
    private String writtenValue = "";

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        writtenValue = "";
        doAnswer((Answer<Integer>) invocation -> {
            System.out.println("do answer");
            String line = invocation.getArgument(0);
            System.out.println(line);
            writtenValue = writtenValue + line;
            return 1;
        }).when(channel).write(anyString());
    }

    @Test
    public void writeScalar() throws IOException, ArchivingException {
        ArchivingTemporaryFile archivingTemporaryFile = new ArchivingTemporaryFile("attributeName", "tableName", AttrDataFormat._SCALAR, AttrWriteType._READ,
                (long) 10000, mockArchivingDatabase, "D:\\tmp\\test",
                "", DatabaseConnectionConfig.DataBaseType.ORACLE);
        archivingTemporaryFile.setFileWriter(channel);
        archivingTemporaryFile.openFile();
        AttributeScalarEvent event = new AttributeScalarEvent();
        event.setValue(12.0, null);
        event.setTimeStamp(System.currentTimeMillis());
        archivingTemporaryFile.processEventScalar(event);
        logger.info("mock write data to file:\n " + writtenValue);
        assertThat(writtenValue, containsString("1.20000000000000000E+01"));
    }

    @Test
    public void writeRWScalar() throws IOException, ArchivingException {
        ArchivingTemporaryFile archivingTemporaryFile = new ArchivingTemporaryFile("attributeName", "tableName", AttrDataFormat._SCALAR, AttrWriteType._READ_WRITE,
                (long) 10000, mockArchivingDatabase, "D:\\tmp\\test",
                "", DatabaseConnectionConfig.DataBaseType.ORACLE);
        archivingTemporaryFile.setFileWriter(channel);
        archivingTemporaryFile.openFile();
        AttributeScalarEvent event = new AttributeScalarEvent();
        event.setValue(new int[]{3, 6}, null);
        event.setTimeStamp(System.currentTimeMillis());
        event.setDataType(TangoConst.Tango_DEV_LONG);
        event.setWritable(AttrWriteType._READ_WRITE);

        archivingTemporaryFile.processEventScalar(event);
        logger.info("mock write data to file:\n " + writtenValue);
        assertThat(writtenValue, containsString("3.00000000000000000E+00\",\"6.00000000000000000E+00"));
    }

    @Test
    public void writeScalarNull() throws IOException, ArchivingException {
        ArchivingTemporaryFile archivingTemporaryFile = new ArchivingTemporaryFile("attributeName", "tableName", AttrDataFormat._SCALAR, AttrWriteType._READ,
                (long) 10000, mockArchivingDatabase, "D:\\tmp\\test",
                "", DatabaseConnectionConfig.DataBaseType.ORACLE);
        archivingTemporaryFile.setFileWriter(channel);
        archivingTemporaryFile.openFile();
        AttributeScalarEvent event = new AttributeScalarEvent();
        event.setValue(null, null);
        event.setTimeStamp(System.currentTimeMillis());
        archivingTemporaryFile.processEventScalar(event);
        logger.info("mock write data to file:\n " + writtenValue);
        assertThat(writtenValue, containsString("\"\""));
    }

    @Test
    public void writeSpectrum() throws IOException, ArchivingException {
        ArchivingTemporaryFile archivingTemporaryFile = new ArchivingTemporaryFile("attributeName", "tableName", AttrDataFormat._SPECTRUM, AttrWriteType._READ,
                (long) 10000, mockArchivingDatabase, "D:\\tmp\\test",
                "", DatabaseConnectionConfig.DataBaseType.ORACLE);

        archivingTemporaryFile.setFileWriter(channel);
        archivingTemporaryFile.openFile();
        SpectrumEventRO event = new SpectrumEventRO();
        event.setValue(new double[]{12.0, 13.0, 14.0}, null);
        event.setDimX(3);
        event.setTimeStamp(System.currentTimeMillis());
        archivingTemporaryFile.processEventSpectrum(event);
        logger.info("total mock write data to file:\n " + writtenValue);
        assertThat(writtenValue, containsString("1.20000000000000000E+01,1.30000000000000000E+01,1.40000000000000000E+01"));
        assertThat(writtenValue, containsString("3.0"));
    }


}
