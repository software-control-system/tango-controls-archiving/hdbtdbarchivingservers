package org.tango.archiving.collector.mode;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.archiving.config.InsertionModes;
import org.junit.Ignore;
import org.junit.Test;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ModeHandlerTest {

    @Test
    public void periodic() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes().setPeriodic(true).setPeriodPeriodic(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(2, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(2)), equalTo(true));
    }

    private AttributeScalarEvent getCurrentEvent(Object value, int dataType) {
        AttributeScalarEvent event = new AttributeScalarEvent();
        event.setValue(value, new boolean[]{});
        event.setDataType(dataType);
        event.setTimeStamp(3600000);
        return event;
    }

    private AttributeScalarEvent getPreviousEvent(Object value) {
        AttributeScalarEvent event = new AttributeScalarEvent();
        event.setValue(value, new boolean[]{});
        event.setTimeStamp(0);
        return event;
    }

    @Test
    public void periodic1Hour() throws DevFailed {
        final InsertionModes archiverMode = new InsertionModes().setPeriodic(true).setPeriodPeriodic(3600000);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        assertThat(mode.isDataArchivable("test", getCurrentEvent(3, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(2)), equalTo(true));
    }

    @Test
    public void testPeriodicSpectrum() {
        final InsertionModes archiverMode = new InsertionModes().setPeriodic(true).setPeriodPeriodic(3600000);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        SpectrumEventRO currentValue = new SpectrumEventRO();
        currentValue.setTimeStamp(3600000);
        SpectrumEventRO previousValue = new SpectrumEventRO();
        previousValue.setTimeStamp(0);
        mode.isSpectrumDataArchivable("test", currentValue, previousValue);
    }

    @Test
    public void testDifferenceSpectrum() {
        final InsertionModes archiverMode = new InsertionModes().setDifference(true).setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        SpectrumEventRO currentValue = new SpectrumEventRO();
        currentValue.setValue(new double[]{1, 2}, new boolean[]{});
        SpectrumEventRO previousValue = new SpectrumEventRO();
        currentValue.setValue(new double[]{1, 3}, new boolean[]{});
        mode.isSpectrumDataArchivable("test", currentValue, previousValue);
    }

    @Test
    public void differenceYes() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(2, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(3)), equalTo(true));
    }

    @Test
    @Ignore
    public void differenceNo() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(2, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(2)), equalTo(false));
    }

    @Test
    public void differenceNull() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(null, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(2)), equalTo(true));
    }

    @Test
    public void differenceNullNull() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(null, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(null)), equalTo(false));
    }

    @Test
    public void differenceStringYes() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent("hello", TangoConst.Tango_DEV_STRING), getPreviousEvent("bye")), equalTo(true));
    }

    @Test
    public void differenceStateYes() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(DevState.FAULT, TangoConst.Tango_DEV_STATE), getPreviousEvent(DevState.ON)), equalTo(true));
    }

    @Test
    public void differenceStateNo() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        assertThat(mode.isDataArchivable("test", getCurrentEvent(DevState.FAULT, TangoConst.Tango_DEV_STATE), getPreviousEvent(DevState.FAULT)), equalTo(false));
    }

    @Test
    public void differenceStringNo() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setDifference(true);
        archiverMode.setPeriodDifference(1);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent("hello", TangoConst.Tango_DEV_STRING), getPreviousEvent("hello")), equalTo(false));
    }

    @Test
    public void absoluteYes() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setAbsolute(true);
        archiverMode.setPeriodAbsolute(1);
        archiverMode.setDecreaseDeltaAbsolute(2.0);
        archiverMode.setIncreaseDeltaAbsolute(5.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(3, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(5)), equalTo(true));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(4, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1)), equalTo(true));
    }

    @Test
    public void absoluteTest() throws InterruptedException, DevFailed {
        //absolute=1000 milliseconds valInf=1.0 valSup=-2.0
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setAbsolute(true);
        archiverMode.setPeriodAbsolute(1);
        archiverMode.setDecreaseDeltaAbsolute(-2.0);
        archiverMode.setIncreaseDeltaAbsolute(1.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(5, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(8)), equalTo(true));
    }

    @Test
    public void absoluteSlowDrift() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setAbsolute(true);
        archiverMode.setPeriodAbsolute(1);
        archiverMode.setDecreaseDeltaAbsolute(5.0);
        archiverMode.setIncreaseDeltaAbsolute(0.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(20, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(3)), equalTo(true));
    }

    @Test
    public void absoluteNo() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setAbsolute(true);
        archiverMode.setPeriodAbsolute(1);
        archiverMode.setDecreaseDeltaAbsolute(0.0);
        archiverMode.setIncreaseDeltaAbsolute(5.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-1, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-3)), equalTo(false));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(5, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(4)), equalTo(false));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(1.9, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-3)), equalTo(false));
    }

    @Test
    public void relativeNo() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setRelative(true);
        archiverMode.setPeriodRelative(1);
        archiverMode.setIncreasePercentRelative(2.0);
        archiverMode.setDecreasePercentRelative(1.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-1, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-3)), equalTo(true));
    }

    @Test
    public void relativeYes() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setRelative(true);
        archiverMode.setPeriodRelative(1);
        archiverMode.setIncreasePercentRelative(2.0);
        archiverMode.setDecreasePercentRelative(1.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-1, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(true));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-1.1, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.2)), equalTo(true));
    }

    @Test
    public void thresholdYes() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setThreshold(true);
        archiverMode.setPeriodThreshold(1);
        archiverMode.setMinThresholdValue(-1.0);
        archiverMode.setMaxThresholdValue(2.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-1, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(true));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(2, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(true));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-2, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(true));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(3, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(true))
        ;
    }

    @Test
    public void thresholdNo() throws InterruptedException, DevFailed {
        final InsertionModes archiverMode = new InsertionModes();
        archiverMode.setThreshold(true);
        archiverMode.setPeriodThreshold(1);
        archiverMode.setMinThresholdValue(-1.0);
        archiverMode.setMaxThresholdValue(2.0);
        ModeHandler mode = new ModeHandler(archiverMode);
        mode.start("test");
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(-0.9, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(false));
        TimeUnit.MILLISECONDS.sleep(1);
        assertThat(mode.isDataArchivable("test", getCurrentEvent(1.9, TangoConst.Tango_DEV_DOUBLE), getPreviousEvent(-1.1)), equalTo(false));
    }


}
