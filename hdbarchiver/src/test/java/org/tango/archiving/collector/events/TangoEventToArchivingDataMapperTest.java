package org.tango.archiving.collector.events;


import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import org.junit.Test;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;

import java.lang.reflect.Array;
import java.math.BigInteger;

import static org.junit.Assert.assertEquals;


public class TangoEventToArchivingDataMapperTest {


    @Test
    public void mapScalarEventRO() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert(1);
        AttributeScalarEvent event = new AttributeScalarEvent();
        new TangoEventToArchivingDataMapper().setScalarValue(event, da, AttrWriteType._READ);
        assertEquals(1, event.getValue());
    }

    @Test
    public void mapScalarEventULong64RO() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert_u64(1);
        AttributeScalarEvent event = new AttributeScalarEvent();
        new TangoEventToArchivingDataMapper().setScalarValue(event, da, AttrWriteType._READ);
        assertEquals(BigInteger.valueOf(1), event.getValue());
    }

    @Test
    public void mapScalarEventWO() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert(2);
        AttributeScalarEvent event = new AttributeScalarEvent();
        new TangoEventToArchivingDataMapper().setScalarValue(event, da, AttrWriteType._WRITE);
        assertEquals(2, event.getValue());
    }

    @Test
    public void mapScalarEventRW() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert(new int[]{1, 3});
        AttributeScalarEvent event = new AttributeScalarEvent();
        new TangoEventToArchivingDataMapper().setScalarValue(event, da, AttrWriteType._READ_WRITE);
        assertEquals(1, Array.get(event.getValue(), 0));
        assertEquals(3, Array.get(event.getValue(), 1));
    }

    @Test
    public void mapSpectrumEventRW() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert(new int[]{1, 3, 5, 7}, 1, 2);
        SpectrumEventRW event = new SpectrumEventRW();
        new TangoEventToArchivingDataMapper().setSpectrumReadWriteValue(da, event);
        assertEquals(1, Array.get(event.getValue(), 0));
        assertEquals(3, Array.get(event.getValue(), 1));
        assertEquals(5, Array.get(event.getValue(), 2));
    }

    @Test
    public void mapSpectrumEventRO() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert(new int[]{1, 3}, 1, 2);
        SpectrumEventRO event = new SpectrumEventRO();
        new TangoEventToArchivingDataMapper().setSpectrumReadValue(da, event);
        assertEquals(1, Array.get(event.getValue(), 0));
        assertEquals(3, Array.get(event.getValue(), 1));
    }
    @Test
    public void mapSpectrumEventULong64RO() throws DevFailed {
        DeviceAttribute da = new DeviceAttribute("");
        da.insert_u64(new long[]{1, 3}, 1, 2);
        SpectrumEventRO event = new SpectrumEventRO();
        new TangoEventToArchivingDataMapper().setSpectrumReadValue(da, event);
        assertEquals(BigInteger.valueOf(1), Array.get(event.getValue(), 0));
        assertEquals(BigInteger.valueOf(3), Array.get(event.getValue(), 1));
    }

}
