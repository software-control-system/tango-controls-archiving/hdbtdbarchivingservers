package org.tango.archiving.collector.mode;

import org.junit.Test;
import org.tango.archiving.collector.mode.ArchivingMode;
import org.tango.archiving.collector.mode.ModeChronometers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class ModeChronometersTest {

    @Test
    public void testOver() throws InterruptedException {
        int time = 10;
        ModeChronometers modeChronometers = new ModeChronometers();
        modeChronometers.startMode(ArchivingMode.ABSOLUTE, time);
        Thread.sleep(time);
        assertThat(modeChronometers.isOver(ArchivingMode.ABSOLUTE, time), equalTo(true));

    }

    @Test
    public void testOverBefore10Percent() throws InterruptedException {
        int time = 10;
        ModeChronometers modeChronometers = new ModeChronometers();
        modeChronometers.startMode(ArchivingMode.ABSOLUTE, time);
        Thread.sleep(9);
        assertThat(modeChronometers.isOver(ArchivingMode.ABSOLUTE, time), equalTo(true));
    }

    @Test
    public void testOverAfter10Percent() throws InterruptedException {
        int time = 10;
        ModeChronometers modeChronometers = new ModeChronometers();
        modeChronometers.startMode(ArchivingMode.ABSOLUTE, time);
        Thread.sleep(time);
        assertThat(modeChronometers.isOver(ArchivingMode.ABSOLUTE, time), equalTo(true));
    }


    @Test
    public void testNotOver() {
        int time = 100;
        ModeChronometers modeChronometers = new ModeChronometers();
        modeChronometers.startMode(ArchivingMode.ABSOLUTE, time);
        assertThat(modeChronometers.isOver(ArchivingMode.ABSOLUTE, time), equalTo(false));
    }
}
