package org.tango.archiving.server.archiver;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.hamcrest.core.StringContains;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.test.ArchivingDataDevice;
import org.tango.archiving.test.MockHdbArchiver;
import org.tango.server.ServerManager;
import org.tango.utils.DevFailedUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class HdbArchiverTest {

    final static Logger logger = LoggerFactory.getLogger(HdbArchiverTest.class);
    private static final String noDbDeviceName = "archiving/hdb-oracle/hdbarchiver.01";
    // private static final String noDbGiopPort = "12354";
    private static final String noDbInstanceName = "ora";
    public static String deviceNameRoot;
    private static String deviceName;
    private static Process process;

    @BeforeClass
    public static void startDevice() throws DevFailed, IOException {
        System.setProperty("without.atk", "true");
        System.setProperty("org.tango.server.checkalarms", "false");
        startRootServer();
        startNoDb(HdbArchiver.class);
    }

    private static void startRootServer() throws IOException {
        try (ServerSocket socket = new ServerSocket(0)) {
            // start root server
            socket.setReuseAddress(true);
            socket.close();
            deviceNameRoot = "tango://localhost:" + socket.getLocalPort() + "/" + ArchivingDataDevice.NO_DB_DEVICE_NAME
                    + "#dbase=no";
            final String classpath = System.getProperty("java.class.path");
            final ProcessBuilder pb = new ProcessBuilder("java", "-cp", classpath, ArchivingDataDevice.class.getCanonicalName(),
                    Integer.toString(socket.getLocalPort()));
            process = pb.start();
            inheritIO(process.getInputStream(), System.out);
            inheritIO(process.getErrorStream(), System.err);
            logger.debug("WAITING for root device startup");
            try {
                Thread.sleep(6000);
            } catch (final InterruptedException e) {
            }
            logger.debug("WAITING for root device DONE");
        }
    }

    private static void startNoDb(final Class<?> deviceClass) throws DevFailed, IOException {

        try (ServerSocket socket = new ServerSocket(0)) {
            socket.setReuseAddress(true);
            socket.close();
            System.setProperty("OAPort", Integer.toString(socket.getLocalPort()));
            deviceName = "tango://localhost:" + Integer.toString(socket.getLocalPort()) + "/" + noDbDeviceName + "#dbase=no";
        }
        ServerManager.getInstance().addClass(deviceClass.getCanonicalName(), MockHdbArchiver.class);
        final String fileProps = HdbArchiver.class.getResource("/HdbArchiverProps").getPath();
        ServerManager.getInstance()
                .startError(new String[]{noDbInstanceName, "-nodb", "-dlist", noDbDeviceName, "-file=" + fileProps},
                        "HdbArchiver");
        // wait device start-up
        DeviceProxy proxy = new DeviceProxy(deviceName);
        while (proxy.state().equals(DevState.INIT)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        logger.info(proxy.state().toString());
        logger.info(proxy.status());
    }

    private static void inheritIO(final InputStream src, final PrintStream dest) {
        new Thread(() -> {
            final Scanner sc = new Scanner(src);
            while (sc.hasNextLine()) {
                dest.println("//forked process// " + sc.nextLine());
            }
            dest.flush();
            sc.close();
        }).start();
    }

    @AfterClass
    public static void stop() throws DevFailed {
        process.destroy();
        ServerManager.getInstance().stop();
    }

    @Test
    public void scalarBooleanPeriodic() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startScalarBooleanPeriodic");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void scalarDoublePeriodic() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startScalarDoublePeriodic");
        Thread.sleep(600);
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void spectrumDoublePeriodic() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startSpectrumDoublePeriodic");
        Thread.sleep(600);
        for (int i = 0; i < 3; i++) {
            Thread.sleep(MockHdbArchiver.PERIOD);
            DeviceData response = proxy.command_inout("isArchivingOK");
            assertThat(response.extractBoolean(), equalTo(true));
        }
    }

    @Test
    public void startSpectrumULongPeriodic() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startSpectrumULongPeriodic");
        Thread.sleep(600);
        for (int i = 0; i < 3; i++) {
            Thread.sleep(MockHdbArchiver.PERIOD);
            DeviceData response = proxy.command_inout("isArchivingOK");
            assertThat(response.extractBoolean(), equalTo(true));
        }
    }


    @Test
    @Ignore
    public void scalarDoublePeriodicLoop() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startScalarDoublePeriodic");
        Thread.sleep(600);
        for (int i = 0; i < 100; i++) {
            Thread.sleep(MockHdbArchiver.PERIOD);
            DeviceData response = proxy.command_inout("isArchivingOK");
            assertThat(response.extractBoolean(), equalTo(true));
        }
    }

    @Test
    public void scalarLongPeriodic() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startScalarLongPeriodic");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void scalarDoubleDifference() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startScalarDoubleDifference");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void scalarDoubleAbsolute() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startScalarDoubleAbsolute");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void scalarError() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startError");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void scalarNaN() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startNaN");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    @Ignore("events without tangodb are not supported")
    public void scalarEvent() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("startEvent");
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("isArchivingOK");
        assertThat(response.extractBoolean(), equalTo(true));
    }

    @Test
    public void changePeriodTest() throws Exception {
        DeviceProxy proxy = new DeviceProxy(deviceName);
        proxy.command_inout("initMock");

        AttributeLightMode attributeLightMode = new AttributeLightMode();
        String attributeName = HdbArchiverTest.deviceNameRoot + "/aDouble";
        TangoAttribute attribute = new TangoAttribute(attributeName);
        attributeLightMode.setAttributeCompleteName(attributeName);
        attributeLightMode.setDataType(attribute.getDataType());
        attributeLightMode.setDataFormat(attribute.getDeviceAttribute().getDataFormat().value());
        attributeLightMode.setWritable(attribute.getWriteType().value());
        Mode archMode = new Mode();
        ModePeriode modePeriode = new ModePeriode();
        modePeriode.setPeriod(1000);
        archMode.setModeP(modePeriode);
        attributeLightMode.setMode(archMode);

        ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
        archivingMessConfig.add(attributeLightMode);
        System.out.println("TriggerArchiveConf " + Arrays.toString(archivingMessConfig.toArray()));
        DeviceData request = new DeviceData();
        request.insert(archivingMessConfig.toArray());
        proxy.command_inout("TriggerArchiveConf", request);
        try {
            Thread.sleep(600);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        DeviceData response = proxy.command_inout("getCollectorsStatus");
        String[] collectorsStatus = response.extractStringArray();
        System.out.println("collector status:\n" + String.join("\n", collectorsStatus) + "\n");
        assertNotNull(collectorsStatus);
        assertEquals("1 polling collectors and 0 event collectors", collectorsStatus[2]);
        assertEquals("DefaultScalarPooling-MODE_P + 1000 - 1 attribute.s - is refreshing = true",
                collectorsStatus[4]);
        assertEquals("attributes : " + attributeName.toLowerCase() + " , ", collectorsStatus[6]);

        // Changement de la periode
        attributeLightMode.getMode().getModeP().setPeriod(2000);
        archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
        archivingMessConfig.add(attributeLightMode);
        System.out.println("TriggerArchiveConf " + Arrays.toString(archivingMessConfig.toArray()));
        request.insert(archivingMessConfig.toArray());
        proxy.command_inout("TriggerArchiveConf", request);
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        response = proxy.command_inout("getCollectorsStatus");
        collectorsStatus = response.extractStringArray();
        System.out.println("collector status:\n" + String.join("\n", collectorsStatus) + "\n");
        assertNotNull(collectorsStatus);
        assertEquals("1 polling collectors and 0 event collectors", collectorsStatus[2]);
        assertEquals("DefaultScalarPooling-MODE_P + 2000 - 1 attribute.s - is refreshing = true", collectorsStatus[4]);
        assertEquals("attributes : " + attributeName.toLowerCase() + " , ", collectorsStatus[6]);
    }

    @Test
    public void changeAddAttributeTest() throws DevFailed {
        try {
            DeviceProxy proxy = new DeviceProxy(deviceName);
            proxy.command_inout("initMock");
            DeviceData request = new DeviceData();

            AttributeLightMode attributeLightMode = new AttributeLightMode();
            String attributeName = HdbArchiverTest.deviceNameRoot + "/aDouble";
            TangoAttribute attribute = new TangoAttribute(attributeName);
            attributeLightMode.setAttributeCompleteName(attributeName);
            attributeLightMode.setDataType(attribute.getDataType());
            attributeLightMode.setDataFormat(attribute.getDeviceAttribute().getDataFormat().value());
            attributeLightMode.setWritable(attribute.getWriteType().value());
            Mode archMode = new Mode();
            ModePeriode modePeriode = new ModePeriode();
            modePeriode.setPeriod(1000);
            archMode.setModeP(modePeriode);
            attributeLightMode.setMode(archMode);

            AttributeLightMode attributeLightMode2 = new AttributeLightMode();
            String attributeName2 = HdbArchiverTest.deviceNameRoot + "/aLong";
            TangoAttribute attribute2 = new TangoAttribute(attributeName2);
            attributeLightMode2.setAttributeCompleteName(attributeName2.toLowerCase());
            attributeLightMode2.setAttributeCompleteName(attributeName2);
            attributeLightMode2.setDataType(attribute2.getDataType());
            attributeLightMode2.setDataFormat(attribute2.getDeviceAttribute().getDataFormat().value());
            attributeLightMode2.setWritable(attribute2.getWriteType().value());
            attributeLightMode2.setMode(archMode);

            AttributeLightMode attributeLightMode3 = new AttributeLightMode();
            String attributeName3 = HdbArchiverTest.deviceNameRoot + "/aBoolean";
            TangoAttribute attribute3 = new TangoAttribute(attributeName3);
            attributeLightMode3.setAttributeCompleteName(attributeName3.toLowerCase());
            attributeLightMode3.setAttributeCompleteName(attributeName3);
            attributeLightMode3.setDataType(attribute3.getDataType());
            attributeLightMode3.setDataFormat(attribute3.getDeviceAttribute().getDataFormat().value());
            attributeLightMode3.setWritable(attribute3.getWriteType().value());
            Mode archMode2 = new Mode();
            archMode2.setModeP(modePeriode);
            ModeDifference modeDifference = new ModeDifference();
            modeDifference.setPeriod(100);
            archMode2.setModeD(modeDifference);
            attributeLightMode3.setMode(archMode2);

            ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
            archivingMessConfig.add(attributeLightMode);
            archivingMessConfig.add(attributeLightMode2);
            archivingMessConfig.add(attributeLightMode3);
            request.insert(archivingMessConfig.toArray());
/*
false, 9, tango://localhost:63184/1/1/1#dbase=no/aDouble, 5, 0, 0, , NULL, 2, MODE_P, 1000,
11, tango://localhost:63184/1/1/1#dbase=no/aBoolean, 1, 0, 0, , NULL, 4, MODE_P, 1000, MODE_D, 100,
9, tango://localhost:63184/1/1/1#dbase=no/aLong, 3, 0, 0, , NULL, 2, MODE_P, 1000
 */

            request.insert(archivingMessConfig.toArray());
            proxy.command_inout("TriggerArchiveConf", request);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            DeviceData response = proxy.command_inout("getCollectorsStatus");
            String[] collectorsStatus = response.extractStringArray();
            System.out.println(String.join("\n", collectorsStatus));
            assertNotNull(collectorsStatus);
            assertEquals("2 polling collectors and 0 event collectors", collectorsStatus[2]);
            assertEquals(
                    "DefaultScalarPooling-MODE_P + 1000 - 2 attribute.s - is refreshing = true",
                    collectorsStatus[4]);
            assertThat(collectorsStatus[6], StringContains.containsString(attributeName2.toLowerCase()));
            assertThat(collectorsStatus[6], StringContains.containsString(attributeName.toLowerCase()));
            assertEquals(
                    "DefaultScalarPooling-MODE_P + 1000 MODE_D + 100 - 1 attribute.s - is refreshing = true",
                    collectorsStatus[9]);
            assertThat(collectorsStatus[11], StringContains.containsString(attributeName3.toLowerCase()));
            // proxy.command_inout("stopAll");
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void zInitTest() throws DevFailed, InterruptedException {
        for (int i = 0; i < 50; i++) {
            DeviceProxy proxy = new DeviceProxy(deviceName);
            proxy.command_inout("initMock");
            DeviceData test1 = proxy.command_inout("getCollectorsStatus");
            String[] collectorsStatusTest = test1.extractStringArray();
            System.out.println("collector status:\n" + String.join("\n", collectorsStatusTest) + "\n");
            if (collectorsStatusTest[0].startsWith("collecting errors:1")) {
                Assert.fail("collector should be empty " + i);
            }
            proxy.command_inout("startError");
            TimeUnit.MILLISECONDS.sleep(100);
        }

    }

}
