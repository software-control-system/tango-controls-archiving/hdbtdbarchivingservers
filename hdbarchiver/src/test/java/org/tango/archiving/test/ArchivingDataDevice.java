package org.tango.archiving.test;

import fr.esrf.Tango.DevFailed;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.TransactionType;
import org.tango.utils.DevFailedUtils;

@Device(transactionType = TransactionType.NONE)
public class ArchivingDataDevice {

    public static final String NO_DB_DEVICE_NAME = "1/1/1";
    double aDouble = 0;
    int aLong = 0;
    boolean aBoolean = false;
    String aString = "hello";

    int ctr = 0;
   // double[] values = new double[]{0, 1, 2, 3, 4, 5, 4, 3, 2, 1};
    double[] values = new double[]{ 3, 4, 5, 4, 3};

    int[] valuesLong = new int[]{ 3, 4, 5, 4, 3};
    public static void main(final String[] args) throws DevFailed {
        System.setProperty("OAPort", args[0]);
        ServerManager.getInstance().addClass(ArchivingDataDevice.class.getCanonicalName(), ArchivingDataDevice.class);
        ServerManager.getInstance().startError(new String[]{"1", "-nodb", "-dlist", NO_DB_DEVICE_NAME},
                ArchivingDataDevice.class.getSimpleName() + "/1");
    }

    @Attribute
    public double getADouble() {
        return aDouble++;
    }

    @Attribute
    public double[] getADoubleSpectrum() {
        return values;
    }

    @Attribute
    public int[] getAULongSpectrum() {
        return valuesLong;
    }

    @Attribute
    public int getALong() {
        return aLong++;
    }

    @Attribute
    public boolean getABoolean() {
        aBoolean = !aBoolean;
        return aBoolean;
    }

    @Attribute
    public String getAString() {
        return aString;
    }

    @Attribute
    public double getAbsolute() {
        if (ctr == values.length - 1) {
            ctr = 0;
        }
        return values[ctr++];
    }

    @Attribute
    public String getANull() {
        return null;
    }

    @Attribute
    public double getNaN() {
        return Double.NaN;
    }

    @Attribute
    public double getError() throws DevFailed {
        throw DevFailedUtils.newDevFailed("test error");
    }


}
