package org.tango.archiving.test;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.TangoCollectorService;
import org.tango.archiving.collector.data.AAttributeEvent;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.server.archiver.HdbArchiver;
import org.tango.archiving.server.archiver.HdbArchiverTest;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.TransactionType;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@Device(transactionType = TransactionType.NONE)
public class MockHdbArchiver extends HdbArchiver {

    public static final int PERIOD = 100;
    private final Logger logger = LoggerFactory.getLogger(MockHdbArchiver.class);
    // private int insertionCounter = 0;
    private final List<Long> timestampsMillis = new ArrayList<>();
    private final List<Long> periods = new ArrayList<>();
    ArchivingInfra mockArchivingInfra = Mockito.mock(ArchivingInfra.class);
    private boolean isArchivingOK = false;
    private long previousTime = System.currentTimeMillis();

    @Override
    protected TangoCollectorService buildDbProxy(DatabaseConnectionConfig params, boolean insertErrors) {
        logger.debug("building mock DB proxy");
        return new TangoCollectorService(mockArchivingInfra, deviceName, forceUseEvents, !isUsingTemporaryFiles(), 0);
    }

    @Command
    public void startMultiPeriodic() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = new ArchivingConfigs();
        for (int i = 0; i < 10000; i++) {
            ArchivingConfig mode = createPeriodicTask(HdbArchiverTest.deviceNameRoot + "/aBoolean");
            tasks.addConfiguration(mode);
        }

        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            // ScalarEvent event = invocation.getArgument(0);
            // assertArchiving(event, Boolean.class);
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void initMock() {
        System.out.println("##INIT mock");
        logger.debug("start init mock device");
        MockitoAnnotations.initMocks(this);
        isArchivingOK = false;
        previousTime = System.currentTimeMillis();
        if (tangoCollectorService != null)
            tangoCollectorService.stopAll();
        periods.clear();
        logger.debug("init mock device done");
        doNothing().when(mockArchivingInfra).closeConnection();
    }

    private ArchivingConfig createPeriodicTask(String attributeName) throws DevFailed {
        TangoAttribute attribute = new TangoAttribute(attributeName);
        ArchivingConfig attributeLightMode = new ArchivingConfig();
        AttributeConfig attributeConfig = new AttributeConfig();
        attributeConfig.setFullName(attributeName);
        attributeConfig.setFormat(attribute.getDeviceAttribute().getDataFormat().value());
        attributeConfig.setWriteType(attribute.getWriteType().value());
        attributeLightMode.setAttributeConfig(attributeConfig);
        InsertionModes archMode = new InsertionModes();
        archMode.setPeriodic(true);
        archMode.setPeriodPeriodic(PERIOD);
        attributeLightMode.setModes(archMode);
        return attributeLightMode;
    }

    @Command
    public void startScalarBooleanPeriodic() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(HdbArchiverTest.deviceNameRoot + "/aBoolean");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            assertArchiving(event, Boolean.class);
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    private ArchivingConfigs createPeriodic(String attributeName) throws DevFailed {
        final ArchivingConfigs tasks = new ArchivingConfigs();
        tasks.addConfiguration(createPeriodicTask(attributeName));
        return tasks;
    }

    private void assertArchiving(AAttributeEvent<?> event, Class<?> eventType) {
        long currentTime;
        System.out.println("######################event = " + event);
        if (event.getValue() != null) {
            currentTime = event.getTimeStamp();
            timestampsMillis.add(currentTime);
            long realPeriod = currentTime - previousTime;
            periods.add(realPeriod);
            System.out.println("realPeriod = " + realPeriod);
            System.out.println("PERIOD = " + PERIOD);
            logger.info("archiving period is {} ms, data type = {}", realPeriod,
                    event.getValue().getClass().getCanonicalName());
            if (event.getValue().getClass().isAssignableFrom(eventType) /*&& realPeriod <= (PERIOD + 20)*/) {
                isArchivingOK = true;
                System.out.println("isArchivingOK = " + isArchivingOK);
            } else {
                logger.error("FAILED for type {}={}, value type or period", event.getValue().getClass(), eventType);
                //logger.error("FAILED for {}, period {}", event.getAttributeCompleteName(), realPeriod);
                isArchivingOK = false;
                System.out.println("isArchivingOK = " + isArchivingOK);
            }
        } else {
            logger.info("value of {} is null", event.getAttributeCompleteName());
            currentTime = System.currentTimeMillis();
            isArchivingOK = true;
            System.out.println("isArchivingOK = " + isArchivingOK);
        }
        previousTime = currentTime;
      /*  if (insertionCounter < 2) {
            isArchivingOK = true;
            System.out.println("isArchivingOK = " + isArchivingOK);
        }*/
        //insertionCounter++;
        logger.info("###############OK= {} - insertion periods are {}", isArchivingOK, periods);
    }

    @Command
    public void startScalarDoublePeriodic() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(HdbArchiverTest.deviceNameRoot + "/aDouble");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            assertArchiving(event, Double.class);
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startSpectrumDoublePeriodic() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(
                HdbArchiverTest.deviceNameRoot + "/aDoubleSpectrum");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            SpectrumEventRO event = invocation.getArgument(0);
            assertArchiving(event, double[].class);
            return null;
        }).when(mockArchivingInfra).store(any(SpectrumEventRO.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startSpectrumULongPeriodic() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(
                HdbArchiverTest.deviceNameRoot + "/aULongSpectrum");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            SpectrumEventRO event = invocation.getArgument(0);
            assertArchiving(event, int[].class);
            return null;
        }).when(mockArchivingInfra).store(any(SpectrumEventRO.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startScalarLongPeriodic() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(HdbArchiverTest.deviceNameRoot + "/aLong");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            assertArchiving(event, Integer.class);
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startScalarDoubleDifference() throws ArchivingException, DevFailed {
        initMock();

        ArchivingConfigs tasks = new ArchivingConfigs();
        ArchivingConfig config = new ArchivingConfig();
        AttributeConfig attribute = new AttributeConfig();
        attribute.setFullName(HdbArchiverTest.deviceNameRoot + "/aDouble");
        attribute.setFormat(AttrDataFormat._SCALAR);
        attribute.setWriteType(AttrWriteType._READ);
        config.setAttributeConfig(attribute);
        InsertionModes archMode = new InsertionModes();
        archMode.setPeriodPeriodic(10000);
        archMode.setPeriodic(true);
        archMode.setDifference(true);
        archMode.setPeriodDifference(PERIOD);
        config.setModes(archMode);
        tasks.addConfiguration(config);
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            assertArchiving(event, Double.class);
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startScalarDoubleAbsolute() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = new ArchivingConfigs();
        ArchivingConfig attributeLightMode = new ArchivingConfig();
        AttributeConfig attribute = new AttributeConfig();
        attribute.setFullName(HdbArchiverTest.deviceNameRoot + "/absolute");
        attribute.setFormat(AttrDataFormat._SCALAR);
        attribute.setWriteType(AttrWriteType._READ);
        attributeLightMode.setAttributeConfig(attribute);
        InsertionModes archMode = new InsertionModes();
        archMode.setPeriodPeriodic(10000);
        archMode.setPeriodic(true);
        archMode.setAbsolute(true);
        archMode.setPeriodAbsolute(PERIOD);
        archMode.setIncreaseDeltaAbsolute(1.0);
        archMode.setDecreaseDeltaAbsolute(2.0);
        attributeLightMode.setModes(archMode);
        tasks.addConfiguration(attributeLightMode);
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            assertArchiving(event, Double.class);
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startError() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(HdbArchiverTest.deviceNameRoot + "/error");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(final InvocationOnMock invocation) {
                AttributeScalarEvent event = invocation.getArgument(0);
                assertArchiving(event, Double.class);
                return null;
            }
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startNaN() throws ArchivingException, DevFailed {
        initMock();
        final ArchivingConfigs tasks = createPeriodic(HdbArchiverTest.deviceNameRoot + "/nan");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            if (event.getValue().equals(Double.NaN)) {
                isArchivingOK = true;
            }
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    @Command
    public void startEvent() throws DevFailed, ArchivingException {
        initMock();
        // activate events
        // this.isforceUseEvents = true;
        final ArchivingConfigs tasks = createEvent(HdbArchiverTest.deviceNameRoot + "/absolute");
        when(mockArchivingInfra.getArchiverCurrentTasks(anyString())).thenReturn(tasks);
        doAnswer((Answer<Void>) invocation -> {
            AttributeScalarEvent event = invocation.getArgument(0);
            System.out.println("event = " + event);
            isArchivingOK = true;
            return null;
        }).when(mockArchivingInfra).store(any(AttributeScalarEvent.class));
        // launch archiving
        this.retryForAll();
    }

    private ArchivingConfigs createEvent(String attributeName) throws DevFailed {
        TangoAttribute attribute = new TangoAttribute(attributeName);
        final ArchivingConfigs tasks = new ArchivingConfigs();
        ArchivingConfig attributeLightMode = new ArchivingConfig();
        AttributeConfig attributeConfig = new AttributeConfig();
        attributeConfig.setFullName(attributeName);
        attributeConfig.setFormat(attribute.getDeviceAttribute().getDataFormat().value());
        attributeConfig.setWriteType(attribute.getWriteType().value());
        attributeLightMode.setAttributeConfig(attributeConfig);
        InsertionModes modes = new InsertionModes();
        modes.setEvent(true);
        attributeLightMode.setModes(modes);
        tasks.addConfiguration(attributeLightMode);
        return tasks;
    }

    @Command
    public boolean isArchivingOK() {
        return isArchivingOK;
    }
}
