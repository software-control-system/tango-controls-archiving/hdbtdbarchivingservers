package org.tango.archiving.test;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttrReadEvent;
import fr.esrf.TangoApi.CallBack;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.events.EventData;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRefFactory;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.junit.Ignore;
import org.junit.Test;
import org.tango.utils.DevFailedUtils;

import java.util.Arrays;

@Ignore
public class StartStopArchivingTest {

    @Test
    public void startSpectrumDifference() throws DevFailed, ArchivingException {

        //   IArchivingManagerApiRef hdbManager = StartStopArchivingTest.connectHdb();


        String attributeName = "archiving/source/tangotest.1/double_spectrum_ro";
        TangoAttribute attribute = new TangoAttribute(attributeName);
        AttributeLightMode attributeLightMode = new AttributeLightMode();
        attributeLightMode.setAttributeCompleteName(attributeName);
        attributeLightMode.setDataType(attribute.getDataType());
        attributeLightMode.setDataFormat(attribute.getDeviceAttribute().getDataFormat().value());
        attributeLightMode.setWritable(attribute.getWriteType().value());
        Mode archMode = new Mode();
        // ModeDifference mode = new ModeDifference();
        //mode.setPeriod(10000);
        //archMode.setModeD(mode);
        attributeLightMode.setMode(archMode);
        ModePeriode modePeriode = new ModePeriode();
        modePeriode.setPeriod(100000);
        archMode.setModeP(modePeriode);
        final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
        archivingMessConfig.add(attributeLightMode);
        System.out.println(Arrays.toString(archivingMessConfig.toArray()));
        //  hdbManager.archivingStart(archivingMessConfig);
    }

    @Test
    public void start() throws DevFailed, ArchivingException {

        IArchivingManagerApiRef hdbManager = StartStopArchivingTest.connectHdb();

        String attributeName = "java/attributecomposer/spjz.1/mean";
        TangoAttribute attribute = new TangoAttribute(attributeName);
        AttributeLightMode attributeLightMode = new AttributeLightMode();
        attributeLightMode.setAttributeCompleteName(attributeName);
        attributeLightMode.setDataType(attribute.getDataType());
        attributeLightMode.setDataFormat(attribute.getDeviceAttribute().getDataFormat().value());
        attributeLightMode.setWritable(attribute.getWriteType().value());
        attributeLightMode.setDevice_in_charge("archiving/hdb-oracle/hdbarchiver.events");
        Mode archMode = new Mode();
        ModePeriode modePeriode = new ModePeriode();
        modePeriode.setPeriod(10000);
        archMode.setModeP(modePeriode);
        attributeLightMode.setMode(archMode);
        final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
        archivingMessConfig.add(attributeLightMode);
        hdbManager.archivingStart(archivingMessConfig);

    }

    private static IArchivingManagerApiRef connectHdb() throws DevFailed {
// jdbc:oracle:thin:@lutin.synchrotron-soleil.fr:1521:TEST11SE
        IArchivingManagerApiRef hdbManager;
        final DataBaseParameters params = new DataBaseParameters();
        params.setHost("195.221.4.203");
        params.setUser("hdb");
        params.setPassword("hdb");
        params.setName("TEST11SE");
        params.setSchema("hdb");
        params.setDbType(DataBaseParameters.DataBaseType.ORACLE);
        try {
            final AbstractDataBaseConnector hdbConnector = ConnectionFactory.connect(params);
            hdbManager = ArchivingManagerApiRefFactory.getInstance(true, hdbConnector);
            hdbManager.archivingConfigure();
        } catch (ArchivingException e) {
            throw e.toTangoException();
        }
        return hdbManager;
    }

    @Test
    public void startDoublePeriodic() throws DevFailed, ArchivingException {
        IArchivingManagerApiRef hdbManager = StartStopArchivingTest.connectHdb();

        String attributeName = "archiving/source/tangotest.1/ushort_scalar";
        TangoAttribute attribute = new TangoAttribute(attributeName);
        AttributeLightMode attributeLightMode = new AttributeLightMode();
        attributeLightMode.setAttributeCompleteName(attributeName);
        attributeLightMode.setDataType(attribute.getDataType());
        attributeLightMode.setDataFormat(attribute.getDeviceAttribute().getDataFormat().value());
        attributeLightMode.setWritable(attribute.getWriteType().value());
        attributeLightMode.setDevice_in_charge("archiving/hdb-oracle/hdbarchiver.03");
        Mode archMode = new Mode();
        ModePeriode modePeriode = new ModePeriode();
        modePeriode.setPeriod(1000);
        archMode.setModeP(modePeriode);
        attributeLightMode.setMode(archMode);
        final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
        archivingMessConfig.add(attributeLightMode);
        hdbManager.archivingStart(archivingMessConfig);
    }

    @Test
    public void startPeriodicWithArchiver() {
        DeviceProxy deviceProxy = null;
        try {
            deviceProxy = new DeviceProxy("archiving/hdb-oracle/hdbarchiver.03");
            DeviceData dd = new DeviceData();
            //false, 9, tango/tangotest/2/double_scalar, 5, 0, 3, archiving/hdb-oracle/hdbarchiver.03, NULL, 2, MODE_P, 2147483000,
            //  dd.insert(new String[]{"false","9", "archiving/source/tangotest.1/double_scalar","5","0","3", "ga/archiving/timescale-archiver","NULL","2", "MODE_P", "10000"});
            // deviceProxy.command_inout("TriggerArchiveConf", dd);
            TangoAttribute a = new TangoAttribute("tango/tangotest/2/double_scalar");
           dd.insert(new String[]{"false", "9", "tango/tangotest/2/double_scalar", String.valueOf(a.getDataType()), "0", "3", "archiving/hdb-oracle/hdbarchiver.03", "NULL", "2", "MODE_P", "200000"});
           // dd.insert(new String[]{"false", "8", "archiving/source/tangotest.1/ushort_scalar", String.valueOf(a.getDataType()), "0", "3", "archiving/hdb-oracle/hdbarchiver.03", "NULL", "1", "MODE_EVT"});
            deviceProxy.command_inout("TriggerArchiveConf", dd);

        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw new RuntimeException(e);
        }

    }

    @Test
    public void startEvent(){
        //ga/py/event-test.1
        DeviceProxy deviceProxy = null;
        try {
            deviceProxy = new DeviceProxy("archiving/hdb-oracle/hdbarchiver.03");
            DeviceData dd = new DeviceData();
            //false, 9, tango/tangotest/2/double_scalar, 5, 0, 3, archiving/hdb-oracle/hdbarchiver.03, NULL, 2, MODE_P, 2147483000,
            //  dd.insert(new String[]{"false","9", "archiving/source/tangotest.1/double_scalar","5","0","3", "ga/archiving/timescale-archiver","NULL","2", "MODE_P", "10000"});
            // deviceProxy.command_inout("TriggerArchiveConf", dd);
            TangoAttribute a = new TangoAttribute("GA/py/event-test.1/push");
            System.out.println(a.getDataType());
            System.out.println(a.getWriteType().value());
            System.out.println(a.read());

            DeviceProxy dev2 = new DeviceProxy("GA/py/event-test.1");
            CallBack cb = new CallBack(){
                @Override
                public void attr_read(AttrReadEvent evt) {
                    DeviceAttribute[] results = evt.argout;
                    for (int i = 0; i < results.length; i++) {
                        try {
                            System.out.println(results[i].extractUShort());
                        } catch (DevFailed e) {
                            throw new RuntimeException(e);
                        }
                    }

                }

                @Override
                public void push_event(EventData evt) {
                    super.push_event(evt);
                }
            };
            dev2.subscribe_event("push", TangoConst.ARCHIVE_EVENT, cb, new String[0]);
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            // dd.insert(new String[]{"false", "9", "archiving/source/tangotest.1/ushort_scalar", String.valueOf(a.getDataType()), "0", "3", "archiving/hdb-oracle/hdbarchiver.03", "NULL", "2", "MODE_P", "1000"});
            //dd.insert(new String[]{"false", "8", "ga/py/event-test.1/push", String.valueOf(a.getDataType()), "0", String.valueOf(a.getWriteType().value()), "archiving/hdb-oracle/hdbarchiver.03", "NULL", "1", "MODE_EVT"});
            //deviceProxy.command_inout("TriggerArchiveConf", dd);
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw new RuntimeException(e);
        }
    }


    @Test
    public void stop() throws DevFailed, ArchivingException {
        IArchivingManagerApiRef hdbManager = StartStopArchivingTest.connectHdb();
        hdbManager.archivingStop("tango/tangotest/2/double_scalar");
    }
}
