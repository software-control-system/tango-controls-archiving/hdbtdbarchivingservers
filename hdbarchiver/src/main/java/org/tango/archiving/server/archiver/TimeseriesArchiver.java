package org.tango.archiving.server.archiver;

import org.tango.server.ServerManager;

public class TimeseriesArchiver {

    public static void main(final String[] args) {
        HdbArchiver.loadVersion();
        ServerManager.getInstance().addClass("TimeseriesArchiver", HdbArchiver.class);
        ServerManager.getInstance().start(args, "TimeseriesArchiver");
    }
}
