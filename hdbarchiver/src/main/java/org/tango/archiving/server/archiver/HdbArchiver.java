package org.tango.archiving.server.archiver;


import com.fasterxml.jackson.core.JsonProcessingException;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.archiving.collector.TangoCollectorService;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.logging.LoggingLevel;
import org.tango.logging.LoggingManager;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.AttributeProperties;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceManagement;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.device.DeviceManager;
import org.tango.utils.DevFailedUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

@Device(transactionType = TransactionType.NONE)
public class HdbArchiver {

    private static final short NA = 10;
    private static final short SUCCEEDED = 20;
    private static final short FAILED = 30;
    private static final long MEGABYTE = 1024L * 1024L;
    protected static String VERSION;
    private final Logger logger = LoggerFactory.getLogger(HdbArchiver.class);


    @DeviceProperty(description = "if true, only event can be used")
    protected boolean forceUseEvents = false;
    protected TangoCollectorService tangoCollectorService;
    protected String deviceName;
    @DeviceProperty(description = "Database port")
    protected String dbPort = "";
    @DeviceProperty(description = "minimum insertion period allowed in ms", defaultValue = "100")
    protected int minimumInsertionPeriod = 100;
    @DeviceProperty(isMandatory = true, description = "Database type: ORACLE, MYSQL or POSTGRESQL")
    private String dbType;
    @DeviceProperty(isMandatory = true, description = "Database host name")
    private String dbHost;
    @DeviceProperty(isMandatory = true, description = "Database name")
    private String dbName;
    @DeviceProperty(description = "Database schema, used only for Oracle")
    private String dbSchema;
    @DeviceProperty(isMandatory = true, description = "Database user name")
    private String dbUser;
    @DeviceProperty(isMandatory = true, description = "Database password associated with dbUser")
    private String dbPassword;
    @DeviceProperty(description = "minimum connection")
    private short dbMinPoolSize = 0;
    @DeviceProperty
    private short dbMaxPoolSize = 10;
    @DeviceProperty
    private int dbCnxInactivityTimeout = 10;
    @DeviceProperty
    private boolean insertErrors = true;
    /**
     * Defines whether the archiver is a "dedicated archiver". A
     * "dedicated archiver" is an archiver specialized in archiving a specific
     * list of attributes, and none others. If the value is false, the related
     * attribute "reservedAttributes" won't be used. <b>Default value :
     * </b>false
     */
    @DeviceProperty(description = "Specialized in archiving a specific list of attributes")
    private boolean isDedicated = false;
    /**
     * Defines whether the archiver will log to a diary. <b>Default value :
     * </b>false
     */
    @DeviceProperty
    private boolean hasDiary = false;

    // TODO: manage class properties CommitCounter / CommitPeriod
    /**
     * The criticity threshold of the archiver's logging. Only messages with a
     * criticity higher than this attribute will be logged to the diary.
     * Possible values are:
     * <UL>
     * <LI>DEBUG
     * <LI>INFO
     * <LI>WARNING
     * <LI>ERROR
     * <LI>CRITIC
     * </UL>
     * <b>Default value : </b>DEBUG
     */
    @DeviceProperty
    private String diaryLogLevel = "DEBUG";
    /**
     * The path of the diary logs. Don't include a filename, diary files are
     * named automatically.
     */
    @DeviceProperty
    private String diaryPath;
    /**
     * The list of attributes that will always be archived on this archiver no
     * matter the load-balancing situation. Be aware that: - this list will be
     * ignored if the related attribute "isDedicated" is false or missing - one
     * attribute shouldn't be on the reservedAttributes list of several
     * archivers
     */
    @DeviceProperty(description = "used if isDedicated property set to true. attribute list attached to this archiver")
    private String[] reservedAttributes;
    private List<String> reservedAttributesList;
    @State
    private volatile DeviceState state = DeviceState.OFF;
    @Status
    private volatile String status;
    private String startupDatabaseTimeStatus;
    @DeviceManagement
    private DeviceManager deviceManager;

    public static void main(final String[] args) {
        loadVersion();
        ServerManager.getInstance().start(args, HdbArchiver.class);
    }

    public static void loadVersion() {
        VERSION = ResourceBundle.getBundle("application").getString("project.version");
    }

    public void setMinimumInsertionPeriod(final int minimumInsertionPeriod) {
        this.minimumInsertionPeriod = minimumInsertionPeriod;
    }


    public void setForceUseEvents(final boolean forceUseEvents) {
        this.forceUseEvents = forceUseEvents;
    }

    @Attribute
    public String getVersion() {
        return VERSION;
    }

    @Delete
    public void delete() {
        tangoCollectorService.stopAll();
    }

    @Init(lazyLoading = true)
    public void init() throws DevFailed, ArchivingException {
        logger.info("init in");
        state = DeviceState.OFF;
        logger.info("isforceUseEvents= {}, isDedicated = {}", forceUseEvents, isDedicated);
        logger.info("reservedAttributes = {}", Arrays.toString(reservedAttributes));
        try {
            if (hasDiary) {
                final String logFile = deviceName.substring(deviceName.lastIndexOf("/"), deviceName.length()) + ".log";
                final String fullLogFileName = diaryPath + "/" + logFile;
                LoggingManager.getInstance().addFileAppender(fullLogFileName, deviceName);
                LoggingManager.getInstance().setLoggingLevel(deviceName, LoggingLevel.getLevelFromString(diaryLogLevel).toInt());
            }
        } catch (final DevFailed e) {
            logger.error("failed to configure logging {}", DevFailedUtils.toString(e));
        }
        connectDatabase();
        // start archiving
        final ArchivingConfigs myCurrentTasks = tangoCollectorService.startAllArchiverCollects();
        logger.debug("archiving config to start {}", myCurrentTasks);
        logger.info("init device OK");
    }

    protected void connectDatabase() throws ArchivingException {
        status = "Connecting to DB";
        final DatabaseConnectionConfig params = new DatabaseConnectionConfig();
        params.setDbType(DatabaseConnectionConfig.DataBaseType.valueOf(dbType.toUpperCase()));
        params.setHost(dbHost);
        params.setUser(dbUser);
        params.setPassword(dbPassword);
        params.setName(dbName);
        params.setPort(dbPort);
        params.setSchema(dbSchema);
        params.setIdleTimeout(dbCnxInactivityTimeout);
        params.setMinPoolSize(dbMinPoolSize);
        params.setMaxPoolSize(dbMaxPoolSize);

        logger.info("connecting to archiving DB {}", params);
        long tick = System.currentTimeMillis();
        tangoCollectorService = buildDbProxy(params, insertErrors);

        long tack = System.currentTimeMillis();
        long duration = tack - tick;
        logger.info("database connection took {} ms", duration);
        status = "Database connection took " + duration + " ms.\nStarting archiving...";
        startupDatabaseTimeStatus = "Database connection took " + duration + " ms.\n";
    }


    protected TangoCollectorService buildDbProxy(DatabaseConnectionConfig params, boolean insertErrors) throws ArchivingException {
        ArchivingInfra timeseriesInfra = new ArchivingInfra(params, true, !isUsingTemporaryFiles(), insertErrors);
        return new TangoCollectorService(timeseriesInfra, deviceName, forceUseEvents, !isUsingTemporaryFiles(), minimumInsertionPeriod);
    }

    protected boolean isUsingTemporaryFiles() {
        return false;
    }

    @Attribute(name = "KO")
    @AttributeProperties(unit = "attrs")
    public int getKoNumber() {
        return tangoCollectorService.getKoAttributes().size();
    }

    @Attribute(name = "Paused")
    @AttributeProperties(unit = "attrs")
    public int getPausedNumber() {
        return tangoCollectorService.getPausedAttributes().size();
    }

    @Command(name = "GetKoAttributes")
    public String[] getKoAttributes() {
        List<Map.Entry<String, String>> tmpList = new ArrayList<>(tangoCollectorService.getKoAttributes().entrySet());
        return tmpList.stream().map(value -> value.getKey() + " : " + value.getValue()).toArray(String[]::new);
    }

    /**
     * Get errors messages for insertions and KO attributes
     *
     * @return
     */
    @Command(name = "GetErrorMessages")
    public String[] getErrorMessages() {
        Map<String, String> errors = tangoCollectorService.getCollectorErrors();
        List<Map.Entry<String, String>> errorList = new ArrayList<>(errors.entrySet());
        errorList.addAll(tangoCollectorService.getInsertionsErrors().entrySet());
        errorList.addAll(tangoCollectorService.getKoAttributes().entrySet());
        return errorList.stream().map(value -> value.getKey() + " : " + value.getValue()).toArray(String[]::new);
    }

    @Command(name = "GetErrorMessages")
    public String[] getCollectorErrorMessages() {
        Map<String, String> errors = tangoCollectorService.getCollectorErrors();
        List<Map.Entry<String, String>> errorList = new ArrayList<>(errors.entrySet());
        return errorList.stream().map(value -> value.getKey() + " : " + value.getValue()).toArray(String[]::new);
    }

    @Command(name = "GetInsertionErrorMessages")
    public String[] getInsertionErrorMessages() {
        Map<String, String> errors = tangoCollectorService.getInsertionsErrors();
        List<Map.Entry<String, String>> errorList = new ArrayList<>(errors.entrySet());
        return errorList.stream().map(value -> value.getKey() + " : " + value.getValue()).toArray(String[]::new);
    }

    @Command(name = "GetOkAttributes")
    public String[] getOkAttributes() {
        return tangoCollectorService.getOkAttributes().toArray(new String[0]);
    }

    @Command(name = "GetPausedAttributes")
    public String[] getPausedAttributes() {
        return tangoCollectorService.getPausedAttributes().toArray(new String[0]);
    }

    @Attribute
    @AttributeProperties(unit = "attrs")
    public int getScalarCharge() {
        return tangoCollectorService.getScalarCharge();
    }

    @Attribute
    @AttributeProperties(unit = "attrs")
    public int getSpectrumCharge() {
        return tangoCollectorService.getSpectrumCharge();
    }

    @Attribute
    @AttributeProperties(unit = "attrs")
    public int getImageCharge() {
        return tangoCollectorService.getImageCharge();
    }

    /**
     * Execute command "TriggerArchiveConf" on device. This command is invoked
     * when the archiving of a group of attributes is triggered. The group of
     * attributes is therefore encapsulated in an ArchivingMessConfig type
     * object. The use of this command suppose that the database is ready to
     * host those attributes's values. That means registrations were made, the
     * associated tables built, ...
     *
     * @param argin The group of attributes to archive
     * @see ArchivingMessConfig
     * @see AttributeLightMode
     */
    @Command(name = "TriggerArchiveConf")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public void triggerArchiveConf(final String[] argin) {
        logger.info("Received request to start {}", Arrays.toString(argin));
        ArchivingConfigs configs = new ArchivingConfigs(argin, true);
        ArchivingConfigs filteredConfigs = new ArchivingConfigs();
        if (isDedicated) {
            // keep only dedicated attributes
            Map<String, ArchivingConfig> map = configs.getArchivingConfigsAsMap();
            List<String> toStart = map.keySet().stream()
                    .map(String::toLowerCase)
                    .filter(reservedAttributesList::contains).collect(Collectors.toList());
            for (String a : toStart) {
                filteredConfigs.addConfiguration(map.get(a));
            }
            logger.info("Dedicated archiver, will start {}", filteredConfigs);
            tangoCollectorService.startAttributesCollect(filteredConfigs, true);
        } else {
            tangoCollectorService.startAttributesCollect(configs, true);
        }

    }


    /**
     * Execute command "StopArchiveConf" on device. This command is invoked when
     * stopping the archiving of a group of attributes.
     *
     * @param argin The group of attributes
     */
    @Command(name = "StopArchiveConf", inTypeDesc = "The list of attributes configurations")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public void stopArchiveConf(final String[] argin) throws DevFailed {
        try {
            logger.debug("stopArchiveConf - in");
            final Collection<AttributeLightMode> myConf = ArchivingManagerApiRef.stoppingVector(argin);
            for (final AttributeLightMode attrMode : myConf) {
                String attributeName = attrMode.getAttributeCompleteName();
                if (tangoCollectorService.isArchived(attributeName)) {
                    // stop only if attribute is managed by this archiver
                    tangoCollectorService.stopAttributeCollect(attributeName, true, isUsingTemporaryFiles());
                    logger.debug("stopping archiving of {} OK", attributeName);
                } else {
                    throw DevFailedUtils.newDevFailed(attributeName + " is not archived by this device");
                }
            }
        } catch (ArchivingException e) {
            throw e.toTangoException();
        } finally {
            logger.debug("stopArchiveConf - out");
        }
    }

    /**
     * Execute command "StopArchiveAtt" on device. Command that stops the
     * archiving of  attributes.
     *
     * @param attributeNames the attributes on which archiving must be stopped
     */
    @Command(name = "StopArchiveAtt", inTypeDesc = "The list of attribute names to stop")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public void stopArchiveAtt(final String[] attributeNames) throws DevFailed {
        logger.debug("stopping archiving of {}", Arrays.toString(attributeNames));
        try {
            for (String attributeName : attributeNames) {
                if (tangoCollectorService.isArchived(attributeName)) {
                    // stop only if attribute is managed by this archiver
                    tangoCollectorService.stopAttributeCollect(attributeName, true, isUsingTemporaryFiles());
                    logger.debug("stopping archiving of {} OK", attributeName);
                } else {
                    throw DevFailedUtils.newDevFailed(attributeName + " is not archived by this device");
                }
            }

        } catch (final ArchivingException e) {
            logger.error("stop failed", e);
            throw e.toTangoException();
        }
    }

    @Command(name = "PauseContext", inTypeDesc = "the archiving context id")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public void pauseContext(final int contextId) throws ArchivingException {
        logger.info("Pausing context for context {}", contextId);
        tangoCollectorService.pauseContext(contextId);
    }

    @Command(name = "ResumeContext", inTypeDesc = "the archiving context id")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public void resumeContext(final int contextId) throws ArchivingException {
        logger.info("Resuming context for context {}", contextId);
        tangoCollectorService.resumeContext(contextId);
    }

    /**
     * Execute command "RetryForAttribute" on device. Tries to start archiving
     * for a given attribute, specified by its complete name.
     *
     * @param attributeToRetry The complete name of the attribute
     * @return A return code, can be either: 10 (the archiver isn't in charge of
     * the specified attribute) 20 (the retry succeeded) or 30 (the
     * retry failed)
     * @throws fr.esrf.Tango.DevFailed
     */
    @Command(name = "RetryForAttribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public short retryForAttribute(final String attributeToRetry) {
        short result;
        if (attributeToRetry == null || attributeToRetry.isEmpty()) {
            logger.debug("no retry done - no attribute");
            result = NA;
        } else if (tangoCollectorService == null) {
            logger.debug("no retry done - no connection to the database");
            result = NA;
        } else {
            try {
                final ArchivingConfigs myCurrentTasks = tangoCollectorService.startConfiguredAttributesCollect(attributeToRetry);
                if (!myCurrentTasks.getArchivingConfigs().isEmpty()) {
                    logger.debug("retry attribute {}", attributeToRetry);
                    result = SUCCEEDED;
                } else {
                    logger.debug(attributeToRetry + " no retry done - config is empty");
                    result = NA;
                }
            } catch (final Throwable e) {
                logger.warn("Failed during retry_for_attribute ( " + attributeToRetry + " ):", e);
                result = FAILED;
            }
        }
        return result;
    }

    /**
     * Execute command "RetryForKoAttributes" on device. Looks up the complete
     * list of attributes that:
     * <UL>
     * <LI>the archiver is supposed to be taking care of
     * <LI>aren't correctly archiving, according to the archiver's internal controls
     * </UL>
     * , then starts archiving on all of them.
     *
     * @return A return code, can be either: 10 (the archiver isn't in charge of
     * any attribute, or they are all correctly archiving) 20 (the retry
     * succeeded) or 30 (the retry failed)
     * @throws fr.esrf.Tango.DevFailed
     */
    @Command(name = "RetryForKoAttributes")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public short retryForKoAttributes() throws DevFailed {
        logger.debug("retryForKoAttributes");
        return retryForAttributes(tangoCollectorService.getKoAttributes().keySet().toArray(new String[0]));
    }

    /**
     * Execute command "RetryForAttributes" on device. Tries to start archiving
     * for a given list of attributes, specified by their complete names.
     *
     * @param attributesToRetry The complete names of the attributes
     * @return A return code, can be either: 10 (the archiver isn't in charge of
     * any of the specified attributes) 20 (the retry succeeded) or 30
     * (the retry failed)
     * @throws fr.esrf.Tango.DevFailed
     */
    @Command(name = "RetryForAttributes")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public short retryForAttributes(final String[] attributesToRetry) {
        short result;
        if (attributesToRetry == null || attributesToRetry.length == 0 || tangoCollectorService == null) {
            result = NA;
        } else {
            try {
                final ArchivingConfigs myCurrentTasks = tangoCollectorService.startConfiguredAttributesCollect(attributesToRetry);
                if (!myCurrentTasks.getArchivingConfigs().isEmpty()) {
                    result = SUCCEEDED;
                } else {
                    result = NA;
                }
            } catch (Throwable e) {
                logger.warn("Failed during retryForAttributes:", e);
                result = FAILED;
            }

        }
        return result;
    }

    @Command(name = "GetErrorMessageForAttribute")
    public String getErrorMessageForAttribute(final String attributeName) {
        String attrName = attributeName.toLowerCase();
        String message = tangoCollectorService.getErrorMessageForAttribute(attrName);
        if (message == null || message.isEmpty()) {
            message = tangoCollectorService.getKoAttributes().get(attrName);
            if (message == null || message.isEmpty()) {
                message = tangoCollectorService.getInsertionsErrors().get(attrName);
            }
        }
        return message;
    }

    /**
     * Execute command "RetryForAll" on device. Looks up the complete list of
     * attributes the archiver is supposed to be taking care of, then starts
     * archiving on all of them. Those that are already archiving aren't
     * impacted.
     *
     * @return A return code, can be either: 10 (the archiver isn't in charge of
     * any attribute) 20 (the retry succeeded) or 30 (the retry failed)
     * @throws fr.esrf.Tango.DevFailed
     */
    @Command(name = "RetryForAll")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.INIT})
    public short retryForAll() throws DevFailed {
        short result;
        if (tangoCollectorService == null) {
            result = FAILED;
        } else {
            try {
                final ArchivingConfigs myCurrentTasks = tangoCollectorService.startAllArchiverCollects();
                logger.debug("retry done for attributes {}", myCurrentTasks);
                if (!myCurrentTasks.getArchivingConfigs().isEmpty()) {
                    logger.debug("retry for all attributes");
                    result = SUCCEEDED;
                } else {
                    result = NA;
                }
            } catch (final Throwable t) {
                logger.warn("Failed during retryForAll:", t);
                result = FAILED;
            }
        }
        return result;
    }

    /**
     * returns a detailed state of the device.
     *
     * @return The detailed state
     */
    @Command
    public String[] getCollectorsStatus() throws DevFailed {
        return tangoCollectorService.getCollectorsStatus();
    }

    @Attribute
    @AttributeProperties(unit = "errors")
    public int getCollectorErrorNr() {
        return tangoCollectorService.getCollectorErrorNr();
    }

    @Attribute
    @AttributeProperties(unit = "errors")
    public int getInsertionErrorNr() {
        return tangoCollectorService.getInsertionsErrors().size();
    }

    public DeviceState getState() {
        updateStateStatus();
        return state;
    }

    public void setState(final DeviceState state) {
        this.state = state;
    }

    private void updateStateStatus() {
        String tmpStatus = "";
        if (state != DeviceState.FAULT) {
            tmpStatus = startupDatabaseTimeStatus;
            if (tangoCollectorService.isEmpty()) {
                state = DeviceState.OFF;
                tmpStatus += "No attributes to archive";
            } else {
                tmpStatus += "Last starting of attributes time was "
                        + tangoCollectorService.getStartingTime() + " ms\n";
                tmpStatus += "Attributes' collect status: \n - " +
                        tangoCollectorService.getOkAttributes().size() + " OK\n - "
                        + tangoCollectorService.getKoAttributes().size() + " KO\n - "
                        + tangoCollectorService.getPausedAttributes().size() + " paused\n";
                tmpStatus += " - " + tangoCollectorService.getCollectorErrorNr() + " collect error(s)\n";
                tmpStatus += " - " + tangoCollectorService.getInsertionsErrors().size() + " insertion error(s)\n";
                if (!tangoCollectorService.getKoAttributes().isEmpty()) {
                    state = DeviceState.DISABLE;
                } else if (!tangoCollectorService.getInsertionsErrors().isEmpty()) {
                    state = DeviceState.ALARM;
                } else {
                    state = DeviceState.ON;
                }
            }
        }
        status = tmpStatus;
    }

    public String getStatus() {
        updateStateStatus();
        return status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    @Command
    public String[] getKPIs() {
        return ArchivingKPI.getReportAsArray();
    }

    @Command
    public String getExtendedKPIs() throws JsonProcessingException {
        return ArchivingKPI.getJsonReport();
    }

    @Attribute
    @AttributeProperties(unit = "errors/s", format = "%5.2f", description = "collector insertion errors")
    public double getInsertionErrorsRate() {
        return ArchivingKPI.getErrorOneMinuteRate();
    }

    @Attribute
    @AttributeProperties(unit = "%", format = "%5.2f", description = "collector scale delays in percent")
    public double getCollectorDelay() {
        return ArchivingKPI.getMeanCollectorDelay();
    }

    @Attribute
    @AttributeProperties(unit = "insertions/s", format = "%5.2f")
    public double getInsertionRate() {
        return ArchivingKPI.getOneMinuteRate();
    }

    @Attribute
    @AttributeProperties(unit = "Mo", format = "%10.2f")
    public double getMemory() {
        return ArchivingKPI.getTotalUsedMemory() / MEGABYTE;
    }

    public void setReservedAttributes(final String[] reservedAttributes) {
        this.reservedAttributes = reservedAttributes;
        reservedAttributesList = Arrays.stream(reservedAttributes).map(String::toLowerCase)
                .collect(Collectors.toList());
    }

    public void setDedicated(final boolean isDedicated) {
        this.isDedicated = isDedicated;
    }

    public void setDeviceManager(final DeviceManager deviceManager) {
        this.deviceManager = deviceManager;
        deviceName = deviceManager.getName();
    }

    public void setDbCnxInactivityTimeout(final int dbCnxInactivityTimeout) {
        this.dbCnxInactivityTimeout = dbCnxInactivityTimeout;
    }

    public void setDbPassword(final String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void setDbUser(final String dbUser) {
        this.dbUser = dbUser;
    }

    public void setDbSchema(final String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public void setDbHost(final String dbHost) {
        this.dbHost = dbHost;
    }

    public void setDbMinPoolSize(final short dbMinPoolSize) {
        this.dbMinPoolSize = dbMinPoolSize;
    }

    public void setDbMaxPoolSize(final short dbMaxPoolSize) {
        this.dbMaxPoolSize = dbMaxPoolSize;
    }

    public void setDbType(final String dbType) {
        this.dbType = dbType;
    }

    public void setDiaryPath(final String diaryPath) {
        this.diaryPath = diaryPath;
    }

    public void setDiaryLogLevel(final String diaryLogLevel) {
        this.diaryLogLevel = diaryLogLevel;
    }

    public void setHasDiary(final boolean hasDiary) {
        this.hasDiary = hasDiary;
    }

    public void setInsertErrors(boolean insertErrors) {
        this.insertErrors = insertErrors;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }
}
