package org.tango.archiving.collector.polling;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrQuality;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.ITangoCollector;
import org.tango.archiving.collector.data.AAttributeEvent;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.data.TangoDataPersister;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ArchivingContextList;
import org.tango.archiving.collector.mode.ModeChronometers;
import org.tango.archiving.collector.mode.ModeHandler;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public abstract class DefaultPollingCollector<T extends AAttributeEvent<E>, E> implements ITangoCollector {


    protected final ArchivingErrorCollector errorCollector;
    final TangoDataPersister persister;
    private final String name;
    private final ThreadFactory namedThreadFactory;
    private final Logger logger = LoggerFactory.getLogger(DefaultPollingCollector.class);
    private final Map<String, Pair<ArchivingConfig, TangoAttribute>> attributesList = new ConcurrentHashMap<>();
    private final ArchivingContextList contextList = new ArchivingContextList();
    /**
     * An ModeHandler is associated to each Collector to handle the archiving mode
     */
    private final ModeHandler modeHandler;
    private ScheduledExecutorService executor;
    private ScheduledFuture<?> future = null;
    private volatile double collectingDelayPercentage = 0;
    private volatile double collectingTimeDelay = 0;
    /**
     * This field represent the refreshing rate of the Collector
     */
    private int collectingPeriod;
    private volatile long lastReadTimestamp = 0;
    private CompletableFuture<Void> forcingFuture;

    public DefaultPollingCollector(ModeHandler modeHandler, ArchivingInfra archivingInfra, ArchivingErrorCollector errorCollector) {
        this.errorCollector = errorCollector;
        persister = new TangoDataPersister(errorCollector, archivingInfra, true, modeHandler);
        this.modeHandler = modeHandler;
        calculateMinRefreshInterval();
        name = this.getClass().getSimpleName() + "-" + modeHandler.getMode().toSimpleString();
        namedThreadFactory = new ThreadFactoryBuilder().setNameFormat(name).build();
    }

    @Override
    public String getName() {
        return name;
    }

    private void calculateMinRefreshInterval() {
        InsertionModes mode = modeHandler.getMode();
        if (mode.isPeriodic()) {
            collectingPeriod = mode.getPeriodPeriodic();
        }
        if (mode.isAbsolute() && collectingPeriod > mode.getPeriodAbsolute()) {
            collectingPeriod = mode.getPeriodAbsolute();
        }
        if (mode.isRelative() && collectingPeriod > mode.getPeriodRelative()) {
            collectingPeriod = mode.getPeriodRelative();
        }
        if (mode.isThreshold() && collectingPeriod > mode.getPeriodThreshold()) {
            collectingPeriod = mode.getPeriodThreshold();
        }
        if (mode.isDifference() && collectingPeriod > mode.getPeriodDifference()) {
            collectingPeriod = mode.getPeriodDifference();
        }
    }

    public synchronized void startCollecting() {
        if (!attributesList.isEmpty()) {
            if (future == null || future.isCancelled()) {
                executor = Executors.newScheduledThreadPool(1, namedThreadFactory);
                lastReadTimestamp = System.currentTimeMillis();
                future = executor.scheduleAtFixedRate(collect(), collectingPeriod, collectingPeriod, TimeUnit.MILLISECONDS);
                logger.info("start collecting for {} attributes on collector {}", attributesList.size(), name);
            }
            if (forcingFuture != null && !forcingFuture.isDone()) {
                logger.debug("joining previous FORCING collect of {}", name);
                // join the previous execution
                forcingFuture.join();
            }
            // force a collect for newly added attributes
            logger.info("FORCING collect for {} on {} attributes", name, attributesList.size());
            forcingFuture = CompletableFuture.runAsync(() -> doCollect(true));
        }
    }

    private void doCollect(boolean isForced) {
        try {
            long start = System.currentTimeMillis();
            if (!isForced) {
                long period = start - lastReadTimestamp;
                double delayPercentage = 100.0 * (period - collectingPeriod) / collectingPeriod;
                if (delayPercentage <= -ModeChronometers.TOLERANCE_PERCENT) {
                    long remainingTime = collectingPeriod - period;
                    // remove a tolerance of 5% to the remaining time
                    remainingTime = remainingTime - (long) (collectingPeriod * 0.1);
                    logger.info("collecting is too early by {} % ({}/{} ms), pausing for {} ms", delayPercentage, period, collectingPeriod, remainingTime);
                    TimeUnit.MILLISECONDS.sleep(remainingTime);
                    logger.debug("pause done ({}/{} ms)", System.currentTimeMillis() - lastReadTimestamp, collectingPeriod);
                } else if (delayPercentage > ModeChronometers.TOLERANCE_PERCENT) {
                    logger.warn("collecting is late by {}% ({}/{} ms)", delayPercentage, period, collectingPeriod);
                }
            }

            // Launch async reads
            Map<Integer, TangoAttribute> idList = new HashMap<>();
            for (Pair<ArchivingConfig, TangoAttribute> att : attributesList.values()) {
                TangoAttribute attribute = att.getRight();
                String attributeCompleteName = att.getLeft().getAttributeConfig().getFullName();
                try {
                    if (attribute == null) {
                        logger.debug("previous connection for {} has failed, retry it", attributeCompleteName);
                        // means that the previous connection  has failed, retry it
                        attribute = new TangoAttribute(attributeCompleteName);
                    }
                    int id = attribute.getAttributeProxy().read_asynch();
                    idList.put(id, attribute);
                } catch (DevFailed e) {
                    if (attribute != null) {
                        insertError(attribute, attributeCompleteName, e);
                    }
                    logger.warn("Failed to get value from {}, {}", attributeCompleteName, DevFailedUtils.toString(e));
                    errorCollector.addError(attributeCompleteName, e);
                    notifyError();
                }
            }
            // get results and insert
            for (Map.Entry<Integer, TangoAttribute> entry : idList.entrySet()) {
                TangoAttribute attribute = entry.getValue();
                Integer id = entry.getKey();
                if (attribute != null) {
                    if (id == null) {
                        logger.warn("no data collected for {}", attribute.getName());
                    } else {
                        try {
                            readAndInsertAttribute(attribute, id);
                        } catch (Throwable e) {
                            logger.error("read async BUG ", e);
                        }
                    }
                }
            }
            if (!isForced) {
                long collectingTime = System.currentTimeMillis() - start;
                collectingTimeDelay = Math.max(0, collectingTime - collectingPeriod);
                collectingDelayPercentage = (100 * collectingTimeDelay / collectingPeriod);
                ArchivingKPI.notifyCollectorDelay(collectingDelayPercentage);
                if (collectingDelayPercentage > 10) {
                    logger.warn("collecting time is exceeding by {}% ({}/{} ms)", collectingDelayPercentage, collectingTime, collectingPeriod);
                }
            }
        } catch (Throwable e) {
            logger.error("unknown BUG ", e);
        }
    }

    private Runnable collect() {
        return () -> doCollect(false);
    }

    private void insertError(TangoAttribute tangoAttribute, String attributeName, DevFailed ex) {
        logger.debug("inserting null value for {}", attributeName);
        T event = initEvent();
        event.setAttributeCompleteName(attributeName);
        try {
            event.setDataType(tangoAttribute.getDataType());
        } catch (DevFailed devFailed) {
            logger.warn("Cannot insert attribute {} in error {}", attributeName, DevFailedUtils.toString(devFailed));
        }
        event.setWritable(tangoAttribute.getWriteType().value());

        setNullValue(event);
        event.setTimeStamp(System.currentTimeMillis());
        if (ex.errors.length > 0) {
            // try to avoid too many insertions
            String errorMsg = ex.errors[0].desc;
            if (errorMsg.contains("reply for asynchronous call")) {
                // avoid too many insertions in DB use always the same message
                errorMsg = "Device reply for asynchronous call is not yet arrived";
            }
            event.setErrorMessage(errorMsg);
        } else {
            event.setErrorMessage(attributeName + " error message could not be retrieved");
        }
        event.setQuality(AttrQuality._ATTR_INVALID);
        persist(event);
    }

    private void readAndInsertAttribute(TangoAttribute tangoAttribute, int id) {
        long readTick = System.currentTimeMillis();
        T event = null;
        try {
            String attributeName = tangoAttribute.getName().toLowerCase();
            try {
                event = initEvent();
                event.setAttributeCompleteName(attributeName);
                event.setDataType(tangoAttribute.getDataType());
                event.setWritable(tangoAttribute.getWriteType().value());
                DeviceAttribute reply = tangoAttribute.getAttributeProxy().read_reply(id, collectingPeriod)[0];
                if (reply.hasFailed()) {
                    throw new DevFailed(reply.getErrStack());
                }
                errorCollector.removeError(attributeName);
                event = getValue(tangoAttribute, reply, event);
                event.setTimeStamp(reply.getTimeValMillisSec());
                notifyElement();
            } catch (DevFailed e) {
                logger.warn("Failed to get value from {} = {}", attributeName, DevFailedUtils.toString(e));
                errorCollector.addError(attributeName, e);
                notifyError();
                setNullValue(event);
                event.setTimeStamp(System.currentTimeMillis());
                if (e.errors.length > 0) {
                    String errorMsg = e.errors[0].desc;
                    if (errorMsg.contains("reply for asynchronous call")) {
                        // try to avoid too many insertions
                        errorMsg = "Device reply for asynchronous call is not yet arrived";
                    }
                    event.setErrorMessage(errorMsg);
                } else {
                    event.setErrorMessage("Device reply for asynchronous call is not yet arrived");
                }
                event.setQuality(AttrQuality._ATTR_INVALID);
            }
            lastReadTimestamp = System.currentTimeMillis();
            long readTime = lastReadTimestamp - readTick;
            logger.debug("get reply of {} took {} ms", attributeName, readTime);
            ArchivingKPI.notifyReadTime(readTime);
            persist(event);
        } catch (Throwable e) {
            logger.error("unknown BUG ", e);
        }
    }


    @Override
    public String toString() {
        return "DefaultPollingCollector{" +
                " name=" + name +
                ", modeHandler=" + modeHandler +
                ", collectingPeriod=" + collectingPeriod +
                ", attributesList=" + attributesList +
                ", collectingDelayPercentage=" + collectingDelayPercentage +
                ", collectingTimeDelay=" + collectingTimeDelay +
                '}';
    }

    abstract T initEvent();

    abstract void notifyError();

    abstract void notifyElement();

    abstract void persist(T event);    @Override
    public void removeAttribute(String attributeName, boolean closeFile) throws ArchivingException {
        String attribute = attributeName.toLowerCase();
        stopAttributeCollect(attribute, closeFile);
        contextList.removeAttribute(attributeName);
    }

    abstract T getValue(TangoAttribute attribute, DeviceAttribute deviceAttribute, T event) throws DevFailed;

    abstract void setNullValue(final T event);



    @Override
    public void addAttribute(ArchivingConfig config) throws ArchivingException {
        String attName = config.getAttributeConfig().getFullName().toLowerCase();
        logger.debug("add archiving for {}", attName);
        try {
            String device = TangoUtil.getfullDeviceNameForAttribute(attName);
            new DeviceProxy(device);
        } catch (DevFailed e) {
            if (e.errors.length > 0 && e.errors[0].reason.equals("TangoApi_DATABASE_CONNECTION_FAILED")) {
                logger.error("Failed to connect to tango db for {}, error {}", attName, DevFailedUtils.toString(e));
                throw new ArchivingException(attName + " start failed, Tango DB connection error, no collect performed");
            } else {
                logger.warn("Device does not exist for attribute {}, error {}", attName, DevFailedUtils.toString(e));
                throw new ArchivingException(attName + "'s device does not exist, no collect performed");
            }
        }
        modeHandler.start(attName);
        persister.setIsFirstValue(attName);
        TangoAttribute att = null;
        try {
            att = new TangoAttribute(attName);
        } catch (DevFailed e) {
            logger.warn("Impossible to access attribute " + attName, e);
        } finally {
            attributesList.put(attName, new MutablePair<>(config, att));
            contextList.addAttribute(attName, config);
        }
    }


    private void stopAttributeCollect(String attributeName, boolean closeFile) throws ArchivingException {
        try {
            attributesList.remove(attributeName);
            persister.removeAttribute(attributeName, closeFile);
            modeHandler.stop(attributeName);
            errorCollector.removeError(attributeName);
        } catch (final Exception e) {
            throw new ArchivingException(e);
        } finally {
            if (attributesList.isEmpty()) {
                stopCollecting();
            }
        }
        logger.debug("{} removed from collector {}", attributeName, name);
    }


    @Override
    public boolean isCollected(String attribute) {
        boolean isCollected = false;
        for (String att : attributesList.keySet()) {
            if (att.equalsIgnoreCase(attribute)) {
                isCollected = true;
                break;
            }
        }
        return isCollected;
    }


    @Override
    public void pauseContext(final int contextId) throws ArchivingException {
        final Set<String> attributeToPause = contextList.pauseContext(contextId);
        for (String attributeName : attributeToPause) {
            stopAttributeCollect(attributeName, false);
        }
    }


    @Override
    public void resumeContext(final int contextId) throws ArchivingException {
        final Collection<ArchivingConfig> attributeToPause = contextList.resumeContext(contextId);
        for (ArchivingConfig attribute : attributeToPause) {
            addAttribute(attribute);
        }
    }

    @Override
    public Set<String> getPausedAttributes() {
        return contextList.getPausedAttributes();
    }

    @Override
    public String assessment() {
        final StringBuilder ass = new StringBuilder();
        ass.append("\n----------\n");
        ass.append(name).append(" - ");
        ass.append(attributesList.size()).append(" attribute.s - is refreshing = ").append(future != null);
        ass.append("\nLast delay is ").append(collectingDelayPercentage).append(" %, and ").append(collectingTimeDelay);
        ass.append(" ms.\nattributes : ");
        for (String att : getAttributeList())
            ass.append(att).append(" , ");
        ass.append("\n").append(contextList.getReport());
        return ass.toString();
    }


    @Override
    public void clear() {
        stopCollecting();
        attributesList.clear();
    }


    @Override
    public int[] loadAssessment() {
        final int[] ret = new int[3];
        for (Pair<ArchivingConfig, TangoAttribute> att : attributesList.values()) {
            if (att.getLeft().getAttributeConfig().getFormat() == AttrDataFormat._SCALAR) {
                ret[0]++;
            } else if (att.getLeft().getAttributeConfig().getFormat() == AttrDataFormat._SPECTRUM) {
                ret[1]++;
            } else if (att.getLeft().getAttributeConfig().getFormat() == AttrDataFormat._IMAGE) {
                ret[2]++;
            }
        }
        return ret;
    }


    @Override
    public InsertionModes getMode() {
        return modeHandler.getMode();
    }


    public Collection<String> getAttributeList() {
        return attributesList.keySet();
    }

    private synchronized void stopCollecting() {
        if (future != null) {
            future.cancel(true);
            executor.shutdownNow();
            try {
                executor.awaitTermination(500, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                executor.shutdownNow();
            }
            logger.info("stopping collector {} done ", name);
        }
        executor = null;
        future = null;
    }


}
