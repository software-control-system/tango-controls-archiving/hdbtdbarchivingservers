package org.tango.archiving.collector.polling;

import fr.soleil.tango.clientapi.TangoAttribute;
import org.tango.archiving.collector.data.ImageEventRO;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ModeHandler;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.tango.clientapi.InsertExtractUtils;

public class DefaultImageROPolling extends DefaultPollingCollector<ImageEventRO, boolean[][]> {

    public DefaultImageROPolling(ModeHandler modeHandler, ArchivingInfra archivingInfra,
            ArchivingErrorCollector errorCollector) {
        super(modeHandler, archivingInfra, errorCollector);
    }

    @Override
    void setNullValue(ImageEventRO event) {
        event.setValue(null, new boolean[][] { { true } });
    }

    @Override
    protected ImageEventRO getValue(TangoAttribute tangoAttribute,DeviceAttribute deviceAttribute, ImageEventRO event) throws DevFailed {
        Object value = InsertExtractUtils.extract(deviceAttribute);
        event.setDimX(deviceAttribute.getDimX());
        event.setDimY(deviceAttribute.getDimY());
        event.setValue(value, null);
        return event;
    }

    @Override
    protected ImageEventRO initEvent() {
        ImageEventRO event = new ImageEventRO();
        event.setDataFormat(AttrDataFormat._IMAGE);
        return event;
    }

    @Override
    protected void notifyError() {
    }

    @Override
    protected void notifyElement() {

    }

    @Override
    protected void persist(ImageEventRO event) {
        persister.persistImage(event);
    }

}
