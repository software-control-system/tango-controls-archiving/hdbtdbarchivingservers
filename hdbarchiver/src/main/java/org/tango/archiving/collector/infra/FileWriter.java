package org.tango.archiving.collector.infra;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

class FileWriter {

    private FileChannel channel;

    synchronized void checkDirs(final String path) {
        final File pathDir = new File(path);
        if (!pathDir.exists()) {
            pathDir.mkdirs();
        }
    }

	@SuppressWarnings("resource")
	synchronized void open(String fileName) throws FileNotFoundException {
        channel = new FileOutputStream(new File(fileName)).getChannel();
    }

    synchronized void close() throws IOException {
        channel.close();
    }

    synchronized void write(final String line) throws IOException {
        final ByteBuffer buff = ByteBuffer.wrap(line.getBytes());
        channel.write(buff);
    }
}
