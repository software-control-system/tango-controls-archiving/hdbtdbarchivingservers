package org.tango.archiving.collector.mode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.Chronometer;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ABEILLE on 14/03/2017.
 */
public class ModeChronometers {

    public static final int TOLERANCE_PERCENT = 10;
    private Logger logger = LoggerFactory.getLogger(ModeChronometers.class);
    private Map<ArchivingMode, Chronometer> chronometerMap = new HashMap<ArchivingMode, Chronometer>();

    public ModeChronometers() {
        chronometerMap.put(ArchivingMode.PERIODIC, new Chronometer());
        chronometerMap.put(ArchivingMode.DIFFERENCE, new Chronometer());
        chronometerMap.put(ArchivingMode.ABSOLUTE, new Chronometer());
        chronometerMap.put(ArchivingMode.RELATIVE, new Chronometer());
        chronometerMap.put(ArchivingMode.THRESHOLD, new Chronometer());
    }

    /**
     * Start a chronometer for a given archiving mode
     *
     * @param mode
     * @param duration
     */
    public void startMode(ArchivingMode mode, long duration) {
        chronometerMap.get(mode).start(duration);
    }

    /**
     * Check is chronometer is over with a tolerance of 10%
     *
     * @param mode
     * @param duration
     * @return
     */
    public boolean isOver(ArchivingMode mode, long duration) {
        long elapsedTime = chronometerMap.get(mode).getElapsedTime();
        boolean isOver = false;
        double elapsedTimePercent = 100 * (double) (elapsedTime - duration) / (double) duration;
        if ((elapsedTimePercent >= 0 || -elapsedTimePercent <= TOLERANCE_PERCENT)) {
            isOver = true;
        }
        return isOver;
    }

    public long getElapsedTime(ArchivingMode mode) {
        return chronometerMap.get(mode).getElapsedTime();
    }

    public void printStatus() {
        for (Map.Entry<ArchivingMode, Chronometer> entry : chronometerMap.entrySet()) {
            logger.debug("{} is over = {}", entry.getKey(), (entry.getValue().getElapsedTime() / 1000));
        }
    }

}
