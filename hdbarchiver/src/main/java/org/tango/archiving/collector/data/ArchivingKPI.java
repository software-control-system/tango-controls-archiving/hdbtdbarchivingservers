package org.tango.archiving.collector.data;

import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.json.MetricsModule;
import com.codahale.metrics.jvm.ClassLoadingGaugeSet;
import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

public class ArchivingKPI {

    private static final MetricRegistry METRIC_REGISTRY = new MetricRegistry();
    private static final Histogram COLLECTOR_NR = METRIC_REGISTRY.histogram("collector-number");
    private static final Histogram INSERTION_TIMING = METRIC_REGISTRY.histogram("insertion-time");
    private static final Histogram READ_TIMING = METRIC_REGISTRY.histogram("read-time");
    private static final Histogram COLLECTOR_DELAY = METRIC_REGISTRY.histogram("collector-delay");
    private static final Meter REQUESTS_SCALAR = METRIC_REGISTRY.meter("scalar-insertion");
    private static final Meter REQUESTS_SPECTRUM = METRIC_REGISTRY.meter("spectrum-insertion");
    private static final Meter REQUESTS_IMAGE = METRIC_REGISTRY.meter("image-insertion");
    private static final Meter REQUESTS_NULL = METRIC_REGISTRY.meter("null-insertion");
    private static final Meter REQUESTS_NAN = METRIC_REGISTRY.meter("nan-insertion");
    private static final Meter REQUESTS_ERROR = METRIC_REGISTRY.meter("error-insertion");
    private static final Meter REQUESTS_TANGO_SCALAR_ERROR = METRIC_REGISTRY.meter("error-scalar-tango");
    private static final Meter REQUESTS_TANGO_SCALAR = METRIC_REGISTRY.meter("scalar-tango");
    private static final Meter REQUESTS_TANGO_SPECTRUM_ERROR = METRIC_REGISTRY.meter("error-spectrum-tango");
    private static final Meter REQUESTS_TANGO_SPECTRUM = METRIC_REGISTRY.meter("spectrum-tango");
    private static final ThreadStatesGaugeSet JVM_THREADS = METRIC_REGISTRY.register("jvm.thread-states",
            new ThreadStatesGaugeSet());
    private static final MemoryUsageGaugeSet JVM_MEMORY = METRIC_REGISTRY.register("jvm.memory",
            new MemoryUsageGaugeSet());
    private static final ClassLoadingGaugeSet JVM_CLASSES = METRIC_REGISTRY.register("jvm.classes",
            new ClassLoadingGaugeSet());
//    private static final FileDescriptorRatioGauge JVM_FILEDESC = METRIC_REGISTRY.register("jvm.fd_usage",
//            new FileDescriptorRatioGauge());
//    private static final GarbageCollectorMetricSet JVM_GC = METRIC_REGISTRY.register("jvm.gc",
//            new GarbageCollectorMetricSet());
    /*  private static final Slf4jReporter slf4jReporter = Slf4jReporter.forRegistry(METRIC_REGISTRY).convertRatesTo(TimeUnit.SECONDS)
            .convertDurationsTo(TimeUnit.MILLISECONDS)
            .build();*/

    /*  static {
           ConsoleReporter reporter = ConsoleReporter.forRegistry(METRIC_REGISTRY)
                  .convertRatesTo(TimeUnit.SECONDS)
                  .convertDurationsTo(TimeUnit.MILLISECONDS)
                  .build();
          reporter.start(1, TimeUnit.SECONDS);
    
      }*/

    private ArchivingKPI() {

    }

    public static void notifyCollectorNumber(int number) {
        COLLECTOR_NR.update(number);
    }

    public static void notifyInsertionTime(long time) {
        INSERTION_TIMING.update(time);
    }

    public static void notifyCollectorDelay(double percent) {
        COLLECTOR_DELAY.update((long) percent);
    }

    public static void notifyReadTime(long time) {
        READ_TIMING.update(time);
    }

    public static void notifyTangoScalar() {
        REQUESTS_TANGO_SCALAR.mark();
    }

    public static void notifyTangoScalarError() {
        REQUESTS_TANGO_SCALAR_ERROR.mark();
    }

    public static void notifyTangoSpectrum() {
        REQUESTS_TANGO_SPECTRUM.mark();
    }

    public static void notifyTangoSpectrumError() {
        REQUESTS_TANGO_SPECTRUM_ERROR.mark();
    }

    public static void notifyInsertion(AttributeScalarEvent event) {
        if (event.getValue() == null) {
            REQUESTS_NULL.mark();
        } else if ((event.getValue() instanceof Number) && Double.isNaN(((Number) event.getValue()).doubleValue())) {
            REQUESTS_NAN.mark();
        } else {
            REQUESTS_SCALAR.mark();
        }
    }

    public static void notifyInsertion(SpectrumEventRO event) {
        if (event.getValue() == null) {
            REQUESTS_NULL.mark();
        } else {
            REQUESTS_SPECTRUM.mark();
        }
    }

    public static void notifyInsertion(SpectrumEventRW event) {
        if (event.getValue() == null) {
            REQUESTS_NULL.mark();
        } else {
            REQUESTS_SPECTRUM.mark();
        }
    }

    public static void notifyInsertion(ImageEventRO event) {
        if (event.getValue() == null) {
            REQUESTS_NULL.mark();
        } else {
            REQUESTS_IMAGE.mark();
        }
    }

    public static void notifyInsertionError() {
        REQUESTS_ERROR.mark();
    }

    public static double getOneMinuteRate() {
        return REQUESTS_SCALAR.getOneMinuteRate() + REQUESTS_SPECTRUM.getOneMinuteRate()
                + REQUESTS_IMAGE.getOneMinuteRate() + REQUESTS_NAN.getOneMinuteRate() + REQUESTS_NULL.getOneMinuteRate();
    }

    public static double getMeanCollectorDelay() {
        return COLLECTOR_DELAY.getSnapshot().getMean();
    }

    public static double getErrorOneMinuteRate() {
        return REQUESTS_ERROR.getOneMinuteRate();
    }

    public static double getTotalUsedMemory() {
        return Double.parseDouble(((Gauge<?>) JVM_MEMORY.getMetrics().get("total.used")).getValue().toString());
    }

    public static String getReport() {
        StringBuilder report = new StringBuilder("Scalar insertions:");
        addMeterReport(report, REQUESTS_SCALAR);
        report = new StringBuilder("spectrum insertions:");
        addMeterReport(report, REQUESTS_SPECTRUM);
        report.append("\nNull insertions:");
        addMeterReport(report, REQUESTS_NULL);
        report.append("\nNaN insertions:");
        addMeterReport(report, REQUESTS_NAN);
        report.append("\nErrors:");
        addMeterReport(report, REQUESTS_ERROR);
        return report.toString();
    }

    private static void addMeterReport(StringBuilder report, Meter meter) {
        report.append("count = ").append(meter.getCount());
        report.append(String.format("//mean rate = %2.2f ", meter.getMeanRate()));
        report.append(String.format("//1-minute rate = %2.2f ", meter.getOneMinuteRate()));
        report.append(String.format("//5-minute rate = %2.2f ", meter.getFiveMinuteRate()));
        report.append(String.format("//15-minute rate = %2.2f events/sec.", meter.getFifteenMinuteRate()));
    }

    public static String[] getReportAsArray() {
        List<String> report = new ArrayList<>();
        report.add("Tango scalar collection:");
        addMeterReport(report, REQUESTS_TANGO_SCALAR);
        report.add("Scalar insertions:");
        addMeterReport(report, REQUESTS_SCALAR);
        report.add("Tango spectrum collection:");
        addMeterReport(report, REQUESTS_TANGO_SPECTRUM);
        report.add("Spectrum insertions:");
        addMeterReport(report, REQUESTS_SPECTRUM);
        report.add("Tango scalar error collection:");
        addMeterReport(report, REQUESTS_TANGO_SCALAR_ERROR);
        report.add("Tango spectrum error collection:");
        addMeterReport(report, REQUESTS_TANGO_SPECTRUM_ERROR);
        report.add("Null insertions:");
        addMeterReport(report, REQUESTS_NULL);
        report.add("NaN insertions:");
        addMeterReport(report, REQUESTS_NAN);
        report.add("Insertion errors:");
        addMeterReport(report, REQUESTS_ERROR);
        return report.toArray(new String[0]);
    }

    private static void addMeterReport(List<String> report, Meter meter) {
        StringBuilder stringBuilder = new StringBuilder();
        addMeterReport(stringBuilder, meter);
        report.add(stringBuilder.toString());
    }

    public static String[] getJVMReport() {
        List<String> report = new ArrayList<>();
        // report.add("JVM Memory:");
        // report.add(getMetrics(JVM_MEMORY.getMetrics()));
        report.add("JVM Threads:");
        report.add(getMetrics(JVM_THREADS.getMetrics()));
        report.add("JVM Classes:");
        report.add(getMetrics(JVM_CLASSES.getMetrics()));
        // report.add("JVM GC:");
        // report.add(getMetrics(JVM_GC.getMetrics()));
        return report.toArray(new String[0]);
    }

    private static String getMetrics(Map<String, Metric> metricSet) {
        StringBuilder result = new StringBuilder();
        TreeMap<String, Metric> sorted = new TreeMap<>(metricSet);
        for (Map.Entry<String, Metric> metric : sorted.entrySet()) {
            result.append(metric.getKey()).append(" = ").append(((Gauge<?>) metric.getValue()).getValue()).append("\n");
        }
        return result.toString();
    }

    public static String getJsonReport() throws JsonProcessingException {
        ObjectMapper jsonMapper = new ObjectMapper()
                .registerModule(new MetricsModule(TimeUnit.SECONDS, TimeUnit.MILLISECONDS, false));
        return jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(METRIC_REGISTRY);
    }

    public static MetricRegistry getMetricRegistry() {
        return METRIC_REGISTRY;
    }
}
