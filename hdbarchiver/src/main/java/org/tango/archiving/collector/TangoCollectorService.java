package org.tango.archiving.collector;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.utils.DevFailedUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class TangoCollectorService {
    final AtomicReference<String> startingTime = new AtomicReference<>("N/A");
    private final Logger logger = LoggerFactory.getLogger(TangoCollectorService.class);
    private final CollectorPool collectorPool;
    private final ArchivingInfra archivingInfra;
    private final String archiverName;
    private final boolean isForceUseEvents;
    private final boolean isDBInsertion;
    private final Map<String, String> koAttributes = new HashMap<>();
    private final Set<String> okAttributes = new HashSet<>();
    private final int minimumInsertionPeriod;

    private long exportPeriod;
    private String dbPath;

    private String dsPath;
    private int attributePerFile;

    private boolean forceExportOnStart;

    private int scalarCharge;
    private int spectrumCharge;
    private int imageCharge;

    public TangoCollectorService(ArchivingInfra archivingInfra, String archiverName, boolean isForceUseEvents, boolean isDBInsertion, int minimumInsertionPeriod) {
        this.collectorPool = new CollectorPool();
        this.archivingInfra = archivingInfra;
        this.archiverName = archiverName;
        this.isForceUseEvents = isForceUseEvents;
        this.isDBInsertion = isDBInsertion;
        this.minimumInsertionPeriod = minimumInsertionPeriod;
        logger.debug("build tango collector service with: force event =  {}, db insertion = {}, infra {}",
                isForceUseEvents, isDBInsertion, archivingInfra);
        //TODO closeFile
    }

    public Map<String, String> getInsertionsErrors() {
        return archivingInfra.getInsertionErrors();
    }

    public String getFilePathForAttribute(String attributesName) {
        return archivingInfra.getFilePathForAttribute(attributesName);
    }

    public Map<String, String> getKoAttributes() {
        return koAttributes;
    }

    public Set<String> getOkAttributes() {
        return okAttributes;
    }

    public Set<String> getPausedAttributes() {
        return collectorPool.getPausedAttributes();
    }

    /**
     * Asynchronously start already configured attributes
     *
     * @param attributeNames The attribute's names
     * @return The actual started configuration
     * @throws ArchivingException
     */
    public ArchivingConfigs startConfiguredAttributesCollect(String... attributeNames) throws ArchivingException {
        Map<String, ArchivingConfig> tasks = archivingInfra.getArchiverCurrentTasks(archiverName).getArchivingConfigsAsMap();
        ArchivingConfigs configs = new ArchivingConfigs();
        List<String> attributeNameList = Arrays.stream(attributeNames)
                .map(String::toLowerCase)
                .collect(Collectors.toList());
        for (final String attributeName : attributeNameList) {
            configs.addConfiguration(tasks.get(attributeName));
        }
        logger.debug("starting attributes {}", configs);
        startAttributesCollectAsync(configs);
        return configs;
    }

    private void startAttributesCollectAsync(final ArchivingConfigs attributeLightModes) {
        CompletableFuture.runAsync(() -> {
            startAttributesCollect(attributeLightModes, false);
        });
    }

    public void startAttributesCollect(final ArchivingConfigs attributeConfigs, boolean updateConfigurationInDb) {
        long tick = System.currentTimeMillis();
        final AtomicInteger i = new AtomicInteger();
        int attributesNr = attributeConfigs.getArchivingConfigs().size();
        attributeConfigs.getArchivingConfigs().forEach(attribute -> {
                    startAttributeCollection(attribute, updateConfigurationInDb);
                    logger.info("start of {} done, {}/{}", attribute.getAttributeConfig().getFullName(), i.incrementAndGet(), attributesNr);
                }
        );
        collectorPool.startCollecting();
        long tack = System.currentTimeMillis();
        startingTime.set(Long.toString(tack - tick));
        logger.info("starting archiving of {} attributes took {} ms", attributesNr, startingTime);
        logger.info("start archiving done with {} KO and {} OK", koAttributes.size(), okAttributes.size());
    }

    /**
     * Start collecting a Tango attribute
     *
     * @param config
     * @param updateConfigurationInDb
     */
    private void startAttributeCollection(ArchivingConfig config, boolean updateConfigurationInDb) {
        String attributeName = config.getAttributeConfig().getFullName();
        try {
            logger.debug("starting collecting {}", config);
            if (isForceUseEvents) {
                // force using only tango events
                logger.debug("forcing archiving with event for {}", attributeName);
                final InsertionModes modes = config.getModes();
                modes.setEvent(true);
                modes.setPeriodic(false);
                modes.setAbsolute(false);
                modes.setThreshold(false);
                modes.setDifference(false);
                modes.setRelative(false);
            }
            config.setArchiver(archiverName);
            if (isDBInsertion) {
                startAttribute(config);
            } else {
                startAttributeWithTemporaryFiles(config);
            }
            if (updateConfigurationInDb) {
                // update modes in db with attribute config
                logger.debug("Writing attribute modes in DB of {}", attributeName);
                archivingInfra.updateAttributeMode(config, archiverName);
            }
            okAttributes.add(attributeName.toLowerCase());
            koAttributes.remove(attributeName.toLowerCase());
        } catch (ArchivingException e) {
            try {
                stopAttributeCollect(attributeName, false, !isDBInsertion);
            } catch (ArchivingException ignored) {
            }
            final String name = attributeName.toLowerCase();
            koAttributes.put(name, e.getMessage());
            okAttributes.remove(name);
            logger.warn("error starting archiving", e);
            logger.warn("attribute {} is KO ", attributeName);
        } catch (final Throwable e) {
            try {
                stopAttributeCollect(attributeName, false, !isDBInsertion);
            } catch (ArchivingException ignored) {
            }
            final String name = attributeName.toLowerCase();
            koAttributes.put(name, e.getMessage());
            okAttributes.remove(name);
            logger.error("error starting archiving ", e);
        } finally {
            updateLoad();
        }
    }

    private void startAttribute(final ArchivingConfig config) throws ArchivingException {
        String attributeName = config.getAttributeConfig().getFullName();
        logger.debug("removing attribute {} on pool {}", attributeName, collectorPool);
        collectorPool.remove(attributeName, false);
        logger.debug("starting attribute {} on pool {}", attributeName, collectorPool);
        collectorPool.start(config, archivingInfra);
    }

    private void startAttributeWithTemporaryFiles(final ArchivingConfig config) throws ArchivingException, IOException {
        logger.debug("start tdb archiving with exportPeriod = {} and attributePerFile = {}", exportPeriod, attributePerFile);
        logger.debug("start archiving for {}", config);
        collectorPool.remove(config.getAttributeConfig().getFullName(), false);
        collectorPool.startWithTemporaryFiles(config, archivingInfra, dbPath, dsPath, attributePerFile, exportPeriod);
        if (forceExportOnStart) {
            try {
                this.exportDataToDb(config.getAttributeConfig().getFullName());
            } catch (DevFailed e) {
                logger.error(DevFailedUtils.toString(e), e);
            }
        }
    }

    public void stopAttributeCollect(String attCompleteName, boolean updateConfigurationInDb, boolean closeFile) throws ArchivingException {
        logger.debug("stopAttributeCollect of {}", attCompleteName);
        if (updateConfigurationInDb) {
            archivingInfra.stopAttributeMode(attCompleteName);
        }
        collectorPool.remove(attCompleteName, closeFile);
        koAttributes.remove(attCompleteName.toLowerCase());
        okAttributes.remove(attCompleteName.toLowerCase());
        archivingInfra.removeInsertionError(attCompleteName);
        logger.debug("{} successfully stopped", attCompleteName);
        updateLoad();
    }

    private void updateLoad() {
        final int[] loads = collectorPool.getCollectorLoad();
        scalarCharge = loads[0];
        spectrumCharge = loads[1];
        imageCharge = loads[2];
    }

    public String exportDataToDb(final String attributeName) throws DevFailed {
        String tableName;
        try {
            tableName = archivingInfra.exportFile2Db(attributeName);
        } catch (final ArchivingException e) {
            logger.error(e.toString());
            throw e.toTangoException();
        } catch (final IOException e) {
            throw DevFailedUtils.newDevFailed(e);
        }
        logger.info("Exporting data of {} to table {}", attributeName, tableName);
        return tableName;
    }

    public void pauseContext(int contextId) throws ArchivingException {
        collectorPool.pauseContext(contextId);
    }

    public void resumeContext(int contextId) throws ArchivingException {
        collectorPool.resumeContext(contextId);
    }

    private void checkMinPeriod(String attributeName, InsertionModes modes) throws DevFailed {
        if (modes.isPeriodic() && modes.getPeriodPeriodic() < minimumInsertionPeriod
                || modes.isDifference() && modes.getPeriodDifference() < minimumInsertionPeriod
                || modes.isAbsolute() && modes.getPeriodAbsolute() < minimumInsertionPeriod
                || modes.isRelative() && modes.getPeriodRelative() < minimumInsertionPeriod
                || modes.isThreshold() && modes.getPeriodThreshold() < minimumInsertionPeriod) {
            throw DevFailedUtils.newDevFailed(attributeName + " archiving period is below configured level: "
                    + minimumInsertionPeriod + " for " + modes);
        }
    }

    /**
     * Start all attributes for the current archiver
     *
     * @return The started attributes configuration
     * @throws ArchivingException
     */
    public ArchivingConfigs startAllArchiverCollects() throws ArchivingException {
        ArchivingConfigs tasks = archivingInfra.getArchiverCurrentTasks(archiverName);
        startAttributesCollectAsync(tasks);
        return tasks;
    }

    public void setTemporaryFileConfiguration(long exportPeriod, String dbPath, String dsPath, int attributePerFile, boolean forceExportOnStart) {
        this.exportPeriod = exportPeriod;
        this.dbPath = dbPath;
        this.dsPath = dsPath;
        this.attributePerFile = attributePerFile;
        this.forceExportOnStart = forceExportOnStart;
    }

    public String getStartingTime() {
        return startingTime.get();
    }


    public Map<String, String> getCollectorErrors() {
        return collectorPool.getCollectorErrors();
    }

    public String getErrorMessageForAttribute(final String attributeName) {
        return collectorPool.getErrorMessage(attributeName);
    }

    public String[] getCollectorsStatus() throws DevFailed {
        return collectorPool.getCollectorsStatus().split("\n");
    }

    public int getCollectorErrorNr() {
        return collectorPool.getCollectorErrorNr();
    }

    public int getScalarCharge() {
        return scalarCharge;
    }

    public int getSpectrumCharge() {
        return spectrumCharge;
    }

    public int getImageCharge() {
        return imageCharge;
    }

    public boolean isArchived(String attributeName) {
        return koAttributes.containsKey(attributeName.toLowerCase()) || okAttributes.contains(attributeName.toLowerCase());
    }

    public void stopAll() {
        koAttributes.clear();
        okAttributes.clear();
        collectorPool.clear();
        archivingInfra.closeConnection();
    }

    public boolean isEmpty() {
        return koAttributes.isEmpty() && okAttributes.isEmpty();
    }
}
