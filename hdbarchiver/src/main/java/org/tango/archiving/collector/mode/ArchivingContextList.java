package org.tango.archiving.collector.mode;

import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ArchivingContextList {
    private final Logger logger = LoggerFactory.getLogger(ArchivingContextList.class);

    private final Map<Integer, ArchivingContext> contextList = new ConcurrentHashMap<>();

    public final Set<String> pauseContext(int contextId) {
        Set<String> attributes = new HashSet<>();
        if (contextList.containsKey(contextId)) {
            ArchivingContext context = contextList.get(contextId);
            context.setActive(true);
            attributes = context.getAttributeConfigMap().keySet();
            logger.debug("pausing context {} for {}", contextList, attributes);
        }
        return attributes;
    }

    public final Collection<ArchivingConfig> resumeContext(int contextId) {
        Collection<ArchivingConfig> attributes = new HashSet<>();
        if (contextList.containsKey(contextId)) {
            ArchivingContext context = contextList.get(contextId);
            context.setActive(false);
            attributes = context.getAttributeConfigMap().values();
            logger.debug("resuming context {} for {}", contextList, attributes);
        }
        return attributes;
    }

    public final void removeAttribute(String attributeName) {
        // remove attribute from contexts
        for (ArchivingContext context : contextList.values()) {
            context.removeAttribute(attributeName);
        }
    }

    public final void addAttribute(String attributeName, ArchivingConfig config) {
        // remove attribute from other contexts
        for (ArchivingContext context : contextList.values()) {
            context.removeAttribute(attributeName);
        }
        // add to context list
        Integer contextId = config.getModes().getContextId();
        if (contextId != null) {
            if (contextList.containsKey(contextId)) {
                contextList.get(contextId).addAttribute(attributeName, config);
            } else {
                contextList.put(contextId, new ArchivingContext(contextId).addAttribute(attributeName, config));
            }
        }
    }

    public String getReport() {
        StringBuilder stringBuilder = new StringBuilder();
        for (ArchivingContext context : contextList.values()) {
            stringBuilder.append("Context ").append(context.getContextId()).append(" is paused = ").append(context.isActive());
            stringBuilder.append(" for attributes:").append(context.getAttributeConfigMap().keySet());
        }
        return stringBuilder.toString();
    }

    public Set<String> getPausedAttributes() {
        Set<String> result = new HashSet<>();
        for (ArchivingContext context : contextList.values()) {
            if (context.isActive()) {
                result.addAll(context.getAttributeConfigMap().keySet());
            }
        }
        return result;
    }
}
