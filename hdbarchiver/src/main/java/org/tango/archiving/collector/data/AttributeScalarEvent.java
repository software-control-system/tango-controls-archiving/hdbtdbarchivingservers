// +======================================================================
// $Source: /cvsroot/tango-cs/tango/api/java/fr/soleil/TangoArchiving/ArchivingTools/Tools/ScalarEvent.java,v $
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  ScalarEvent.
//						(Chinkumo Jean) - Aug 29, 2005
//
// $Author: pierrejoseph $
//
// $Revision: 1.11 $
//
// $Log: ScalarEvent.java,v $
// Revision 1.11  2007/06/04 09:03:04  pierrejoseph
// New contructor + value = null treatement in various methods
//
// Revision 1.10  2007/03/20 12:50:09  ounsy
// 1.4 compatibility
//
// Revision 1.9  2007/03/14 09:19:14  ounsy
// added a method avoidUnderFlow ()
//
// Revision 1.8  2006/10/31 16:54:24  ounsy
// milliseconds and null values management
//
// Revision 1.7  2006/07/20 09:24:15  ounsy
// minor changes
//
// Revision 1.6  2006/05/12 09:23:10  ounsy
// CLOB_SEPARATOR in GlobalConst
//
// Revision 1.5  2006/05/04 14:30:41  ounsy
// CLOB_SEPARATOR centralized in ConfigConst
//
// Revision 1.4  2006/03/13 14:46:45  ounsy
// State as an int management
// Long as an int management
//
// Revision 1.3  2006/03/10 11:31:00  ounsy
// state and string support
//
// Revision 1.2  2005/11/29 17:11:17  chinkumo
// no message
//
// Revision 1.1.2.2  2005/11/15 13:34:38  chinkumo
// no message
//
// Revision 1.1.2.1  2005/09/09 08:21:24  chinkumo
// First commit !
//
//
// copyleft :	Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================

package org.tango.archiving.collector.data;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.hdbtdb.api.tools.ScalarEvent;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class AttributeScalarEvent extends AAttributeEvent<boolean[]> {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");


    public Object getReadValue() {
        Object read_value = null;
        switch (getWritable()) {
            case AttrWriteType._READ:
            case AttrWriteType._WRITE:
            case AttrWriteType._READ_WITH_WRITE:
                read_value = getValue();
                break;
            case AttrWriteType._READ_WRITE:
                if (getValue() != null) {
                    read_value = Array.get(getValue(), 0);
                }
                break;
        }
        return read_value;
    }

    @Override
    public String[] toArray() {
        String[] scalarEvent;
        scalarEvent = new String[8];
        scalarEvent[0] = getAttributeCompleteName();
        scalarEvent[1] = Integer.toString(getDataFormat());
        scalarEvent[2] = Integer.toString(getDataType());
        scalarEvent[3] = Integer.toString(getWritable());
        scalarEvent[4] = Long.toString(getTimeStamp());
        scalarEvent[5] = getTableName();
        scalarEvent[6] = valueToString(0);
        scalarEvent[7] = valueToString(1);
        return scalarEvent;
    }

    public String valueToString(final int pos) {
        String valueToString;
        Object value = getValue();
        if (value == null) {
            valueToString = GlobalConst.ARCHIVER_NULL_VALUE;
        } else if ((value instanceof Object[]) && ((Object[]) value)[pos] == null) {
            valueToString = GlobalConst.ARCHIVER_NULL_VALUE;
        } else {
            valueToString = GlobalConst.ARCHIVER_NULL_VALUE;
            switch (getDataFormat()) {
                case AttrDataFormat._SCALAR:
                    switch (getWritable()) {
                        case AttrWriteType._READ:
                        case AttrWriteType._WRITE:
                            if (value instanceof DevState) {
                                valueToString = toString(((DevState) value).value());
                            } else {
                                valueToString = toString(value);
                            }
                            break;
                        case AttrWriteType._READ_WITH_WRITE:
                        case AttrWriteType._READ_WRITE:
                            valueToString = toString(Array.get(value, pos));
                            break;
                    }
                    break;
                case AttrDataFormat._SPECTRUM:
                case AttrDataFormat._IMAGE:
                    valueToString = toString(value);
                    break;
            }
        }
        return valueToString;
    }

    @Override
    public String toString() {
        StringBuilder scEvSt = new StringBuilder();

        scEvSt.append(getAttributeCompleteName()).append("[timestamp: ")
                .append(DATE_FORMAT.format(new Date(getTimeStamp()))).append(" - value: ");

        if (getWritable() == AttrWriteType._READ || getWritable() == AttrWriteType._READ_WITH_WRITE
                || getWritable() == AttrWriteType._READ_WRITE) {
            scEvSt.append("read value :  ").append(valueToString(0));
        }
        if (getWritable() == AttrWriteType._WRITE || getWritable() == AttrWriteType._READ_WITH_WRITE
                || getWritable() == AttrWriteType._READ_WRITE) {
            scEvSt.append(", write value : ").append(valueToString(1));
        }
        if (getDataType() == TangoConst.Tango_DEV_ENUM) {
            scEvSt.append(" - read enum label:").append(Arrays.toString(getEnumReadLabel()));
            scEvSt.append(" - write enum label:").append(Arrays.toString(getEnumWriteLabel()));
        }
        scEvSt.append("]");
        return scEvSt.toString();
    }

    public ScalarEvent toAPIEvent() {
        ScalarEvent event = new ScalarEvent();
        event.setAttributeCompleteName(getAttributeCompleteName());
        event.setTimeStamp(getTimeStamp());
        event.setDataFormat(getDataFormat());
        event.setDataType(getDataType());
        event.setTableName(getTableName());
        event.setValue(getValue(), getNullElements());
        event.setWritable(getWritable());
        return event;
    }

}
