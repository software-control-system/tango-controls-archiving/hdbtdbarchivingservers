package org.tango.archiving.collector.polling;

import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.tango.archiving.collector.data.SpectrumEventRW;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ModeHandler;
import org.tango.utils.ArrayUtils;

import java.math.BigInteger;

public class DefaultSpectrumRWPolling extends DefaultPollingCollector<SpectrumEventRW, boolean[]> {

    public DefaultSpectrumRWPolling(ModeHandler modeHandler, ArchivingInfra archivingInfra,
            ArchivingErrorCollector errorCollector) {
        super(modeHandler, archivingInfra, errorCollector);
    }

    @Override
    void setNullValue(SpectrumEventRW event) {
        event.setValue(null, new boolean[] { true });
    }

    @Override
    protected SpectrumEventRW getValue(TangoAttribute tangoAttribute, DeviceAttribute deviceAttribute, SpectrumEventRW event) throws DevFailed {
        Object read = InsertExtractUtils.extract(deviceAttribute);
        Object write = InsertExtractUtils.extractWrite(deviceAttribute, AttrWriteType.from_int(event.getWritable()),
                AttrDataFormat.SPECTRUM);
        if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ULONG64) {
            long[] longArrayRead = (long[]) read;
            BigInteger[] bigIntegers = new BigInteger[longArrayRead.length];
            for (int i = 0; i < longArrayRead.length; i++) {
                bigIntegers[i] = new BigInteger(Long.toUnsignedString(longArrayRead[i]));
            }
            read = bigIntegers;
            long[] longArrayWrite = (long[]) write;
            BigInteger[] bigIntegersWrite = new BigInteger[longArrayWrite.length];
            for (int i = 0; i < longArrayWrite.length; i++) {
                bigIntegersWrite[i] = new BigInteger(Long.toUnsignedString(longArrayWrite[i]));
            }
            write = bigIntegersWrite;
        }
        Object value = ArrayUtils.addAll(read, write);
        int dimX = deviceAttribute.getDimX();
        event.setValue(value, null);
        event.setDimX(dimX);
        event.setQuality(deviceAttribute.getQuality().value());
        if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ENUM) {
            final AttributeInfoEx infoEx = tangoAttribute.getAttributeProxy().getDeviceProxy().
                    get_attribute_info_ex(deviceAttribute.getName());
            short[] arrayRead = (short[]) read;
            String[] labelsRead = new String[arrayRead.length];
            for (int i = 0; i < arrayRead.length; i++) {
                labelsRead[i] = infoEx.getEnumLabel(arrayRead[i]);
            }
            event.setEnumReadLabel(labelsRead);
            short[] arrayWrite = (short[]) write;
            String[] labelsWrite = new String[arrayWrite.length];
            for (int i = 0; i < arrayWrite.length; i++) {
                labelsWrite[i] = infoEx.getEnumLabel(arrayWrite[i]);
            }
            event.setEnumReadLabel(labelsWrite);
        }
        return event;
    }

    @Override
    protected SpectrumEventRW initEvent() {
        SpectrumEventRW event = new SpectrumEventRW();
        event.setDataFormat(AttrDataFormat._SPECTRUM);
        return event;
    }

    @Override
    protected void notifyError() {
        ArchivingKPI.notifyTangoSpectrumError();
    }

    @Override
    protected void notifyElement() {
        ArchivingKPI.notifyTangoSpectrum();
    }

    @Override
    protected void persist(SpectrumEventRW event) {
        persister.persistSpectrum(event);
        ArchivingKPI.notifyInsertion(event);
    }

}
