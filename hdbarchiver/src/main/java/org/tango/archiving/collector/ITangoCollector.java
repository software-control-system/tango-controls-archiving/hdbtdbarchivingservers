package org.tango.archiving.collector;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;

import java.util.Set;

public interface ITangoCollector {

    void addAttribute(ArchivingConfig attributeLightMode) throws ArchivingException;

    void removeAttribute(String attributeName, final boolean closeFile) throws ArchivingException;

    boolean isCollected(String attribute) throws ArchivingException;

    void pauseContext(int contextId) throws ArchivingException;
    void resumeContext(int contextId) throws ArchivingException;

    Set<String> getPausedAttributes() ;

    String assessment();

    void clear();

    int[] loadAssessment();

    InsertionModes getMode();

    String getName();
}
