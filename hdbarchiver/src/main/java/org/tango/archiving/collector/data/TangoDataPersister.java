package org.tango.archiving.collector.data;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ModeHandler;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TangoDataPersister {
    /**
     * This Map is used to store the last value of an attribute. This value will
     * be compared to the current one (cf. Modes)
     */
    private final Map<String, AAttributeEvent<?>> lastValueMap = new ConcurrentHashMap<>();
    private final ArchivingInfra hdbProxy;
    private final ArchivingErrorCollector errorCollector;
    private final Map<String, Long> lastTimestampsMap = new ConcurrentHashMap<>();
    private final Map<String, Boolean> isFirstValueList = new ConcurrentHashMap<>();
    private final ModeHandler modeHandler;
    private final Logger logger = LoggerFactory.getLogger(TangoDataPersister.class);
    private final XLogger xlogger = XLoggerFactory.getXLogger(TangoDataPersister.class);
    private final boolean checkData;

    public TangoDataPersister(ArchivingErrorCollector errorCollector, ArchivingInfra hdbProxy, boolean checkData,
                              ModeHandler modeHandler) {
        this.hdbProxy = hdbProxy;
        this.modeHandler = modeHandler;
        this.errorCollector = errorCollector;
        this.checkData = checkData;
    }

    public void persistScalar(final AttributeScalarEvent scalarEvent) {
        xlogger.entry();
        long start = System.currentTimeMillis();
        boolean doArchive = false;
        if (isTimeStampNotZero(scalarEvent)) {
            final String attributeName = scalarEvent.getAttributeCompleteName();
            try {
                if (!checkData) {
                    doArchive = true;
                } else if (isFirstValue(attributeName.toLowerCase())) {
                    doArchive = true;
                    isFirstValueList.put(attributeName.toLowerCase(), false);
                    logger.info("{} first value, forcing archiving", attributeName);
                } else {
                    doArchive = modeHandler.isDataArchivable(attributeName.toLowerCase(), scalarEvent,
                            (AttributeScalarEvent) (getLastValue(scalarEvent)));
                }
            } catch (final Throwable e) {
                manageInsertionError(attributeName, e);
            }
            if (doArchive) {
                logger.debug("inserting {} into archiving infra", scalarEvent);
                hdbProxy.store(scalarEvent);
                setLastValue(scalarEvent);
                ArchivingKPI.notifyInsertion(scalarEvent);
            }
        }
        notifyInsertionTime(start, scalarEvent.getAttributeCompleteName(), doArchive);
        xlogger.exit();
    }

    private boolean isTimeStampNotZero(final AAttributeEvent<?> attributeEvent) {
        final String name = attributeEvent.getAttributeCompleteName();
        final long newTime = attributeEvent.getTimeStamp();
        boolean doArchive = true;
        if (newTime == 0) {
            String error = "NOARCHIVING - received a zero timestamp for " + name;
            logger.info(error);
            errorCollector.addError(name, new ArchivingException(error));
            doArchive = false;
        }
        final Long lastTimestampStack = lastTimestampsMap.get(name.toLowerCase());
        if (lastTimestampStack == null) {
            doArchive = true;
        } else if (newTime == lastTimestampStack) {
            String error = "NOARCHIVING - attribute " + name + " timestamp hasn't change - timestamp: "
                    + new Timestamp(newTime);
            logger.info(error);
            errorCollector.addError(name, new ArchivingException(error));
            doArchive = false;
        }
        return doArchive;
    }

    private boolean isFirstValue(String name) {
        boolean result = false;
        if (isFirstValueList.containsKey(name.toLowerCase())) {
            result = isFirstValueList.get(name.toLowerCase());
        }
        return result;
    }

    private AAttributeEvent<?> getLastValue(final AAttributeEvent<boolean[]> scalarEvent) {
        return scalarEvent.getAttributeCompleteName() == null ? null
                : lastValueMap.get(scalarEvent.getAttributeCompleteName().toLowerCase());
    }

    private void manageInsertionError(String attributeName, Throwable e) {
        ArchivingKPI.notifyInsertionError();
        errorCollector.addError(attributeName, e);
        logger.error("error storing attribute " + attributeName, e);
    }

    private void setLastValue(final AAttributeEvent<?> scalarEvent) {
        String key = scalarEvent == null ? null : scalarEvent.getAttributeCompleteName().toLowerCase();
        if (key != null) {
            setLastTimestamp(scalarEvent);
            lastValueMap.put(key, scalarEvent);
        }
    }

    private void notifyInsertionTime(long start, String name, boolean doArchive) {
        long end = System.currentTimeMillis();
        if (doArchive) {
            ArchivingKPI.notifyInsertionTime(end - start);
            logger.debug("insertion in DB of {} took {} ms", name, end - start);
        } else {
            logger.debug("{} was not inserted in db, took {} ms", name, end - start);
        }
    }

    private void setLastTimestamp(final AAttributeEvent<?> scalarEvent) {
        if (scalarEvent != null) {
            final String name = scalarEvent.getAttributeCompleteName().toLowerCase();
            if (logger.isDebugEnabled()) {
                Long previousTimeStamp = lastTimestampsMap.get(name);
                if (previousTimeStamp != null && checkData) {
                    long period = scalarEvent.getTimeStamp() - previousTimeStamp;
                    logger.debug("measured period is {} ms for mode {}", period,
                            modeHandler.getMode().toSimpleString());
                }
            }
            lastTimestampsMap.put(name, scalarEvent.getTimeStamp());
        }
    }

    /**
     * Support check on difference mode
     *
     * @param event
     */
    public void persistSpectrum(final SpectrumEventRO event) {
        long start = System.currentTimeMillis();
        boolean doArchive = false;
        if (isTimeStampNotZero(event)) {
            try {
                final String attributeName = event.getAttributeCompleteName();
                if (isFirstValue(attributeName.toLowerCase())) {
                    doArchive = true;
                    isFirstValueList.put(attributeName.toLowerCase(), false);
                    logger.debug("{} first value, forcing archiving", attributeName);
                } else {
                    // check if should be inserted into DB
                    if (checkData) {
                        if (modeHandler.isSpectrumDataArchivable(attributeName, event,
                                (SpectrumEventRO) getLastValue(event))) {
                            doArchive = true;
                        }
                    } else {
                        doArchive = true;
                    }
                }
                if (doArchive) {
                    logger.debug("inserting {} into archiving infra", event);
                    hdbProxy.store(event);
                    ArchivingKPI.notifyInsertion(event);
                }
                setLastValue(event);
            } catch (final Throwable e) {
                manageInsertionError(event.getAttributeCompleteName(), e);
            }
        }
        notifyInsertionTime(start, event.getAttributeCompleteName(), doArchive);
    }

    public void persistSpectrum(final SpectrumEventRW event) {
        long start = System.currentTimeMillis();
        boolean doArchive = false;
        if (isTimeStampNotZero(event)) {
            doArchive = true;
            logger.debug("inserting spectrum {} into archiving infra", event);
            hdbProxy.store(event);
            ArchivingKPI.notifyInsertion(event);
            setLastTimestamp(event);
        }
        notifyInsertionTime(start, event.getAttributeCompleteName(), doArchive);
    }

    public void persistImage(final ImageEventRO event) {
        long start = System.currentTimeMillis();
        boolean doArchive = false;
        if (isTimeStampNotZero(event)) {
            doArchive = true;
            logger.debug("inserting image {} into archiving db", event);
            hdbProxy.store(event);
            ArchivingKPI.notifyInsertion(event);
            setLastTimestamp(event);

        }
        notifyInsertionTime(start, event.getAttributeCompleteName(), doArchive);
    }

    public void removeAttribute(String attributeName, boolean closeFile) throws IOException, ArchivingException {
        lastTimestampsMap.remove(attributeName.toLowerCase());
        isFirstValueList.remove(attributeName.toLowerCase());
        errorCollector.removeError(attributeName);
        hdbProxy.removeAttributeFile(attributeName, closeFile);
    }

    public void setIsFirstValue(String name) {
        isFirstValueList.put(name.toLowerCase(), true);
    }
}
