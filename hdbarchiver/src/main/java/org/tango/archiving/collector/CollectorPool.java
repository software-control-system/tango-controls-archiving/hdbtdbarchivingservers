package org.tango.archiving.collector;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.polling.DefaultPollingCollector;
import org.tango.utils.TangoUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class CollectorPool {
    private final Logger logger = LoggerFactory.getLogger(CollectorPool.class);
    // key = attribute name
    private final Map<String, ITangoCollector> collectorPool = new ConcurrentHashMap<>();
    // key = mode/data type/format (imposed by ATK polling mechanism)
    private final Map<PollingMode, ITangoCollector> pollingCollectorPool = new ConcurrentHashMap<>();
    // key = device name
    private final Map<String, EventCollector> eventCollectorPool = new ConcurrentHashMap<>();
    private final CollectorBuilder builder = new CollectorBuilder();
    private final ArchivingErrorCollector errorCollector = new ArchivingErrorCollector();

    public CollectorPool() {
        ArchivingKPI.notifyCollectorNumber(0);
    }

    public synchronized void startCollecting() {
        for (ITangoCollector collector : pollingCollectorPool.values()) {
            ((DefaultPollingCollector<?, ?>) collector).startCollecting();
        }
    }

    public synchronized void pauseContext(int contextId) throws ArchivingException {
        for (ITangoCollector collector : pollingCollectorPool.values()) {
            collector.pauseContext(contextId);
        }
    }

    public synchronized void resumeContext(int contextId) throws ArchivingException {
        for (ITangoCollector collector : pollingCollectorPool.values()) {
            collector.resumeContext(contextId);
        }
    }

    public Set<String> getPausedAttributes() {
        Set<String> result = new HashSet<>();
        for (ITangoCollector collector : pollingCollectorPool.values()) {
            result.addAll(collector.getPausedAttributes());
        }
        return result;
    }

    public synchronized void startWithTemporaryFiles(final ArchivingConfig config, final ArchivingInfra db, String dbPath, String dsPath, int attributePerFile, final long exportPeriod)
            throws ArchivingException, IOException {

        if (config != null) {
            registerNewAttributeFile(config, db, dbPath, dsPath, attributePerFile, exportPeriod);
            try {
                start(config, db);
            } catch (ArchivingException e) {
                db.removeAttributeFile(config.getAttributeConfig().getFullName(), true);
                throw e;
            }
        }
    }

    private synchronized void registerNewAttributeFile(final ArchivingConfig config, final ArchivingInfra db,
                                                       final String dbPath, String dsPath, final int attributePerFile, final long exportPeriod)
            throws ArchivingException, IOException {
        if (config != null) {
            String attrName = config.getAttributeConfig().getFullName();
            try {
                db.registerNewAttributeFile(config, dbPath, dsPath, attributePerFile, exportPeriod);
                errorCollector.removeError(attrName);
            } catch (ArchivingException | IOException e) {
                errorCollector.addError(attrName, e);
                throw e;
            }
        }
    }

    /**
     * This method create a new HdbCollector instance if required associated to
     * an attribute.
     *
     * @param config Attribute associated to the looked collector.
     */
    public synchronized void start(final ArchivingConfig config, final ArchivingInfra db)
            throws ArchivingException {
        if (config != null) {
            logger.debug("createCollectorAndAddSource for {}", config);
            String attrName = config.getAttributeConfig().getFullName().toLowerCase();
            synchronized (collectorPool) {
                final ITangoCollector collector = collectorPool.get(attrName);
                if (collector == null) {
                    if (config.getModes().isEvent()) {
                        String deviceName;
                        try {
                            deviceName = TangoUtil.getfullDeviceNameForAttribute(attrName);
                        } catch (DevFailed e) {
                            throw new ArchivingException(e);
                        }
                        EventCollector eventCollector = eventCollectorPool.get(deviceName.toLowerCase());
                        if (eventCollector == null) {
                            eventCollector = new EventCollector(db, errorCollector, deviceName);
                            eventCollectorPool.put(deviceName, eventCollector);
                            logger.trace("new event collector {}", eventCollector.assessment());
                        }
                        eventCollector.addAttribute(config);
                        collectorPool.put(attrName, eventCollector);
                    } else {
                        PollingMode pollingMode = getPollingMode(config);
                        ITangoCollector pollingCollector = pollingCollectorPool.get(pollingMode);
                        if (pollingCollector == null) {
                            pollingCollector = builder.build(config, db, errorCollector);
                            pollingCollectorPool.put(pollingMode, pollingCollector);
                            logger.debug("new polling collector {}", pollingCollector.getName());
                        }
                        pollingCollector.addAttribute(config);
                        collectorPool.put(attrName, pollingCollector);
                        ArchivingKPI.notifyCollectorNumber(pollingCollectorPool.size() + eventCollectorPool.size());
                        logger.debug("starting polling collector for {}", config.getAttributeConfig().getFullName());
                    }
                } else {
                    logger.info("{} is already started with collector {}", attrName, collector.assessment());
                }
            }
        }
    }

    private static PollingMode getPollingMode(final ArchivingConfig config) {
        PollingMode pollingMode;
        if (config.getAttributeConfig().getFormat() != AttrDataFormat._SCALAR) {
            pollingMode = new PollingMode(config.getAttributeConfig().getFormat(),
                    config.getModes());
        } else {
            // Scalar, one collector per archiving mode
            pollingMode = new PollingMode(config.getAttributeConfig().getFormat(),
                    config.getModes());
        }
        return pollingMode;
    }

    public synchronized void remove(final String attributeName, boolean closeFile) throws ArchivingException {
        ITangoCollector collector = collectorPool.get(attributeName.toLowerCase());
        errorCollector.removeError(attributeName.toLowerCase());
        if (collector != null && collector.isCollected(attributeName)) {
            logger.debug("removing {} from collector {}", attributeName, collector.getName());
            collector.removeAttribute(attributeName, closeFile);
            collectorPool.remove(attributeName.toLowerCase());
        }
        if (collector != null) {
            int nbAttr = Arrays.stream(collector.loadAssessment()).sum();
            if (nbAttr == 0) {
                collector.clear();
                List<PollingMode> keyToRemove = new ArrayList<PollingMode>();
                for (Entry<PollingMode, ITangoCollector> c : pollingCollectorPool.entrySet()) {
                    if (c.getValue().equals(collector)) {
                        keyToRemove.add(c.getKey());
                    }
                }
                keyToRemove.forEach(pollingCollectorPool::remove);
                List<String> keyEventToRemove = new ArrayList<String>();
                for (Entry<String, EventCollector> c : eventCollectorPool.entrySet()) {
                    if (c.getValue().equals(collector)) {
                        keyEventToRemove.add(c.getKey());
                    }
                }
                keyEventToRemove.forEach(eventCollectorPool::remove);
            }
        }
        ArchivingKPI.notifyCollectorNumber(pollingCollectorPool.size() + eventCollectorPool.size());
    }


    public synchronized void clear() {
        logger.debug("clearing all collectors {}", collectorPool.values());
        for (ITangoCollector collector : collectorPool.values()) {
            collector.clear();
        }
        collectorPool.clear();
        eventCollectorPool.clear();
        pollingCollectorPool.clear();
        errorCollector.clear();

        ArchivingKPI.notifyCollectorNumber(0);
    }

    @Override
    public String toString() {
        final StringBuilder ass = new StringBuilder();
        ass.append(pollingCollectorPool.size()).append(" polling collectors and ");
        ass.append(eventCollectorPool.size()).append(" event collectors");
        for (final ITangoCollector hdbCollector : pollingCollectorPool.values()) {
            ass.append(hdbCollector.assessment());
        }
        for (final EventCollector hdbCollector : eventCollectorPool.values()) {
            ass.append(hdbCollector.assessment());
        }
        return ass.toString();
    }

    public String getCollectorsStatus() {
        final StringBuilder ass = new StringBuilder();
        ass.append("collecting errors:").append(errorCollector.getErrors().size()).append("\n");
        for (Map.Entry<String, String> entry : errorCollector.getErrors().entrySet()) {
            ass.append(entry.getKey()).append(" : ").append(entry.getValue()).append("\n");
        }
        ass.append("========================\n");
        ass.append(pollingCollectorPool.size()).append(" polling collectors and ");
        ass.append(eventCollectorPool.size()).append(" event collectors");
        for (final ITangoCollector hdbCollector : pollingCollectorPool.values()) {
            ass.append(hdbCollector.assessment());
        }
        for (final EventCollector hdbCollector : eventCollectorPool.values()) {
            ass.append(hdbCollector.assessment());
        }
        return ass.toString();
    }

    public String getErrorMessage(String attributeName) {
        String error = errorCollector.getError(attributeName);
        if (error == null) {
            error = "";
        }
        return error;
    }

    public int getCollectorErrorNr() {
        return getCollectorErrors().size();
    }

    public Map<String, String> getCollectorErrors() {
        return errorCollector.getErrors();
    }

    public int[] getCollectorLoad() {
        final int[] ret = new int[3];
        for (final ITangoCollector collector : pollingCollectorPool.values()) {
            final int[] collectorLoad = collector.loadAssessment();
            ret[0] += collectorLoad[0];
            ret[1] += collectorLoad[1];
            ret[2] += collectorLoad[2];
        }
        for (final ITangoCollector collector : eventCollectorPool.values()) {
            final int[] collectorLoad = collector.loadAssessment();
            ret[0] += collectorLoad[0];
            ret[1] += collectorLoad[1];
            ret[2] += collectorLoad[2];
        }
        return ret;
    }

    private static class PollingMode {
        private final int format;
        private final String mode;

        public PollingMode(int format, InsertionModes mode) {
            this.format = format;
            this.mode = mode.toSimpleString();
        }

        @Override
        public int hashCode() {
            return HashCodeBuilder.reflectionHashCode(this);
        }

        @Override
        public boolean equals(Object obj) {
            return EqualsBuilder.reflectionEquals(this, obj);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("format", format)
                    .append("mode", mode)
                    .toString();
        }
    }
}
