package org.tango.archiving.collector;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.events.TangoEventsAdapter;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.events.EventListener;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.utils.DevFailedUtils;
import org.tango.utils.TangoUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class EventCollector implements ITangoCollector {
    // key attribute name
    private final Map<String, EventListener> eventListenerMap = new HashMap<>();
    private final Logger logger = LoggerFactory.getLogger(EventCollector.class);
    private final ArchivingInfra archivingInfra;
    private final ArchivingErrorCollector errorCollector;
    private final String deviceName;
    // key attribute name
    private final Map<String, Integer> attrFormats = new ConcurrentHashMap<>();
    private TangoEventsAdapter eventsAdapter;

    public EventCollector(ArchivingInfra archivingInfra, ArchivingErrorCollector errorCollector, String deviceName) throws ArchivingException {
        this.archivingInfra = archivingInfra;
        this.errorCollector = errorCollector;
        this.deviceName = deviceName;
        try {
            logger.debug("start tango event adapter for device {}", deviceName);
            eventsAdapter = new TangoEventsAdapter(deviceName);
        } catch (DevFailed e) {
            logger.warn("event collector creation failed for device {} with error {}",
                    deviceName, DevFailedUtils.toString(e));
            throw new ArchivingException(deviceName + " device does not exist, no event subscription performed");
        }
    }

    @Override
    public String getName() {
        return "Event "+ deviceName;
    }

    @Override
    public void addAttribute(final ArchivingConfig config) throws ArchivingException {
        try {
            String attributeName = TangoUtil.getAttributeName(config.getAttributeConfig().getFullName()).toLowerCase();
            logger.debug("start event on device {}, attribute {}", deviceName, attributeName);
            EventListener listener = eventListenerMap.get(attributeName);
            if (listener == null) {
                listener = new EventListener(archivingInfra, errorCollector, config.getAttributeConfig().getWriteType());
                eventListenerMap.put(attributeName, listener);
            }
            eventsAdapter.addTangoArchiveListener(listener, attributeName, new String[0]);
            attrFormats.put(attributeName, config.getAttributeConfig().getFormat());
        } catch (final DevFailed e) {
            logger.error("subscription failed {}", DevFailedUtils.toString(e));
            throw new ArchivingException(e);
        }
    }

    @Override
    public void removeAttribute(final String attributeName, boolean closeFile) throws ArchivingException {
        logger.debug("removing event source for {}", attributeName);
        try {
            String deviceNameTmp = TangoUtil.getfullDeviceNameForAttribute(attributeName);
            if (deviceNameTmp.equalsIgnoreCase(deviceName)) {
                String attributeNameShort = TangoUtil.getAttributeName(attributeName).toLowerCase();
                EventListener listener = eventListenerMap.get(attributeNameShort);
                if (listener != null) {
                    listener.stop(attributeNameShort, closeFile);
                    eventsAdapter.removeTangoArchiveListener(listener, deviceName);
                    eventListenerMap.remove(attributeNameShort);
                }
            }
            attrFormats.remove(attributeName.toLowerCase());
        } catch (final DevFailed e) {
            throw new ArchivingException(e);
        }
    }

    @Override
    public boolean isCollected(String attributeName) {
        return (eventListenerMap.get(attributeName.toLowerCase()) != null);
    }

    @Override
    public void pauseContext(final int contextId) throws ArchivingException {
        //TODO
    }

    @Override
    public void resumeContext(final int contextId) throws ArchivingException {
        //TODO
    }

    @Override
    public Set<String> getPausedAttributes() {
        return null;
    }

    @Override
    public String assessment() {
        return "\n----------\n" +
                this.getClass().getCanonicalName() + " - for device " + deviceName + ", " +
                eventListenerMap.size() + " attribute.s collected with Tango EVENTS" +
                "\nattributes : " + eventListenerMap.keySet();
    }

    @Override
    public void clear() {
        // XXXX TangoEventsAdapter does not provide an API to clear its content
        // workaround, rebuild it
        try {
            eventsAdapter = new TangoEventsAdapter(deviceName);
        } catch (DevFailed e) {
            logger.error("creating of event adapter for {} failed", deviceName);
        }
        attrFormats.clear();
    }

    @Override
    public int[] loadAssessment() {
        int scalarCharge = 0;
        int spectrumCharge = 0;
        int imageCharge = 0;
        for (int format : attrFormats.values()) {
            switch (format) {
                case AttrDataFormat._SCALAR:
                    scalarCharge++;
                    break;
                case AttrDataFormat._SPECTRUM:
                    spectrumCharge++;
                    break;
                case AttrDataFormat._IMAGE:
                    imageCharge++;
                    break;
            }
        }
        return new int[]{scalarCharge, spectrumCharge, imageCharge};
    }

    @Override
    public InsertionModes getMode() {
        InsertionModes mode = new InsertionModes();
        mode.setEvent(true);
        return mode;
    }
}
