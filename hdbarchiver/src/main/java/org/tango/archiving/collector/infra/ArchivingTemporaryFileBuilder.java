package org.tango.archiving.collector.infra;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;

import java.io.IOException;

public class ArchivingTemporaryFileBuilder {

    ArchivingTemporaryFile buildArchivingTemporaryFile(final String attributeName, final String tableName,final long exportPeriod,
                                                       final ArchivingConfig config, final IArchivingDatabase database,
                                                       final String workingDsPath,
                                                       final String workingDbPath, DatabaseConnectionConfig.DataBaseType dbType)
            throws IOException, ArchivingException {
        final ArchivingTemporaryFile file = new ArchivingTemporaryFile(attributeName, tableName,
                config.getAttributeConfig().getFormat(),
                config.getAttributeConfig().getWriteType(),
                exportPeriod, database,
                workingDsPath, workingDbPath, dbType);
        file.setFileWriter(new FileWriter());
        file.openFile();
        return file;
    }
}
