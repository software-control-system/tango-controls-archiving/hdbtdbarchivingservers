package org.tango.archiving.collector.error;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.tools.ArchivingDateConstants;
import fr.soleil.lib.project.ObjectUtils;
import org.tango.utils.DevFailedUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ArchivingErrorCollector implements ArchivingDateConstants {

    private final ConcurrentHashMap<String, String> collectorErrors = new ConcurrentHashMap<>();

    public void addError(String attributeName, Throwable e) {
        // final StringWriter sw = new StringWriter();
        // e.printStackTrace(new PrintWriter(sw));
        String date = EU_FORMAT.format(LocalDateTime.now()) + " - ";
        if (attributeName != null && e.getMessage() != null) {
            if (e instanceof DevFailed) {
                collectorErrors.put(attributeName.toLowerCase(), date + DevFailedUtils.toString((DevFailed) e));
            } else {
                collectorErrors.put(attributeName.toLowerCase(), date + e.getMessage());
            }
        } else if (attributeName == null) {
            collectorErrors.put("Unknown attribute", date + e.getMessage());
        } else if (e.getMessage() == null) {
            collectorErrors.put(attributeName.toLowerCase(), date + e.getClass().getCanonicalName());
        }
    }

    public void addError(String attributeName, String message) {
        String date = EU_FORMAT.format(LocalDateTime.now()) + " - ";
        if (attributeName != null && message != null) {
            collectorErrors.put(attributeName.toLowerCase(), date + message);
        } else if (attributeName == null) {
            collectorErrors.put("Unknown attribute", date + message);
        }
    }

    public void removeError(String attributeName) {
        collectorErrors.remove(attributeName.toLowerCase());
    }

    public Map<String, String> getErrors() {
        return new HashMap<>(collectorErrors);
    }

    public String getError(String attributeName) {
        if (attributeName != null) {
            return collectorErrors.get(attributeName.toLowerCase());
        } else {
            return ObjectUtils.EMPTY_STRING;
        }
    }

    public void clear() {
        collectorErrors.clear();
    }
}
