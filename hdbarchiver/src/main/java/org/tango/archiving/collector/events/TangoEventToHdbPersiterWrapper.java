package org.tango.archiving.collector.events;

import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;
import org.tango.archiving.collector.data.TangoDataPersister;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.events.TangoArchiveEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.utils.DevFailedUtils;

public class TangoEventToHdbPersiterWrapper {

    private static final int DEFAULT_TRY_NUMBER = 2;
    private final TangoDataPersister persister;
    private final TangoEventToArchivingDataMapper mapper = new TangoEventToArchivingDataMapper();
    private final Logger logger = LoggerFactory.getLogger(TangoEventToHdbPersiterWrapper.class);

    public TangoEventToHdbPersiterWrapper(TangoDataPersister persister) {
        this.persister = persister;
    }

    public void persist(TangoArchiveEvent archiveEvent, int writeType, ArchivingErrorCollector errorCollector) {
        // FIXME: impossible to archive null value in case of error because the attribute name is unknown
        try {
            AttrDataFormat dataFormat = archiveEvent.getValue().getDataFormat();
            if (dataFormat.equals(AttrDataFormat.SCALAR)) {
                try {
                    AttributeScalarEvent scalarEvent = mapper.mapScalarEvent(archiveEvent, writeType);
                    if(scalarEvent.getErrorMessage()!=null && !scalarEvent.getErrorMessage().isEmpty()){
                        errorCollector.addError(scalarEvent.getAttributeCompleteName(), scalarEvent.getErrorMessage());
                    }
                    persister.persistScalar(scalarEvent);

                } catch (final Throwable e) {
                    ArchivingKPI.notifyTangoScalarError();
                    logger.error("unexpected error", e);
                }
            } else if (dataFormat.equals(AttrDataFormat.SPECTRUM)) {
                try {
                    if (writeType == AttrWriteType._READ) {
                        SpectrumEventRO spectrumEventRo = mapper.mapSpectrumEventRO(archiveEvent);
                        if(spectrumEventRo.getErrorMessage()!=null && !spectrumEventRo.getErrorMessage().isEmpty()){
                            errorCollector.addError(spectrumEventRo.getAttributeCompleteName(), spectrumEventRo.getErrorMessage());
                        }
                        persister.persistSpectrum(spectrumEventRo);
                    } else {
                        SpectrumEventRW spectrumEventRw = mapper.mapSpectrumEventRW(archiveEvent);
                        if(spectrumEventRw.getErrorMessage()!=null && !spectrumEventRw.getErrorMessage().isEmpty()){
                            errorCollector.addError(spectrumEventRw.getAttributeCompleteName(), spectrumEventRw.getErrorMessage());
                        }
                        persister.persistSpectrum(spectrumEventRw);
                    }
                } catch (final Throwable e) {
                    ArchivingKPI.notifyTangoSpectrumError();
                    logger.error("unexpected error", e);
                }
            }
        } catch (final Throwable e) {
            ArchivingKPI.notifyTangoScalarError();
            if (e instanceof DevFailed) {
                logger.error("impossible to persist event data, " + DevFailedUtils.toString((DevFailed) e), e);
            } else {
                logger.error("unexpected error", e);
            }
        }
    }
}
