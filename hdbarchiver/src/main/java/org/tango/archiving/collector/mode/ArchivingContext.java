package org.tango.archiving.collector.mode;

import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ArchivingContext {
    private final int contextId;
    private final Map<String, ArchivingConfig> attributeConfigMap = new ConcurrentHashMap<>();

    public boolean isActive() {
        return isActive;
    }

    public void setActive(final boolean active) {
        isActive = active;
    }

    private boolean isActive;

    public ArchivingContext(int contextId) {
        this.contextId = contextId;
    }

    public Map<String, ArchivingConfig> getAttributeConfigMap() {
        return attributeConfigMap;
    }

    public int getContextId() {
        return contextId;
    }

    public ArchivingContext addAttribute(String attributeName, ArchivingConfig config) {
        attributeConfigMap.put(attributeName, config);
        return this;
    }

    public void removeAttribute(String attributeName) {
        attributeConfigMap.remove(attributeName);
    }
}
