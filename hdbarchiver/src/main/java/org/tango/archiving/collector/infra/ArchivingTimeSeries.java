package org.tango.archiving.collector.infra;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevState;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.build.TangoArchivingServicesBuilder;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.event.insert.AttributeEvent;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import fr.soleil.tango.archiving.services.TangoArchivingInserterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.ImageEventRO;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

public class ArchivingTimeSeries implements IArchivingDatabase {


    private static final Logger LOGGER = LoggerFactory.getLogger(ArchivingTimeSeries.class);

    private final TangoArchivingInserterService timeScale;

    private final boolean insertErrors;

    public ArchivingTimeSeries(DatabaseConnectionConfig parameters, boolean insertErrors) {
        this.insertErrors = insertErrors;

        timeScale = new TangoArchivingServicesBuilder().buildInserter(parameters);
    }

    @Override
    public DataBaseManager getDataBase() {
        return null;
    }

    @Override
    public void registerAttributeMode(final ArchivingConfig config) {
        timeScale.registerAttribute(config.getAttributeConfig().getFullName(),
                config.getModes(), config.getArchiver(), 0);
    }

    @Override
    public void stopAttributeMode(final String attributeName) {
        timeScale.stopAttribute(attributeName);
    }

    @Override
    public boolean isArchived(final String attributeName, final String deviceName) {
        return timeScale.isArchivedByArchiver(attributeName, deviceName);
    }

    @Override
    public void exportToDBScalar(final String remoteFilePath, final String fileName, final String tableName,
                                 final int writable) {
        // not supported
    }

    @Override
    public void exportToDBSpectrum(final String remoteFilePath, final String fileName, final String tableName,
                                   final int writable) {
        // not supported
    }

    @Override
    public void exportToDBImage(final String remoteFilePath, final String fileName, final String tableName,
                                final int writable) {
        // not supported
    }

    @Override
    public ArchivingConfigs getArchiverCurrentTasks(final String archiverName) {
        ArchivingConfigs archiverCurrentTasks = new ArchivingConfigs();
        final List<InsertionModes> archiverTasks = timeScale.getCurrentAttributeModes(archiverName);
        LOGGER.info("Got {} attributes to start on archiver {}", archiverTasks.size(), archiverName);
        for (InsertionModes task : archiverTasks) {
            ArchivingConfig archivingConfig = new ArchivingConfig();
            AttributeConfig attribute = new AttributeConfig();
            final AttributeConfig config = timeScale.getAttributeConfig(task.getAttributeID());
            LOGGER.debug("starting attribute = {} with modes = {}", config, task);
            attribute.setFullName(config.getFullName());
            attribute.setWriteType(config.getWriteType());
            attribute.setFormat(config.getFormat());
            archivingConfig.setAttributeConfig(attribute);
            archivingConfig.setModes(task);
            archiverCurrentTasks.addConfiguration(archivingConfig);
        }
        return archiverCurrentTasks;
    }

    @Override
    public void store(final AttributeScalarEvent scalarEvent) {
        final AttributeEvent value = new AttributeEvent();
        Object readValue = scalarEvent.getReadValue();
        if (readValue instanceof DevState) {
            readValue = ((DevState) readValue).value();
        }
        if (scalarEvent.getErrorMessage() == null) {
            switch (scalarEvent.getWritable()) {
                case AttrWriteType._READ:
                case AttrWriteType._READ_WITH_WRITE:
                    value.setValueR(readValue);
                    break;
                case AttrWriteType._READ_WRITE:
                    value.setValueR(readValue);
                    Object writeValue = Array.get(scalarEvent.getValue(), 1);
                    if (writeValue != null && writeValue instanceof DevState) {
                        writeValue = ((DevState) writeValue).value();
                    }
                    value.setValueW(writeValue);
                    break;
                case AttrWriteType._WRITE:
                    value.setValueW(readValue);
                    break;
            }
        }
        value.setDataTime(new Timestamp(scalarEvent.getTimeStamp()));
        value.setFullName(scalarEvent.getAttributeCompleteName());
        value.setQuality(scalarEvent.getQuality());
        value.setErrorMessage(Optional.ofNullable(scalarEvent.getErrorMessage()));
        value.setEnumReadLabel(scalarEvent.getEnumReadLabel());
        value.setEnumWriteLabel(scalarEvent.getEnumWriteLabel());
        timeScale.insertAttribute(value, AttrDataFormat.SCALAR, insertErrors);
    }

    @Override
    public void store(final SpectrumEventRO spectrumEventRO) {
        final AttributeEvent value = new AttributeEvent();
        value.setValueR(spectrumEventRO.getValue());
        value.setDataTime(new Timestamp(spectrumEventRO.getTimeStamp()));
        value.setFullName(spectrumEventRO.getAttributeCompleteName());
        value.setQuality(spectrumEventRO.getQuality());
        value.setErrorMessage(Optional.ofNullable(spectrumEventRO.getErrorMessage()));
        timeScale.insertAttribute(value, AttrDataFormat.SPECTRUM, insertErrors);
    }

    @Override
    public void store(final SpectrumEventRW spectrumEventRW) {
        final AttributeEvent value = new AttributeEvent();
        value.setValueR(spectrumEventRW.getSpectrumValueRWRead());
        value.setValueW(spectrumEventRW.getSpectrumValueRWWrite());
        value.setDataTime(new Timestamp(spectrumEventRW.getTimeStamp()));
        value.setFullName(spectrumEventRW.getAttributeCompleteName());
        value.setQuality(spectrumEventRW.getQuality());
        value.setErrorMessage(Optional.ofNullable(spectrumEventRW.getErrorMessage()));
        timeScale.insertAttribute(value, AttrDataFormat.SPECTRUM, insertErrors);
    }

    public void store(final ImageEventRO imageEventRO) {
        final AttributeEvent value = new AttributeEvent();
        value.setValueR(imageEventRO.getValue());
        value.setDataTime(new Timestamp(imageEventRO.getTimeStamp()));
        value.setFullName(imageEventRO.getAttributeCompleteName());
        value.setQuality(imageEventRO.getQuality());
        value.setErrorMessage(Optional.ofNullable(imageEventRO.getErrorMessage()));
        timeScale.insertAttribute(value, AttrDataFormat.IMAGE, insertErrors);
    }

    @Override
    public void closeConnection() {

    }
}
