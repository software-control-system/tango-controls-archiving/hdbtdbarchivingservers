package org.tango.archiving.collector.infra;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.ImageEventRO;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ArchivingInfra {

    private final Logger logger = LoggerFactory.getLogger(ArchivingInfra.class);
    private final IArchivingDatabase database;
    private final Map<String, ArchivingTemporaryFile> filesNames = new ConcurrentHashMap<>();
    private final boolean isDBInsertion;
    private final DatabaseConnectionConfig.DataBaseType dbType;

    private final Map<String, String> insertionErrors = new ConcurrentHashMap<>();

    private boolean isTimeseries = false;


    public ArchivingInfra(final DatabaseConnectionConfig parameters, final boolean isHDB,
                          final boolean isDBInsertion, final boolean insertErrors) throws ArchivingException {
        if (parameters.getDbType().equals(DatabaseConnectionConfig.DataBaseType.POSTGRESQL)) {
            isTimeseries = true;
            this.isDBInsertion = true;
            database = new ArchivingTimeSeries(parameters, insertErrors);
            logger.debug("building TTS connection with parameters {}", parameters);
        } else {
            this.isDBInsertion = isDBInsertion;
            database = new ArchivingDatabase(parameters, isHDB, isDBInsertion);
        }
        dbType = parameters.getDbType();
    }

    public void registerNewAttributeFile(final ArchivingConfig config, final String dbPath, final String dsPath, final int attributePerFile,
                                         final long exportPeriod) throws ArchivingException, IOException {
        if (!isTimeseries) {
            String attName = config.getAttributeConfig().getFullName().toLowerCase();
            final String tableName = database.getDataBase().getDbUtil()
                    .getTableName(attName);
            if (filesNames.get(attName) == null) {
                logger.debug("registerNewAttributeFile {} ", attName);
                final ArchivingTemporaryFile myFile = new ArchivingTemporaryFileBuilder().buildArchivingTemporaryFile
                        (attName, tableName,exportPeriod,
                                config, database,
                                dsPath, dbPath, dbType);
                filesNames.put(attName, myFile);
                myFile.setAttributePerFile(attributePerFile);
            }
        }
    }

    public void removeAttributeFile(final String attributeName, final boolean closeFile) throws ArchivingException, IOException {
        if (!isTimeseries) {
            String attName = attributeName.toLowerCase();
            if (closeFile && filesNames.get(attName) != null) {
                filesNames.get(attName).closeFile();
                filesNames.remove(attName);
            }

        }
    }

    public String exportFile2Db(final String attributeName) throws IOException, ArchivingException {
        String result = "";
        if (!isTimeseries) {
            if (filesNames.get(attributeName.toLowerCase()) != null) {
                logger.debug("FORCING export for {}", attributeName);
                result = filesNames.get(attributeName.toLowerCase()).switchFile();
                logger.debug("Export forced done for {} - {}", attributeName, result);
            }
        }
        return result;
    }

    public String getFilePathForAttribute(final String attributeName) {
        if (!isTimeseries) {
            ArchivingTemporaryFile file = filesNames.get(attributeName.toLowerCase());
            if (file == null) {
                return null;
            } else {
                return file.getLocalFilePath();
            }
        }
        return null;
    }


    public void stopAttributeMode(final String attributeName) throws ArchivingException {
        database.stopAttributeMode(attributeName);
    }

    public void updateAttributeMode(final ArchivingConfig config, final String deviceName) throws ArchivingException {
        final String attCompleteName = config.getAttributeConfig().getFullName();
        if (isArchived(attCompleteName, deviceName)) {
            database.stopAttributeMode(attCompleteName);
        }
        database.registerAttributeMode(config);
    }


    public boolean isArchived(final String attributeName, final String deviceName) throws ArchivingException {
        return database.isArchived(attributeName, deviceName);
    }


    public ArchivingConfigs getArchiverCurrentTasks(final String archiverName)
            throws ArchivingException {
        return database.getArchiverCurrentTasks(archiverName);
    }

    /**
     * This method gives to the Archiver the event for filing. *
     *
     * @param scalarEvent event to be archived.
     */
    public void store(final AttributeScalarEvent scalarEvent) {
        logger.debug("store scalar - isDBInsertion : {} ", isDBInsertion);
        String attributeName = scalarEvent.getAttributeCompleteName().toLowerCase();
        insertionErrors.remove(attributeName);
        try {
            if (isDBInsertion) {
                database.store(scalarEvent);
            } else {
                ArchivingTemporaryFile tdbInserter = filesNames.get(attributeName);
                if (tdbInserter == null) {
                    logger.error("could not insert into file for {}", attributeName);
                    throw new ArchivingException("No file inserter found for " + attributeName);
                } else {
                    tdbInserter.processEventScalar(scalarEvent);
                }
            }
        } catch (Throwable e) {
            ArchivingKPI.notifyInsertionError();
            logger.error("Error storing attribute " + attributeName, e);
            if (e.getMessage() != null) {
                insertionErrors.put(attributeName, e.getMessage());
            } else {
                insertionErrors.put(attributeName, e.getClass().getCanonicalName());
            }
        }
    }

    public void store(final SpectrumEventRO spectrumEventRO) {
        logger.debug("store spectrum - isDBInsertion : {} ", isDBInsertion);
        String attributeName = spectrumEventRO.getAttributeCompleteName().toLowerCase();
        insertionErrors.remove(attributeName);
        try {
            if (isDBInsertion) {
                database.store(spectrumEventRO);
            } else {
                ArchivingTemporaryFile tdbInserter = filesNames.get(attributeName);
                if (tdbInserter == null) {
                    logger.error("could not insert into file for {}", attributeName);
                    throw new ArchivingException("No file inserter found for " + attributeName);
                } else {
                    tdbInserter.processEventSpectrum(spectrumEventRO);
                }
            }
        } catch (Throwable e) {
            logger.error("Error storing attribute " + attributeName, e);
            insertionErrors.put(attributeName, e.getMessage());
        }
    }

    public void store(final SpectrumEventRW spectrumEventRW) {
        logger.debug("store spectrum - isDBInsertion : {} ", isDBInsertion);
        String attributeName = spectrumEventRW.getAttributeCompleteName().toLowerCase();
        insertionErrors.remove(attributeName);
        try {
            if (isDBInsertion) {
                database.store(spectrumEventRW);
            } else {
                ArchivingTemporaryFile tdbInserter = filesNames.get(attributeName);
                if (tdbInserter == null) {
                    logger.error("could not insert into file for {}", attributeName);
                    throw new ArchivingException("No file inserter found for " + attributeName);
                } else {
                    tdbInserter.processEventSpectrum(spectrumEventRW);
                }
            }
        } catch (Throwable e) {
            logger.error("Error storing attribute " + attributeName, e);
            insertionErrors.put(attributeName, e.getMessage());
        }

    }

    public void removeInsertionError(String attributeName) {
        insertionErrors.remove(attributeName.toLowerCase());
    }

    public Map<String, String> getInsertionErrors() {
        return new HashMap<>(insertionErrors);
    }

    public void store(final ImageEventRO imageEventRO) {
        logger.debug("store image - isDBInsertion : {} ", isDBInsertion);
        String attributeName = imageEventRO.getAttributeCompleteName().toLowerCase();
        insertionErrors.remove(attributeName);
        try {
            if (isDBInsertion) {
                database.store(imageEventRO);
            } else {
                ArchivingTemporaryFile tdbInserter = filesNames.get(attributeName);
                if (tdbInserter == null) {
                    logger.error("could not insert into file for {}", attributeName);
                    throw new ArchivingException("No file inserter found for " + attributeName);
                } else {
                    tdbInserter.processEventImage(imageEventRO);
                }
            }
        } catch (Throwable e) {
            logger.error("Error storing attribute " + attributeName, e);
            insertionErrors.put(attributeName, e.getMessage());
        }
    }

    public DataBaseManager getDatabaseManager() {
        return database.getDataBase();
    }

    public void closeConnection() {
        insertionErrors.clear();
        if (isDBInsertion) {
            database.closeConnection();
        } else {
            for (ArchivingTemporaryFile tdbInserter : filesNames.values()) {
                try {
                    tdbInserter.closeFile();
                } catch (IOException | ArchivingException e) {
                    //ignore
                }
            }
            filesNames.clear();
        }
    }
}
