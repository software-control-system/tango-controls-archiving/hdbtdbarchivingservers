package org.tango.archiving.collector.polling;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ModeHandler;

import java.math.BigInteger;

public class DefaultScalarPooling extends DefaultPollingCollector<AttributeScalarEvent, boolean[]> {


    public DefaultScalarPooling(ModeHandler modeHandler, ArchivingInfra archivingInfra,
                                ArchivingErrorCollector errorCollector) {
        super(modeHandler, archivingInfra, errorCollector);
    }

    @Override
    protected AttributeScalarEvent initEvent() {
        AttributeScalarEvent event = new AttributeScalarEvent();
        event.setDataFormat(AttrDataFormat._SCALAR);
        return event;
    }

    @Override
    protected void notifyError() {
        ArchivingKPI.notifyTangoScalarError();
    }

    @Override
    protected void notifyElement() {
        ArchivingKPI.notifyTangoScalar();
    }

    @Override
    protected void persist(AttributeScalarEvent event) {
        persister.persistScalar(event);
    }

    @Override
    protected AttributeScalarEvent getValue(TangoAttribute tangoAttribute, DeviceAttribute deviceAttribute, AttributeScalarEvent event) throws DevFailed {
        Object value;
        AttrWriteType writeType = AttrWriteType.from_int(event.getWritable());
        AttrDataFormat format = AttrDataFormat.SCALAR;
        AttributeInfoEx infoEx = null;
        if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ENUM) {
            // FIXME: find a workaround to avoid a Tango synchronous call
            infoEx = tangoAttribute.getAttributeProxy().getDeviceProxy().
                    get_attribute_info_ex(deviceAttribute.getName());
        }
        if (writeType.value() == AttrWriteType._READ_WRITE) {
            Object read = InsertExtractUtils.extractRead(deviceAttribute, format);
            Object write = InsertExtractUtils.extractWrite(deviceAttribute, writeType, format);
            if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ENUM) {
                assert infoEx != null;
                event.setEnumReadLabel(infoEx.getEnumLabel((short) read));
                event.setEnumWriteLabel(infoEx.getEnumLabel((short) write));
            } else if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ULONG64 && read instanceof Long) {
                read = new BigInteger(Long.toUnsignedString((long) read));
                write = new BigInteger(Long.toUnsignedString((long) write));
            }
            value = new Object[]{read, write};
        } else {
            // if AttrWriteType._WRITE -> write value is put in read part
            value = InsertExtractUtils.extractRead(deviceAttribute, format);
            if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ENUM) {
                assert infoEx != null;
                event.setEnumWriteLabel(infoEx.getEnumLabel((short) value));
            } else if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ULONG64 && value instanceof Long) {
                value = new BigInteger(Long.toUnsignedString((long) value));
            }
        }
        event.setQuality(deviceAttribute.getQuality().value());
        event.setValue(value, null);
        return event;
    }

    @Override
    void setNullValue(AttributeScalarEvent event) {
        event.setValue(null, new boolean[]{true});
    }


}
