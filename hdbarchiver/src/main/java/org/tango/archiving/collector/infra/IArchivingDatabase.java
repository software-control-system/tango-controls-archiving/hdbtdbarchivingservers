package org.tango.archiving.collector.infra;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.ImageEventRO;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;

import java.io.IOException;
import java.sql.SQLException;

public interface IArchivingDatabase {


    DataBaseManager getDataBase();

    void registerAttributeMode(final ArchivingConfig config) throws ArchivingException;

    void stopAttributeMode(final String attributeName) throws ArchivingException;

    boolean isArchived(final String attributeName, final String deviceName) throws ArchivingException;

    void exportToDBScalar(final String remoteFilePath, final String fileName, final String tableName, final int writable) throws ArchivingException;

    void exportToDBSpectrum(final String remoteFilePath, final String fileName, final String tableName, final int writable) throws ArchivingException;

    void exportToDBImage(final String remoteFilePath, final String fileName, final String tableName, final int writable) throws ArchivingException;

    ArchivingConfigs getArchiverCurrentTasks(final String archiverName) throws ArchivingException;


    void store(final AttributeScalarEvent scalarEvent) throws ArchivingException;

    void store(final SpectrumEventRO spectrumEventRO) throws ArchivingException;

    void store(final SpectrumEventRW spectrumEventRW) throws ArchivingException;

    void store(final ImageEventRO imageEventRO) throws SQLException, IOException, ArchivingException;

    void closeConnection();
}
