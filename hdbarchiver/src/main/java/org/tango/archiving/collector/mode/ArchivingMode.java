package org.tango.archiving.collector.mode;

/**
 * Created by ABEILLE on 14/03/2017.
 */
public enum ArchivingMode {
    PERIODIC,
    /**
     * current > previous + previous * upper percent limit
     * current < previous - previous * lower percent limit
     * option slow drift:
     * slow drift = false => The comparison is done with the previous read value..
     * slow drift = true => The comparison is done with the previous archived value.
     */
    RELATIVE,
    /**
     * current < previous - lower limit
     * current > previous + upper limit
     * slow drift = false => The comparison is done with the previous read value.
     * slow drift = true => The comparison is done with the previous archived value.
     */
    ABSOLUTE,
    /**
     * current >= upper limit
     * current <= lower limit
     */
    THRESHOLD,
    /**
     * current!=previous
     */
    DIFFERENCE,
    /**
     * archiving done on Tango archive event
     */
    EVENT
}
