package org.tango.archiving.collector.polling;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import fr.soleil.tango.clientapi.TangoAttribute;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ModeHandler;

import java.math.BigInteger;
import java.util.Arrays;

public class DefaultSpectrumROPolling extends DefaultPollingCollector<SpectrumEventRO, boolean[]> {

    public DefaultSpectrumROPolling(ModeHandler modeHandler, ArchivingInfra archivingInfra,
                                    ArchivingErrorCollector errorCollector) {
        super(modeHandler, archivingInfra, errorCollector);
    }

    @Override
    protected SpectrumEventRO initEvent() {
        SpectrumEventRO event = new SpectrumEventRO();
        event.setDataFormat(AttrDataFormat._SPECTRUM);
        return event;
    }

    @Override
    protected void notifyError() {
        ArchivingKPI.notifyTangoSpectrumError();
    }

    @Override
    protected void notifyElement() {
        ArchivingKPI.notifyTangoSpectrum();
    }

    @Override
    protected void persist(SpectrumEventRO event) {
        persister.persistSpectrum(event);
    }

    protected SpectrumEventRO getValue(TangoAttribute tangoAttribute, DeviceAttribute deviceAttribute, SpectrumEventRO event) throws DevFailed {
        Object value = InsertExtractUtils.extract(deviceAttribute);
        if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ULONG64) {
            long[] longArray = (long[]) value;
            BigInteger[] bigIntegers = new BigInteger[longArray.length];
            for (int i = 0; i < longArray.length; i++) {
                bigIntegers[i] = new BigInteger(Long.toUnsignedString(longArray[i]));
            }
            value = bigIntegers;
        }
        event.setValue(value, null);
        event.setDimX(deviceAttribute.getDimX());
        event.setQuality(deviceAttribute.getQuality().value());
        if (tangoAttribute.getDataType() == TangoConst.Tango_DEV_ENUM) {
            final AttributeInfoEx infoEx = tangoAttribute.getAttributeProxy().getDeviceProxy().
                    get_attribute_info_ex(deviceAttribute.getName());
            short[] array = (short[]) value;
            String[] labels = new String[array.length];
            for (int i = 0; i < array.length; i++) {
                labels[i] = infoEx.getEnumLabel(array[i]);
            }
            event.setEnumReadLabel(labels);
        }
        return event;
    }

    @Override
    void setNullValue(SpectrumEventRO event) {
        event.setValue(null, new boolean[]{true});
    }

}
