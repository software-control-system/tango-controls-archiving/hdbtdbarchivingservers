package org.tango.archiving.collector.events;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoApi.events.TangoArchive;
import fr.esrf.TangoApi.events.TangoArchiveEvent;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.tango.clientapi.InsertExtractUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;
import org.tango.utils.ArrayUtils;
import org.tango.utils.DevFailedUtils;

import java.math.BigInteger;

public class TangoEventToArchivingDataMapper {

    public static final String SEPARATOR = "/";
    private static final Logger LOGGER = LoggerFactory.getLogger(TangoEventToArchivingDataMapper.class);

    public AttributeScalarEvent mapScalarEvent(TangoArchiveEvent event, int writeType) {
        AttributeScalarEvent mapped = new AttributeScalarEvent();
        TangoArchive source = (TangoArchive) event.getSource();
        try {
            mapped.setDataFormat(AttrDataFormat._SCALAR);
            mapped.setWritable(writeType);
            DeviceAttribute deviceAttribute = event.getValue();
            mapped.setTimeStamp(deviceAttribute.getTime());
            mapped.setDataType(deviceAttribute.getType());
            mapped.setAttributeCompleteName(source.getEventSupplier().get_name() + SEPARATOR + deviceAttribute.getName());
            mapped.setQuality(deviceAttribute.getQuality().value());
            setScalarValue(mapped, deviceAttribute, writeType);
        } catch (DevFailed e) {
            String err = DevFailedUtils.toString(e);
            LOGGER.error(err, e);
            mapped.setErrorMessage(err);
            if (e.errors.length > 0) {
                mapped.setErrorMessage(e.errors[0].desc);
            }
        }
        return mapped;
    }

    void setScalarValue(AttributeScalarEvent scalarEvent, DeviceAttribute attrib, int writeType) throws DevFailed {
      //  AttributeInfoEx infoEx = null;
        AttrDataFormat format = AttrDataFormat.SCALAR;
        Object value;
        // TODO : add support for devenum labels
      /*  if (attrib.getType() == TangoConst.Tango_DEV_ENUM) {
            // FIXME: find a workaround to avoid a Tango synchronous call
            infoEx = ().getDeviceProxy().
                    get_attribute_info_ex(deviceAttribute.getName());
        }*/
        if (writeType == AttrWriteType._READ_WRITE) {
            Object read = InsertExtractUtils.extractRead(attrib, format);
            Object write = InsertExtractUtils.extractWrite(attrib, AttrWriteType.from_int(writeType), format);
           /* if (attrib.getType() == TangoConst.Tango_DEV_ENUM) {
                assert infoEx != null;
                event.setEnumReadLabel(infoEx.getEnumLabel((short) read));
                event.setEnumWriteLabel(infoEx.getEnumLabel((short) write));
            } else */
            if (attrib.getType() == TangoConst.Tango_DEV_ULONG64 && read instanceof Long) {
                read = new BigInteger(Long.toUnsignedString((long) read));
                write = new BigInteger(Long.toUnsignedString((long) write));
            }
            value = new Object[]{read, write};
        } else {
            // if AttrWriteType._WRITE -> write value is put in read part
            value = InsertExtractUtils.extractRead(attrib, format);
          /*  if (attrib.getType() == TangoConst.Tango_DEV_ENUM) {
                assert infoEx != null;
                event.setEnumWriteLabel(infoEx.getEnumLabel((short) value));
            } else */
            if (attrib.getType() == TangoConst.Tango_DEV_ULONG64 && value instanceof Long) {
                value = new BigInteger(Long.toUnsignedString((long) value));
            }
        }
        scalarEvent.setValue(value, null);
    }

    public SpectrumEventRO mapSpectrumEventRO(TangoArchiveEvent event) {
        TangoArchive arch = (TangoArchive) event.getSource();
        SpectrumEventRO spectrumEvent = new SpectrumEventRO();
        try {
            DeviceProxy proxy = arch.getEventSupplier();
            DeviceAttribute deviceAttribute = event.getValue();
            setSpectrumReadValue(deviceAttribute, spectrumEvent);
            spectrumEvent.setAttributeCompleteName(proxy.name() + SEPARATOR + deviceAttribute.getName());
            spectrumEvent.setTimeStamp(deviceAttribute.getTime());
            spectrumEvent.setDataType(deviceAttribute.getType());
            spectrumEvent.setWritable(AttrWriteType._READ);
            spectrumEvent.setDimX(deviceAttribute.getDimX());
            spectrumEvent.setQuality(deviceAttribute.getQuality().value());
        } catch (DevFailed e) {
            String err = DevFailedUtils.toString(e);
            LOGGER.error(err, e);
            if (e.errors.length > 0) {
                spectrumEvent.setErrorMessage(e.errors[0].desc);
            }
        }
        return spectrumEvent;
    }

     void setSpectrumReadValue(DeviceAttribute deviceAttribute, SpectrumEventRO spectrumEvent) throws DevFailed {
        Object read = InsertExtractUtils.extract(deviceAttribute);
        if (deviceAttribute.getType() == TangoConst.Tango_DEV_ULONG64) {
            long[] longArrayRead = (long[]) read;
            BigInteger[] bigIntegers = new BigInteger[longArrayRead.length];
            for (int i = 0; i < longArrayRead.length; i++) {
                bigIntegers[i] = new BigInteger(Long.toUnsignedString(longArrayRead[i]));
            }
            spectrumEvent.setValue(bigIntegers, null);
        } else {
            spectrumEvent.setValue(read, null);
        }
    }

    public SpectrumEventRW mapSpectrumEventRW(TangoArchiveEvent event) {
        TangoArchive arch = (TangoArchive) event.getSource();
        SpectrumEventRW spectrumEvent = new SpectrumEventRW();
        try {
            DeviceProxy proxy = arch.getEventSupplier();
            DeviceAttribute deviceAttribute = event.getValue();
            spectrumEvent.setTimeStamp(deviceAttribute.getTime());
            spectrumEvent.setDataType(deviceAttribute.getType());
            setSpectrumReadWriteValue(deviceAttribute, spectrumEvent);
            spectrumEvent.setAttributeCompleteName(proxy.name() + SEPARATOR + deviceAttribute.getName());
            spectrumEvent.setWritable(AttrWriteType._READ);
            spectrumEvent.setDimX(deviceAttribute.getDimX());
            spectrumEvent.setQuality(deviceAttribute.getQuality().value());
        } catch (DevFailed e) {
            String err = DevFailedUtils.toString(e);
            LOGGER.error(err, e);
            if (e.errors.length > 0) {
                spectrumEvent.setErrorMessage(e.errors[0].desc);
            }
        }
        return spectrumEvent;
    }

    void setSpectrumReadWriteValue(DeviceAttribute deviceAttribute, SpectrumEventRW spectrumEvent) throws DevFailed {
        Object read = InsertExtractUtils.extract(deviceAttribute);
        Object write = InsertExtractUtils.extractWrite(deviceAttribute, AttrWriteType.READ_WRITE, AttrDataFormat.SPECTRUM);
        if (deviceAttribute.getType() == TangoConst.Tango_DEV_ULONG64) {
            long[] longArrayRead = (long[]) read;
            BigInteger[] bigIntegers = new BigInteger[longArrayRead.length];
            for (int i = 0; i < longArrayRead.length; i++) {
                bigIntegers[i] = new BigInteger(Long.toUnsignedString(longArrayRead[i]));
            }
            long[] longArrayWrite = (long[]) write;
            BigInteger[] bigIntegersWrite = new BigInteger[longArrayWrite.length];
            for (int i = 0; i < longArrayWrite.length; i++) {
                bigIntegersWrite[i] = new BigInteger(Long.toUnsignedString(longArrayWrite[i]));
            }
            Object value = ArrayUtils.addAll(bigIntegers, bigIntegersWrite);
            spectrumEvent.setValue(value, null);
        } else {
            Object value = ArrayUtils.addAll(read, write);
            spectrumEvent.setValue(value, null);
        }
    }
}
