package org.tango.archiving.collector.mode;

import fr.soleil.tango.archiving.config.InsertionModes;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.SpectrumEventRO;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevState;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.lib.project.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import java.util.HashMap;
import java.util.Map;

public class ModeHandler {
    private final Logger logger = LoggerFactory.getLogger(ModeHandler.class);
    private final InsertionModes mode;


    private final Map<String, ModeChronometers> chronometersMap = new HashMap<String, ModeChronometers>();

    private final Map<String, Double> relativeSlowDriftValueMap = new HashMap<String, Double>();
    private final Map<String, Double> absoluteSlowDriftValueMap = new HashMap<String, Double>();

    public ModeHandler(final InsertionModes archiverMode) {
        mode = archiverMode;
    }

    public void start(final String attributeName) {
        logger.debug("{} - start mode chronometers for modes {}", attributeName, mode.toSimpleString());
        ModeChronometers chronometers = new ModeChronometers();
        if (mode.isAbsolute())
            chronometers.startMode(ArchivingMode.ABSOLUTE, mode.getPeriodAbsolute());
        if (mode.isRelative())
            chronometers.startMode(ArchivingMode.RELATIVE, mode.getPeriodRelative());
        if (mode.isPeriodic())
            chronometers.startMode(ArchivingMode.PERIODIC, mode.getPeriodPeriodic());
        if (mode.isDifference())
            chronometers.startMode(ArchivingMode.DIFFERENCE, mode.getPeriodDifference());
        if (mode.isThreshold())
            chronometers.startMode(ArchivingMode.THRESHOLD, mode.getPeriodThreshold());
        chronometersMap.put(attributeName.toLowerCase(), chronometers);
    }

    public void stop(final String attributeName) {
        logger.debug("{} - stop mode chronometers for modes {}", attributeName, mode);
        chronometersMap.remove(attributeName.toLowerCase());
    }

    public void clear() {
        chronometersMap.clear();
    }

    public InsertionModes getMode() {
        return mode;
    }

    public boolean isDataArchivable(final String attributeName, final AttributeScalarEvent currentEvent,
                                    final AttributeScalarEvent previousEvent) throws DevFailed {
        Object previousValue;
        if (previousEvent == null) {
            previousValue = null;
        } else {
            previousValue = previousEvent.getReadValue();
        }
        Object currentValue = currentEvent.getReadValue();
        double currentEventD = -1;
        double previousEventD = -1;

        final boolean isCurrentNull = currentValue == null;
        final boolean isPreviousNull = previousValue == null;

        switch (currentEvent.getDataType()) {
            case TangoConst.Tango_DEV_STRING:
                // here, currentEvent and previousEvent are String values
                if (!ObjectUtils.sameObject(currentValue, previousValue)) {
                    currentEventD = previousEventD + 1;// so that the difference mode's archiving condition is triggered
                }
                break;
            case TangoConst.Tango_DEV_STATE:
                final DevState currentEventState = (DevState) currentValue;
                final DevState previousEventState = (DevState) previousValue;
                currentEventD = isCurrentNull ? -1 : currentEventState.value();
                previousEventD = isPreviousNull ? -1 : previousEventState.value();
                break;
            case TangoConst.Tango_DEV_LONG:
            case TangoConst.Tango_DEV_ULONG:
            case TangoConst.Tango_DEV_DOUBLE:
            case TangoConst.Tango_DEV_LONG64:
            case TangoConst.Tango_DEV_ULONG64:
            case TangoConst.Tango_DEV_FLOAT:
            case TangoConst.Tango_DEV_UCHAR:
            case TangoConst.Tango_DEV_SHORT:
            case TangoConst.Tango_DEV_USHORT:
                currentEventD = isCurrentNull ? -1 : ((Number) currentValue).doubleValue();
                previousEventD = isPreviousNull ? -1 : ((Number) previousValue).doubleValue();
                break;
            case TangoConst.Tango_DEV_BOOLEAN:
                currentEventD = isCurrentNull ? -1 : ((Boolean) currentValue).booleanValue() ? 1 : 0;
                previousEventD = isPreviousNull ? -1 : ((Boolean) previousValue).booleanValue() ? 1 : 0;
                break;
            default:
                if ((currentValue instanceof Number) && (previousValue instanceof Number)) {
                    currentEventD = isCurrentNull ? -1 : ((Number) currentValue).doubleValue();
                    previousEventD = isPreviousNull ? -1 : ((Number) previousValue).doubleValue();
                }
                if (currentValue instanceof String && previousValue instanceof String) {
                    String newCurrentEventString = currentValue == null ? "" : (String) currentValue;
                    String newPreviousEventString = previousValue == null ? "" : (String) previousValue;
                    if (!newCurrentEventString.equals(newPreviousEventString)) {
                        currentEventD = previousEventD + 1;// so that the difference
                        // mode's archiving
                        // condition is triggered
                    }
                }
                break;
        }

        boolean result = false;
        // logger.debug("Mode = {}", mode.toStringWatcher());
        // logger.debug("attModeCounter = {}", attModeCounter.toString());
        // All the modes must be tested at least to increment the various
        // counters
        String attributeNameLower = attributeName.toLowerCase();
        ModeChronometers chronometers = chronometersMap.get(attributeNameLower);
        if(chronometers == null){
            throw DevFailedUtils.newDevFailed("archiving mode chronometer is not started for "+ attributeName);
        }
        logger.debug("{} - testing if triggering for modes {}", attributeName, mode.toSimpleString());
        long previousTimestamp = 0;
        if (previousEvent != null) {
            previousTimestamp = previousEvent.getTimeStamp();
        }
        long currentTimestamp = System.currentTimeMillis();
        if (currentEvent != null) {
            currentTimestamp = currentEvent.getTimeStamp();
        }

        if (isDataArchivableModeP(chronometers, previousTimestamp, currentTimestamp)) {
            result = true;
        }
        if (isDataArchivableModeA(attributeNameLower, chronometers, isCurrentNull, isPreviousNull, currentEventD,
                previousEventD)) {
            result = true;
        }
        if (isDataArchivableModeR(attributeNameLower, chronometers, isCurrentNull, isPreviousNull, currentEventD,
                previousEventD)) {
            result = true;
        }
        if (isDataArchivableModeT(chronometers, isCurrentNull, currentEventD)) {
            result = true;
        }
        if (isDataArchivableModeD(chronometers, isCurrentNull, isPreviousNull, currentEventD, previousEventD)) {
            result = true;
        }
        logger.debug("{} trigger insertion = {}", attributeName, result);
        return result;
    }

    /**
     * Check done only for RO attributes and difference mode
     */
    public boolean isSpectrumDataArchivable(final String attributeName, final SpectrumEventRO currentEvent,
                                            final SpectrumEventRO previousEvent) {

        boolean result = true;

        ModeChronometers chronometers = chronometersMap.get(attributeName.toLowerCase());
        logger.debug("{} - testing if triggering for modes {}", attributeName, mode.toSimpleString());
        long previousTimestamp = 0;
        if (previousEvent != null) {
            previousTimestamp = previousEvent.getTimeStamp();
        }
        if (!isDataArchivableModeP(chronometers, previousTimestamp, currentEvent.getTimeStamp())) {
            result = false;
        }
        Object currentValue = currentEvent.getValue();
        Object previousValue = null;
        if (previousEvent != null) {
            previousValue = previousEvent.getValue();
        }
        if (mode.isDifference()) {
            if (chronometers.isOver(ArchivingMode.DIFFERENCE, mode.getPeriodDifference())) {
                if (currentValue == null && previousValue == null) {
                    result = false;
                } else if (currentValue == null || previousValue == null) {
                    logger.debug("trigger difference for value {}", currentEvent);
                    result = true;
                } else {
                    String currentEventValueAsString = currentEvent.getValueAsString();
                    String previousEventValueAsString = previousEvent.getValueAsString();
                    if (!currentEventValueAsString.equals(previousEventValueAsString)) {
                        result = true;
                        logger.debug("difference elapsed time is {} ms, will trigger archiving of spectrum {}",
                                chronometers.getElapsedTime(ArchivingMode.DIFFERENCE), currentEvent);
                    }
                }
                chronometers.startMode(ArchivingMode.DIFFERENCE, mode.getPeriodDifference());
            }
        }
        return result;
    }

    private boolean isDataArchivableModeP(final ModeChronometers chronometers, long previousTimeStamp, long currentTimeStamp) {
        boolean doArchive = false;

        if (mode.isPeriodic()) {
            long actualPeriod = currentTimeStamp - previousTimeStamp;
            long expectedPeriod = mode.getPeriodPeriodic();
            if (actualPeriod >= expectedPeriod || chronometers.isOver(ArchivingMode.PERIODIC, expectedPeriod)) {
                logger.debug("periodic elapsed time is {} ms with timestamps period of {} ms, will trigger archiving",
                        chronometers.getElapsedTime(ArchivingMode.PERIODIC), actualPeriod);
                doArchive = true;
                chronometers.startMode(ArchivingMode.PERIODIC, expectedPeriod);
            }
        }
        return doArchive;
    }

    private boolean isDataArchivableModeA(final String attributeName, final ModeChronometers chronometers,
                                          final boolean isCurrentNull, final boolean isPreviousNull, final double currentEvent,
                                          double previousEvent) {
        boolean doArchive = false;
        if (mode.isAbsolute()) {
            logger.debug("absolute elapsed time is {} ms", chronometers.getElapsedTime(ArchivingMode.ABSOLUTE));

            if (chronometers.isOver(ArchivingMode.ABSOLUTE, mode.getPeriodAbsolute())) {
                chronometers.startMode(ArchivingMode.ABSOLUTE, mode.getPeriodAbsolute());
                if (isCurrentNull && isPreviousNull) {
                    doArchive = false;
                } else if (isCurrentNull || isPreviousNull) {
                    logger.debug("trigger absolute insertion for value {}", currentEvent);
                    doArchive = true;
                }
                if (mode.isSlowDriftAbsolute()) {
                    if (absoluteSlowDriftValueMap.get(attributeName) == null) {
                        absoluteSlowDriftValueMap.put(attributeName, previousEvent);
                    } else {
                        previousEvent = absoluteSlowDriftValueMap.get(attributeName);
                    }
                }
                final double lowerLimit = previousEvent - Math.abs(mode.getDecreaseDeltaAbsolute());
                final double upperLimit = previousEvent + Math.abs(mode.getIncreaseDeltaAbsolute());
                logger.debug("checking absolute insertion currentEvent <= {} || currentEvent >= {}", lowerLimit, upperLimit);
                if (currentEvent <= lowerLimit || currentEvent >= upperLimit) {
                    if (mode.isSlowDriftAbsolute()) {
                        absoluteSlowDriftValueMap.put(attributeName, currentEvent);
                    }
                    logger.debug("trigger absolute insertion for value {}", currentEvent);
                    doArchive = true;
                }
            }
        }
        return doArchive;
    }

    private boolean isDataArchivableModeR(final String attributeName, final ModeChronometers chronometers,
                                          final boolean isCurrentNull, final boolean isPreviousNull, final double currentEvent,
                                          double previousEvent) {
        boolean doArchive = false;

        if (mode.isRelative()) {
            logger.debug("relative elapsed time is {} ms", chronometers.getElapsedTime(ArchivingMode.RELATIVE));
            if (chronometers.isOver(ArchivingMode.RELATIVE, mode.getPeriodRelative())) {
                logger.debug("previous value is = {}, current value is = {} ", previousEvent, currentEvent);
                chronometers.startMode(ArchivingMode.RELATIVE, mode.getPeriodRelative());

                if (isCurrentNull && isPreviousNull) {
                    doArchive = false;
                } else if (isCurrentNull || isPreviousNull) {
                    logger.debug("trigger relative insertion for value {}", currentEvent);
                    doArchive = true;
                }

                if (mode.isSlowDriftRelative()) {
                    if (relativeSlowDriftValueMap.get(attributeName) == null) {
                        relativeSlowDriftValueMap.put(attributeName, previousEvent);
                    } else {
                        previousEvent = relativeSlowDriftValueMap.get(attributeName);
                    }
                }
                double lower_limit = 0;
                double upper_limit = 0;

                final double percentInf = Math.abs(mode.getDecreasePercentRelative() / 100.0);
                final double percentSup = Math.abs(mode.getDecreasePercentRelative() / 100.0);
                logger.debug("percentInf =  {}, percentSup = {}", percentInf, percentSup);
                final double deltaInf = percentInf * Math.abs(previousEvent);
                final double deltaSup = percentSup * Math.abs(previousEvent);
                logger.debug("deltaInf =  {}, deltaSup = {}", deltaInf, deltaSup);
                lower_limit = previousEvent - deltaInf;
                upper_limit = previousEvent + deltaSup;
                logger.debug("relative insertion condition is currentEv < {} || currentEvent > {}", lower_limit,
                        upper_limit);
                if (currentEvent < lower_limit || currentEvent > upper_limit) {
                    if (mode.isSlowDriftRelative()) {
                        relativeSlowDriftValueMap.put(attributeName, currentEvent);
                        logger.debug("slow drift, currentEvent  = {},", currentEvent);
                    }
                    logger.debug("trigger relative insertion for value {}", currentEvent);
                    doArchive = true;
                } else {
                    doArchive = false;
                }
            }
        }

        return doArchive;
    }

    private boolean isDataArchivableModeT(final ModeChronometers chronometers, final boolean isCurrentNull,
                                          final double currentEvent) {
        boolean doArchive = false;
        if (mode.isThreshold()) {
            logger.debug("threshold elapsed time is {} ms", chronometers.getElapsedTime(ArchivingMode.THRESHOLD));
            if (chronometers.isOver(ArchivingMode.THRESHOLD, mode.getPeriodThreshold())) {
                chronometers.startMode(ArchivingMode.THRESHOLD, mode.getPeriodThreshold());
                if (isCurrentNull) {
                    doArchive = false;
                } else if (currentEvent <= mode.getMinThresholdValue()
                        || currentEvent >= mode.getMaxThresholdValue()) {
                    logger.debug("trigger threshold for value {}", currentEvent);
                    doArchive = true;
                }
            }
        }
        return doArchive;
    }

    private boolean isDataArchivableModeD(final ModeChronometers chronometers, final boolean isCurrentNull,
                                          final boolean isPreviousNull, final double currentEvent, final double previousEvent) {
        boolean doArchive = false;
        if (mode.isDifference()) {
            logger.debug("difference elapsed time is {} ms", chronometers.getElapsedTime(ArchivingMode.DIFFERENCE));

            if (chronometers.isOver(ArchivingMode.DIFFERENCE, mode.getPeriodDifference())) {
                chronometers.startMode(ArchivingMode.DIFFERENCE, mode.getPeriodDifference());
                if (isCurrentNull && isPreviousNull || (Double.isNaN(currentEvent) && Double.isNaN(previousEvent))) {
                    doArchive = false;
                } else if (isCurrentNull || isPreviousNull) {
                    logger.debug("trigger difference for value {}", currentEvent);
                    doArchive = true;
                }
                logger.debug("difference,  previous {}, current {}", previousEvent, currentEvent);
                if (currentEvent != previousEvent) {
                    logger.debug("trigger difference for value {}", currentEvent);
                    doArchive = true;
                }
            }
        }
        return doArchive;
    }

    @Override
    public String toString() {
        return "ModeHandler{" +
                "mode=" + mode +
                ", chronometersMap=" + chronometersMap +
                ", relativeSlowDriftValueMap=" + relativeSlowDriftValueMap +
                ", absoluteSlowDriftValueMap=" + absoluteSlowDriftValueMap +
                '}';
    }

}
