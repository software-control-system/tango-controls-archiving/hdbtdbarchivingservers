package org.tango.archiving.collector.infra;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.HDBDataBaseManager;
import fr.soleil.archiving.hdbtdb.api.TDBDataBaseManager;
import fr.soleil.archiving.hdbtdb.api.management.attributes.hdb.insert.IHdbAttributeInsert;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRefFactory;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.EventMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeAbsolu;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeDifference;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModePeriode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.ModeRelatif;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.data.ArchivingKPI;
import org.tango.archiving.collector.data.AttributeScalarEvent;
import org.tango.archiving.collector.data.ImageEventRO;
import org.tango.archiving.collector.data.SpectrumEventRO;
import org.tango.archiving.collector.data.SpectrumEventRW;

import java.util.Collection;

public class ArchivingDatabase implements IArchivingDatabase {

    private final DataBaseManager dataBaseManager;
    private final IArchivingManagerApiRef manager;
    private final Logger logger = LoggerFactory.getLogger(ArchivingDatabase.class);
    private IHdbAttributeInsert inserter;


    public ArchivingDatabase(DatabaseConnectionConfig parameters, boolean isHDB, boolean isDBInsertion)
            throws ArchivingException {
        DataBaseParameters config = new DataBaseParameters();
        config.setDbType(DataBaseParameters.DataBaseType.parseDataBaseType(parameters.getDbType().name()));
        config.setName(parameters.getName());
        config.setHost(parameters.getHost());
        config.setUser(parameters.getUser());
        config.setPassword(parameters.getPassword());
        config.setSchema(parameters.getSchema());
        //config.setConnectionTimeout(10000L);
        config.setInactivityTimeout(parameters.getIdleTimeout());
        config.setMaxPoolSize((short) parameters.getMaxPoolSize());
        if (parameters.getHealthRegistry() != null) {
            config.setHealthRegistry(parameters.getHealthRegistry());
        }
        if (parameters.getMetricRegistry() != null) {
            config.setMetricRegistry(parameters.getMetricRegistry());
        }
        // get databases metrics
        parameters.setMetricRegistry(ArchivingKPI.getMetricRegistry());
        manager = ArchivingManagerApiRefFactory.getInstance(isHDB, ConnectionFactory.connect(config));
        // connect do database
        manager.archivingConfigureWithoutArchiverListInit();
        dataBaseManager = manager.getDataBase();
        if (isDBInsertion) {
            inserter = ((HDBDataBaseManager) dataBaseManager).getHdbAttributeInsert();
        }
    }

    /* public boolean isDbConnected() {
        return dataBaseManager.isDbConnected();
    }*/

    @Override
    public DataBaseManager getDataBase() {
        return dataBaseManager;
    }

    @Override
    public void registerAttributeMode(final ArchivingConfig config) throws ArchivingException {
        logger.warn("registerAttributeMode {}", config.getAttributeConfig().getFullName());
        System.out.println("registerAttributeMode " + config.getAttributeConfig().getFullName());
        AttributeLightMode lightMode = new AttributeLightMode();
        lightMode.setAttributeCompleteName(config.getAttributeConfig().getFullName());
        lightMode.setDevice_in_charge(config.getArchiver());
        lightMode.setWritable(config.getAttributeConfig().getWriteType());
        lightMode.setDataFormat(config.getAttributeConfig().getFormat());
        Mode mode = new Mode();
        InsertionModes modes = config.getModes();
        if (modes.isPeriodic()) {
            ModePeriode p = new ModePeriode();
            p.setPeriod(modes.getPeriodPeriodic());
            mode.setModeP(p);
        }
        if (modes.isAbsolute()) {
            ModeAbsolu m = new ModeAbsolu();
            m.setPeriod(modes.getPeriodAbsolute());
            m.setSlow_drift(modes.isSlowDriftAbsolute());
            m.setValInf(modes.getDecreaseDeltaAbsolute());
            m.setValSup(modes.getIncreaseDeltaAbsolute());
            mode.setModeA(m);
        }
        if (modes.isDifference()) {
            ModeDifference d = new ModeDifference();
            d.setPeriod(modes.getPeriodDifference());
            mode.setModeD(d);
        }
        if (modes.isRelative()) {
            ModeRelatif r = new ModeRelatif();
            r.setPeriod(modes.getPeriodRelative());
            r.setSlow_drift(modes.isSlowDriftRelative());
            r.setPercentInf(modes.getDecreasePercentRelative());
            r.setPercentSup(modes.getIncreasePercentRelative());
            mode.setModeR(r);
        }
        if (modes.isEvent()) {
            mode.setEventMode(new EventMode());
        }
        lightMode.setMode(mode);
        logger.debug("registerAttributeMode lightMode = {}", lightMode);
        dataBaseManager.getMode().insertModeRecord(lightMode);
    }

    @Override
    public void stopAttributeMode(final String attributeName) throws ArchivingException {
        // stop current archiving (set stop_date = now)
        dataBaseManager.getMode().updateModeRecord(attributeName);
    }

    @Override
    public boolean isArchived(final String attributeName, final String deviceName) throws ArchivingException {
        return dataBaseManager.getMode().isArchived(attributeName, deviceName);
    }

    @Override
    public void exportToDBScalar(final String remoteFilePath, final String fileName, final String tableName,
                                 final int writable) throws ArchivingException {
        if (dataBaseManager instanceof TDBDataBaseManager) {
            ((TDBDataBaseManager) dataBaseManager).getTdbExport().exportToDB_Data(remoteFilePath, fileName, tableName,
                    writable);
        }
    }

    @Override
    public void exportToDBSpectrum(final String remoteFilePath, final String fileName, final String tableName,
                                   final int writable) throws ArchivingException {
        if (dataBaseManager instanceof TDBDataBaseManager) {
            ((TDBDataBaseManager) dataBaseManager).getTdbExport().exportToDB_Data(remoteFilePath, fileName, tableName,
                    writable);
        }
    }

    @Override
    public void exportToDBImage(final String remoteFilePath, final String fileName, final String tableName,
                                final int writable) throws ArchivingException {
        if (dataBaseManager instanceof TDBDataBaseManager) {
            ((TDBDataBaseManager) dataBaseManager).getTdbExport().exportToDB_Data(remoteFilePath, fileName, tableName,
                    writable);
        }
    }

    @Override
    public ArchivingConfigs getArchiverCurrentTasks(final String archiverName) throws ArchivingException {

        ArchivingConfigs archiverCurrentTasks = new ArchivingConfigs();
        final boolean facility = manager.getFacility();

        final Collection<AttributeLightMode> legacyTasks;
        try {
            legacyTasks = dataBaseManager.getMode().getArchiverCurrentTasks(
                    (facility ? "//" + ApiUtil.get_db_obj().get_tango_host() + "/" : "") + archiverName);
        } catch (DevFailed e) {
            throw new ArchivingException(e);
        }
        for (AttributeLightMode mode : legacyTasks) {
            logger.debug("AttributeLightMode {}", mode);
            ArchivingConfig config = new ArchivingConfig();
            AttributeConfig attribute = new AttributeConfig();
            attribute.setFullName(mode.getAttributeCompleteName());
            attribute.setFormat(mode.getDataFormat());
            attribute.setWriteType(mode.getWritable());
            config.setAttributeConfig(attribute);
            InsertionModes modes = new InsertionModes();
            modes.setAttributeName(mode.getAttributeCompleteName());
            if (mode.getMode().getModeP() != null) {
                modes.setPeriodic(true);
                modes.setPeriodPeriodic(mode.getMode().getModeP().getPeriod());
            }
            if (mode.getMode().getModeA() != null) {
                modes.setAbsolute(true);
                modes.setPeriodAbsolute(mode.getMode().getModeA().getPeriod());
                modes.setSlowDriftAbsolute(mode.getMode().getModeA().isSlow_drift());
                modes.setDecreaseDeltaAbsolute(mode.getMode().getModeA().getValInf());
                modes.setIncreaseDeltaAbsolute(mode.getMode().getModeA().getValSup());
            }
            if (mode.getMode().getModeD() != null) {
                modes.setDifference(true);
                modes.setPeriodDifference(mode.getMode().getModeD().getPeriod());
            }
            if (mode.getMode().getModeR() != null) {
                modes.setRelative(true);
                modes.setPeriodRelative(mode.getMode().getModeR().getPeriod());
                modes.setSlowDriftRelative(mode.getMode().getModeR().isSlow_drift());
                modes.setDecreasePercentRelative(mode.getMode().getModeR().getPercentInf());
                modes.setIncreasePercentRelative(mode.getMode().getModeR().getPercentSup());
            }
            if (mode.getMode().getEventMode() != null) {
                modes.setEvent(true);
            }
            config.setModes(modes);
            logger.debug("getArchiverCurrentTasks {}", config);
            archiverCurrentTasks.addConfiguration(config);
        }
        return archiverCurrentTasks;
    }

    @Override
    public void store(final AttributeScalarEvent scalarEvent) throws ArchivingException {
        inserter.insert_ScalarData(scalarEvent.toAPIEvent());
    }

    @Override
    public void store(final SpectrumEventRO spectrumEventRO) throws ArchivingException {
        inserter.insert_SpectrumData_RO(spectrumEventRO.toAPIEvent());
    }

    @Override
    public void store(final SpectrumEventRW spectrumEventRW) throws ArchivingException {
        inserter.insert_SpectrumData_RW(spectrumEventRW.toAPIEvent());
    }

    @Override
    public void store(final ImageEventRO imageEventRO) {
        // Not supported
        // inserter.insert_ImageData_RO(imageEventRO.toAPIEvent());
    }

    @Override
    public void closeConnection() {
        inserter.closeConnection();
    }
}
