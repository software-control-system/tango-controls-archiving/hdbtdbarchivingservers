package org.tango.archiving.collector.data;

public abstract class ASpectrumEvent extends AAttributeEvent<boolean[]> {
	protected int dimX;
	protected final int dimY = 0;

	public int getDimX() {
		return dimX;
	}

	public void setDimX(int dim_x) {
		this.dimX = dim_x;
	}

	public int getDimY() {
		return dimY;
	}
}
