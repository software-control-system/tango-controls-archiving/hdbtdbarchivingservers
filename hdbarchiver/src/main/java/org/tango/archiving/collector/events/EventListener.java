package org.tango.archiving.collector.events;

import org.tango.archiving.collector.data.TangoDataPersister;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.events.ITangoArchiveListener;
import fr.esrf.TangoApi.events.TangoArchive;
import fr.esrf.TangoApi.events.TangoArchiveEvent;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.utils.DevFailedUtils;

import java.io.IOException;

public class EventListener implements ITangoArchiveListener {
    private final TangoEventToHdbPersiterWrapper tangoEventToHdbPersiter;
    private final TangoDataPersister tangoPersister;
    private final int writeType;
    private final Logger logger = LoggerFactory.getLogger(EventListener.class);

    private final ArchivingErrorCollector errorCollector;


    public EventListener(ArchivingInfra archivingInfra, ArchivingErrorCollector errorCollector, int writeType) {
        tangoPersister = new TangoDataPersister(errorCollector, archivingInfra, false, null);
        tangoEventToHdbPersiter = new TangoEventToHdbPersiterWrapper(tangoPersister);
        this.writeType = writeType;
        this.errorCollector = errorCollector;
    }

    @Override
    public void archive(TangoArchiveEvent tangoArchiveEvent) {
        if (logger.isDebugEnabled()) {
            TangoArchive source = (TangoArchive) tangoArchiveEvent.getSource();
            //TODO: for devenum attributes, set enums labels
            try {
                logger.debug("event received for {}/{}", source.getEventSupplier().get_name(), tangoArchiveEvent.getValue().getName());
            } catch (DevFailed devFailed) {
                logger.warn("event received for {} with error {}", source.getEventSupplier().get_name(), DevFailedUtils.toString(devFailed));
            }
        }
        tangoEventToHdbPersiter.persist(tangoArchiveEvent, writeType, errorCollector);
    }

    public void stop(String attributeName, boolean closeFile) throws ArchivingException {
        try {
            tangoPersister.removeAttribute(attributeName, closeFile);
        } catch (IOException e) {
            throw new ArchivingException(e);
        }
    }
}
