package org.tango.archiving.collector;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import org.tango.archiving.collector.error.ArchivingErrorCollector;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.archiving.collector.mode.ModeHandler;
import org.tango.archiving.collector.polling.DefaultImageROPolling;
import org.tango.archiving.collector.polling.DefaultScalarPooling;
import org.tango.archiving.collector.polling.DefaultSpectrumROPolling;
import org.tango.archiving.collector.polling.DefaultSpectrumRWPolling;

public class CollectorBuilder {

//    private static final Logger LOGGER = LoggerFactory.getLogger(CollectorBuilder.class);

    /**
     * This method returns the instance of HdbCollector associated / associable
     * to an attribute. In this method, attributes are grouped by mode.
     *
     * @param attributeLightMode Attribute associated to the looked collector.
     * @return the instance of HdbCollector associated / associable to an
     * attribute.
     */

    public ITangoCollector build(final ArchivingConfig attributeLightMode, ArchivingInfra db,
                                 ArchivingErrorCollector errorCollector) throws ArchivingException {
        final String name = attributeLightMode.getAttributeConfig().getFullName();
        final AttrDataFormat dataFormat = AttrDataFormat.from_int(attributeLightMode.getAttributeConfig().getFormat());
        final AttrWriteType writable = AttrWriteType.from_int(attributeLightMode.getAttributeConfig().getWriteType());
        final ModeHandler modeHandler = new ModeHandler(attributeLightMode.getModes());
        ITangoCollector collector = null;
        if (dataFormat.equals(AttrDataFormat.SCALAR)) {
            collector = new DefaultScalarPooling(modeHandler, db, errorCollector);
        } else if (dataFormat.equals(AttrDataFormat.SPECTRUM)) {
            if (writable.equals(AttrWriteType.READ_WRITE)) {
                collector = new DefaultSpectrumRWPolling(modeHandler, db, errorCollector);
            } else if (writable.equals(AttrWriteType.READ) || writable.equals(AttrWriteType.WRITE)) {
                collector = new DefaultSpectrumROPolling(modeHandler, db, errorCollector);
            }
        } else if (dataFormat.equals(AttrDataFormat.IMAGE)) {
            collector = new DefaultImageROPolling(modeHandler, db, errorCollector);
        } else {
            throw new ArchivingException("Cannot start attribute " + name + ", data format " + dataFormat + " not supported");
        }
        return collector;
    }

}
