package org.tango.archiving.server.archiver;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.collector.TangoCollectorService;
import org.tango.archiving.collector.infra.ArchivingInfra;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.TransactionType;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;


@Device(transactionType = TransactionType.NONE)
public class TdbArchiver extends HdbArchiver {

    public static final int DEFAULT_EXPORT_PERIOD = 1800000;

    private Logger logger = LoggerFactory.getLogger(TdbArchiver.class);
    /**
     * Path to the working directory. <br>
     * This directory will be used to temporarily store data. <br>
     * Data will finally be stored into the database. <br>
     * <b>Default value : </b> C:\tango\TdbArchiver
     */
    @DeviceProperty
    private String dsPath;
    /**
     * Path used by the database service to access temporary data through the
     * network.<br>
     * <p/>
     * <b>Default value : </b> C:\tango\TdbArchiver
     */
    @DeviceProperty
    private String dbPath;
    /**
     * Number of lines per file. <b>Default value :100</b>
     */
    @DeviceProperty(defaultValue = "100")
    private int attributePerFile = 100;
    /**
     * Period at which to insert temporary files in DB in ms. Default value :1800000
     */
    @DeviceProperty(defaultValue = "1800000")
    private long exportPeriod = DEFAULT_EXPORT_PERIOD;

    /**
     * Force export when device start . <br>
     * <b>Default value : </b> C:\tango\TdbArchiver
     */
    @DeviceProperty(defaultValue = "false")
    private boolean forceExportOnStart = false;

    public static void main(final String[] args) {
        VERSION = ResourceBundle.getBundle("application").getString("project.version");
        ServerManager.getInstance().start(args, TdbArchiver.class);
    }

    protected TangoCollectorService buildDbProxy(DatabaseConnectionConfig params, boolean insertErrors) throws ArchivingException {
        ArchivingInfra timeseriesInfra = new ArchivingInfra(params, false, !isUsingTemporaryFiles(), insertErrors);
        TangoCollectorService tangoCollectorServiceBuild = new TangoCollectorService(timeseriesInfra, deviceName, forceUseEvents, !isUsingTemporaryFiles(), minimumInsertionPeriod);
        tangoCollectorServiceBuild.setTemporaryFileConfiguration(exportPeriod, dbPath, dsPath, attributePerFile, forceExportOnStart);
        return tangoCollectorServiceBuild;
    }

    @Override
    protected boolean isUsingTemporaryFiles() {
        return true;
    }

    /**
     * Execute command "ExportData2Db" on device. This command need an
     * AttributeLightMode type object. An AttributeLightMode type object
     * encapsulate all the informations needed to found the Collector in charge
     * of the archiving of the specified attribute The informations needed are
     * the type, the format, the writable property and the archiving mode
     *
     * @param argin the attribute from witch data are expected.
     */
    @Command(inTypeDesc = "String[] format : [String attributeName, int  dataType, int dataFormat, int writable, String deviceInCharge, String triggerTime, int modeArraySize, ...]")
    public String exportData2Db(final String[] argin) throws DevFailed {
        return tangoCollectorService.exportDataToDb(argin[0]);
    }

    @Command(name = "GetOpenFiles", outTypeDesc = "String[] attributeName : fullPathFileName")
    public String[] getOpenFiles() {
        List<String> result = new ArrayList<>();
        for (String attributesName : getOkAttributes()) {
            result.add(attributesName + " : "
                    + StringUtils.defaultIfEmpty(tangoCollectorService.getFilePathForAttribute(attributesName), "KO"));
        }
        for (String attributesName : getKoAttributes()) {
            result.add(attributesName + " : KO");
        }
        return result.toArray(new String[0]);
    }

    public String getDsPath() {
        return dsPath;
    }

    public void setDsPath(String dsPath) {
        this.dsPath = dsPath;
    }

    public String getDbPath() {
        return dbPath;
    }

    public void setDbPath(String dbPath) {
        this.dbPath = dbPath;
    }

    public int getAttributePerFile() {
        return attributePerFile;
    }

    public void setAttributePerFile(int attributePerFile) {
        this.attributePerFile = attributePerFile;
    }

    public void setExportPeriod(long exportPeriod) {
        this.exportPeriod = exportPeriod;
    }

    public void setForceExportOnStart(boolean forceExportOnStart) {
        this.forceExportOnStart = forceExportOnStart;
    }


}
