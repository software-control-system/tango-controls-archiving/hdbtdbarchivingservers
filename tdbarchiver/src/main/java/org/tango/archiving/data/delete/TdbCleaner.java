package org.tango.archiving.data.delete;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.GetArchivingConf;
import fr.soleil.archiving.hdbtdb.api.TDBDataBaseManager;
import fr.soleil.archiving.hdbtdb.api.management.modes.TdbMode;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRefFactory;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.database.connection.DataBaseParameters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import java.util.StringTokenizer;

public class TdbCleaner {
	 final static Logger logger = LoggerFactory.getLogger(TdbCleaner.class);
    private static final short FAIL = 1;
    private static final short SUCCESS = 2;
    // private int keepingPeriod;
    private static final short NOTHING_TO_DO = 3;
    private static final short MINUTES = 1;
    private static final short HOURS = 2;
    private static final short DAYS = 3;
    private final String tdbUser;
    private final String tdbPwd;
    private DataBaseManager dataBaseApi;
    private IArchivingManagerApiRef manager;

    public TdbCleaner(String[] args) {
        this.tdbUser = args[0];
        this.tdbPwd = args[1];
        // this.keepingPeriod = Integer.parseInt(args [ 2 ]);
    }

    /**
     * @param args
     * @throws ArchivingException
     */
    public static void main(String[] args) throws ArchivingException {
        if (args == null || args.length != 2) {
            end(FAIL, "Bad arguments. Usage: java TdbCleaner tdbUser tdbPwd");// keepingPeriod"
            // );
        }

        TdbCleaner tdbCleaner = new TdbCleaner(args);
        tdbCleaner.verify();
        tdbCleaner.process();
    }

    private static void end(int code, String message) {
        logger.info(message);
        switch (code) {
            case FAIL:
                System.exit(1);
                break;

            case SUCCESS:
            case NOTHING_TO_DO:
                System.exit(0);
                break;

            default:
            	logger.error("Incorrect exit code");
                System.exit(1);
        }
    }

    private void verify() {
        try {
            DataBaseParameters parameters = new DataBaseParameters();
            parameters.setParametersFromTango(ConfigConst.TDB_CLASS_DEVICE);
            parameters.setUser(tdbUser);
            parameters.setPassword(tdbPwd);
            this.manager = ArchivingManagerApiRefFactory
                    .getInstance(false, ConnectionFactory.connect(parameters));
            this.manager.archivingConfigureWithoutArchiverListInit();
            this.dataBaseApi = this.manager.getDataBase();
        } catch (ArchivingException e) {
            e.printStackTrace();
            end(FAIL, "TdbCleaner/verify/can't connect to the TDB database");
        } catch (DevFailed e) {
            logger.error(DevFailedUtils.toString(e));
            e.printStackTrace();
            end(FAIL, "TdbCleaner/verify/can't connect to the TDB database");
        }
    }

    private void process() {
        String[] attributeList = null;
        try {
            attributeList = ((TdbMode) this.dataBaseApi.getMode()).getArchivedAtt();
        } catch (Exception e) {
            e.printStackTrace();
            end(FAIL, "TdbCleaner/process/can't load the list of attributes");
        }

        if (attributeList == null || attributeList.length == 0) {
            end(NOTHING_TO_DO, "TdbCleaner/process/no attributes to clean");
        }

        try {
            long time = computeRetentionPeriod();
            logger.debug("Retention = {} ms", time);
            ((TDBDataBaseManager) this.dataBaseApi).getTdbDeleteAttribute()
                    .deleteOldRecords(time, attributeList);
        } catch (Exception e) {
            e.printStackTrace();
            end(FAIL, "TdbCleaner/process/can't delete the old records");
        }

    }

    /**
     * Compute the retention period : the default value is 3 days.
     *
     * @return the retention period
     */
    protected long computeRetentionPeriod() {
        short timeUnity = DAYS;
        int timePeriod = 3;
        boolean useDefault = false;

        try {
            // Read the retention period property
            String retentionValue = GetArchivingConf
                    .readStringInDB(ConfigConst.TDB_CLASS_DEVICE, "RetentionPeriod");

            // Parse it
            StringTokenizer st = new StringTokenizer(retentionValue, "/");
            String type_s = st.nextToken();

            if (type_s != "") {
                if ("hours".equals(type_s.trim())) {
                    timeUnity = HOURS;
                } else if ("minutes".equals(type_s.trim())) {
                    timeUnity = MINUTES;
                } else
                    timeUnity = DAYS;

                // time extraction
                timePeriod = Integer.parseInt(st.nextToken());
                if (timePeriod <= 0) {
                	logger.error("Invalid period : default value is used (3 days)");
                    useDefault = true;
                }
            }
        } catch (ArchivingException e) {
        	logger.error("Properties is not used due to exception : default value is used (3 days)");
            // Nothing to do use default value
            useDefault = true;
        } catch (Exception e) {
        	logger.error("Exception raised " + e + " default value is used (3 days)");
            useDefault = true;
        }

        if (useDefault) {
            timeUnity = DAYS;
            timePeriod = 3;
        }

        switch (timeUnity) {
            case MINUTES:
                return (long) (timePeriod) * (long) (60 * 1000);
            case HOURS:
                return (long) (timePeriod) * (long) (3600 * 1000);
            default:
                return (long) (timePeriod) * (long) (24 * 3600 * 1000);
        }
    }

	public String getTdbUser() {
		return tdbUser;
	}

	public String getTdbPwd() {
		return tdbPwd;
	}
}

