package org.tango.archiving.server.extractor;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.attribute.ISetValueUpdater;

public class DataAttribute implements IAttributeBehavior, ISetValueUpdater {

    private final AttributeConfiguration configuration = new AttributeConfiguration();
    private final AttributeValue readValue;
    private final AttributeValue writeValue;

    public DataAttribute(String name, int dataType, AttrDataFormat dataFormat, int writeType, AttributeValue readValue, AttributeValue writeValue) throws DevFailed {
        configuration.setName(name);
        configuration.setTangoType(dataType, dataFormat);
        configuration.setWritable(AttrWriteType.from_int(writeType));
        configuration.setPolled(true);
        configuration.setPollingPeriod(0);
        this.readValue = readValue;
        this.writeValue = writeValue;
    }

    @Override
    public AttributeConfiguration getConfiguration() throws DevFailed {
        return configuration;
    }

    @Override
    public AttributeValue getValue() throws DevFailed {
        AttributeValue result;
        if (readValue == null) {
            result = writeValue;
        } else {
            result = readValue;
        }
        return result;
    }

    @Override
    public void setValue(AttributeValue attributeValue) throws DevFailed {

    }

    @Override
    public StateMachineBehavior getStateMachine() throws DevFailed {
        return null;
    }

    @Override
    public AttributeValue getSetValue() throws DevFailed {
        return writeValue;
    }
}
