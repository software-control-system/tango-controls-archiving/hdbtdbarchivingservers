package org.tango.archiving.server.extractor;

import java.util.StringTokenizer;
import java.util.concurrent.atomic.AtomicInteger;

import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarDoubleStringArray;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.database.connection.DataBaseParameters;

public interface IArchivingAccess {

    /**
     * counter to create dynamic attributes
     */
    AtomicInteger attributeID = new AtomicInteger();
    Logger LOGGER = LoggerFactory.getLogger(IArchivingAccess.class);

    void connectDatabase(final DatabaseConnectionConfig params) throws DevFailed;

    String getInfo();

    String getHost();

    String getUser();

    String[] getAttDefinitionData(final String argin) throws DevFailed;

    String[] getAttPropertiesData(final String argin) throws DevFailed;

    String getAttFullName(final int argin) throws DevFailed;

    int getAttId(final String argin) throws DevFailed;

    String[] getAttNameAll() throws DevFailed;

    String[] getAttNameFilterFormat(final short argin) throws DevFailed;

    String[] getAttNameFilterType(final short argin) throws DevFailed;

    int getAttCountAll() throws DevFailed;

    int getAttCountFilterFormat(final short argin) throws DevFailed;

    int getAttCountFilterType(final short argin) throws DevFailed;

    String[] getDomains() throws DevFailed;

    int getDomainsCount() throws DevFailed;

    String[] getFamilies() throws DevFailed;

    int getFamiliesCount() throws DevFailed;

    String[] getFamiliesByDomain(final String argin) throws DevFailed;

    int getFamiliesByDomainCount(final String argin) throws DevFailed;

    String[] getMembers() throws DevFailed;

    int getMembersCount() throws DevFailed;

    String[] getMembersByDomainFamily(final String... argin) throws DevFailed;

    int GetMembersByDomainFamilyCount(final String... argin) throws DevFailed;

    int getAttributesByDomainFamilyMembersCount(final String... argin) throws DevFailed;

    String[] getCurrentArchivedAtt() throws DevFailed;

    boolean isArchived(final String argin) throws DevFailed;

    String[] getArchivingMode(final String argin) throws DevFailed;

    int getAttDataCount(final String argin) throws DevFailed;

    double getAttDataAvg(final String argin) throws DevFailed;

    String getNewestValue(final String attributeName) throws DevFailed;

    default DevFailed buildNoDataDevFailed(String attributeName) {
        return DevFailedUtils.newDevFailed("no data found for " + attributeName);
    }

    // DbData[] getAttDataBetweenDates(final String... argin) throws DevFailed;

    String getNearestValue(final String... argin) throws DevFailed;

    // DbData[] getAttDataBetweenDatesSampling(final String... argin) throws DevFailed;

    double getAttDataAvgBetweenDates(final String... argin) throws DevFailed;

    // DbData[] getAttDataInfOrSupThan(final String... argin) throws DevFailed;

    DevVarDoubleStringArray extractBetweenDates(final boolean withErrors, final String... argin) throws DevFailed;

    // DbData[] getAttDataInfOrSupThanBetweenDates(final String... argin) throws DevFailed;

    int getAttDataBetweenDatesCount(final String... argin) throws DevFailed;

    // DbData[] getAttDataInfThan(final String... argin) throws DevFailed;

    int getAttDataInfOrSupThanCount(final String... argin) throws DevFailed;

    // DbData[] getAttDataInfThanBetweenDates(final String... argin) throws DevFailed;

    int getAttDataInfOrSupThanBetweenDatesCount(final String... argin) throws DevFailed;

    // DbData[] getAttDataLastN(final String... argin) throws DevFailed;

    int getAttDataInfThanCount(final String... argin) throws DevFailed;

    int getAttDataInfThanBetweenDatesCount(final String... argin) throws DevFailed;

    double getAttDataMax(final String argin) throws DevFailed;

    double getAttDataMaxBetweenDates(final String... argin) throws DevFailed;

    // DbData[] getAttDataSupThan(final String... argin) throws DevFailed;

    double getAttDataMin(final String argin) throws DevFailed;

    // DbData[] getAttDataSupAndInfThan(final String... argin) throws DevFailed;

    double getAttDataMinBetweenDates(final String... argin) throws DevFailed;

    // DbData[] getAttDataSupAndInfThanBetweenDates(final String... argin) throws DevFailed;

    int getAttDataSupThanCount(final String... argin) throws DevFailed;

    // DbData[] getAttDataSupThanBetweenDates(final String... argin) throws DevFailed;

    int getAttDataSupAndInfThanCount(final String... argin) throws DevFailed;

    int getAttDataSupAndInfThanBetweenDatesCount(final String... argin) throws DevFailed;

    int getAttDataSupThanBetweenDatesCount(final String... argin) throws DevFailed;

    String getMaxTime(final String argin) throws DevFailed;

    String getMinTime(final String argin) throws DevFailed;

    DevVarLongStringArray getAttDataBetweenDates(final boolean withErrors, String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataBetweenDatesSampling(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataInfOrSupThan(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataInfOrSupThanBetweenDates(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataInfThan(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataInfThanBetweenDates(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataLastN(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataSupThan(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataSupAndInfThan(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataSupAndInfThanBetweenDates(String... argin) throws DevFailed;

    DevVarLongStringArray getAttDataSupThanBetweenDates(String... argin) throws DevFailed;

    default void exportData(String attributeName) throws DevFailed {
    }

    default DevVarLongStringArray addAttribute(final DeviceManager deviceManager, final DynamicManager dynamicManager,
            final DbData... dbData) throws DevFailed {
        DevVarLongStringArray argout;
        if ((dbData == null) || (dbData.length < 2) || ((dbData[0] == null) && (dbData[1] == null))) {
            argout = getNoDataAttribute();
        } else {
            argout = null;
            for (DbData data : dbData) {
                if ((data != null) && ((data.getTimedData() == null) || (data.getTimedData().length == 0))) {
                    argout = getNoDataAttribute();
                    break;
                }
            }
            if (argout == null) {
                argout = new DevVarLongStringArray();
                DbData readData = dbData[0], writeData = dbData[1], refData = readData == null ? writeData : readData;

                // Build new Attribute's name
                String attributeName = getName(refData.getName(), attributeID.getAndIncrement());

                int dataType;
                dataType = refData.getDataType();
                if (dataType == TangoConst.Tango_DEV_STATE) {
                    // The STATE type attributes are translated in STRING TYPE because
                    // the State type is not well
                    // managed in Tango layers
                    dataType = TangoConst.Tango_DEV_STRING;
                } else if (dataType == TangoConst.Tango_DEV_USHORT || dataType == TangoConst.Tango_DEV_UCHAR) {
                    // U_* types are not supported by polling buffer, so use another
                    // supported type
                    dataType = TangoConst.Tango_DEV_SHORT;
                } else if (dataType == TangoConst.Tango_DEV_ULONG) {
                    dataType = TangoConst.Tango_DEV_LONG;
                } else if (dataType == TangoConst.Tango_DEV_ULONG64) {
                    dataType = TangoConst.Tango_DEV_LONG64;
                }
                final int writeType = refData.getWritable();

                NullableTimedData[] readValues;
                AttributeValue lastReadValue = null;
                org.tango.server.attribute.AttributeValue[] readHistory = null;
                if (readData != null) {
                    readValues = readData.getTimedData();
                    readHistory = new org.tango.server.attribute.AttributeValue[readValues.length];
                    for (int i = 0; i < readValues.length; i++) {
                        readHistory[i] = new org.tango.server.attribute.AttributeValue();
                        readHistory[i].setValue(readValues[i].getValue(), readValues[i].getTime());
                    }
                    lastReadValue = readHistory[readHistory.length - 1];
                }
                NullableTimedData[] writeValues;
                AttributeValue lastWriteValue = null;
                org.tango.server.attribute.AttributeValue[] writeHistory = null;
                if (writeData != null) {
                    writeValues = writeData.getTimedData();
                    writeHistory = new org.tango.server.attribute.AttributeValue[writeValues.length];
                    for (int i = 0; i < writeValues.length; i++) {
                        writeHistory[i] = new org.tango.server.attribute.AttributeValue();
                        writeHistory[i].setValue(writeValues[i].getValue(), writeValues[i].getTime());
                    }
                    lastWriteValue = writeHistory[writeValues.length - 1];
                }
                LOGGER.info("adding attribute {}", attributeName);
                dynamicManager.addAttribute(new DataAttribute(attributeName, dataType,
                        AttrDataFormat.from_int(refData.getDataFormat()), writeType, lastReadValue, lastWriteValue));
                deviceManager.startPolling(attributeName);
                deviceManager.triggerPolling(attributeName);
                LOGGER.info("filling data history of attribute {}", attributeName);
                deviceManager.fillAttributeHistory(attributeName, readHistory, writeHistory, null);

                argout.lvalue = new int[] { refData.getTimedData().length };
                argout.svalue = new String[] { attributeName };
            }
        }
        return argout;
    }

    default DevVarLongStringArray getNoDataAttribute() {
        final int[] lv = { 0 };
        final String[] sv = { "NO_DATA" };
        final DevVarLongStringArray res = new DevVarLongStringArray(lv, sv);
        return res;
    }

    default String getName(final String completeName, final int id) {
        final String partialName = getPartialName(completeName);
        final StringBuilder buff = new StringBuilder();
        buff.append(partialName);
        buff.append("_");
        buff.append(String.valueOf(id));
        return buff.toString().toLowerCase();
    }

    /**
     * @param completeName
     * @return
     */
    default String getPartialName(final String completeName) {
        try {
            final StringTokenizer st = new StringTokenizer(completeName, "/");
            st.nextToken();// domain
            st.nextToken();// family
            st.nextToken();// member
            return st.nextToken();// attribute
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
