package org.tango.archiving.server.extractor;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarDoubleStringArray;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.Tango.DispLevel;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import mx4j.log.Logger;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceManagement;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.DynamicManagement;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;

import java.lang.reflect.Array;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

@Device(transactionType = TransactionType.NONE)
public class HdbExtractor {

    //    private static final Logger LOGGER = LoggerFactory.getLogger(HdbExtractor.class);
    private static final String VERSION = ResourceBundle.getBundle("application").getString("project.version");
    @DeviceProperty
    protected String dbHost = "";
    @DeviceProperty
    protected String dbName = "";
    @DeviceProperty
    protected String dbSchema = "";
    @DeviceProperty
    protected String dbUser = "";
    @DeviceProperty
    protected String dbPassword = "";
    @DeviceProperty
    protected String dbType = "";
    @DeviceProperty
    protected String dbPort = "";
    @DeviceManagement
    protected DeviceManager deviceManager;
    @DynamicManagement
    protected DynamicManager dynamicManager;
    protected IArchivingAccess databaseAccess;
    @State
    private DeviceState state;
    @Status
    private String status;
    @Attribute(displayLevel = DispLevel._EXPERT)
    private boolean extractErrorMessages = false;
    /**
     * counter to create dynamic attributes
     */
    private AtomicInteger attributeID = new AtomicInteger();

    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, HdbExtractor.class);
    }

    public void setDbType(final String dbType) {
        this.dbType = dbType;
    }

    public void setDbPort(final String dbPort) {
        this.dbPort = dbPort;
    }

    public boolean isExtractErrorMessages() {
        return extractErrorMessages;
    }

    public void setExtractErrorMessages(boolean extractErrorMessages) {
        this.extractErrorMessages = extractErrorMessages;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public void setDbSchema(final String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public DeviceState getState() {
        return state;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void setDeviceManager(DeviceManager deviceManager) {
        this.deviceManager = deviceManager;
    }

    public void setDynamicManager(DynamicManager dynamicManager) {
        this.dynamicManager = dynamicManager;
    }

    @Init
    public void init() throws DevFailed {
        connectDatabase();
        state = DeviceState.ON;
        status = "ready";
    }

    protected void connectDatabase(String archiverClassName, boolean isHdb) throws DevFailed{
        // configure class properties names to retrieve configuration
        final DatabaseConnectionConfig params = new DatabaseConnectionConfig();
        DatabaseConnectionConfig.TangoDbProperties tangoDbProperties =
                new DatabaseConnectionConfig.TangoDbProperties("dbType", "dbHost", "dbName",
                        "dbSchema", "dbUser", "dbPassword",
                        "dbMinPoolSize", "dbMaxPoolSize",
                        "dbCnxInactivityTimeout", "dbPort");
        // retrieve archiver class properties
        params.setParametersFromTango(archiverClassName, tangoDbProperties);
        // override with device properties
        if (!dbHost.isEmpty())
            params.setHost(dbHost);
        if (!dbName.isEmpty())
            params.setName(dbName);
        if (!dbSchema.isEmpty())
            params.setSchema(dbSchema);
        if (!dbUser.isEmpty())
            params.setUser(dbUser);
        if (!dbPassword.isEmpty())
            params.setPassword(dbPassword);
        if (!dbPort.isEmpty())
            params.setPort(dbPort);
        if (!dbType.isEmpty())
            params.setDbType(DatabaseConnectionConfig.DataBaseType.valueOf(dbType.toUpperCase()));
        if (params.getDbType().equals(DatabaseConnectionConfig.DataBaseType.POSTGRESQL)) {
            databaseAccess = new TimeseriesAccess(deviceManager, dynamicManager);
        } else {
            databaseAccess = new HdbTdbAccess(deviceManager, dynamicManager, isHdb);
        }
        databaseAccess.connectDatabase(params);
    }
    protected void connectDatabase() throws DevFailed {
        connectDatabase(ConfigConst.HDB_CLASS_DEVICE, true);
    }

    @Attribute
    public String getVersion() {
        return VERSION;
    }

    /**
     * Execute command "GetInfo" on device. Returns misc informations about the
     * database and a set of parameters characterizing the connection.
     *
     * @return The informations that characterize the database
     */
    @Command(name = "GetInfo", outTypeDesc = "The information that characterize the database")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getInfo() throws DevFailed {
        return databaseAccess.getInfo();
    }

    /**
     * Execute command "GetHost" on device. Returns the connected database host
     * identifier.
     *
     * @return The connected database host identifier.
     * @throws ArchivingException
     */
    @Command(name = "GetHost", outTypeDesc = "The connected database host identifier")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getDbHost() throws DevFailed {
        return databaseAccess.getHost();
    }

    public void setDbHost(final String dbHost) {
        this.dbHost = dbHost;
    }

    /**
     * Execute command "GetUser" on device. Gets the current user's name used
     * for the connection.
     *
     * @return The current user's name used for the connection.
     * @throws ArchivingException
     */
    @Command(name = "GerUser", outTypeDesc = "The current user's name used for the connection.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getUser() {
        return databaseAccess.getUser();
    }

    /**
     * Execute command "GetAttDefinitionData" on device. Returns an array
     * containing the differents definition informations for the given
     * attribute.
     *
     * @param argin The attribute's name
     * @return Differents definition informations for the given attribute
     */
    @Command(name = "GetAttDefinitionData", outTypeDesc = "Differents definition informations for the given attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getAttDefinitionData(final String argin) throws DevFailed {
        return databaseAccess.getAttDefinitionData(argin);
    }

    /**
     * Execute command "GetAttPropertiesData" on device. Gets the differents
     * properties informations for the given attribute
     *
     * @param argin The attribute's name
     * @return An array containing the differents properties for the given
     * attribute
     */
    @Command(name = "GetAttPropertiesData", outTypeDesc = "An array containing the different properties for the given attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getAttPropertiesData(final String argin) throws DevFailed {
        return databaseAccess.getAttPropertiesData(argin);
    }

    /**
     * Execute command "GetAttFullName" on device. Gets for a specified
     * attribute id its full name as defined in HDB
     *
     * @param argin The attribute's id
     * @return The HDB's full name that characterize the given attribute
     */
    @Command(name = "GetAttFullName", outTypeDesc = "The HDB's full name that characterize the given attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getAttFullName(final int argin) throws DevFailed {
        return databaseAccess.getAttFullName(argin);
    }

    /**
     * Execute command "GetAttId" on device. Gets for a specified attribute its
     * ID as defined in HDB
     *
     * @param argin The attribute's name
     * @return The HDB's ID that characterize the given attribute
     */
    @Command(name = "GetAttId", inTypeDesc = "The attribute's name", outTypeDesc = "The HDB's ID that characterize the given attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttId(final String argin) throws DevFailed {
        return databaseAccess.getAttId(argin);
    }

    /**
     * Execute command "GetAttNameAll" on device. Gets whole list of the
     * attributes registered in HDB
     *
     * @return The whole list of the attributes registered in HDB
     */
    @Command(name = "GetAttNameAll", outTypeDesc = "The whole list of the attributes registered in HDB")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getAttNameAll() throws DevFailed {
        return databaseAccess.getAttNameAll();
    }

    /**
     * Execute command "GetAttNameFilterFormat" on device. Gets the list of
     * <I>HDB</I> registered attributes for the given format
     *
     * @param argin A format [0 -> scalar - 1 -> spectrum - 2 -> image]
     * @return The filtered list of attributes registered in HDB. The filtering
     * is made according to the given format [0 -> scalar - 1 ->
     * spectrum - 2 -> image]
     */
    @Command(name = "GetAttNameFilterFormat", inTypeDesc = " A format [0 -> scalar - 1 -> spectrum - 2 -> image]", outTypeDesc = "he filtered list of attributes registered in HDB. The filtering\n"
            + "is made according to the given format [0 -> scalar - 1 ->\n" + "spectrum - 2 -> image]")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getAttNameFilterFormat(final short argin) throws DevFailed {
        return databaseAccess.getAttNameFilterFormat(argin);
    }

    /**
     * Execute command "GetAttNameFilterType" on device. Gets the list of
     * <I>HDB</I> registered attributes for the given type.
     *
     * @param argin A type [2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->
     *              Tango::DevDouble and 8 -> Tango::DevString]
     * @return The filtered list of attributes registered in HDB. The filtering
     * is made according to the given type [2 -> Tango::DevShort | 3 ->
     * Tango::DevLong | 5 -> Tango::DevDouble and 8 -> Tango::DevString]
     */
    @Command(name = "GetAttNameFilterType", inTypeDesc = "A type [2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->\n"
            + "     *              Tango::DevDouble and 8 -> Tango::DevString]", outTypeDesc = "The filtered list of attributes registered in HDB. The filtering\n"
            + "     * is made according to the given type [2 -> Tango::DevShort | 3 ->\n"
            + "     * Tango::DevLong | 5 -> Tango::DevDouble and 8 -> Tango::DevString]")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getAttNameFilterType(final short argin) throws DevFailed {
        return databaseAccess.getAttNameFilterType(argin);
    }

    /**
     * Execute command "GetAttCountAll" on device. Gets the total number of
     * attributes defined in HDB.
     *
     * @return The total number of attributes defined in HDB
     */
    @Command(name = "GetAttCountAll", outTypeDesc = "The total number of attributes defined in HDB")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttCountAll() throws DevFailed {
        return databaseAccess.getAttCountAll();
    }

    /**
     * Execute command "GetAttCountFilterFormat" on device. Gets the total
     * number of attributes defined in HDB with the given format.
     *
     * @param argin A format [0 -> scalar - 1 -> spectrum - 2 -> image]
     * @return The total number of attributes defined in HDB with the given
     * format [0 -> scalar - 1 -> spectrum - 2 -> image]
     */
    @Command(name = "GetAttCountFilterFormat", inTypeDesc = "A format [0 -> scalar - 1 -> spectrum - 2 -> image]", outTypeDesc = "The total number of attributes defined in HDB with the given\n"
            + " format [0 -> scalar - 1 -> spectrum - 2 -> image]")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttCountFilterFormat(final short argin) throws DevFailed {
        return databaseAccess.getAttCountFilterFormat(argin);

    }

    /**
     * Execute command "GetAttCountFilterType" on device. Gets the total number
     * of attributes defined in HDB with the given type.
     *
     * @param argin A type [2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->
     *              Tango::DevDouble and 8 -> Tango::DevString]
     * @return The total number of attributes defined in HDB with the given type
     * [2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->
     * Tango::DevDouble and 8 -> Tango::DevString]
     */
    @Command(name = "GetAttCountFilterType", inTypeDesc = "A type [2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->\n"
            + "  Tango::DevDouble and 8 -> Tango::DevString]", outTypeDesc = "The total number of attributes defined in HDB with the given type\n"
            + " [2 -> Tango::DevShort | 3 -> Tango::DevLong | 5 ->\n"
            + " Tango::DevDouble and 8 -> Tango::DevString]")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttCountFilterType(final short argin) throws DevFailed {
        return databaseAccess.getAttCountFilterType(argin);
    }

    /**
     * Execute command "GetDomains" on device. Gets all the registered domains.
     *
     * @return The registered domains
     */
    @Command(name = "GetDomains", outTypeDesc = "The registered domains")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getDomains() throws DevFailed {
        return databaseAccess.getDomains();
    }

    /**
     * Execute command "GetDomainsCount" on device. Returns the number of
     * distinct registered domains.
     *
     * @return The number of distinct registered domains.
     */
    @Command(name = "GetDomainsCount", outTypeDesc = "The number of distinct registered domains")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getDomainsCount() throws DevFailed {
        return databaseAccess.getDomainsCount();
    }

    /**
     * Execute command "GetFamilies" on device. Gets all the registered families
     *
     * @return The registered families
     */
    @Command(name = "GetFamilies", outTypeDesc = "The registered families")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getFamilies() throws DevFailed {
        return databaseAccess.getFamilies();
    }

    /**
     * Execute command "GetFamiliesCount" on device. Returns the number of
     * distinct registered families.
     *
     * @return The number of distinct registered families.
     */
    @Command(name = "GetFamiliesCount", outTypeDesc = "The number of distinct registered families.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getFamiliesCount() throws DevFailed {
        return databaseAccess.getFamiliesCount();
    }

    /**
     * Execute command "GetFamiliesByDomain" on device. Gets all the registered
     * families for the given domain.
     *
     * @param argin The given domain
     * @return The registered families for the given domain
     */
    @Command(name = "GetFamiliesByDomain", outTypeDesc = "The registered families for the given domain")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getFamiliesByDomain(final String argin) throws DevFailed {
        return databaseAccess.getFamiliesByDomain(argin);
    }

    /**
     * Execute command "GetFamiliesByDomainCount" on device. Returns the number
     * of distinct registered families for a given domain.
     *
     * @param argin A domain name
     * @return The number of distinct registered families for a given domain.
     */
    @Command(name = "GetFamiliesByDomainCount", outTypeDesc = "The number of distinct registered families for a given domain.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getFamiliesByDomainCount(final String argin) throws DevFailed {
        return databaseAccess.getFamiliesByDomainCount(argin);
    }

    /**
     * Execute command "GetMembers" on device. Gets all the registered members
     *
     * @return The registered members
     */
    @Command(name = "GetMembers", outTypeDesc = "The registered members")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getMembers() throws DevFailed {
        return databaseAccess.getMembers();
    }

    /**
     * Execute command "GetMembersCount" on device. Returns the number of
     * distinct members.
     *
     * @return The number of distinct members.
     */
    @Command(name = "GetMembersCount", outTypeDesc = "The number of distinct members.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getMembersCount() throws DevFailed {
        return databaseAccess.getMembersCount();
    }

    /**
     * Execute command "GetMembersByDomainFamily" on device. Gets all the
     * registered members for the given domain and family
     *
     * @param argin The given domain and family
     * @return The registered members for the given domain and family
     */
    @Command(name = "GetMembersByDomainFamily", inTypeDesc = "The given domain and family", outTypeDesc = "The registered members for the given domain and family")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getMembersByDomainFamily(final String... argin) throws DevFailed {
        return databaseAccess.getMembersByDomainFamily(argin);
    }

    /**
     * Execute command "GetMembersByDomainFamilyCount" on device. Returns the
     * number of distinct registered members for the given domain and family.
     *
     * @param argin A domain name, a family name
     * @return The number of distinct registered members for the given domain
     * and family.
     */
    @Command(name = "GetMembersByDomainFamilyCount", inTypeDesc = "A domain name, a family name", outTypeDesc = "The number of distinct registered members for the given domain and family.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int GetMembersByDomainFamilyCount(final String... argin) throws DevFailed {
        return databaseAccess.GetMembersByDomainFamilyCount(argin);
    }

    /**
     * Execute command "GetAttributesByDomainFamilyMembersCount" on device.
     * Returns the number of registered the attributes for a given domain,
     * family, member.
     *
     * @param argin A domain name, a family name, a member name.
     * @return The number of registered the attributes for a given domain,
     * family, member.
     */
    @Command(name = "GetAttributesByDomainFamilyMembersCount", inTypeDesc = "A domain name, a family name, a member name.", outTypeDesc = "The number of registered the attributes for a given domain family, member.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttributesByDomainFamilyMembersCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttributesByDomainFamilyMembersCount(argin);
    }

    /**
     * Execute command "GetCurrentArchivedAtt" on device. Gets the list of
     * attributes that are being archived in <I>HDB</I>
     *
     * @return The list of attributes that are being archived
     */
    @Command(name = "GetCurrentArchivedAtt", outTypeDesc = "The list of attributes that are being archived")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getCurrentArchivedAtt() throws DevFailed {
        return databaseAccess.getCurrentArchivedAtt();
    }

    /**
     * Execute command "IsArchived" on device. Returns "true" if the attribute
     * of given name is currently archived, "false" otherwise.
     *
     * @param argin The attribute's name
     * @return true if the given attribute is being archived
     */
    @Command(name = "IsArchived", outTypeDesc = "true if the given attribute is being archived")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public boolean isArchived(final String argin) throws DevFailed {
        return databaseAccess.isArchived(argin);
    }

    /**
     * Execute command "GetArchivingMode" on device. Gets the archiving mode
     * used for the specified attribute.
     *
     * @param argin The attribute's name
     * @return The archiving mode used for the specified attribute
     */
    @Command(name = "GetArchivingMode", inTypeDesc = "The attribute's name", outTypeDesc = "The archiving mode used for the specified attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String[] getArchivingMode(final String argin) throws DevFailed {
        return databaseAccess.getArchivingMode(argin);
    }

    /**
     * Execute command "GetAttDataCount" on device. Returns the number of the
     * data archieved for an attribute.
     *
     * @param argin An attribute name.
     * @return The number of the data archieved for an attribute.
     */
    @Command(name = "GetAttDataCount", inTypeDesc = "An attribute name.", outTypeDesc = "The number of the data archived for an attribute.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataCount(final String argin) throws DevFailed {
        return databaseAccess.getAttDataCount(argin);
    }

    /**
     * Execute command "GetAttDataAvg" on device. Returns the average value
     * generated by the given attribute.
     *
     * @param argin The attribute's name
     * @return The average of the values generated by the attribute
     */
    @Command(name = "GetAttDataAvg", inTypeDesc = "The attribute's name", outTypeDesc = "The average of the values generated by the attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public double getAttDataAvg(final String argin) throws DevFailed {
        return databaseAccess.getAttDataAvg(argin);
    }

    /**
     * Get the newest inserted value for an attribute. Returns READ part only
     *
     * @param attributeName the attribute name
     * @return timestamp; values
     * @throws DevFailed
     */
    @Command(name = "GetNewestValue", inTypeDesc = "the attribute name", outTypeDesc = "timestamp; values")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getNewestValue(final String attributeName) throws DevFailed {
        return databaseAccess.getNewestValue(attributeName);
    }

    protected Object get(Object value, int index) {
        Object result;
        if ((value != null) && value.getClass().isArray() && (index > -1) && (index < Array.getLength(value))) {
            result = Array.get(value, index);
        } else {
            result = null;
        }
        return result;
    }

    /**
     * Get the nearest value and timestamp of an attribute for a given
     * timestamp. Returns READ part only
     *
     * @param argin [attributeName, timestamp]. timestamp format is YYYY-MM-DD HH24:MI:SS
     * @return timestamp; values
     * @throws DevFailed
     */
    @Command(name = "getNearestValue", inTypeDesc = "attributeName, timestamp]. timestamp format is YYYY-MM-DD HH24:MI:SS", outTypeDesc = "timestamp; values")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getNearestValue(final String... argin) throws DevFailed {
        return databaseAccess.getNearestValue(argin);
    }

//    private DevFailed buildNoDataDevFailed(String attributeName) {
//        return DevFailedUtils.newDevFailed("no data found for " + attributeName);
//    }

    /**
     * Execute command "GetAttDataAvgBetweenDates" on device. Returns the
     * average value generated by the given attribute and between the two given
     * dates.
     *
     * @param argin The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS)
     *              and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return The average value generated by the given attribute and between
     * the two given dates.
     */
    @Command(name = "GetAttDataAvgBetweenDates", inTypeDesc = "he attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The average value generated by the given attribute and between"
            + " the two given dates.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public double getAttDataAvgBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataAvgBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataBetweenDates" on device. Retrieves data
     * beetwen two dates, for a given scalar attribute. Create a dynamic
     * attribute, retrieve data from database and prepare result for
     * attribute_history call.
     *
     * @param argin The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS)
     *              and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataBetweenDates", inTypeDesc = "The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataBetweenDates(extractErrorMessages, argin);
    }

    /**
     * Extract an attribute's data for HDB
     *
     * @param argin The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS)
     *              and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return Long: the timestamps , String: values (read values only
     * @throws DevFailed
     */
    @Command(name = "ExtractBetweenDates", inTypeDesc = "The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "Long: the timestamps , String: values (read values only)")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarDoubleStringArray extractBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.extractBetweenDates(extractErrorMessages, argin);
    }

    /**
     * Execute command "GetAttDataBetweenDatesSampling" on device. Retrieves data
     * between two dates, for a given scalar attribute. Create a dynamic
     * attribute, retrieve data from database and prepare result for
     * attribute_history call.
     *
     * @param argin : The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS)
     *              and the ending date (YYYY-MM-DD HH24:MI:SS), and
     *              the sampling type (ALL, SECOND, MINUTE, HOUR, DAY)
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataBetweenDatesSampling", inTypeDesc = "The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS) "
            + "and the ending date (YYYY-MM-DD HH24:MI:SS), and\n"
            + "the sampling type (ALL, SECOND, MINUTE, HOUR, DAY)", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataBetweenDatesSampling(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataBetweenDatesSampling(argin);
    }

    protected void exportData(String attributeName) throws DevFailed {
        // do nothing, only used for TdbArchiver
    }

    /**
     * Execute command "GetAttDataBetweenDatesCount" on device. Returns the
     * number of data beetwen two dates and, for a given scalar attribute.
     *
     * @param argin The attribute's name, the beginning (YYYY-MM-DD HH24:MI:SS)
     *              date and the ending date (YYYY-MM-DD HH24:MI:SS).
     * @return The number of data beetwen two dates and, for a given scalar
     * attribute.
     */
    @Command(name = "GetAttDataBetweenDatesCount", inTypeDesc = "The attribute's name, the beginning (YYYY-MM-DD HH24:MI:SS)\n"
            + "date and the ending date (YYYY-MM-DD HH24:MI:SS).", outTypeDesc = "The number of data beetwen two dates and, for a given scalar")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataBetweenDatesCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataBetweenDatesCount(argin);
    }

    /**
     * Execute command "GetAttDataInfOrSupThan" on device. Retrieves all data
     * that are lower than the given value x OR higher than the given value y.
     * Create a dynamic attribute, retrieve the corresponding data from database
     * and prepare result for attribute_history call.
     *
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataInfOrSupThan", inTypeDesc = "The attribute's name, the lower limit and the upper limit", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataInfOrSupThan(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfOrSupThan(argin);
    }

    /**
     * Execute command "GetAttDataInfOrSupThanCount" on device. Returns the
     * number of data lower than the given value x OR higher than the given
     * value y.
     *
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return The number of scalar data lower than the given value x OR higher
     * than the given value y.
     */
    @Command(name = "GetAttDataInfOrSupThanCount", inTypeDesc = "The attribute's name, the lower limit and the upper limit", outTypeDesc = "The number of scalar data lower than the given value x OR higher than the given value y.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataInfOrSupThanCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfOrSupThanCount(argin);
    }

    /**
     * Execute command "GetAttDataInfOrSupThanBetweenDates" on device. Retrieves
     * data beetwen two dates (date_1 & date_2) - Data are lower than the given
     * value x OR higher than the given value y. Create a dynamic attribute,
     * retrieve the corresponding data from database and prepare result for
     * attribute_history call.
     *
     * @param argin The attribute's name, the lower limit, the upper limit, the
     *              beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date
     *              (YYYY-MM-DD HH24:MI:SS)
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataInfOrSupThanBetweenDates", inTypeDesc = "The attribute's name, the lower limit, the upper limit, the\n"
            + "beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataInfOrSupThanBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfOrSupThanBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataInfOrSupThanBetweenDatesCount" on device.
     * Returns the number of data beetwen two dates (date_1 & date_2). Data are
     * lower than the given value x OR higher than the given value y.
     *
     * @param argin The attribute's name, the lower limit, the upper limit, the
     *              beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date
     *              (YYYY-MM-DD HH24:MI:SS).
     * @return The number of scalar data lower than the given value x OR higher
     * than the given value y, beetwen two dates and for the specified
     * attribute.
     */
    @Command(name = "GetAttDataInfOrSupThanBetweenDatesCount", inTypeDesc = "The attribute's name, the lower limit, the upper limit, the\n"
            + "beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS).", outTypeDesc = "The number of scalar data lower than the given value x OR higher\n"
            + "than the given value y, beetwen two dates and for the specified attribute.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataInfOrSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfOrSupThanBetweenDatesCount(argin);
    }

    /**
     * Execute command "GetAttDataInfThan" on device. Retrieves all the data
     * that are lower than the given value x. Create a dynamic attribute,
     * retrieve the corresponding data from database and prepare result for
     * attribute_history call.
     *
     * @param argin The attribute's name, the upper limit
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataInfThan", inTypeDesc = "The attribute's name, the upper limit", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataInfThan(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfThan(argin);
    }

    /**
     * Execute command "GetAttDataInfThanCount" on device. Returns the number of
     * data lower than the given value.
     *
     * @param argin The attribute's name and the upper limit.
     * @return The number of scalar data lower than the given value and for the
     * specified attribute.
     */
    @Command(name = "GetAttDataInfThanCount", inTypeDesc = "The attribute's name and the upper limit.", outTypeDesc = "The number of scalar data lower than the given value and for the\n specified attribute.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataInfThanCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfThanCount(argin);
    }

    /**
     * Execute command "GetAttDataInfThanBetweenDates" on device. Retrieves data
     * beetwen two dates (date_1 & date_2) - Data are lower than the given value
     * x. Create a dynamic attribute, retrieve the corresponding data from
     * database and prepare result for attribute_history call.
     *
     * @param argin The attribute's name, the upper limit, the beginning date
     *              (YYYY-MM-DD HH24:MI:SS) and the ending date ((YYYY-MM-DD HH24:MI:SS)
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataInfThanBetweenDates", inTypeDesc = "The attribute's name, the upper limit, the beginning date\n"
            + "(YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataInfThanBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfThanBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataInfThanBetweenDatesCount" on device. Returns
     * the number data lower than the given value x, and beetwen two dates
     * (date_1 & date_2).
     *
     * @param argin The attribute's name, the upper limit, the beginning date
     *              (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return The number data lower than the given value x, and beetwen two
     * dates (date_1 & date_2).
     */
    @Command(name = "GetAttDataInfThanBetweenDatesCount", inTypeDesc = "The attribute's name, the upper limit, the beginning date\n"
            + "(YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The number data lower than the given value x, and beetwen two dates (date_1 & date_2).")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataInfThanBetweenDatesCount(argin);
    }

    /**
     * Execute command "GetAttDataLastN" on device. Retrieves the last n
     * archived data, for a given scalar attribute. Create a dynamic attribute,
     * retrieve the corresponding data from database and prepare result for
     * attribute_history call.
     *
     * @param argin The attribute's name and the number of wished data
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataLastN", inTypeDesc = "The attribute's name and the number of wished data", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataLastN(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataLastN(argin);
    }

    /**
     * Execute command "GetAttDataMax" on device. Returns the biggest value
     * generated by the attribute.
     *
     * @param argin The attribute's name
     * @return The biggest value generated by the attribute
     */
    @Command(name = "GetAttDataMax", inTypeDesc = "The attribute's name", outTypeDesc = "The biggest value generated by the attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public double getAttDataMax(final String argin) throws DevFailed {
        return databaseAccess.getAttDataMax(argin);
    }

    /**
     * Execute command "GetAttDataMaxBetweenDates" on device. Returns the
     * biggest value generated between the two given dates.
     *
     * @param argin The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS)
     *              and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return The biggest value generated between the two given dates.
     */
    @Command(name = "GetAttDataMaxBetweenDates", inTypeDesc = "The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS) "
            + "and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The biggest value generated between the two given dates.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public double getAttDataMaxBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataMaxBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataMin" on device. Returns the smallest scalar
     * value generated by the attribute.
     *
     * @param argin The attribute's name
     * @return The smallest value generated by the attribute
     */
    @Command(name = "GetAttDataMin", inTypeDesc = "The attribute's name", outTypeDesc = "The smallest value generated by the attribute")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public double getAttDataMin(final String argin) throws DevFailed {
        return databaseAccess.getAttDataMin(argin);
    }

    /**
     * Execute command "GetAttDataMinBetweenDates" on device. Returns the
     * smallest scalar value generated by the given attribute and between two
     * given dates
     *
     * @param argin The attribute's name, the beginning date ((YYYY-MM-DD HH24:MI:SS)
     *              and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return The smallest scalar value generated by the given attribute and
     * between the two given dates.
     */
    @Command(name = "GetAttDataMinBetweenDates", inTypeDesc = "The attribute's name, the beginning date (YYYY-MM-DD HH24:MI:SS)"
            + " and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The smallest scalar value generated by the given attribute and"
            + " between the two given dates.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public double getAttDataMinBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataMinBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataSupThan" on device. Retrieves all the data
     * that are higher than the given value x. Create a dynamic attribute,
     * retrieve the corresponding data from database and prepare result for
     * attribute_history call.
     *
     * @param argin The attribute's name and the lower limit
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataSupThan", inTypeDesc = "The attribute's name and the lower limit", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataSupThan(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupThan(argin);
    }

    /**
     * Execute command "GetAttDataSupThanCount" on device. Returns the number of
     * data higher than the given value.
     *
     * @param argin The attribute's name and the lower limit.
     * @return The number of data higher than the given value.
     */
    @Command(name = "GetAttDataSupThanCount", inTypeDesc = "The attribute's name and the lower limit.", outTypeDesc = "The number of data higher than the given value.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataSupThanCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupThanCount(argin);
    }

    /**
     * Execute command "GetAttDataSupAndInfThan" on device. Retrieves all data
     * that are higher than the given value x AND lower than the given value y.
     * Create a dynamic attribute, retrieve the corresponding data from database
     * and prepare result for attribute_history call.
     *
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataSupAndInfThan", inTypeDesc = "The attribute's name, the lower limit and the upper limit", outTypeDesc = "String : The new created dynamic attribute name, Long : the number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataSupAndInfThan(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupAndInfThan(argin);
    }

    /**
     * Execute command "GetAttDataSupAndInfThanCount" on device. Returns data
     * that are highter than the given value x AND lower than the given value y.
     *
     * @param argin The attribute's name, the lower limit and the upper limit
     * @return The data that are highter than the given value x AND lower than
     * the given value y.
     */
    @Command(name = "GetAttDataSupAndInfThanCount", inTypeDesc = "The attribute's name, the lower limit and the upper limit", outTypeDesc = "The data that are highter than the given value x AND lower than the given value y.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataSupAndInfThanCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupAndInfThanCount(argin);
    }

    /**
     * Execute command "GetAttDataSupAndInfThanBetweenDates" on device.
     * Retrieves data beetwen two dates (date_1 & date_2) - Data are higher than
     * the given value x AND lower than the given value y. Create a dynamic
     * attribute, retrieve the corresponding data from database and prepare
     * result for attribute_history call.
     *
     * @param argin The attribute's name, the lower limit, the upper limit, the
     *              beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date
     *              (YYYY-MM-DD HH24:MI:SS)
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataSupAndInfThanBetweenDates", inTypeDesc = "The attribute's name, the lower limit, the upper limit, the\n"
            + "beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataSupAndInfThanBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupAndInfThanBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataSupAndInfThanBetweenDatesCount" on device.
     * Returns the number of data higher than the given value x, (AND) lower
     * than the given value y, and beetwen two dates (date_1 & date_2).
     *
     * @param argin The attribute's name, the lower limit, the upper limit, the
     *              beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date
     *              (YYYY-MM-DD HH24:MI:SS).
     * @return The number of data higher than the given value x, (AND) lower
     * than the given value y, and beetwen two dates (date_1 & date_2).
     */
    @Command(name = "GetAttDataSupAndInfThanBetweenDatesCount", inTypeDesc = "The attribute's name, the lower limit, the upper limit, the"
            + "beginning date (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The number of data higher than the given value x, (AND) lower"
            + " than the given value y, and between two dates (date_1 & date_2).")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataSupAndInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupAndInfThanBetweenDatesCount(argin);
    }

    /**
     * Execute command "GetAttDataSupThanBetweenDates" on device. Retrieves data
     * beetwen two dates (date_1 & date_2) - Data are higher than the given
     * value x. Create a dynamic attribute, retrieve the corresponding data from
     * database and prepare result for attribute_history call.
     *
     * @param argin The attribute's name, the lower limit, the beginning date
     *              (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return String : The new created dynamic attribute name, Long : the
     * number of data.
     */
    @Command(name = "GetAttDataSupThanBetweenDates", inTypeDesc = "he attribute's name, the lower limit, the beginning date "
            + "(YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The new created dynamic attribute name, Long : the"
            + " number of data.")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public DevVarLongStringArray getAttDataSupThanBetweenDates(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupThanBetweenDates(argin);
    }

    /**
     * Execute command "GetAttDataSupThanBetweenDatesCount" on device. Returns
     * the number of data higher than the given value y, and beetwen two dates
     * (date_1 & date_2).
     *
     * @param argin The attribute's name, the lower limit, the beginning date
     *              (YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)
     * @return The number of data higher than the given value y, and beetwen two
     * dates (date_1 & date_2).
     */
    @Command(name = "GetAttDataSupThanBetweenDatesCount", inTypeDesc = "The attribute's name, the lower limit, the beginning date\n"
            + "(YYYY-MM-DD HH24:MI:SS) and the ending date (YYYY-MM-DD HH24:MI:SS)", outTypeDesc = "The number of data higher than the given value y, and beetwen two"
            + " dates (date_1 & date_2).")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public int getAttDataSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        return databaseAccess.getAttDataSupThanBetweenDatesCount(argin);
    }

    /**
     * Execute command "RemoveDynamicAttribute" on device. Remove the dynamic
     * attribute.
     *
     * @param attributeName The HdbExtractor dynamic attribute's name
     */
    @Command(name = "RemoveDynamicAttribute", inTypeDesc = "The HdbExtractor dynamic attribute's name")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public void removeDynamicAttribute(final String attributeName) throws DevFailed {
        dynamicManager.removeAttribute(attributeName);
    }

    @Command(name = "RemoveDynamicAttributes")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public void removeDynamicAttributes() throws DevFailed {
        dynamicManager.clearAttributes();
        attributeID.set(0);
    }

    @Command(name = "GetMaxTime")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getMaxTime(final String argin) throws DevFailed {
        return databaseAccess.getMaxTime(argin);
    }

    @Command(name = "GetMinTime")
    @StateMachine(deniedStates = {DeviceState.FAULT, DeviceState.UNKNOWN})
    public String getMinTime(final String argin) throws DevFailed {
        return databaseAccess.getMinTime(argin);
    }

}
