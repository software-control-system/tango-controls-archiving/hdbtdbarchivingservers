package org.tango.archiving.server.extractor;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarDoubleStringArray;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.build.TangoArchivingServicesBuilder;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.AttributeParameters;
import fr.soleil.tango.archiving.event.select.AttributeValueSeries;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;
import fr.soleil.tango.archiving.services.TangoArchivingFetcherService;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.attribute.AttributeTangoType;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.OptionalLong;


public class TimeseriesAccess implements IArchivingAccess {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeseriesAccess.class);
    private final DeviceManager deviceManager;
    private final DynamicManager dynamicManager;

    protected TangoArchivingFetcherService fetcherService;
    protected TangoArchivingConfigService configService;

    public TimeseriesAccess(DeviceManager deviceManager, DynamicManager dynamicManager) {
        this.deviceManager = deviceManager;
        this.dynamicManager = dynamicManager;
    }

    @Override
    public void connectDatabase(final DatabaseConnectionConfig params) {
        fetcherService = new TangoArchivingServicesBuilder().buildFetcher(params);
        configService = new TangoArchivingServicesBuilder().buildConfigFetcher(params);
    }

    @Override
    public String getInfo() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String getHost() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String getUser() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getAttDefinitionData(final String argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getAttPropertiesData(final String argin) {
        final Optional<AttributeParameters> params = configService.getAttributeParameters(argin);
        return params.map(attributeParameters -> new String[]{attributeParameters.toString()}).orElseGet(() -> new String[]{});
    }

    @Override
    public String getAttFullName(final int argin) {
        final Optional<AttributeConfig> name = configService.getAttributeConfig(argin);
        return name.map(AttributeConfig::getFullName).orElse("");
    }

    @Override
    public int getAttId(final String argin) throws DevFailed {
        OptionalInt id = configService.getAttributeID(argin);
        if (!id.isPresent()) {
            throw DevFailedUtils.newDevFailed("no id found for " + argin);
        }
        return id.getAsInt();
    }

    @Override
    public String[] getAttNameAll() {
        final List<AttributeConfig> result = configService.getAttributesByAttributePattern("%");
        String[] toReturn = new String[result.size()];
        int i = 0;
        for (AttributeConfig conf : result) {
            toReturn[i++] = conf.getFullName();
        }
        return toReturn;
    }

    @Override
    public String[] getAttNameFilterFormat(final short argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getAttNameFilterType(final short argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public int getAttCountAll() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public int getAttCountFilterFormat(final short argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public int getAttCountFilterType(final short argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getDomains() {
        final List<String> configs = configService.getAllDomains();
        return configs.toArray(new String[0]);
    }

    @Override
    public int getDomainsCount() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getFamilies() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public int getFamiliesCount() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getFamiliesByDomain(final String domain) {
        final List<String> configs = configService.getFamiliesByDomain(domain);
        return configs.toArray(new String[0]);
    }

    @Override
    public int getFamiliesByDomainCount(final String argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getMembers() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public int getMembersCount() {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getMembersByDomainFamily(final String... argin) {
        final List<String> configs = configService.getMembersByDomainFamily(argin[0], argin[1]);
        return configs.toArray(new String[0]);
    }

    @Override
    public int GetMembersByDomainFamilyCount(final String... argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public int getAttributesByDomainFamilyMembersCount(final String... argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String[] getCurrentArchivedAtt() {
        final List<AttributeConfig> configs = configService.getAttributesByDomain("%");
        List<String> result = new ArrayList<>();
        configs.forEach(config -> result.add(config.getFullName()));
        return result.toArray(new String[0]);
    }

    @Override
    public boolean isArchived(final String argin) {
        return configService.isArchived(argin);
    }

    @Override
    public String[] getArchivingMode(final String argin) throws DevFailed {
        OptionalInt id = configService.getAttributeID(argin);
        if (!id.isPresent()) {
            throw DevFailedUtils.newDevFailed("no id found for " + argin);
        }
        return toStringArray(configService.getCurrentInsertionModes(id.getAsInt()));
    }

    private String[] toStringArray(Object toConvert) {
        List<String> array = new ArrayList<>();
        Arrays.stream(toConvert.getClass().getDeclaredFields()).forEach(f -> {
            try {
                f.setAccessible(true);
                Object value = f.get(toConvert);
                if (value != null) {
                    array.add(f.getName() + ": " + f.get(toConvert));
                }
            } catch (IllegalAccessException e) {
                array.add(f.getName() + ": error");
            }
        });
        return array.toArray(new String[0]);
    }

    @Override
    public int getAttDataCount(final String argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public double getAttDataAvg(final String argin) {
        throw new fr.soleil.tango.archiving.exception.TangoTimeseriesException("not supported function");
    }

    @Override
    public String getNewestValue(final String attributeName) {
        final fr.soleil.tango.archiving.event.select.AttributeValue value = fetcherService.getLast(attributeName,
                true);
        return getAttributeValueAsString(value);
    }

    @Override
    public String getNearestValue(final String... argin) throws DevFailed {
        if (argin.length == 2) {
            final String attributeName = argin[0];
            // YYYY-MM-DD HH24:MI:SS
            final Timestamp timestamp = getTimestamp(argin[1]);
            // String returned format: "Timestamp as long"; "read or writevalue"
            fr.soleil.tango.archiving.event.select.AttributeValue value = fetcherService
                    .getClosestValue(attributeName, timestamp, true);
            return getAttributeValueAsString(value);
        } else {
            throw DevFailedUtils
                    .newDevFailed("argin must have to inputs: attribute name, timestamp as YYY-MM-DD HH24:MI:SS");
        }
    }

    private Timestamp getTimestamp(String timestampString) throws DevFailed {
        List<String> patterns = new ArrayList<>();
        patterns.add("yyyy-MM-dd HH:mm:ss.SSS");
        patterns.add("yyyy-MM-dd HH:mm:ss");
        patterns.add("yyyy-MM-dd HH:mm");
        patterns.add("dd-MM-yyyy HH:mm:ss.SSS");
        patterns.add("dd-MM-yyyy HH:mm:ss");
        patterns.add("dd-MM-yyyy HH:mm");

        Timestamp timestamp = null;
        OptionalLong timeStampLong;
        for (String pattern : patterns) {
            timeStampLong = formatTimeStamp(timestampString, pattern);
            if (timeStampLong.isPresent()) {
                timestamp = new Timestamp(timeStampLong.getAsLong());
                break;
            }
        }
        if (timestamp == null) {
            throw DevFailedUtils.newDevFailed("impossible to parse date " + timestampString);
        }
        return timestamp;
    }

    private OptionalLong formatTimeStamp(String timestampString, String pattern) {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        formatter.setLenient(false);
        OptionalLong timestamp = OptionalLong.empty();
        try {
            timestamp = OptionalLong.of(formatter.parse(timestampString).getTime());
        } catch (ParseException e) {
            // empty
        }
        return timestamp;
    }

    @Override
    public double getAttDataAvgBetweenDates(final String... argin) throws DevFailed {
        double argout;
        if (argin.length == 3) {
            String attributeName = argin[0];
            final Timestamp from = getTimestamp(argin[1]);
            final Timestamp to = getTimestamp(argin[2]);
            final fr.soleil.tango.archiving.event.select.AttributeValue value = fetcherService
                    .getAggregateValue(attributeName, "avg", from, to);
            if (value.getValueR() != null) {
                argout = Double.parseDouble(value.getValueR().toString());
            } else if (value.getValueR() != null) {
                argout = Double.parseDouble(value.getValueW().toString());
            } else {
                argout = Double.NaN;
            }
        } else {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        }
        return argout;
    }

    private String getAttributeValueAsString(fr.soleil.tango.archiving.event.select.AttributeValue value) {
        LOGGER.debug("got value {}", value);
        String result;
        if (value.getDataTime() == null) {
            result = "No data";
        } else {
            result = Long.toString(value.getDataTime().getTime());
            Object readValue = value.getValueR();
            Object writeValue = value.getValueW();
            if (value.getErrorMessage().isPresent()) {
                result = result + "; " + value.getErrorMessage().get();
            } else {
                if (Objects.nonNull(readValue)) {
                    if (readValue.getClass().isArray()) {
                        result = result + "; " + ArrayUtils.toString(readValue);
                    } else {
                        result = result + "; " + readValue;
                    }
                }
                if (Objects.nonNull(writeValue)) {
                    if (writeValue.getClass().isArray()) {
                        result = result + "; " + ArrayUtils.toString(writeValue);
                    } else {
                        result = result + "; " + writeValue;
                    }
                }
                if (Objects.isNull(readValue) && Objects.isNull(writeValue)) {
                    result = result + "; Unknown error";
                }
            }
        }
        return result;
    }

    @Override
    public DevVarDoubleStringArray extractBetweenDates(final boolean withErrors, final String... argin)
            throws DevFailed {
        DevVarDoubleStringArray argout;
        if (argin.length != 3) {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        } else {
            String attributeName = argin[0].trim();
            final Timestamp from = getTimestamp(argin[1]);
            final Timestamp to = getTimestamp(argin[2]);
            final AttributeValueSeries data = fetcherService.getBetween(attributeName, from, to, withErrors);
            LOGGER.debug("fetching {} values for {}", data.getAttributeValues().size(), attributeName);
            final double[] timeStamps = new double[data.getAttributeValues().size()];
            final String[] values = new String[data.getAttributeValues().size()];
            int i = 0;
            for (fr.soleil.tango.archiving.event.select.AttributeValue event : data.getAttributeValues()) {
                timeStamps[i] = event.getDataTime().getTime();
                switch (data.getAttributeConfig().getWriteType()) {
                    case AttrWriteType._READ_WRITE:
                        values[i] = event.getValueR() + ";" + event.getValueW();
                        break;
                    case AttrWriteType._WRITE:
                        values[i] = String.valueOf(event.getValueW());
                        break;
                    case AttrWriteType._READ:
                        values[i] = String.valueOf(event.getValueR());
                        break;
                }
                i++;
            }
            argout = new DevVarDoubleStringArray(timeStamps, values);
        }
        return argout;
    }

    @Override
    public int getAttDataBetweenDatesCount(final String... argin) throws DevFailed {
        return extractBetweenDates(false, argin).svalue.length;
    }

    @Override
    public int getAttDataInfOrSupThanCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");

    }

    @Override
    public int getAttDataInfOrSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public int getAttDataInfThanCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public int getAttDataInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public double getAttDataMax(final String argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public double getAttDataMaxBetweenDates(final String... argin) throws DevFailed {
        double argout;
        if (argin.length == 3) {
            String attributeName = argin[0];
            final Timestamp from = getTimestamp(argin[1]);
            final Timestamp to = getTimestamp(argin[2]);
            final fr.soleil.tango.archiving.event.select.AttributeValue value = fetcherService
                    .getAggregateValue(attributeName, "max", from, to);
            if (value.getValueR() != null) {
                argout = Double.parseDouble(value.getValueR().toString());
            } else {
                argout = Double.parseDouble(value.getValueW().toString());
            }
        } else {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        }
        return argout;
    }

    @Override
    public double getAttDataMin(final String argin) throws DevFailed {

        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public double getAttDataMinBetweenDates(final String... argin) throws DevFailed {
        double argout;
        if (argin.length == 3) {
            String attributeName = argin[0];
            final Timestamp from = getTimestamp(argin[1]);
            final Timestamp to = getTimestamp(argin[2]);
            final fr.soleil.tango.archiving.event.select.AttributeValue value = fetcherService
                    .getAggregateValue(attributeName, "min", from, to);
            if (value.getValueR() != null) {
                argout = Double.parseDouble(value.getValueR().toString());
            } else {
                argout = Double.parseDouble(value.getValueW().toString());
            }
        } else {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        }
        return argout;
    }

    @Override
    public int getAttDataSupThanCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public int getAttDataSupAndInfThanCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public int getAttDataSupAndInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public int getAttDataSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public String getPartialName(final String completeName) {
        return completeName;
    }

    @Override
    public String getMaxTime(final String argin) throws DevFailed {
        final fr.soleil.tango.archiving.event.select.AttributeValue value = fetcherService.getLast(argin, true);
        return value.getDataTime().toString();
    }

    @Override
    public String getMinTime(final String argin) throws DevFailed {
        // TODO getMinTime
        return "TODO";
    }

    @Override
    public DevVarLongStringArray getAttDataBetweenDates(final boolean withErrors, final String... argin)
            throws DevFailed {
        final String attributeName = argin[0];
        final Timestamp from = getTimestamp(argin[1]);
        final Timestamp to = getTimestamp(argin[2]);
        LOGGER.info("Get data {} between {} and {}", attributeName, from, to);
        final AttributeValueSeries data = fetcherService.getBetween(attributeName, from, to, withErrors);
        return addAttribute(data);
    }

    @Override
    public DevVarLongStringArray getAttDataBetweenDatesSampling(final String... argin) throws DevFailed {
        if (argin.length != 4) {
            throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
        }
        final String attributeName = argin[0];
        final Timestamp from = getTimestamp(argin[1]);
        final Timestamp to = getTimestamp(argin[2]);
        // (ALL, SECOND, MINUTE, HOUR, DAY)
        String samplingInput = argin[3].toLowerCase(Locale.ENGLISH);
        final AttributeValueSeries values;
        LOGGER.info("Get data {} between {} and {} with sampling {}", attributeName, from, to, samplingInput);
        if (samplingInput.equals("all")) {
            values = fetcherService.getBetween(attributeName, from, to, false);
        } else {
            String sampling = "'1 " + samplingInput + "'";
            values = fetcherService.getWithSampling(attributeName, sampling, "min", from, to);
        }
        return addAttribute(values);
    }

    @Override
    public DevVarLongStringArray getAttDataInfOrSupThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfOrSupThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataLastN(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupAndInfThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupAndInfThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    public DevVarLongStringArray addAttribute(AttributeValueSeries dbData) throws DevFailed {
        DevVarLongStringArray argout;
        if (dbData == null || dbData.getAttributeValues().isEmpty()) {
            argout = getNoDataAttribute();
        } else {
            argout = new DevVarLongStringArray();

            // Build new Attribute's name
            String attributeName = getName(dbData.getAttributeConfig().getName(), attributeID.getAndIncrement());
            final int writeType = dbData.getAttributeConfig().getWriteType();
            final List<fr.soleil.tango.archiving.event.select.AttributeValue> valuesFromDb = dbData
                    .getAttributeValues();
            LOGGER.info("got {} values from database", valuesFromDb.size());
            org.tango.server.attribute.AttributeValue[] readHistory = new org.tango.server.attribute.AttributeValue[valuesFromDb
                    .size()];

            org.tango.server.attribute.AttributeValue[] writeHistory = null;
            AttributeValue lastWriteValue = null;
            if (writeType == AttrWriteType._READ) {
                for (int i = 0; i < valuesFromDb.size(); i++) {
                    readHistory[i] = new org.tango.server.attribute.AttributeValue();
                    Object value = valuesFromDb.get(i).getValueR();
                    readHistory[i].setValue(value, valuesFromDb.get(i).getDataTime().getTime());
                }
            } else if (writeType == AttrWriteType._WRITE) {
                for (int i = 0; i < valuesFromDb.size(); i++) {
                    readHistory[i] = new org.tango.server.attribute.AttributeValue();
                    Object value = valuesFromDb.get(i).getValueW();
                    readHistory[i].setValue(value, valuesFromDb.get(i).getDataTime().getTime());
                }
            } else { // writeType == AttrWriteType._READ_WRITE)
                writeHistory = new org.tango.server.attribute.AttributeValue[valuesFromDb.size()];
                for (int i = 0; i < valuesFromDb.size(); i++) {
                    readHistory[i] = new org.tango.server.attribute.AttributeValue();
                    Object valueR = valuesFromDb.get(i).getValueR();
                    Object valueW = valuesFromDb.get(i).getValueW();
                    readHistory[i].setValue(valueR, valuesFromDb.get(i).getDataTime().getTime());
                    writeHistory[i] = new org.tango.server.attribute.AttributeValue();
                    writeHistory[i].setValue(valueW, valuesFromDb.get(i).getDataTime().getTime());
                }
                lastWriteValue = writeHistory[writeHistory.length - 1];
            }
            AttributeValue lastReadValue = readHistory[readHistory.length - 1];
            LOGGER.debug("last value is [read={}, write={}] of type {}", lastReadValue, lastWriteValue,
                    lastReadValue.getValue().getClass().getCanonicalName());

            // determine attribute data type
            int dataType = AttributeTangoType.getTypeFromClass(lastReadValue.getValue().getClass()).getTangoIDLType();
            LOGGER.info("creating dynamic attribute {} with data type {} and format {}", attributeName, dataType, dbData.getAttributeConfig().getFormat());
            dynamicManager.addAttribute(new DataAttribute(attributeName, dataType,
                    AttrDataFormat.from_int(dbData.getAttributeConfig().getFormat()), writeType, lastReadValue,
                    lastWriteValue));
            deviceManager.startPolling(attributeName);
            deviceManager.triggerPolling(attributeName);
            LOGGER.info("filling data history of attribute {}", attributeName);
            deviceManager.fillAttributeHistory(attributeName, readHistory, writeHistory, null);
            LOGGER.info("filling data history of attribute {} DONE", attributeName);

            argout.lvalue = new int[]{dbData.getAttributeValues().size()};
            argout.svalue = new String[]{attributeName};

        }
        return argout;
    }
}
