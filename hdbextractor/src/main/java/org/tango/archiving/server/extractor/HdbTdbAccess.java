package org.tango.archiving.server.extractor;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarDoubleStringArray;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.DataBaseManagerFactory;
import fr.soleil.archiving.hdbtdb.api.TDBDataBaseManager;
import fr.soleil.archiving.hdbtdb.api.management.attributes.adtapt.IAdtAptAttributes;
import fr.soleil.archiving.hdbtdb.api.management.attributes.extractor.AttributeExtractor;
import fr.soleil.archiving.hdbtdb.api.tools.SamplingType;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.archiving.hdbtdb.api.utils.database.DbUtils;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.device.DeviceManager;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import java.lang.reflect.Array;
import java.sql.Timestamp;

public class HdbTdbAccess implements IArchivingAccess {

    private static final Logger LOGGER = LoggerFactory.getLogger(HdbTdbAccess.class);

    private final DeviceManager deviceManager;
    private final DynamicManager dynamicManager;
    private final boolean isHDB;
    protected DataBaseManager dataBaseManager;
    protected AttributeExtractor extractor;

    public HdbTdbAccess(DeviceManager deviceManager, DynamicManager dynamicManager, boolean isHDB) {
        this.deviceManager = deviceManager;
        this.dynamicManager = dynamicManager;
        this.isHDB = isHDB;
    }


    public void connectDatabase(final DatabaseConnectionConfig params) throws DevFailed {
        try {
            LOGGER.info("init connection to archiving DB with params: {}", params);
            DataBaseParameters legacyParams = new DataBaseParameters();
            legacyParams.setDbType(DataBaseParameters.DataBaseType.parseDataBaseType(params.getDbType().name()));
            legacyParams.setHost(params.getHost());
            legacyParams.setSchema(params.getSchema());
            legacyParams.setName(params.getName());
            legacyParams.setUser(params.getUser());
            legacyParams.setPassword(params.getPassword());
            legacyParams.setInactivityTimeout(params.getIdleTimeout());
            legacyParams.setMaxPoolSize((short) params.getMaxPoolSize());
            if (params.getHealthRegistry() != null) {
                legacyParams.setHealthRegistry(params.getHealthRegistry());
            }
            if (params.getMetricRegistry() != null) {
                legacyParams.setMetricRegistry(params.getMetricRegistry());
            }
            AbstractDataBaseConnector connector;
            try {
                connector = ConnectionFactory.connect(legacyParams);
            } catch (ArchivingException e) {
                throw new RuntimeException(e);
            }
            dataBaseManager = DataBaseManagerFactory.getDataBaseManager(isHDB, connector);
            extractor = dataBaseManager.getExtractor();
        } catch (ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String getInfo() {
        return dataBaseManager.getConnectionInfo();
    }

    @Override
    public String getHost() {
        return dataBaseManager.getHost();
    }

    @Override
    public String getUser() {
        return dataBaseManager.getUser();
    }

    @Override
    public String[] getAttDefinitionData(final String argin) throws DevFailed {
        try {
            return DbUtils.toStringArray(dataBaseManager.getAttribute().getAttDefinitionData(argin));
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getAttPropertiesData(final String argin) throws DevFailed {
        try {
            IAdtAptAttributes adtAptAttributes = dataBaseManager.getAttribute();
            return DbUtils.toStringArray(adtAptAttributes.getProperties().getAttPropertiesData(argin,
                    adtAptAttributes.isCaseSentitiveDatabase()));
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

    }

    @Override
    public String getAttFullName(final int argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getNames().getAttFullName(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int getAttId(final String argin) throws DevFailed {
        int id = -1;
        if (dataBaseManager != null) {
            try {
                // argout = this.dbProxy.getDataBase().getAttID(argin);
                IAdtAptAttributes adtAptAttributes = dataBaseManager.getAttribute();
                id = adtAptAttributes.getIds().getAttID(argin, adtAptAttributes.isCaseSentitiveDatabase());
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        }
        return id;
    }

    @Override
    public String[] getAttNameAll() throws DevFailed {
        try {
            return DbUtils.toStringArray(dataBaseManager.getAttribute().getNames().getAttributes());
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getAttNameFilterFormat(final short argin) throws DevFailed {
        try {
            return DbUtils.toStringArray(dataBaseManager.getAttribute().getNames().getAttributesNamesF(argin));
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getAttNameFilterType(final short argin) throws DevFailed {
        try {
            return DbUtils.toStringArray(dataBaseManager.getAttribute().getNames().getAttributesNamesT(argin));
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int getAttCountAll() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getNames().getAttributesCount();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int getAttCountFilterFormat(final short argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getNames().getAttributesCountF(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

    }

    @Override
    public int getAttCountFilterType(final short argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getNames().getAttributesCountT(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getDomains() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getDomains().getDomains();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int getDomainsCount() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getDomains().getDomainsCount();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getFamilies() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getFamilies().getFamilies();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int getFamiliesCount() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getFamilies().getFamiliesCount();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getFamiliesByDomain(final String argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getFamilies().getFamilies(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }

    }

    @Override
    public int getFamiliesByDomainCount(final String argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getFamilies().getFamiliesCount(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getMembers() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getMembers().getMembers();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int getMembersCount() throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getMembers().getMembersCount();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getMembersByDomainFamily(final String... argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getMembers().getMembers(argin[0].trim(), argin[1].trim());
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public int GetMembersByDomainFamilyCount(final String... argin) throws DevFailed {

        if (argin.length != 2) {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        } else {
            try {
                return dataBaseManager.getAttribute().getMembers().getMembersCount(argin[0], argin[1]);
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        }
    }

    @Override
    public int getAttributesByDomainFamilyMembersCount(final String... argin) throws DevFailed {
        try {
            return dataBaseManager.getAttribute().getNames().getAttributesCount(argin[0], argin[1], argin[2]);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getCurrentArchivedAtt() throws DevFailed {
        try {
            return dataBaseManager.getMode().getCurrentArchivedAtt();
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public boolean isArchived(final String argin) throws DevFailed {
        try {
            return dataBaseManager.getMode().isArchived(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String[] getArchivingMode(final String argin) throws DevFailed {
        String[] argout;
        try {
            final Mode mode = dataBaseManager.getMode().getCurrentArchivingMode(argin);
            argout = mode == null ? null : mode.toArray();
            if (argout == null || argout.length == 0) {
                throw DevFailedUtils.newDevFailed("Invalid attribute: " + argin, "Invalid attribute: " + argin);
            }
        } catch (ArchivingException e) {
            throw e.toTangoException();

        }
        return argout;
    }

    @Override
    public int getAttDataCount(final String argin) throws DevFailed {
        try {
            exportData(argin);
            return extractor.getDataGetters().getAttDataCount(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public double getAttDataAvg(final String argin) throws DevFailed {
        try {
            exportData(argin);
            return extractor.getMinMaxAvgGetters().getAttDataAvg(argin);
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
    }

    @Override
    public String getNewestValue(final String attributeName) throws DevFailed {
        String argout;
        try {
            exportData(attributeName);
            final DbData data = extractor.getDataGetters().getNewestValue(attributeName);
            if (data.getTimedData() != null && data.getTimedData()[0] != null) {
                argout = data.getTimedData()[0].getTime() + "; ";
                final Object value = getFirstArrayValue(data.getTimedData()[0].getValue());
                argout = argout + value;
            } else {
                throw DevFailedUtils.newDevFailed("no data found for " + attributeName);
            }
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        return argout;
    }

    @Override
    public String getNearestValue(final String... argin) throws DevFailed {
        String argout;
        if (argin.length == 2) {
            final String attributeName = argin[0];
            final String timestamp = argin[1];
            try {
                exportData(attributeName);
                final DbData[] data2 = extractor.getDataGetters().getNearestValue(attributeName, timestamp);
                if ((data2 != null) && (data2.length == 2) && ((data2[0] != null) || (data2[1] != null))) {
                    DbData refData = DbData.getFirstDbData(data2);
                    if ((refData.getTimedData() != null) && (refData.getTimedData()[0] != null)) {
                        argout = refData.getTimedData()[0].getTime() + "; ";
                        final Object value = getFirstArrayValue(refData.getTimedData()[0].getValue());
                        argout = argout + value;
                    } else {
                        throw buildNoDataDevFailed(attributeName);
                    }
                } else {
                    throw buildNoDataDevFailed(attributeName);
                }
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        } else {
            throw DevFailedUtils.newDevFailed("input must be of size 2 [attributeName, timestamp] ");
        }
        return argout;
    }

    @Override
    public double getAttDataAvgBetweenDates(final String... argin) throws DevFailed {
        double argout;
        if (argin.length == 3) {
            try {
                exportData(argin[0]);
                argout = extractor.getMinMaxAvgGettersBetweenDates().getAttDataAvgBetweenDates(argin);
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        } else {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        }
        return argout;
    }

    @Override
    public DevVarDoubleStringArray extractBetweenDates(final boolean withErrors, final String... argin) throws DevFailed {
        DevVarDoubleStringArray argout = null;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("Wrong number of parameters");
            } else {
                try {
                    // Get data from db.
                    exportData(argin[0]);
                    final DbData[] dbData = extractor.getDataGettersBetweenDates()
                            .getAttDataBetweenDates(SamplingType.getSamplingType(SamplingType.ALL), argin);
                    DbData refData = DbData.getFirstDbData(dbData);
                    final NullableTimedData[] tuples = refData.getTimedData();
                    final double[] timeStamps = new double[tuples.length];
                    final String[] values = new String[tuples.length];
                    for (int i = 0; i < tuples.length; i++) {
                        timeStamps[i] = tuples[i].getTime();
                        values[i] = String.valueOf(getFirstArrayValue(tuples[i].getValue()));
                    }
                    argout = new DevVarDoubleStringArray(timeStamps, values);
                } catch (final ArchivingException e) {
                    e.printStackTrace();
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataBetweenDatesCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                exportData(argin[0]);
                try {
                    argout = extractor.getDataGettersBetweenDates().getAttDataBetweenDatesCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataInfOrSupThanCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGetters().getAttDataInfOrSupThanCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataInfOrSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 5) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGettersBetweenDates().getAttDataInfOrSupThanBetweenDatesCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataInfThanCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 2) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGetters().getAttDataInfThanCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 4) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGettersBetweenDates().getAttDataInfThanBetweenDatesCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public double getAttDataMax(final String argin) throws DevFailed {

        double argout = 0;
        if (dataBaseManager != null) {
            try {
                exportData(argin);
                argout = extractor.getMinMaxAvgGetters().getAttDataMax(argin);
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        }
        return argout;
    }

    @Override
    public double getAttDataMaxBetweenDates(final String... argin) throws DevFailed {
        double argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getMinMaxAvgGettersBetweenDates().getAttDataMaxBetweenDates(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public double getAttDataMin(final String argin) throws DevFailed {

        double argout = 0;
        if (dataBaseManager != null) {
            try {
                exportData(argin);
                argout = extractor.getMinMaxAvgGetters().getAttDataMin(argin);
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        }
        return argout;
    }

    @Override
    public double getAttDataMinBetweenDates(final String... argin) throws DevFailed {

        double argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getMinMaxAvgGettersBetweenDates().getAttDataMinBetweenDates(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataSupThanCount(final String... argin) throws DevFailed {

        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 2) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGetters().getAttDataSupThanCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataSupAndInfThanCount(final String... argin) throws DevFailed {

        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGetters().getAttDataSupAndInfThanCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataSupAndInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 5) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGettersBetweenDates().getAttDataSupAndInfThanBetweenDatesCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public int getAttDataSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        int argout = 0;
        if (dataBaseManager != null) {
            if (argin.length != 4) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    argout = extractor.getInfSupGettersBetweenDates().getAttDataSupThanBetweenDatesCount(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return argout;
    }

    @Override
    public String getMaxTime(final String argin) throws DevFailed {
        String argout;
        try {
            final Timestamp last = dataBaseManager.getDbUtil().getTimeOfLastInsert(argin, true);
            if (last == null) {
                argout = "NO VALUES RECORDED";
            } else {
                argout = last.toString();
            }
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        return argout;
    }

    @Override
    public String getMinTime(final String argin) throws DevFailed {
        String argout;
        try {
            final Timestamp last = dataBaseManager.getDbUtil().getTimeOfLastInsert(argin, false);
            if (last == null) {
                argout = "NO VALUES RECORDED";
            } else {
                argout = last.toString();
            }
        } catch (final ArchivingException e) {
            throw e.toTangoException();
        }
        return argout;
    }

    @Override
    public DevVarLongStringArray getAttDataBetweenDates(final boolean withErrors, final String... argin) throws DevFailed {
        DbData[] dbData;
        if (argin.length != 3) {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        } else {
            try {
                // Get data from db.
                exportData(argin[0]);
                dbData = extractor.getDataGettersBetweenDates()
                        .getAttDataBetweenDates(SamplingType.getSamplingType(SamplingType.ALL), argin);
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataBetweenDatesSampling(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        SamplingType samplingType;
        if (argin.length != 4) {
            throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
        }
        try {
            samplingType = SamplingType.getSamplingTypeByLabel(argin[3]);
        } catch (final IllegalArgumentException iae) {
            throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Invalid sampling type. Valid types are ALL, SECOND, MINUTE, HOUR, or DAY");
        }
        if (dataBaseManager != null) {
            try {
                // Get data from db.
                exportData(argin[0]);
                dbData = extractor.getDataGettersBetweenDates().getAttDataBetweenDates(samplingType,
                        argin);

            } catch (final ArchivingException e) {
                e.printStackTrace();
                throw e.toTangoException();
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataInfOrSupThan(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    dbData = extractor.getInfSupGetters().getAttDataInfOrSupThan(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataInfOrSupThanBetweenDates(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 5) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    dbData = extractor.getInfSupGettersBetweenDates()
                            .getAttDataInfOrSupThanBetweenDates(argin);

                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataInfThan(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 2) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    dbData = extractor.getInfSupGetters().getAttDataInfThan(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataInfThanBetweenDates(final String... argin) throws DevFailed {
        DbData[] argout = null;
        if (dataBaseManager != null) {
            if (argin.length != 4) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    argout = extractor.getInfSupGettersBetweenDates()
                            .getAttDataInfThanBetweenDates(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, argout);
    }

    @Override
    public DevVarLongStringArray getAttDataLastN(final String... argin) throws DevFailed {
        DbData[] argout = null;
        if (dataBaseManager != null) {
            if (argin.length != 2) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    argout = extractor.getDataGetters().getAttDataLastN(argin);
                } catch (final ArchivingException e) {
                    e.printStackTrace();
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, argout);
    }

    @Override
    public DevVarLongStringArray getAttDataSupThan(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 2) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    // Get data from db.
                    exportData(argin[0]);
                    dbData = extractor.getInfSupGetters().getAttDataSupThan(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataSupAndInfThan(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 3) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    dbData = extractor.getInfSupGetters().getAttDataSupAndInfThan(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataSupAndInfThanBetweenDates(final String... argin) throws DevFailed {
        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 5) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    // Get data from db.
                    exportData(argin[0]);
                    dbData = extractor.getInfSupGettersBetweenDates()
                            .getAttDataSupAndInfThanBetweenDates(argin);

                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    @Override
    public DevVarLongStringArray getAttDataSupThanBetweenDates(final String... argin) throws DevFailed {

        DbData[] dbData = null;
        if (dataBaseManager != null) {
            if (argin.length != 4) {
                throw DevFailedUtils.newDevFailed("CONFIGURATION_ERROR", "Wrong number of parameters");
            } else {
                try {
                    exportData(argin[0]);
                    // Get data from db.
                    dbData = extractor.getInfSupGettersBetweenDates()
                            .getAttDataSupThanBetweenDates(argin);
                } catch (final ArchivingException e) {
                    throw e.toTangoException();
                }
            }
        }
        return addAttribute(deviceManager, dynamicManager, dbData);
    }

    private Object getFirstArrayValue(Object value) {
        Object result;
        if ((value != null) && value.getClass().isArray()) {
            result = Array.get(value, 0);
        } else {
            result = null;
        }
        return result;
    }

    public void exportData(String attributeName) throws DevFailed {
        if (dataBaseManager instanceof TDBDataBaseManager) {
            LOGGER.debug("forcing export of file for {}", attributeName);
            try {
                ((TDBDataBaseManager) dataBaseManager).getTdbExport().ExportData2Tdb(attributeName,
                        new Timestamp(System.currentTimeMillis()).toString());
            } catch (ArchivingException e) {
                throw e.toTangoException();
            }
        }
    }

}
