package org.tango.server.extractor;

import org.tango.archiving.server.extractor.HdbExtractor;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Device;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoDs.TangoConst;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.common.api.tools.NullableTimedData;

@Device
public class MockHdbExtractor extends HdbExtractor {

    public final static String NO_DB_DEVICE_NAME = "tmp/test/dev";
    public final static String INSTANCE_NAME = "1";
    public final static String SERVER_NAME = MockHdbExtractor.class.getSimpleName() + "/" + INSTANCE_NAME;

    // FIXME: attribute history does not work with no db device
    /*  public static void startNoDb(final int portNr) throws DevFailed {
        try {
            System.setProperty("OAPort", Integer.toString(portNr));
            ServerManager.getInstance().addClass(MockHdbExtractor.class.getCanonicalName(), MockHdbExtractor.class);
            ServerManager.getInstance().startError(new String[]{INSTANCE_NAME, "-nodb", "-dlist", NO_DB_DEVICE_NAME},
                    SERVER_NAME);
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }*/

    public static void start() throws DevFailed {
        ServerManager.getInstance().addClass(MockHdbExtractor.class.getCanonicalName(), MockHdbExtractor.class);
        ServerManager.getInstance().startError(new String[] { INSTANCE_NAME }, MockHdbExtractor.class.getSimpleName());
    }

    /*  @Init
    @Override
    public void init() throws DevFailed {
        super.setState(DeviceState.ON);
        super.setStatus("ready");
        System.out.println("init mock device");
    }*/

    @Override
    protected void connectDatabase() throws DevFailed {
        databaseAccess = new DummyDatabase();
    }

    @Command
    public DevVarLongStringArray getAttDataBetweenDatesRead() throws DevFailed {
        NullableTimedData[] timedData = new NullableTimedData[1000];
        for (int i = 0; i < 1000; i++) {
            timedData[i] = new NullableTimedData();
            timedData[i].setDataType(TangoConst.Tango_DEV_DOUBLE);
            timedData[i].setValue(new double[] { 12.3 + i }, null);
            timedData[i].setTime(System.currentTimeMillis());
            timedData[i].setX(1);
        }
        final DbData dbData = new DbData("dummy/dummy/dummy/dummy");
        dbData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        dbData.setDataFormat(AttrDataFormat._SCALAR);
        dbData.setTimedData(timedData);
        dbData.setWritable(AttrWriteType._READ);
        return databaseAccess.addAttribute(deviceManager, dynamicManager, new DbData[] { dbData, null });
    }

    @Command
    public DevVarLongStringArray getAttDataBetweenDatesSpectrumRead() throws DevFailed {
        NullableTimedData[] timedData = new NullableTimedData[300];
        for (int i = 0; i < 300; i++) {
            timedData[i] = new NullableTimedData();
            timedData[i].setDataType(TangoConst.Tango_DEV_DOUBLE);
            timedData[i].setValue(new double[] { 12.3 + i, i, i - 1 }, null);
            timedData[i].setTime(System.currentTimeMillis());
            timedData[i].setX(3);
        }
        final DbData dbData = new DbData("dummy/dummy/dummy/dummy");
        dbData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        dbData.setDataFormat(AttrDataFormat._SPECTRUM);
        dbData.setTimedData(timedData);
        dbData.setWritable(AttrWriteType._READ);
        return databaseAccess.addAttribute(deviceManager, dynamicManager, new DbData[] { dbData, null });
    }

    @Command
    public DevVarLongStringArray getAttDataBetweenDatesWrite() throws DevFailed {
        NullableTimedData[] readTimedData = new NullableTimedData[3], writeTimedData = new NullableTimedData[3];
        for (int i = 0; i < 3; i++) {
            readTimedData[i] = new NullableTimedData();
            writeTimedData[i] = new NullableTimedData();
            readTimedData[i].setDataType(TangoConst.Tango_DEV_DOUBLE);
            writeTimedData[i].setDataType(TangoConst.Tango_DEV_DOUBLE);
            readTimedData[i].setValue(new double[] { 12.3 + i }, null);
            writeTimedData[i].setValue(new double[] { i }, null);
            long time = System.currentTimeMillis();
            readTimedData[i].setTime(time);
            writeTimedData[i].setTime(time);
            readTimedData[i].setX(1);
            writeTimedData[i].setX(1);
        }

        final DbData readData = new DbData("dummy/dummy/dummy/dummy");
        final DbData writeData = new DbData("dummy/dummy/dummy/dummy");
        readData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        writeData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        readData.setDataFormat(AttrDataFormat._SCALAR);
        writeData.setDataFormat(AttrDataFormat._SCALAR);
        readData.setTimedData(readTimedData);
        writeData.setTimedData(writeTimedData);
        readData.setWritable(AttrWriteType._READ_WRITE);
        writeData.setWritable(AttrWriteType._READ_WRITE);
        return databaseAccess.addAttribute(deviceManager, dynamicManager, new DbData[] { readData, writeData });
    }

    @Command
    public DevVarLongStringArray getAttDataBetweenDatesWriteOnly() throws DevFailed {
        NullableTimedData[] timedData = new NullableTimedData[3];
        for (int i = 0; i < 3; i++) {
            timedData[i] = new NullableTimedData();
            timedData[i].setDataType(TangoConst.Tango_DEV_DOUBLE);
            timedData[i].setValue(new double[] { 12.3 + i }, null);
            timedData[i].setTime(System.currentTimeMillis());
            timedData[i].setX(1);
        }

        final DbData dbData = new DbData("dummy/dummy/dummy/dummy");
        dbData.setDataType(TangoConst.Tango_DEV_DOUBLE);
        dbData.setDataFormat(AttrDataFormat._SCALAR);
        dbData.setTimedData(timedData);
        dbData.setWritable(AttrWriteType._WRITE);

        return databaseAccess.addAttribute(deviceManager, dynamicManager, new DbData[] { null, dbData });
    }

}
