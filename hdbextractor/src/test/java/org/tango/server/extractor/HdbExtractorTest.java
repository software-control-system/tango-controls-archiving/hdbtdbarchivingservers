package org.tango.server.extractor;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DeviceDataHistory;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;


public class HdbExtractorTest {

    private static String deviceName;

    @BeforeClass
    public static void before() throws DevFailed {
        try {
            //System.setProperty("TANGO_HOST", "192.168.56.101:10000");
            //System.setProperty("TANGO_HOST", "dev-db1:20001,dev-db1:20002");
            // MockHdbExtractor.startNoDb(1234);
            //  deviceName = "tango://localhost:1234/" + MockHdbExtractor.NO_DB_DEVICE_NAME + "#dbase=no";
            deviceName = MockHdbExtractor.NO_DB_DEVICE_NAME;
            Database tangoDb = ApiUtil.get_db_obj();
            String serverName = MockHdbExtractor.SERVER_NAME;
            System.out.println("add server " + serverName);
            System.out.println("add device " + deviceName);
            tangoDb.add_device(deviceName, MockHdbExtractor.class.getCanonicalName(), serverName);
            tangoDb.get_device_list("");
            MockHdbExtractor.start();
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }

    }

    @After
    public void after() throws DevFailed {
        TangoCommand cmd = new TangoCommand(deviceName, "RemoveDynamicAttributes");
        cmd.execute();
    }

    @Test
    public void connect() throws DevFailed {
        DeviceProxy dev = new DeviceProxy(deviceName);
        System.out.println(dev.state());
        System.out.println(dev.status());

    }

    @Test
    public void extractReadOnly() throws DevFailed {
        try {
            DeviceProxy dev = new DeviceProxy(deviceName);
            TangoCommand cmd = new TangoCommand(deviceName, "getAttDataBetweenDatesRead");
            cmd.execute();

            String[] result = cmd.getStringMixArrayArgout();
            System.out.println("result " + Arrays.toString(result));

            long size = cmd.getNumLongMixArrayArgout()[0];
            assertThat(size, equalTo(1000L));

            DeviceDataHistory[] history = dev.attribute_history(result[0], (int) size);
            assertThat(history.length, equalTo(1000));
            for (int i = 0; i < history.length; i++) {
                System.out.println(history[i].extractDouble());
                assertThat(history[i].extractDouble(), equalTo(i + 12.3));
            }
            TangoAttribute attribute = new TangoAttribute(deviceName + "/" + result[0]);
            assertThat(attribute.read(), equalTo(1011.3));
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void extractSpectrumReadOnly() throws DevFailed {
        try {
            DeviceProxy dev = new DeviceProxy(deviceName);
            TangoCommand cmd = new TangoCommand(deviceName, "getAttDataBetweenDatesSpectrumRead");
            cmd.execute();
            long size = cmd.getNumLongMixArrayArgout()[0];
            assertThat(size, equalTo(300L));
            String[] result = cmd.getStringMixArrayArgout();
            System.out.println("result " + Arrays.toString(result));
            DeviceDataHistory[] history = dev.attribute_history(result[0], (int) size);
            for (int i = 0; i < history.length; i++) {
                assertThat(history[i].extractDoubleArray(), equalTo(new double[]{12.3 + i, i, i - 1}));
            }
            TangoAttribute attribute = new TangoAttribute(deviceName + "/" + result[0]);
            assertThat(attribute.read(), equalTo(new double[]{311.3, 299.0, 298.0}));
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void extractReadWrite() throws DevFailed {
        try {
            DeviceProxy dev = new DeviceProxy(deviceName);
            TangoCommand cmd = new TangoCommand(deviceName, "getAttDataBetweenDatesWrite");
            cmd.execute();
            long size = cmd.getNumLongMixArrayArgout()[0];
            assertThat(size, equalTo(3L));
            String[] result = cmd.getStringMixArrayArgout();
            System.out.println("result " + Arrays.toString(result));
            //read part
            DeviceDataHistory[] history = dev.attribute_history(result[0], (int) size);
            for (int i = 0; i < history.length; i++) {
                System.out.println("read " + history[i].extractDouble());
                assertThat(history[i].extractDoubleArray(), equalTo(new double[]{i + 12.3,i}));
            }
            //write part
/*            DeviceDataHistory[] historyWrite = dev.attribute_history(result[1], (int) size);

            for (int i = 0; i < historyWrite.length; i++) {
                System.out.println(ToStringBuilder.reflectionToString(historyWrite[i]));
                System.out.println("write " + historyWrite[i].extractDouble());
                assertThat(historyWrite[i].extractDouble(), equalTo((double)i));
            }*/
            TangoAttribute attribute = new TangoAttribute(deviceName + "/" + result[0]);
            assertThat(attribute.read(), equalTo(14.3));
            assertThat(attribute.readWritten(), equalTo(2.0));
          //  TangoAttribute attributeWrite = new TangoAttribute(deviceName + "/" + result[1]);
           // assertThat(attributeWrite.read(), equalTo(2.0));
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }

    @Test
    public void extractWriteOnly() throws DevFailed {
        try {
            DeviceProxy dev = new DeviceProxy(deviceName);
            TangoCommand cmd = new TangoCommand(deviceName, "getAttDataBetweenDatesWriteOnly");
            cmd.execute();
            long size = cmd.getNumLongMixArrayArgout()[0];
            assertThat(size, equalTo(3L));
            String[] result = cmd.getStringMixArrayArgout();
            System.out.println("result " + Arrays.toString(result));

            DeviceDataHistory[] history = dev.attribute_history(result[0], (int) size);
            for (int i = 0; i < history.length; i++) {
                assertThat(history[i].extractDouble(), equalTo(i + 12.3));
            }
            TangoAttribute attribute = new TangoAttribute(deviceName + "/" + result[0]);
            assertThat(attribute.read(), equalTo(14.3));
        } catch (DevFailed e) {
            DevFailedUtils.printDevFailed(e);
            throw e;
        }
    }
}
