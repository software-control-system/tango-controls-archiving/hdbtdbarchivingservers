package org.tango.server.extractor;

import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.tango.archiving.server.extractor.IArchivingAccess;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarDoubleStringArray;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.soleil.database.connection.DataBaseParameters;

public class DummyDatabase implements IArchivingAccess {
    @Override
    public void connectDatabase(final DatabaseConnectionConfig params) throws DevFailed {

    }

    @Override
    public String getInfo() {
        return null;
    }

    @Override
    public String getHost() {
        return null;
    }

    @Override
    public String getUser() {
        return null;
    }

    @Override
    public String[] getAttDefinitionData(final String argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public String[] getAttPropertiesData(final String argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public String getAttFullName(final int argin) throws DevFailed {
        return null;
    }

    @Override
    public int getAttId(final String argin) throws DevFailed {
        return 0;
    }

    @Override
    public String[] getAttNameAll() throws DevFailed {
        return new String[0];
    }

    @Override
    public String[] getAttNameFilterFormat(final short argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public String[] getAttNameFilterType(final short argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public int getAttCountAll() throws DevFailed {
        return 0;
    }

    @Override
    public int getAttCountFilterFormat(final short argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttCountFilterType(final short argin) throws DevFailed {
        return 0;
    }

    @Override
    public String[] getDomains() throws DevFailed {
        return new String[0];
    }

    @Override
    public int getDomainsCount() throws DevFailed {
        return 0;
    }

    @Override
    public String[] getFamilies() throws DevFailed {
        return new String[0];
    }

    @Override
    public int getFamiliesCount() throws DevFailed {
        return 0;
    }

    @Override
    public String[] getFamiliesByDomain(final String argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public int getFamiliesByDomainCount(final String argin) throws DevFailed {
        return 0;
    }

    @Override
    public String[] getMembers() throws DevFailed {
        return new String[0];
    }

    @Override
    public int getMembersCount() throws DevFailed {
        return 0;
    }

    @Override
    public String[] getMembersByDomainFamily(final String... argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public int GetMembersByDomainFamilyCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttributesByDomainFamilyMembersCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public String[] getCurrentArchivedAtt() throws DevFailed {
        return new String[0];
    }

    @Override
    public boolean isArchived(final String argin) throws DevFailed {
        return false;
    }

    @Override
    public String[] getArchivingMode(final String argin) throws DevFailed {
        return new String[0];
    }

    @Override
    public int getAttDataCount(final String argin) throws DevFailed {
        return 0;
    }

    @Override
    public double getAttDataAvg(final String argin) throws DevFailed {
        return 0;
    }

    @Override
    public String getNewestValue(final String attributeName) throws DevFailed {
        return null;
    }

    @Override
    public String getNearestValue(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public double getAttDataAvgBetweenDates(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public DevVarDoubleStringArray extractBetweenDates(final boolean withErrors, final String... argin)
            throws DevFailed {
        return null;
    }

    @Override
    public int getAttDataBetweenDatesCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataInfOrSupThanCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataInfOrSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataInfThanCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public double getAttDataMax(final String argin) throws DevFailed {
        return 0;
    }

    @Override
    public double getAttDataMaxBetweenDates(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public double getAttDataMin(final String argin) throws DevFailed {
        return 0;
    }

    @Override
    public double getAttDataMinBetweenDates(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataSupThanCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataSupAndInfThanCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataSupAndInfThanBetweenDatesCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public int getAttDataSupThanBetweenDatesCount(final String... argin) throws DevFailed {
        return 0;
    }

    @Override
    public String getMaxTime(final String argin) throws DevFailed {
        return null;
    }

    @Override
    public String getMinTime(final String argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataBetweenDates(final boolean withErrors, final String... argin)
            throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataBetweenDatesSampling(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfOrSupThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfOrSupThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataInfThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataLastN(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupAndInfThan(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupAndInfThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }

    @Override
    public DevVarLongStringArray getAttDataSupThanBetweenDates(final String... argin) throws DevFailed {
        return null;
    }
}
