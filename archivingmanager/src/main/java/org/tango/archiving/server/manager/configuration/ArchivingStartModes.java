package org.tango.archiving.server.manager.configuration;

import fr.soleil.tango.archiving.config.InsertionModes;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class ArchivingStartModes {
    private boolean isPeriodic = true;
    private int periodPeriodic = 0;
    private boolean isAbsolute = false;
    private boolean isSlowDriftAbsolute;
    private Integer periodAbsolute = 0;
    private Double decreaseDeltaAbsolute = 0.0;
    private Double increaseDeltaAbsolute = 0.0;
    private boolean isRelative = false;
    private Integer periodRelative = 0;
    private boolean isSlowDriftRelative = false;
    private Double decreasePercentRelative = 0.0;
    private Double increasePercentRelative = 0.0;
    private boolean isThreshold = false;
    private Integer periodThreshold = 0;
    private Double minThresholdValue = 0.0;
    private Double maxThresholdValue = 0.0;
    private boolean isDifference = false;
    private Integer periodDifference = 0;
    private boolean isEvent = false;

    public boolean isPeriodic() {
        return isPeriodic;
    }

    public void setPeriodic(final boolean periodic) {
        isPeriodic = periodic;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("isPeriodic", isPeriodic)
                .append("periodPeriodic", periodPeriodic)
                .append("isAbsolute", isAbsolute)
                .append("isSlowDriftAbsolute", isSlowDriftAbsolute)
                .append("periodAbsolute", periodAbsolute)
                .append("decreaseDeltaAbsolute", decreaseDeltaAbsolute)
                .append("increaseDeltaAbsolute", increaseDeltaAbsolute)
                .append("isRelative", isRelative)
                .append("periodRelative", periodRelative)
                .append("isSlowDriftRelative", isSlowDriftRelative)
                .append("decreasePercentRelative", decreasePercentRelative)
                .append("increasePercentRelative", increasePercentRelative)
                .append("isThreshold", isThreshold)
                .append("periodThreshold", periodThreshold)
                .append("minThresholdValue", minThresholdValue)
                .append("maxThresholdValue", maxThresholdValue)
                .append("isDifference", isDifference)
                .append("periodDifference", periodDifference)
                .append("isEvent", isEvent)
                .toString();
    }

    public int getPeriodPeriodic() {
        return periodPeriodic;
    }

    public void setPeriodPeriodic(final int periodPeriodic) {
        this.periodPeriodic = periodPeriodic;
    }

    public boolean isAbsolute() {
        return isAbsolute;
    }

    public void setAbsolute(final boolean absolute) {
        isAbsolute = absolute;
    }

    public boolean isSlowDriftAbsolute() {
        return isSlowDriftAbsolute;
    }

    public void setSlowDriftAbsolute(final boolean slowDriftAbsolute) {
        isSlowDriftAbsolute = slowDriftAbsolute;
    }

    public Integer getPeriodAbsolute() {
        return periodAbsolute;
    }

    public void setPeriodAbsolute(final Integer periodAbsolute) {
        this.periodAbsolute = periodAbsolute;
    }

    public Double getDecreaseDeltaAbsolute() {
        return decreaseDeltaAbsolute;
    }

    public void setDecreaseDeltaAbsolute(final Double decreaseDeltaAbsolute) {
        this.decreaseDeltaAbsolute = decreaseDeltaAbsolute;
    }

    public Double getIncreaseDeltaAbsolute() {
        return increaseDeltaAbsolute;
    }

    public void setIncreaseDeltaAbsolute(final Double increaseDeltaAbsolute) {
        this.increaseDeltaAbsolute = increaseDeltaAbsolute;
    }

    public boolean isRelative() {
        return isRelative;
    }

    public void setRelative(final boolean relative) {
        isRelative = relative;
    }

    public Integer getPeriodRelative() {
        return periodRelative;
    }

    public void setPeriodRelative(final Integer periodRelative) {
        this.periodRelative = periodRelative;
    }

    public boolean isSlowDriftRelative() {
        return isSlowDriftRelative;
    }

    public void setSlowDriftRelative(final boolean slowDriftRelative) {
        isSlowDriftRelative = slowDriftRelative;
    }

    public Double getDecreasePercentRelative() {
        return decreasePercentRelative;
    }

    public void setDecreasePercentRelative(final Double decreasePercentRelative) {
        this.decreasePercentRelative = decreasePercentRelative;
    }

    public Double getIncreasePercentRelative() {
        return increasePercentRelative;
    }

    public void setIncreasePercentRelative(final Double increasePercentRelative) {
        this.increasePercentRelative = increasePercentRelative;
    }

    public boolean isThreshold() {
        return isThreshold;
    }

    public void setThreshold(final boolean threshold) {
        isThreshold = threshold;
    }

    public Integer getPeriodThreshold() {
        return periodThreshold;
    }

    public void setPeriodThreshold(final Integer periodThreshold) {
        this.periodThreshold = periodThreshold;
    }

    public Double getMinThresholdValue() {
        return minThresholdValue;
    }

    public void setMinThresholdValue(final Double minThresholdValue) {
        this.minThresholdValue = minThresholdValue;
    }

    public Double getMaxThresholdValue() {
        return maxThresholdValue;
    }

    public void setMaxThresholdValue(final Double maxThresholdValue) {
        this.maxThresholdValue = maxThresholdValue;
    }

    public boolean isDifference() {
        return isDifference;
    }

    public void setDifference(final boolean difference) {
        isDifference = difference;
    }

    public Integer getPeriodDifference() {
        return periodDifference;
    }

    public void setPeriodDifference(final Integer periodDifference) {
        this.periodDifference = periodDifference;
    }

    public boolean isEvent() {
        return isEvent;
    }

    public void setEvent(final boolean event) {
        isEvent = event;
    }
}
