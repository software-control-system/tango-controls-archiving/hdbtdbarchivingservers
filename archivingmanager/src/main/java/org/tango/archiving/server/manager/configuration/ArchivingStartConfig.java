package org.tango.archiving.server.manager.configuration;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class ArchivingStartConfig {
    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("attributeFullName", attributeFullName)
                .append("archiverName", archiverName)
                .append("modes", modes)
                .toString();
    }

    private String attributeFullName = "";
    private String archiverName = "";
    private ArchivingStartModes modes = new ArchivingStartModes();

    public String getAttributeFullName() {
        return attributeFullName;
    }

    public void setAttributeFullName(final String attributeFullName) {
        this.attributeFullName = attributeFullName;
    }

    public String getArchiverName() {
        return archiverName;
    }

    public void setArchiverName(final String archiverName) {
        this.archiverName = archiverName;
    }

    public ArchivingStartModes getModes() {
        return modes;
    }

    public void setModes(final ArchivingStartModes modes) {
        this.modes = modes;
    }


}
