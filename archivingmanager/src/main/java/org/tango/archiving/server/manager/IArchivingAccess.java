package org.tango.archiving.server.manager;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;

import java.util.Set;

public interface IArchivingAccess {

    void connect(final DatabaseConnectionConfig params, boolean isHistoric) throws DevFailed;


    String archivingStop(final String[] argin) throws DevFailed;


    void archivingModif(final String[] argin) throws DevFailed;


    void archivingStart(final String[] argin) throws DevFailed;

    void archivingStartJson(final String jsonAsString, final int contextId) throws DevFailed;


    short[] isArchived(final String[] argin) throws DevFailed;

    String[] getArchivingMode(final String argin) throws DevFailed;


    String[] getStatus(final String[] argin) throws DevFailed;

    Set<String> getExportedArchiverList();

    String[] getAllContexts() throws DevFailed;

    int createNewContext(final String contextName, String description) throws DevFailed;

    void shiftArchiver(final String attributeName, final String archiverName) throws DevFailed;
}
