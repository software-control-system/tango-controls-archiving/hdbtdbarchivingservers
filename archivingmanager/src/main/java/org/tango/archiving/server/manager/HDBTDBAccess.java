package org.tango.archiving.server.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.manager.ArchivingManagerApiRefFactory;
import fr.soleil.archiving.hdbtdb.api.manager.IArchivingManagerApiRef;
import fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig;
import fr.soleil.archiving.hdbtdb.api.tools.AttributeLightMode;
import fr.soleil.archiving.hdbtdb.api.tools.mode.*;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.server.manager.configuration.ArchivingStartConfig;
import org.tango.utils.DevFailedUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HDBTDBAccess implements IArchivingAccess {

    private final Logger logger = LoggerFactory.getLogger(HDBTDBAccess.class);
    private IArchivingManagerApiRef dbManager;

    @Override
    public void connect(final DatabaseConnectionConfig params, boolean isHistoric) throws DevFailed {
        try {
            logger.info("connecting to {}", params);
            DataBaseParameters legacyParams = new DataBaseParameters();
            legacyParams.setDbType(DataBaseParameters.DataBaseType.parseDataBaseType(params.getDbType().name()));
            legacyParams.setHost(params.getHost());
            legacyParams.setName(params.getName());
            legacyParams.setSchema(params.getSchema());
            legacyParams.setUser(params.getUser());
            legacyParams.setPassword(params.getPassword());
            legacyParams.setInactivityTimeout(params.getIdleTimeout());
            legacyParams.setMaxPoolSize((short) params.getMaxPoolSize());
            if (params.getHealthRegistry() != null) {
                legacyParams.setHealthRegistry(params.getHealthRegistry());
            }
            if (params.getMetricRegistry() != null) {
                legacyParams.setMetricRegistry(params.getMetricRegistry());
            }
            final AbstractDataBaseConnector connector = ConnectionFactory.connect(legacyParams);
            dbManager = ArchivingManagerApiRefFactory.getInstance(isHistoric, connector);
            dbManager.archivingConfigure();
        } catch (ArchivingException e) {
            logger.error("cannot connect to DB", e);
            throw e.toTangoException();
        }
    }

    @Override
    public String archivingStop(final String[] argin) throws DevFailed {
        logger.info("Entering archiving_stop()");
        try {
            dbManager.archivingStopConf(argin);
        } catch (final ArchivingException e) {
            logger.warn(e.toString(), e);
            throw e.toTangoException();
        }
        logger.info("Exiting archiving_stop()");
        return "";
    }

    @Override
    public void archivingModif(final String[] argin) throws DevFailed {
        logger.info("Entering archiving_modif()");
        final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.creationWithoutFullInformation(argin);
        try {
            dbManager.archivingStopConf(archivingMessConfig.getAttributeList());
            archivingStart(argin);
        } catch (final ArchivingException e) {
            logger.warn(e.toString(), e);
            throw e.toTangoException();
        }
        logger.info("Exiting archiving_modif()");
    }

    @Override
    public void archivingStart(final String[] argin) throws DevFailed {
        try {
            final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.creationWithoutFullInformation(argin);
            dbManager.archivingStart(archivingMessConfig);
        } catch (ArchivingException e) {
            logger.error(e.toString(), e);
            throw e.toTangoException();
        }
    }

    @Override
    public void archivingStartJson(final String configAsString, final int contextId) throws DevFailed {
        ObjectMapper objectMapper = new ObjectMapper();
        List<ArchivingStartConfig> jsonConfig = null;
        try {
            jsonConfig = objectMapper.readValue(configAsString, objectMapper.getTypeFactory().constructCollectionType(List.class, ArchivingStartConfig.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();

        jsonConfig.forEach(conf -> {
                    AttributeLightMode mode = new AttributeLightMode();
                    mode.setDevice_in_charge(conf.getArchiverName());
                    mode.setAttributeCompleteName(conf.getAttributeFullName());
                    Mode modeIn = new Mode();
                    ModePeriode modePeriode = new ModePeriode();
                    modePeriode.setPeriod(conf.getModes().getPeriodPeriodic());
                    modeIn.setModeP(modePeriode);
                    if (conf.getModes().getPeriodDifference() > 0) {
                        ModeDifference modeDifference = new ModeDifference();
                        modeDifference.setPeriod(conf.getModes().getPeriodDifference());
                        modeIn.setModeD(modeDifference);
                    }
                    if (conf.getModes().getPeriodRelative() > 0) {
                        ModeRelatif modeRelatif = new ModeRelatif();
                        modeRelatif.setPeriod(conf.getModes().getPeriodRelative());
                        modeRelatif.setPercentInf(conf.getModes().getDecreasePercentRelative());
                        modeRelatif.setPercentSup(conf.getModes().getIncreasePercentRelative());
                        modeRelatif.setSlow_drift(conf.getModes().isSlowDriftRelative());
                        modeIn.setModeR(modeRelatif);
                    }
                    if (conf.getModes().getPeriodAbsolute() > 0) {
                        ModeAbsolu modeAbsolu = new ModeAbsolu();
                        modeAbsolu.setPeriod(conf.getModes().getPeriodAbsolute());
                        modeAbsolu.setSlow_drift(conf.getModes().isSlowDriftAbsolute());
                        modeAbsolu.setValInf(conf.getModes().getDecreasePercentRelative());
                        modeAbsolu.setValSup(conf.getModes().getIncreasePercentRelative());
                        modeIn.setModeA(modeAbsolu);
                    }
                    if (conf.getModes().isEvent()) {
                        modeIn.setEventMode(new EventMode());
                    }
                    mode.setMode(modeIn);
                    archivingMessConfig.add(mode);
                }
        );
        try {
            dbManager.archivingStart(archivingMessConfig);
        } catch (ArchivingException e) {
            logger.error(e.toString(), e);
            throw e.toTangoException();
        }
    }

    @Override
    public short[] isArchived(final String[] argin) throws DevFailed {
        logger.info("Entering is_archived()");
        short[] argout = new short[argin.length];
        boolean result = false;
        for (int i = 0; i < argin.length; i++) {
            try {
                result = dbManager.isArchived(argin[i]);
            } catch (final ArchivingException e) {
                logger.warn(e.toString(), e);
                throw e.toTangoException();
            }
            if (result) {
                argout[i] = 1;
            } else {
                argout[i] = 0;
            }
        }

        logger.info("Exiting is_archived()");
        return argout;
    }

    @Override
    public String[] getArchivingMode(final String argin) throws DevFailed {
        logger.info("Entering get_archiving_mode()");
        String[] argout;
        try {
            final Mode mode = dbManager.getArchivingMode(argin);
            argout = mode.toArray();
        } catch (final ArchivingException e) {
            logger.error(e.getMessage());
            throw e.toTangoException();
        }
        logger.info("Exiting get_archiving_mode()");
        return argout;
    }

    @Override
    public String[] getStatus(final String[] argin) throws DevFailed {
        logger.info("Entering get_status()");
        String[] argout = new String[argin.length];

        for (int i = 0; i < argin.length; i++) {
            try {
                argout[i] = dbManager.getStatus(argin[i]);
            } catch (final ArchivingException e) {
                throw e.toTangoException();
            }
        }
        logger.info("Exiting get_status()");
        return argout;
    }

    @Override
    public Set<String> getExportedArchiverList() {
        return new HashSet<>(Arrays.asList(dbManager.getMExportedArchiverList()));
    }

    @Override
    public String[] getAllContexts() throws DevFailed {
        throw DevFailedUtils.newDevFailed("Not supported for HDB or TDB");
    }

    @Override
    public int createNewContext(final String contextName, String description) throws DevFailed {
        throw DevFailedUtils.newDevFailed("Not supported for HDB or TDB");
    }

    @Override
    public void shiftArchiver(final String attributeName, final String archiverName) throws DevFailed {
        Mode m;
        final ArchivingMessConfig archivingMessConfig = ArchivingMessConfig.basicObjectCreation();
        try {
            m = dbManager.getArchivingMode(attributeName);
        } catch (ArchivingException e) {
            throw e.toTangoException();
        }
        AttributeLightMode mode = new AttributeLightMode();
        mode.setDevice_in_charge(archiverName);
        mode.setAttributeCompleteName(attributeName);
        mode.setMode(m);
        archivingMessConfig.add(mode);
        try {
            dbManager.archivingStopConf(attributeName);
            dbManager.archivingStart(archivingMessConfig);
        } catch (ArchivingException e) {
            throw e.toTangoException();
        }
    }
}
