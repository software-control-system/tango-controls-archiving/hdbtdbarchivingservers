package org.tango.archiving.server.manager.configuration;

import fr.soleil.tango.archiving.config.InsertionModes;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface InsertionModesMapper {

    InsertionModesMapper INSTANCE = Mappers.getMapper(InsertionModesMapper.class);

    @Mapping(target = "event") // field target is mandatory but not used
    InsertionModes mapModes(ArchivingStartModes modes);
}
