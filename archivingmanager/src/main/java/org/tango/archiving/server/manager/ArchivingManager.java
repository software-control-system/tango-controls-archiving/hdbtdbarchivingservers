package org.tango.archiving.server.manager;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.*;
import org.tango.server.attribute.log.LogAttribute;
import org.tango.server.dynamic.DynamicManager;
import org.tango.utils.DevFailedUtils;

import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Class Description: Device of Archiving system <Br>
 * <Br>
 * <br>
 * <b>ROLE</b><br>
 * This DeviceServer is used in order to manage the archiving of exported Tango
 * attributes device. <Br>
 * <Br>
 * <br>
 * <b>ARCHIVING TYPE</b><br>
 * There is two kind of archiving : <Br>
 * <Br>
 * <Li><b>The Historical archiving</b>, manage by HdbArchiver devices.<br>
 * This kind archiving allows to store in a database all the exported tango attributes.<br>
 * It is a continuous and never deleted storage.<br>
 * The frequency use for this kind of storage is usually small. <Br>
 * <Br> <Li><b>The Temporary archiving</b>, manage by TdbArchiver devices.<br>
 * This kind archiving allows to store in a database all the exported tango attributes.<br>
 * The stored values are kept in the database for a limited time ("Keeping Window" [4 hours - 24 hours]). Before being
 * stored into the database, values are first collected into files (timestamp, read value [, write value]). Those files
 * are periodically exported to the database. This export frequency must also be given ("Export Window" [5 seconds - 1
 * hour]).<br>
 * The frequency use for this kind of storage is usually high. <Br>
 * <Br>
 * <br>
 * <b>ARCHIVING MODE</b><br>
 * An archiving sequence must be tuned to follow one (or several) of the defined archiving modes :<br> <Li>The
 * <b>Periodical mode</b>, this mode allows to archive following a defined frequency.<br> <Li>The <b>Absolute mode</b>,
 * this mode allows to archive a value only if : <br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <var>current_value</var> &gt; <var>previous_archived_value</var> +
 * <var>&delta;<sub>1</sub></var> &nbsp;&nbsp;&nbsp;&nbsp; or <br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <var>current_value</var> &lt; <var>previous_archived_value</var> -
 * <var>&delta;<sub>2</sub></var>.<br> <Li>The <b>Relative mode</b>, this mode allows to archive a value only if :<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <var>current_value</var> &gt; <var>previous_archived_value</var>
 * <var>&micro;<sub>increase</sub></var> &nbsp;&nbsp;&nbsp;&nbsp; or <br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <var>current_value</var> &lt; <var>previous_archived_value</var>
 * <var>&micro;<sub>decrease</sub></var>.<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (<var>&micro;<sub>increase</sub></var> and
 * <var>&micro;<sub>decrease</sub></var> are percentages) <Li>The <b>Threshold mode</b>, this mode allows to archive a
 * value only if :<br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <var>current_value</var> &gt;
 * <var>threshold_value<sub>sup</sub></var> &nbsp;&nbsp;&nbsp;&nbsp; or <br>
 * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <var>current_value</var> &lt;
 * <var>threshold_value<sub>min</sub></var>.<br>
 * (like alarms work).<br>
 * <Br>
 * <Br> <Li>The <b>On Calculation mode</b>, this mode allows to archive according to a user formula.<br> <Li>The <b>On
 * Difference mode</b>, this mode allows to archive when a value changes.<br> <Li>The <b>External mode</b>, this mode
 * allows to archive on the user demand.<br>
 * <br>
 * <br>
 * The 6 last mode must be associated to a periodic mode. <Br>
 * <Br>
 *
 * @author $Author: pierrejoseph $
 * @version $Revision: 1.19 $
 */

@Device
public class ArchivingManager {

    private static final String DEFAULT_ARCHIVER_CLASS = ConfigConst.HDB_CLASS_DEVICE;
    private static String VERSION;
    private final Logger logger = LoggerFactory.getLogger(ArchivingManager.class);
    @DeviceProperty(name = "DbPort")
    protected String dbPort = "";
    private Logger auditLogger;
    @DynamicManagement
    private DynamicManager dynamicManager;
    /**
     * Computer identifier on wich is settled the database. The identifier
     * can be the computer name or its IP address. <br>
     * <b>Default value : </b>
     */
    @DeviceProperty(name = "DbHost", description = "Computer identifier on wich is settled the database. The identifier can be the computer name or its IP address.")
    private String dbHost = "";
    /**
     * Database name.<br>
     * <b>Default value : </b>
     */
    @DeviceProperty(name = "DbName", description = "Database name.")
    private String dbName = "";
    /**
     * User identifier (name) used to connect the database.
     */
    @DeviceProperty(name = "DbUser", description = "User identifier (name) used to connect the database.")
    private String dbUser = "";
    /**
     * Password used to connect the database.
     */
    @DeviceProperty(name = "DbPassword", description = "Password used to connect the database.")
    private String dbPassword = "";
    /**
     * true if the ORACLE RAC connection is activated. This information is
     * appended to all device's (or attributes) name. false otherwise.<br>
     * <b>Default value : </b> false
     */
    // @DeviceProperty
    //private boolean hdbRacConnection;
    //@DeviceProperty
    //private boolean tdbRacConnection;
    @DeviceProperty(name = "DbSchema", description = "Database schema")
    private String dbSchema = "";
    @DeviceProperty(name = "DbType", description = "POSTGRESQL if you want to use Timeseries")
    private String dbType = "";
    @DeviceProperty(name = "ArchiverClass", description = "archiving device class to monitor")
    private String archiverClass = DEFAULT_ARCHIVER_CLASS;
    @State
    private DeviceState state;
    @Status
    private String status;
    private IArchivingAccess databaseAccess;

    public static void main(final String[] args) {
        VERSION = ResourceBundle.getBundle("application").getString("project.version");
        ServerManager.getInstance().start(args, ArchivingManager.class);
    }

    private static DatabaseConnectionConfig getDatabaseConnectionConfig(String archiverClassName) {
        final DatabaseConnectionConfig params = new DatabaseConnectionConfig();
        // get default value from class properties of HDBArchiver
        DatabaseConnectionConfig.TangoDbProperties tangoDbProperties =
                new DatabaseConnectionConfig.TangoDbProperties("dbType", "dbHost", "dbName",
                        "dbSchema", "dbUser", "dbPassword",
                        "dbMinPoolSize", "dbMaxPoolSize",
                        "dbCnxInactivityTimeout", "dbPort");
        // retrieve archiver class properties
        params.setParametersFromTango(archiverClassName, tangoDbProperties);
        return params;
    }

    public DynamicManager getDynamicManager() {
        return dynamicManager;
    }

    public void setDynamicManager(DynamicManager dynamicManager) {
        this.dynamicManager = dynamicManager;
    }

    public void setDbPort(String dbPort) {
        this.dbPort = dbPort;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void setDbType(String dbType) {
        this.dbType = dbType;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public void setArchiverClass(String archiverClass) {
        this.archiverClass = archiverClass;
    }

    @Attribute
    public String getVersion() {
        return VERSION;
    }

    public DeviceState getState() {
        return state;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

    @Init(lazyLoading = true)
    public void init() throws DevFailed {
        auditLogger = LoggerFactory.getLogger("audit." + ArchivingManager.class.getName());
        dynamicManager.addAttribute(new LogAttribute("audit", 100, auditLogger));
        connectDb();
        Set<String> archivers = databaseAccess.getExportedArchiverList();
        final String statusBuilder = archivers.size() + " archivers: " + archivers + "\n";
        state = DeviceState.ON;
        status = statusBuilder;
    }

    private void connectDb() throws DevFailed {
        final boolean isHdb = ConfigConst.HDB_CLASS_DEVICE.equals(archiverClass);
        final DatabaseConnectionConfig params = getDatabaseConnectionConfig(isHdb ? ConfigConst.HDB_CLASS_DEVICE : ConfigConst.TDB_CLASS_DEVICE);
        if (!dbHost.isEmpty())
            params.setHost(dbHost);
        if (!dbUser.isEmpty())
            params.setUser(dbUser);
        if (!dbPassword.isEmpty())
            params.setPassword(dbPassword);
        if (!dbName.isEmpty())
            params.setName(dbName);
        if (!dbSchema.isEmpty())
            params.setSchema(dbSchema);
        if (!dbPort.isEmpty())
            params.setPort(dbPort);
        if (!dbType.isEmpty())
            params.setDbType(DatabaseConnectionConfig.DataBaseType.parseDataBaseType(dbType));
        logger.debug("connecting to {} with parameters {}", isHdb ? "HDB" : "TDB", params);

        if (params.getDbType().equals(DatabaseConnectionConfig.DataBaseType.POSTGRESQL)) {
            databaseAccess = new TimeseriesAccess();
        } else {
            databaseAccess = new HDBTDBAccess();
        }
        databaseAccess.connect(params, isHdb);

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Execute command "ArchivingStop" on device. Stops the
     * archiving for the given attributes.
     *
     * @param argin The attribute list.
     */
    @Command(name = "ArchivingStop", inTypeDesc = "The attribute list")
    public String archivingStop(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering ArchivingStop with params {}", String.join(",", argin));
        String res = databaseAccess.archivingStop(argin);
        auditLogger.debug("Exiting ArchivingStop with result {}", res);
        return res;
    }

    /**
     * Execute command "ArchivingModif" on device. Change the mode of an
     * archiving.
     *
     * @param argin The configuration to switch to... <br>
     *              <blockquote>
     *              <ul>
     *              <li><strong>The first part :</strong>
     *              <ul>
     *              <li><var>argin</var>[0]<b> =</b> the load balancing type of the archiving<br>
     *              &quot;1&quot;, if all the attribute are archived together in the same TdbArchiver/HDBArchiver device, <br>
     *              &quot;0&quot; otherwise.
     *              <li><var>argin</var>[1]<b> =</b> the number of attributes to archive<br>
     *              <li><var>argin</var>[2] to <var>argin</var> [2 + <var>argin</var>[1] - 1] = the name of each attribute
     *              </ul>
     *              <li><strong>The second part (the <i>Mode </i>part) :</strong> <br>
     *              Let us note <i>&quot;<var>index</var>&quot; </i>the last <var>index</var> used (for example, at this
     *              point, <i><var>index</var></i> = 2]).
     *              <ul>
     *              <li><strong>If the Mode is composed of a <i>Periodical Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_P</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the period of the periodic mode in (ms)<br>
     *              <var>index</var> = <var>index</var> + 2<br>
     *              <li><strong>If the Mode is composed of an <i>Absolute Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_A</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the frequency of the <i>absolute mode </i>in (ms)<br>
     *              <var>argin</var>[<var>index</var>+ 3] = the delta value max when decreasing<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the delta value max when increasing<br>
     *              <var>index</var> = <var>index</var> + 4<br>
     *              <li><strong>If the Mode is composed of a <i>Relative Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_R</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the <i>frequency </i>of the <i>relative mode </i>in (ms)<br>
     *              <var>argin</var>[<var>index</var>+ 3] = the <i>decreasing variation </i>associated to this mode<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the <i>increasing variation </i>associated to this mode<br>
     *              <var>index</var> = <var>index</var> + 4<br>
     *              <li><strong>If the Mode is composed of an <i>Threshold Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_T</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the frequency of the <i>threshold mode </i>in (ms)<br>
     *              <var>argin</var>[<var>index</var>+ 3] = the smallest value (min) when decreasing<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the biggest value (max) when increasing<br>
     *              <var>index</var> = <var>index</var> + 4<br>
     *              <li>If the Mode is composed of a <i>On Calculation Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_C</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the <i>frequency </i>of the <i>on calculation mode </i>in (ms)
     *              <br>
     *              <var>argin</var>[<var>index</var>+ 3] = the <i>number of values</i> taken into account<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the <i>type </i>associated to this mode<br>
     *              <var>argin</var>[<var>index</var>+ 5] = Not used at the moment <br>
     *              <var>index</var> = <var>index</var> + 5<br>
     *              <li><strong>If the Mode is composed of an <i>On Difference Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_D</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the <i>frequency </i>of this<i> mode </i>(in ms)<br>
     *              <var>index</var> = <var>index</var> + 2<br>
     *              <li><strong>If the Mode is composed of an <i>External Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_E</code><br>
     *              <var>index</var> = <var>index</var> + 1<br>
     *              </ul>
     *              </ul>
     *              </blockquote>
     * @see fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig
     */
    @Command(name = "ArchivingModif", inTypeDesc = "The configuration to switch to")
    public void archivingModif(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering ArchivingModif with params {}", String.join(",", argin));
        databaseAccess.archivingModif(argin);
        auditLogger.debug("Exiting ArchivingModif");
    }

    /**
     * Execute command "ArchivingStart" on device. Start an
     * archiving for the specified attributes, and following the specified mode.
     *
     * @param argin Archiving arguments... <BR>
     *              <blockquote>
     *              <ul>
     *              <li><strong>The first part :</strong>
     *              <ul>
     *              <li><var>argin</var>[0]<b> =</b> the load balancing type of the archiving<br>
     *              &quot;1&quot;, if all the attribute are archived together in the same TdbArchiver/HdbArchiver device, <br>
     *              &quot;0&quot; otherwise.
     *              <li><var>argin</var>[1]<b> =</b> the number of attributes to archive<br>
     *              <li><var>argin</var>[2] to <var>argin</var> [2 + <var>argin</var>[1] - 1] = the name of each attribute
     *              </ul>
     *              <li><strong>The second part (the <i>Mode </i>part) :</strong> <br>
     *              Let us note <i>&quot;<var>index</var>&quot; </i>the last <var>index</var> used (for example, at this
     *              point, <i><var>index</var></i> = 2]).
     *              <ul>
     *              <li><strong>If the Mode is composed of a <i>Periodical Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_P</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the period of the periodic mode in (ms)<br>
     *              <var>index</var> = <var>index</var> + 2<br>
     *              <li><strong>If the Mode is composed of an <i>Absolute Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_A</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the frequency of the <i>absolute mode </i>in (ms)<br>
     *              <var>argin</var>[<var>index</var>+ 3] = the delta value max when decreasing<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the delta value max when increasing<br>
     *              <var>index</var> = <var>index</var> + 4<br>
     *              <li><strong>If the Mode is composed of a <i>Relative Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_R</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the <i>frequency </i>of the <i>relative mode </i>in (ms)<br>
     *              <var>argin</var>[<var>index</var>+ 3] = the <i>decreasing variation </i>associated to this mode<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the <i>increasing variation </i>associated to this mode<br>
     *              <var>index</var> = <var>index</var> + 4<br>
     *              <li><strong>If the Mode is composed of an <i>Threshold Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_T</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the frequency of the <i>threshold mode </i>in (ms)<br>
     *              <var>argin</var>[<var>index</var>+ 3] = the smallest value (min) when decreasing<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the biggest value (max) when increasing<br>
     *              <var>index</var> = <var>index</var> + 4<br>
     *              <li>If the Mode is composed of a <i>On Calculation Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_C</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the <i>frequency </i>of the <i>on calculation mode </i>in (ms)
     *              <br>
     *              <var>argin</var>[<var>index</var>+ 3] = the <i>number of values</i> taken into account<br>
     *              <var>argin</var>[<var>index</var>+ 4] = the <i>type </i>associated to this mode<br>
     *              <var>argin</var>[<var>index</var>+ 5] = Not used at the moment <br>
     *              <var>index</var> = <var>index</var> + 5<br>
     *              <li><strong>If the Mode is composed of an <i>On Difference Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_D</code><br>
     *              <var>argin</var>[<var>index</var>+ 2] = the <i>frequency </i>of this<i> mode </i>(in ms)<br>
     *              <var>index</var> = <var>index</var> + 2<br>
     *              <li><strong>If the Mode is composed of an <i>External Mode</i></strong><br>
     *              <var>argin</var>[<var>index</var>+ 1] = <code>MODE_E</code><br>
     *              <var>index</var> = <var>index</var> + 1<br>
     *              </ul>
     *              </ul>
     *              </blockquote>
     * @see fr.soleil.archiving.hdbtdb.api.tools.ArchivingMessConfig
     */
    @Command(name = "ArchivingStart", inTypeDesc = "The configuration to start")
    public void archivingStart(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering ArchivingStart with params {}", String.join(",", argin));
        databaseAccess.archivingStart(argin);
        auditLogger.debug("Exiting ArchivingStart");
    }

    /**
     * Execute command "ArchivingStartJson" on device. Start an
     * archiving for the specified attributes, and following the specified mode.
     * The configuration is given as a JSON array and you may specify a context id
     * by adding a <code>;contextId</code> at the end of your input parameter.
     * @param jsonAsString
     * @throws DevFailed
     */
    @Command(name = "ArchivingStartJson", inTypeDesc = "A JSON array. Example:\n [ {\n" +
            "  \"attributeFullName\" : \"tango/tangotest/1/ampli\",\n" +
            "  \"archiverName\" : \"tango://fokrred\",\n" +
            "  \"modes\" : {\n" +
            "    \"periodPeriodic\" : 20000,\n" +
            "    \"periodAbsolute\" : 0,\n" +
            "    \"decreaseDeltaAbsolute\" : 0.0,\n" +
            "    \"increaseDeltaAbsolute\" : 0.0,\n" +
            "    \"periodRelative\" : 1000,\n" +
            "    \"decreasePercentRelative\" : 10.0,\n" +
            "    \"increasePercentRelative\" : 10.0,\n" +
            "    \"periodThreshold\" : 0,\n" +
            "    \"minThresholdValue\" : 0.0,\n" +
            "    \"maxThresholdValue\" : 0.0,\n" +
            "    \"periodDifference\" : 0,\n" +
            "    \"absolute\" : false,\n" +
            "    \"threshold\" : false,\n" +
            "    \"periodic\" : true,\n" +
            "    \"relative\" : true,\n" +
            "    \"slowDriftRelative\" : true,\n" +
            "    \"difference\" : false,\n" +
            "    \"slowDriftAbsolute\" : false,\n" +
            "    \"event\" : false\n" +
            "  }\n" +
            "}, {\n" +
            "  \"attributeFullName\" : \"tango/tangotest/2/ampli\",\n" +
            "  \"archiverName\" : \"\",\n" +
            "  \"modes\" : {\n" +
            "    \"periodPeriodic\" : 20000,\n" +
            "    \"periodAbsolute\" : 3000,\n" +
            "    \"decreaseDeltaAbsolute\" : 12.0,\n" +
            "    \"increaseDeltaAbsolute\" : 13.0,\n" +
            "    \"periodRelative\" : 1000,\n" +
            "    \"decreasePercentRelative\" : 0.0,\n" +
            "    \"increasePercentRelative\" : 0.0,\n" +
            "    \"periodThreshold\" : 0,\n" +
            "    \"minThresholdValue\" : 0.0,\n" +
            "    \"maxThresholdValue\" : 0.0,\n" +
            "    \"periodDifference\" : 0,\n" +
            "    \"absolute\" : true,\n" +
            "    \"threshold\" : false,\n" +
            "    \"periodic\" : true,\n" +
            "    \"relative\" : false,\n" +
            "    \"slowDriftRelative\" : true,\n" +
            "    \"difference\" : false,\n" +
            "    \"slowDriftAbsolute\" : false,\n" +
            "    \"event\" : false\n" +
            "  }\n" +
            "} ]")
    public void archivingStartJson(final String jsonAsString) throws DevFailed {
        auditLogger.debug("Entering ArchivingStartJson with params {}", jsonAsString);
        String[] parts = jsonAsString.split(";");
        int contextId;
        try {
            contextId = Integer.parseInt(parts[1]);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
            contextId = -1;
        }
        databaseAccess.archivingStartJson(parts[0], contextId);
        auditLogger.debug("Exiting ArchivingStartJson");
    }

    /**
     * Execute command "IsArchived" on device. Check the archiving state
     * for each attribute of the given list.
     *
     * @param argin The attribute list.
     * @return For each attribute of the given list...<br>
     * <ul>
     * <li><code>1</code>, if the attribute is currently being archived
     * <li><code>0</code>, otherwise
     * </ul>
     */
    @Command(name = "IsArchived", inTypeDesc = "The attribute list to check", outTypeDesc = "The archiving state for each attribute")
    public short[] isArchived(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering IsArchived with params {}", String.join(",", argin));
        short[] res = databaseAccess.isArchived(argin);
        auditLogger.debug("Exiting IsArchived with result {}", Arrays.toString(res));
        return res;
    }

    /**
     * Execute command "GetArchivingMode" on device. Return the
     * archiving mode applied to an attribute.
     *
     * @param argin The attribute name.
     * @return The applied mode... <br>
     * <blockquote>
     * <ul>
     * Let us note <i>&quot;<var>index</var>&quot; </i>the last <var>index</var> used (for example, at this
     * point, <i><var>index</var></i> = 0]).
     * <li><strong>If the Mode is composed of a <i>Periodical Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_P</code><br>
     * <var>argout</var>[<var>index</var> + 1] = the period of the periodic mode in (ms)<br>
     * <var>index</var> = <var>index</var> + 2<br>
     * <li><strong>If the Mode is composed of an <i>Absolute Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_A</code><br>
     * <var>argout</var>[<var>index</var>+ 1] = the frequency of the <i>absolute mode </i>in (ms)<br>
     * <var>argout</var>[<var>index</var>+ 2] = the delta value max when decreasing<br>
     * <var>argout</var>[<var>index</var>+ 3] = the delta value max when increasing<br>
     * <var>index</var> = <var>index</var> + 4<br>
     * <li><strong>If the Mode is composed of a <i>Relative Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_R</code><br>
     * <var>argout</var>[<var>index</var>+ 1] = the <i>frequency </i>of the <i>relative mode </i>in (ms)<br>
     * <var>argout</var>[<var>index</var>+ 2] = the <i>decreasing variation </i>associated to this mode<br>
     * <var>argout</var>[<var>index</var>+ 3] = the <i>increasing variation </i>associated to this mode<br>
     * <var>index</var> = <var>index</var> + 4<br>
     * <li><strong>If the Mode is composed of an <i>Threshold Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_T</code><br>
     * <var>argout</var>[<var>index</var>+ 1] = the frequency of the <i>threshold mode </i>in (ms)<br>
     * <var>argout</var>[<var>index</var>+ 2] = the smallest value (min) when decreasing<br>
     * <var>argout</var>[<var>index</var>+ 3] = the biggest value (max) when increasing<br>
     * <var>index</var> = <var>index</var> + 4<br>
     * <li>If the Mode is composed of a <i>On Calculation Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_C</code><br>
     * <var>argout</var>[<var>index</var>+ 1] = the <i>frequency </i>of the <i>on calculation mode </i>in (ms)<br>
     * <var>argout</var>[<var>index</var>+ 2] = the <i>number of values</i> taken into account<br>
     * <var>argout</var>[<var>index</var>+ 3] = the <i>type </i>associated to this mode<br>
     * <var>argout</var>[<var>index</var>+ 4] = Not used at the moment <br>
     * <var>index</var> = <var>index</var> + 5<br>
     * <li><strong>If the Mode is composed of an <i>On Difference Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_D</code><br>
     * <var>argout</var>[<var>index</var>+ 1] = the <i>frequency </i>of this<i> mode </i>(in ms)<br>
     * <var>index</var> = <var>index</var> + 2<br>
     * <li><strong>If the Mode is composed of an <i>External Mode</i></strong><br>
     * <var>argout</var>[<var>index</var>] = <code>MODE_E</code><br>
     * <var>index</var> = <var>index</var> + 1<br>
     * </ul>
     * </blockquote>
     */
    @Command(name = "GetArchivingMode", inTypeDesc = "The attribute name", outTypeDesc = "The applied mode")
    public String[] getArchivingMode(final String argin) throws DevFailed {
        auditLogger.debug("Entering GetArchivingMode with params {}", argin);
        String[] res = databaseAccess.getArchivingMode(argin);
        auditLogger.debug("Exiting GetArchivingMode with result {}", String.join(",", res));
        return res;
    }

    /**
     * Execute command "GetStatus" on device. For each attribute of the given
     * list, get the status of the device in charge of its archiving
     *
     * @param argin The attribute list.
     * @return The list of status.
     */
    @Command(name = "GetStatus", inTypeDesc = "The attribute list", outTypeDesc = "The list of status")
    public String[] getStatus(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering GetStatus with params {}", String.join(",", argin));
        String[] res = databaseAccess.getStatus(argin);
        auditLogger.debug("Exiting GetStatus with result {}", String.join(",", res));
        return res;
    }

    /**
     * Create a new context in the database and returns the corresponding id
     *
     * @param argin contextName,contextDescription
     * @return contextId
     * @throws DevFailed if the context cannot be created or if you are using HDB or TDB
     */
    @Command(name = "CreateNewContext", inTypeDesc = "contextName,contextDescription", outTypeDesc = "contextId")
    public int createNewContext(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering CreateNewContext with params {}", String.join(",", argin));
        if (argin.length != 2) {
            throw DevFailedUtils.newDevFailed("Invalid number of arguments");
        }
        int res = databaseAccess.createNewContext(argin[0], argin[1]);
        auditLogger.debug("Exiting CreateNewContext with result {}", res);
        return res;
    }

    /**
     * Get all the contexts declared in the database.
     *
     * @return the array of contexts
     * @throws DevFailed if you are using HDB or TDB
     */
    @Command(name = "GetAllContexts", outTypeDesc = "Every contexts declared")
    public String[] getAllContexts() throws DevFailed {
        auditLogger.debug("Entering GetAllContexts");
        String[] res = databaseAccess.getAllContexts();
        auditLogger.debug("Exiting GetAllContexts with result {}", String.join(",", res));
        return res;
    }

    /**
     * Moves an attribute from one archiver to another
     * @param argin attributeName,archiverName
     * @throws DevFailed if the attribute cannot be moved since it is not archived or if it failed
     */
    @Command(name = "ShiftArchiver", inTypeDesc = "attributeName,archiverName")
    public void shiftArchiver(final String[] argin) throws DevFailed {
        auditLogger.debug("Entering ShiftArchiver with params {}", String.join(",", argin));
        if (argin.length != 2) {
            throw DevFailedUtils.newDevFailed("Invalid number of arguments");
        }
        databaseAccess.shiftArchiver(argin[0], argin[1]);
        auditLogger.debug("Exiting ShiftArchiver");
    }
}
