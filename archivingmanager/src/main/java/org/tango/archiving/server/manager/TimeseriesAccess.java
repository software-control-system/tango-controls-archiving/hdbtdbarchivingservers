package org.tango.archiving.server.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.archiving.TangoArchivingSystemConfigurationBuilder;
import fr.soleil.tango.archiving.TangoArchivingSystemConfigurationService;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.build.TangoArchivingServicesBuilder;
import fr.soleil.tango.archiving.config.AttributeConfig;
import fr.soleil.tango.archiving.config.Context;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfig;
import fr.soleil.tango.archiving.infra.tango.ArchivingConfigs;
import fr.soleil.tango.archiving.infra.tango.TangoArchiverProperties;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;
import fr.soleil.tango.archiving.services.TangoArchivingInserterService;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.server.manager.configuration.ArchivingStartConfig;
import org.tango.archiving.server.manager.configuration.InsertionModesMapper;
import org.tango.utils.DevFailedUtils;

import java.util.*;

public class TimeseriesAccess implements IArchivingAccess {

    private final static String IS_DEDICATED = "IsDedicated";
    private final static String RESERVED_ATTRIBUTES = "ReservedAttributes";
    private final static String STOP_ARCHIVE_CONF = "stopArchiveAtt";
    private final static String TRIGGER_ARCHIVE_CONF = "TriggerArchiveConf";
    private final static String SCALAR_CHARGE = "scalarCharge";
    private final static String SPECTRUM_CHARGE = "spectrumCharge";
    private final static String IMAGE_CHARGE = "imageCharge";
    private final static String INSERTION_RATE = "insertionRate";
    protected TangoArchivingConfigService configService;
    protected TangoArchivingInserterService inserterService;
    private final Logger logger = LoggerFactory.getLogger(TimeseriesAccess.class);
    private TangoArchivingSystemConfigurationService service;

    @Override
    public void connect(final DatabaseConnectionConfig params, final boolean isHistoric) throws DevFailed {
        TangoArchiverProperties properties = new TangoArchiverProperties();
        properties.setArchiverClassName("TimeseriesArchiver");
        properties.setIsDedicatedProperty(IS_DEDICATED);
        properties.setReservedAttributesProperty(RESERVED_ATTRIBUTES);
        properties.setImageLoadAttributeName(IMAGE_CHARGE);
        properties.setSpectrumLoadAttributeName(SPECTRUM_CHARGE);
        properties.setScalarLoadAttributeName(SCALAR_CHARGE);
        properties.setStartArchivingCommandName(TRIGGER_ARCHIVE_CONF);
        properties.setInsertionRateAttributeName(INSERTION_RATE);
        properties.setStopArchivingCommandName(STOP_ARCHIVE_CONF);
        logger.debug("connecting to {}", ToStringBuilder.reflectionToString(params));
        service = new TangoArchivingSystemConfigurationBuilder().build(params, properties);
        service.updateArchiversList();
        configService = new TangoArchivingServicesBuilder().buildConfigFetcher(params);
        inserterService = new TangoArchivingServicesBuilder().buildInserter(params);
    }

    @Override
    public String archivingStop(final String[] argin) throws DevFailed {
        logger.info("stopping archiving of {}", Arrays.toString(argin));
        final Map<String, List<String>> result = service.stopArchiving(argin);
        logger.info("stopping result is {}", result);
        return result.toString();
    }

    @Override
    public void archivingModif(final String[] argin) throws DevFailed {
        archivingStop(argin);
        archivingStart(argin);
    }

    @Override
    public void archivingStart(final String[] argin) throws DevFailed {
        logger.info("starting archiving of {}", Arrays.toString(argin));
        final Map<String, ArchivingConfigs> result = service.startArchiving(new ArchivingConfigs(argin, false));
        logger.info("starting result is {}", result);
    }

    @Override
    public void archivingStartJson(final String configAsString, final int contextId) throws DevFailed {
        ObjectMapper objectMapper = new ObjectMapper();
        List<ArchivingStartConfig> jsonConfig = null;
        try {
            jsonConfig = objectMapper.readValue(configAsString, objectMapper.getTypeFactory().constructCollectionType(List.class, ArchivingStartConfig.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        ArchivingConfigs configs = new ArchivingConfigs();
        for (ArchivingStartConfig conf : jsonConfig) {
            ArchivingConfig archivingConfig = new ArchivingConfig();
            archivingConfig.setArchiver(conf.getArchiverName());
            AttributeConfig attributeConfig = new AttributeConfig();
            String fullAttributeName = conf.getAttributeFullName();
            attributeConfig.setFullName(fullAttributeName);
            final int index = fullAttributeName.lastIndexOf('/');
            final String deviceName = fullAttributeName.substring(0, index);
            final AttributeInfo attributeInfo = new DeviceProxy(deviceName).get_attribute_info(fullAttributeName.substring(index + 1));
            attributeConfig.setType(attributeInfo.data_type);
            attributeConfig.setFormat(attributeInfo.data_format.value());
            attributeConfig.setWriteType(attributeInfo.writable.value());
            archivingConfig.setAttributeConfig(attributeConfig);
            InsertionModes modes = InsertionModesMapper.INSTANCE.mapModes(conf.getModes());
            if (contextId >= 0) { // contextId is optional
                modes.setContextId(contextId);
            }
            archivingConfig.setModes(modes);
            if (!(modes.isPeriodic() || modes.isEvent())){ // Safeguard against "empty modes"
                logger.info("Attribute {} has no periodic or event mode, it will not be archived", fullAttributeName);
                continue;
            }
            configs.addConfiguration(archivingConfig);
        }
        service.startArchiving(configs);
    }

    @Override
    public short[] isArchived(final String[] argin) throws DevFailed {
        short[] result = new short[argin.length];
        for (int i = 0; i < result.length; i++) {
            if (configService.isArchived(argin[i]))
                result[i] = 1;
            else
                result[i] = 0;
        }
        return result;
    }

    @Override
    public String[] getArchivingMode(final String argin) throws DevFailed {
        OptionalInt id = configService.getAttributeID(argin);
        if (!id.isPresent()) {
            throw DevFailedUtils.newDevFailed("no id found for " + argin);
        }
        return toStringArray(configService.getCurrentInsertionModes(id.getAsInt()));
    }

    private String[] toStringArray(Optional<InsertionModes> toConvert) {
        List<String> array = new ArrayList<>();
        if (toConvert.isPresent()) {
            InsertionModes modes = toConvert.get();
            if (modes.isPeriodic()) {
                array.add("MODE_P");
                array.add(String.valueOf(modes.getPeriodPeriodic()));
            }
            if (modes.isAbsolute()) {
                array.add("MODE_A");
                array.add(String.valueOf(modes.getPeriodAbsolute()));
                array.add(modes.getDecreaseDeltaAbsolute().toString());
                array.add(modes.getIncreaseDeltaAbsolute().toString());
                array.add(String.valueOf(modes.isSlowDriftAbsolute()));
            }
            if (modes.isRelative()) {
                array.add("MODE_R");
                array.add(String.valueOf(modes.getPeriodRelative()));
                array.add(modes.getDecreasePercentRelative().toString());
                array.add(modes.getIncreasePercentRelative().toString());
                array.add(String.valueOf(modes.isSlowDriftRelative()));
            }
            if (modes.isThreshold()) {
                array.add("MODE_T");
                array.add(String.valueOf(modes.getPeriodThreshold()));
                array.add(modes.getMinThresholdValue().toString());
                array.add(modes.getMaxThresholdValue().toString());
            }
            if (modes.isDifference()) {
                array.add("MODE_D");
                array.add(String.valueOf(modes.getPeriodDifference()));
            }
            if (modes.isEvent()) {
                array.add("MODE_EVT");
            }
        }
        return array.toArray(new String[0]);
    }

    @Override
    public String[] getStatus(final String[] argin) throws DevFailed {
        throw DevFailedUtils.newDevFailed("not supported function");
    }

    @Override
    public Set<String> getExportedArchiverList() {
        return service.getArchivers();
    }

    @Override
    public String[] getAllContexts() throws DevFailed {
        List<Context> res = configService.getAllContexts();
        return res.stream().map(Context::toString).toArray(String[]::new);
    }

    @Override
    public int createNewContext(final String contextName, String description) throws DevFailed {
        List<Context> res = configService.getAllContexts();
        if (res.stream().anyMatch(c -> c.getName().equals(contextName))) {
            throw DevFailedUtils.newDevFailed("context " + contextName + " already exists");
        }
        return inserterService.createNewContext(contextName, description);
    }

    @Override
    public void shiftArchiver(final String attributeName, final String archiverName) throws DevFailed {
        ArchivingConfigs configs = new ArchivingConfigs();
        ArchivingConfig archivingConfig = new ArchivingConfig();
        archivingConfig.setArchiver(archiverName);
        configService.getAttributeConfig(attributeName).ifPresent(attributeConfig -> {
            archivingConfig.setAttributeConfig(attributeConfig);
            archivingConfig.setModes(configService.getCurrentInsertionModes(attributeConfig.getId()).orElse(null));
            configs.addConfiguration(archivingConfig);
            service.stopArchiving(attributeName);
            service.startArchiving(configs);
        });
    }
}
