package org.tango.archiving.server.manager.configuration;

import fr.soleil.tango.archiving.config.InsertionModes;
import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class InsertionModesMapperTest {

    @Test
    public void test() {
        ArchivingStartModes modes = new ArchivingStartModes();
        modes.setPeriodic(true);
        modes.setPeriodPeriodic(20000);
        modes.setRelative(true);
        modes.setPeriodRelative(1000);
        modes.setDecreasePercentRelative(10.0);
        modes.setIncreasePercentRelative(10.0);
        modes.setSlowDriftRelative(true);
        modes.setDifference(true);
        modes.setDecreaseDeltaAbsolute(12.3);
        InsertionModes result = InsertionModesMapper.INSTANCE.mapModes(modes);
        assertNotNull(result);
        assertThat(result.getPeriodPeriodic(), equalTo(20000));
        assertThat(result.getPeriodRelative(), equalTo(1000));
        assertThat(result.isRelative(), equalTo(true));
    }
}
