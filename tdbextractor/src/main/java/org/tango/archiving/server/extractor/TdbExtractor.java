package org.tango.archiving.server.extractor;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Device;

import java.util.ResourceBundle;

@Device
public class TdbExtractor extends HdbExtractor {

    private static final Logger LOGGER = LoggerFactory.getLogger(TdbExtractor.class);
    private static final String VERSION = ResourceBundle.getBundle("application").getString("project.version");

    public static void main(final String[] args) {
        ServerManager.getInstance().start(args, TdbExtractor.class);
    }

    @Override
    protected void connectDatabase() throws DevFailed {
        connectDatabase(ConfigConst.TDB_CLASS_DEVICE, false);
    }

    @Override
    public String getVersion() {
        return VERSION;
    }

    @Override
    protected void exportData(String attributeName) throws DevFailed {
        databaseAccess.exportData(attributeName);
    }

}
