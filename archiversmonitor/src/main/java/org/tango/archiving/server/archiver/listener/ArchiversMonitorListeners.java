package org.tango.archiving.server.archiver.listener;

import fr.esrf.Tango.AttrQuality;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeInfoEx;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.tango.attributecomposer.IAttributeGroupTaskListener;
import fr.soleil.tango.clientapi.TangoGroupAttribute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.TangoUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

public class ArchiversMonitorListeners implements IAttributeGroupTaskListener {
    public static final String SCALAR_CHARGE = "scalarcharge";
    public static final String SPECTRUM_CHARGE = "spectrumcharge";
    public static final String IMAGE_CHARGE = "imagecharge";
    public static final String INSERTION_RATE = "insertionrate";
    public static final String MEMORY = "memory";
    public static final String STATE = "state";
    public static final String COLLECTOR_ERROR_NR = "collectorerrornr";

    public static final String INSERTION_ERROR_NR = "insertionerrornr";
    public static final String KO = "ko";
    public static final String ERROR = "error";
    public static final String COLLECTOR_DELAY = "collectordelay";
    public static final String ERROR_RATE = "insertionerrorsrate";
    final private static String[] monitoredAttributes = {SCALAR_CHARGE, SPECTRUM_CHARGE, IMAGE_CHARGE, INSERTION_RATE,
            MEMORY, STATE, COLLECTOR_ERROR_NR, INSERTION_ERROR_NR, KO, COLLECTOR_DELAY, ERROR_RATE};
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    final private List<String> devices = new ArrayList<>();
    final private Map<String, Map<String, Object>> values = new HashMap<>();
    final private Map<String, Map<String, String>> errors = new HashMap<>();

    public ArchiversMonitorListeners(String[] exportedArchivers) {
        for (String device : exportedArchivers)
            try {
                new DeviceProxy(device);
                devices.add(device);
                values.put(device.toLowerCase(), new HashMap<>());
                errors.put(device.toLowerCase(), new HashMap<>());
            } catch (DevFailed e) {
                logger.error("Device does not exist " + device, e);
            }
        logger.info("monitored Archivers " + devices.toString());
    }

    @Override
    public void updateDeviceAttribute(DeviceAttribute[] resultGroup) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateReadValue(String completeAttributeName, Object value) {
        try {
            values.get(getDeviceName(completeAttributeName)).put(TangoUtil.getAttributeName(completeAttributeName),
                    value);
            errors.get(getDeviceName(completeAttributeName)).put(TangoUtil.getAttributeName(completeAttributeName),
                    null);
        } catch (DevFailed e) {
            values.get(getDeviceName(completeAttributeName)).put(ERROR, e.toString());
        }
    }

    private String getDeviceName(String completeAttributeName) {
        String device;
        try {
            device = TangoUtil.getfullDeviceNameForAttribute(completeAttributeName);
        } catch (DevFailed e) {
            device = completeAttributeName;
        }
        return device.toLowerCase();
    }

    @Override
    public void updateErrorMessage(String completeAttributeName, String errorMessage) {
        try {
            values.get(getDeviceName(completeAttributeName)).put(TangoUtil.getAttributeName(completeAttributeName),
                    "In error (see attribute reportError for more details)");
            errors.get(getDeviceName(completeAttributeName)).put(TangoUtil.getAttributeName(completeAttributeName),
                    errorMessage);
        } catch (DevFailed e) {
            values.get(getDeviceName(completeAttributeName)).put(ERROR, e.toString());
        }

    }

    @Override
    public void updateWriteValue(String completeAttributeName, Object value) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateWriteValueErrorMessage(String completeAttributeName, String errorMessage) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateAttributeInfoEx(String completeAttributeName, AttributeInfoEx attributeInfo) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateAttributeInfoErrorMessage(String completeAttributeName, String errorMessage) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateQuality(String completeAttributeName, AttrQuality quality) {
        // TODO Auto-generated method stub

    }

    @Override
    public void catchException(Exception exception) {
        // TODO Auto-generated method stub

    }

    @Override
    public void catchDevFailed(DevFailed exception) {
        // TODO Auto-generated method stub

    }

    @Override
    public void readingLoopFinished() {
        // TODO Auto-generated method stub

    }

    public TangoGroupAttribute getGroupAttributes() throws DevFailed {
        List<String> fullAttributeNameList = new ArrayList<>();
        for (String device : devices) {
            for (String attribute : monitoredAttributes) {
                fullAttributeNameList.add(device + "/" + attribute);
            }
        }
        return new TangoGroupAttribute(false,
                fullAttributeNameList.toArray(new String[0]));
    }

    public String[][] getReport() {
        List<String[]> result = new ArrayList<>();
        result.add(new String[]{"Device name", SCALAR_CHARGE, SPECTRUM_CHARGE, IMAGE_CHARGE, INSERTION_RATE, MEMORY,
                STATE, COLLECTOR_ERROR_NR, INSERTION_ERROR_NR, COLLECTOR_DELAY, KO, ERROR_RATE});

        for (Entry<String, Map<String, Object>> device : values.entrySet()) {
            String deviceName = device.getKey();
            Map<String, Object> attributes = device.getValue();
            result.add(new String[]{deviceName,
                    Objects.toString(attributes.get(SCALAR_CHARGE)),
                    Objects.toString(attributes.get(SPECTRUM_CHARGE)),
                    Objects.toString(attributes.get(IMAGE_CHARGE)),
                    Objects.toString(attributes.get(INSERTION_RATE)),
                    Objects.toString(attributes.get(MEMORY)),
                    Objects.toString(attributes.get(STATE)),
                    Objects.toString(attributes.get(COLLECTOR_ERROR_NR)),
                    Objects.toString(attributes.get(INSERTION_ERROR_NR)),
                    Objects.toString(attributes.get(COLLECTOR_DELAY)),
                    Objects.toString(attributes.get(KO)),
                    Objects.toString(attributes.get(ERROR_RATE))
            });
        }
        result.sort((o1, o2) -> o1[0].compareToIgnoreCase(o2[0]));
        return result.toArray(new String[][]{});
    }

    public String[][] getReportError() {
        List<String[]> result = new ArrayList<>();
        result.add(new String[]{"Device name", SCALAR_CHARGE + ERROR, SPECTRUM_CHARGE + ERROR, IMAGE_CHARGE + ERROR,
                INSERTION_RATE + ERROR, MEMORY + ERROR, STATE + ERROR, COLLECTOR_ERROR_NR + ERROR,INSERTION_ERROR_NR+ ERROR,
                COLLECTOR_DELAY + ERROR, KO + ERROR, ERROR_RATE + ERROR});
        for (Entry<String, Map<String, String>> device : errors.entrySet()) {
            String deviceName = device.getKey();
            Map<String, String> attributes = device.getValue();
            result.add(new String[]{deviceName,
                    Objects.toString(attributes.get(SCALAR_CHARGE)),
                    Objects.toString(attributes.get(SPECTRUM_CHARGE)),
                    Objects.toString(attributes.get(IMAGE_CHARGE)),
                    Objects.toString(attributes.get(INSERTION_RATE)),
                    Objects.toString(attributes.get(MEMORY)),
                    Objects.toString(attributes.get(STATE)),
                    Objects.toString(attributes.get(COLLECTOR_ERROR_NR)),
                    Objects.toString(attributes.get(INSERTION_ERROR_NR)),
                    Objects.toString(attributes.get(COLLECTOR_DELAY)),
                    Objects.toString(attributes.get(KO)),
                    Objects.toString(attributes.get(ERROR_RATE))
            });
        }
        result.sort((o1, o2) -> o1[0].compareToIgnoreCase(o2[0]));

        return result.toArray(new String[][]{});
    }

    public int getTotalErrors() {
        return getIntTotal(COLLECTOR_ERROR_NR)+ getIntTotal(INSERTION_ERROR_NR);
    }

    private int getIntTotal(String key) {
        int result = 0;
        for (Entry<String, Map<String, Object>> device : values.entrySet()) {
            Object value = device.getValue().get(key);
            if (value instanceof Integer) {
                result += (int) value;
            }
        }
        return result;
    }

    public int getTotalKo() {
        return getIntTotal(KO);
    }

    public int geTotalChargeScalar() {
        return getIntTotal(SCALAR_CHARGE);
    }

    public int geTotalChargeSpectrum() {
        return getIntTotal(SPECTRUM_CHARGE);
    }

    public int getTotalChargeImage() {
        return getIntTotal(IMAGE_CHARGE);
    }

    public double getTotalInsertionRate() {
        return getDoubleTotal(INSERTION_RATE);
    }

    private double getDoubleTotal(String key) {
        double result = 0;
        for (Entry<String, Map<String, Object>> device : values.entrySet()) {
            Object value = device.getValue().get(key);
            if (value != null && value instanceof Double) {
                result += (double) value;
            }
        }
        return result;
    }

    public double getTotalErrorInsertionRate() {
        return getDoubleTotal(ERROR_RATE);
    }

    public double getTotalMemory() {
        return getDoubleTotal(MEMORY);
    }
}
