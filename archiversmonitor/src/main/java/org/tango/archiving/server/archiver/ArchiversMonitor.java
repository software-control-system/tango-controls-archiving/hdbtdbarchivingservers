package org.tango.archiving.server.archiver;


import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DispLevel;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.Group.Group;
import fr.esrf.TangoApi.Group.GroupCmdReply;
import fr.esrf.TangoApi.Group.GroupCmdReplyList;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.hdbtdb.api.ConfigConst;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.attributecomposer.AttributeGroupReader;
import fr.soleil.tango.attributecomposer.AttributeGroupScheduler;
import fr.soleil.tango.statecomposer.StateResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.archiving.monitoring.database.MonitoringAgent;
import org.tango.archiving.monitoring.database.MonitoringProcess;
import org.tango.archiving.monitoring.database.infra.IDatabaseAccess;
import org.tango.archiving.monitoring.database.infra.LegacyHDBAccess;
import org.tango.archiving.monitoring.database.infra.TimeseriesAccess;
import org.tango.archiving.server.archiver.listener.ArchiversMonitorListeners;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Attribute;
import org.tango.server.annotation.AttributeProperties;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceManagement;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.Status;
import org.tango.server.annotation.TransactionType;
import org.tango.server.device.DeviceManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Device(transactionType = TransactionType.NONE)
public class ArchiversMonitor {

    public static final String DEFAULT_ARCHIVER_CLASS = ConfigConst.HDB_CLASS_DEVICE;
    protected static String VERSION;
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    ArchiversMonitorListeners listeners;
    AttributeGroupReader task;
    AttributeGroupScheduler readScheduler;
    private String[] exportedArchivers = {"archiving/hdb-oracle/hdbarchiver.01"};
    @DeviceProperty(description = "archiving device class to monitor")
    private String archiverClass = DEFAULT_ARCHIVER_CLASS;
    @DeviceProperty(description = "archiver polling period in milliseconds", defaultValue = "10000 milliseconds")
    private int period = 10 * 1000;
    @DeviceProperty(description = "database name, use only is enableDatabaseMonitoring==true")
    private String dbName = "";
    @DeviceProperty(description = "database host name, use only is enableDatabaseMonitoring==true")
    private String dbHost = "";
    @DeviceProperty(description = "database user name, use only is enableDatabaseMonitoring==true")
    private String dbUser = "";
    @DeviceProperty(description = "database password, use only is enableDatabaseMonitoring==true")
    private String dbPassword = "";
    @DeviceProperty(description = "database type: MYSQL, ORACLE, POSTGRESQL")
    private String dbType = "";
    @DeviceProperty(description = "database schema, used only for Oracle databases")
    private String dbSchema = "";
    @DeviceProperty(description = "database port")
    private String dbPort = "";
    @DeviceProperty(description = "Enable checks of last inserted values in database")
    private boolean enableDatabaseMonitoring = false;
    @DeviceProperty(description = "Use for database monitoring: " +
            "add an error margin in minutes on attribute insertion period to consider it in error")
    private int errorMarginPeriod = 15;
    @DeviceProperty(description = "Use for database monitoring: " +
            "pause in ms between checking each attribute")
    private int pauseBetweenAttributes = 3000;
    @DeviceManagement
    private DeviceManager device;
    @State
    private volatile DeviceState state = DeviceState.OFF;
    @Status
    private volatile String status = "";
    private StateResolver stateReader;
    private Group group;
    private MonitoringProcess monitoringProcess;
    private ScheduledExecutorService executor;
    private ScheduledFuture<?> future;
    @DeviceProperty(description = "List of attributes to exclude, can contain wildcar *")
    private String[] excludedAttributes = new String[]{};
    private List<String> excludedAttributesRegex;
    @DeviceProperty(description = "List of attributes to select, can contain wildcar *")
    private String[] selectedAttributes = new String[]{};
    private List<String> selectedAttributesRegex;

    private boolean doFilterAttributes = false;

    public static void main(final String[] args) {
        VERSION = ResourceBundle.getBundle("application").getString("project.version");
        ServerManager.getInstance().start(args, ArchiversMonitor.class);
    }

    public void setDbPort(final String dbPort) {
        this.dbPort = dbPort;
    }

    public void setDbSchema(final String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public void setDbType(final String dbType) {
        this.dbType = dbType;
    }

    public void setExcludedAttributes(final String[] excludedAttributes) {
        this.excludedAttributes = excludedAttributes;
        excludedAttributesRegex = getRegexList(excludedAttributes);
        if (!excludedAttributesRegex.isEmpty())
            doFilterAttributes = true;
        logger.info("will exclude attributes with regex = {}", selectedAttributesRegex);
    }

    private List<String> getRegexList(String[] array) {
        List<String> regexList = new ArrayList<>();
        // format exclusion String to regex
        Arrays.stream(array).forEach(exclusion ->
                regexList.add(
                        exclusion.trim().replace("*", ".*").replace("..*", ".*").
                                replace("?", ".?").replace("..?", ".?")
                )
        );
        return regexList;
    }

    public void setSelectedAttributes(final String[] selectedAttributes) {
        this.selectedAttributes = selectedAttributes;
        selectedAttributesRegex = getRegexList(selectedAttributes);
        logger.info("will select attributes with regex = {}", selectedAttributesRegex);
        if (!selectedAttributesRegex.isEmpty())
            doFilterAttributes = true;
    }

    public void setPauseBetweenAttributes(final int pauseBetweenAttributes) {
        this.pauseBetweenAttributes = pauseBetweenAttributes;
    }

    public void setErrorMarginPeriod(final int errorMarginPeriod) {
        this.errorMarginPeriod = errorMarginPeriod;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public void setDbHost(final String dbHost) {
        this.dbHost = dbHost;
    }

    public void setDbUser(final String dbUser) {
        this.dbUser = dbUser;
    }

    public void setDbPassword(final String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void setEnableDatabaseMonitoring(final boolean enableDatabaseMonitoring) {
        this.enableDatabaseMonitoring = enableDatabaseMonitoring;
    }

    public void setDevice(DeviceManager device) {
        this.device = device;
    }

    public void setArchiverClass(String archiverClass) {
        this.archiverClass = archiverClass;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public String getStatus() {
        String tmpStatus = "";
        if (stateReader != null && stateReader.isStarted()) {
            tmpStatus = "At least one archiver is " + getState();
        } else {
            tmpStatus = "No running archivers";
        }
        if (enableDatabaseMonitoring && monitoringProcess != null) {
            tmpStatus = tmpStatus + "\nDatabase processed attributes: " +
                    monitoringProcess.getCheckedAttributesNr() + "/" + monitoringProcess.getAttributesNr();
        }
        return tmpStatus;
    }

    public DeviceState getState() {
        if (stateReader != null && stateReader.isStarted()) {
            final DeviceState newState = DeviceState.getDeviceState(stateReader.getState());
            if (!state.equals(newState)) {
                state = newState;
            }
        } else {
            state = DeviceState.UNKNOWN;
        }
        return state;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Attribute
    @AttributeProperties(description = "The version of the device", label = "version", unit = "")
    public String getVersion() {
        return VERSION;
    }

    @Delete
    public void delete() {
        stopMonitoringArchivers();
    }

    @Command
    public void stopMonitoringArchivers() {
        if (readScheduler != null) {
            readScheduler.stop();
        }
        if (stateReader != null) {
            stateReader.stop();
        }
    }

    @Init(lazyLoading = true)
    public void init() throws DevFailed, ArchivingException {
        logger.info("init in");
        exportedArchivers = ApiUtil.get_db_obj().get_device_name("*", archiverClass);

        group = new Group("");
        group.add(exportedArchivers);
        // start archivers monitoring
        listeners = new ArchiversMonitorListeners(exportedArchivers);
        task = new AttributeGroupReader(listeners, listeners.getGroupAttributes(), false, true, false);
        readScheduler = new AttributeGroupScheduler();
        startMonitoringArchivers();

        if (enableDatabaseMonitoring) {
            final DatabaseConnectionConfig parameters = getDatabaseConnectionConfig(archiverClass);
            if (!dbType.isEmpty()) {
                parameters.setDbType(DatabaseConnectionConfig.DataBaseType.parseDataBaseType(dbType));
            }
            if (!dbUser.isEmpty()) {
                parameters.setUser(dbUser);
            }
            if (!dbPassword.isEmpty()) {
                parameters.setPassword(dbPassword);
            }
            if (!dbHost.isEmpty()) {
                parameters.setHost(dbHost);
            }
            if (!dbName.isEmpty()) {
                parameters.setName(dbName);
            }
            if (!dbUser.isEmpty()) {
                parameters.setUser(dbUser);
            }
            if (!dbPassword.isEmpty()) {
                parameters.setPassword(dbPassword);
            }
            if (!dbSchema.isEmpty()) {
                parameters.setSchema(dbSchema);
            }
            if (!dbPort.isEmpty()) {
                parameters.setPort(dbPort);
            }

            IDatabaseAccess databaseAccess;
            if (parameters.getDbType().equals(DatabaseConnectionConfig.DataBaseType.POSTGRESQL)) {
                databaseAccess = new TimeseriesAccess();
            } else {
                // TODO: check if isHistoric should be set with a device property
                databaseAccess = new LegacyHDBAccess(true);
            }
            databaseAccess.connectDatabase(parameters);
            MonitoringAgent agent = new MonitoringAgent(databaseAccess);
            monitoringProcess = new MonitoringProcess(agent, excludedAttributesRegex, selectedAttributesRegex,
                    errorMarginPeriod * 60000, pauseBetweenAttributes);
            executor = Executors.newScheduledThreadPool(1);
            // start database monitoring
            startMonitoringDatabase();
        }
        logger.info("init device OK");
    }

    @Command
    public void startMonitoringArchivers() throws DevFailed {
        stopMonitoringArchivers();
        readScheduler.start(task, period);
        if (exportedArchivers.length > 0) {
            stateReader = new StateResolver(period, false);
            stateReader.setMonitoredDevices(period, exportedArchivers);
            stateReader.start(device.getName());
        }
    }

    private static DatabaseConnectionConfig getDatabaseConnectionConfig(String archiverClassName) {
        final DatabaseConnectionConfig params = new DatabaseConnectionConfig();
        // get default value from class properties of HDBArchiver
        DatabaseConnectionConfig.TangoDbProperties tangoDbProperties =
                new DatabaseConnectionConfig.TangoDbProperties("dbType", "dbHost", "dbName",
                        "dbSchema", "dbUser", "dbPassword",
                        "dbMinPoolSize", "dbMaxPoolSize",
                        "dbCnxInactivityTimeout", "dbPort");
        // retrieve archiver class properties
        params.setParametersFromTango(archiverClassName, tangoDbProperties);
        return params;
    }

    @Command
    public void startMonitoringDatabase() {
        stopMonitoringDatabase();
        if (enableDatabaseMonitoring) {
            monitoringProcess.init();
            future = executor.scheduleWithFixedDelay(monitoringProcess, 0, 10,
                    TimeUnit.SECONDS);
        }
    }

    @Command
    public void stopMonitoringDatabase() {
        if (future != null) {
            future.cancel(true);
            executor.shutdown();
        }
    }

    @Attribute
    public String[][] getDataBaseReport() {
        if (enableDatabaseMonitoring) {
            return monitoringProcess.getReport();
        } else {
            return new String[][]{{"database monitoring disable"}};
        }
    }

    @Attribute
    @AttributeProperties(description = "Report of KO attributes detected in database")
    public String[][] getDataBaseKoReport() {
        if (enableDatabaseMonitoring) {
            return monitoringProcess.getKoReport();
        } else {
            return new String[][]{{"database monitoring disable"}};
        }
    }

    @Attribute
    @AttributeProperties(description = "Report of Null value attributes detected in database")
    public String[][] getDataBaseNullReport() {
        if (enableDatabaseMonitoring) {
            return monitoringProcess.getNullReport();
        } else {
            return new String[][]{{"database monitoring disable"}};
        }
    }

    @Attribute
    @AttributeProperties(description = "Number of KO detected in database", unit = "attributes")
    public int getDataBaseKo() {
        if (enableDatabaseMonitoring) {
            return monitoringProcess.getKoSize();
        } else {
            return 0;
        }
    }

    @Attribute
    @AttributeProperties(description = "Number of Null values detected in database", unit = "attributes")
    public int getDataBaseNull() {
        if (enableDatabaseMonitoring) {
            return monitoringProcess.getNullSize();
        } else {
            return 0;
        }
    }

    @Attribute
    @AttributeProperties(description = "Report for read value of this attributes : ScalarCharge SpectrumCharge ImageCharge insertionRate memory state collectorErrorNr")
    public String[][] getArchiversReport() {
        return listeners.getReport();
    }

    @Attribute(displayLevel = DispLevel._EXPERT)
    @AttributeProperties(description = "Errors found while reading report value")
    public String[][] getArchiversErrorReport() {
        return listeners.getReportError();
    }

    @Attribute
    @AttributeProperties(description = "List archiving attributes with collecting errors")
    public String[][] getArchiversCollectErrorList() throws DevFailed {
        Set<String[]> result = new LinkedHashSet<>();
        result.add(new String[]{"Device name", "Attribute in error name", "Error message"});
        getArchiverErrors("GetErrorMessages", result);
        return result.toArray(new String[][]{});
    }

    @Attribute
    @AttributeProperties(description = "List archiving attributes with insertion errors")
    public String[][] getArchiversInsertionErrorList() throws DevFailed {
        Set<String[]> result = new LinkedHashSet<>();
        result.add(new String[]{"Device name", "Attribute in error name", "Error message"});
        getArchiverErrors("GetInsertionErrorMessages", result);
        return result.toArray(new String[][]{});
    }

    private void getArchiverErrors(String cmdName, Set<String[]> result) throws DevFailed {
        GroupCmdReplyList groupResult = group.command_inout(cmdName, true);
        final Enumeration<?> tmpReplyEnumeration = groupResult.elements();
        while (tmpReplyEnumeration.hasMoreElements()) {
            final GroupCmdReply tmpReply = (GroupCmdReply) tmpReplyEnumeration.nextElement();
            try {
                final DeviceData tmpDeviceData = tmpReply.get_data();
                for (String error : tmpDeviceData.extractStringArray()) {
                    String[] split = error.split(":");
                    String attributeName = split[0].trim();
                    String errorMsg = String.join(":", Arrays.copyOfRange(split, 1, split.length));
                    if (doFilterAttributes) {
                        if (!selectedAttributesRegex.isEmpty()) {
                            for (String selection : selectedAttributesRegex) {
                                if (Pattern.compile(selection, Pattern.CASE_INSENSITIVE).matcher(attributeName).matches()) {
                                    result.add(new String[]{tmpReply.dev_name(), attributeName, errorMsg});
                                    break;
                                }
                            }
                        } else if (!excludedAttributesRegex.isEmpty()) {
                            boolean doRemove = false;
                            for (String exclusion : excludedAttributesRegex) {
                                // Check if the attribute name matches the exclusion pattern
                                if (Pattern.compile(exclusion, Pattern.CASE_INSENSITIVE).matcher(attributeName).matches()) {
                                    logger.debug("excluding attribute {}", attributeName);
                                    doRemove = true;
                                }
                            }
                            // Add to result if not marked for removal
                            if (!doRemove) {
                                result.add(new String[]{tmpReply.dev_name(), attributeName, errorMsg});
                            }
                        }
                    } else {
                        result.add(new String[]{tmpReply.dev_name(), attributeName, errorMsg});
                    }
                }
            } catch (final DevFailed e) {
                result.add(new String[]{tmpReply.dev_name(), "device not available", e.toString()});
            }
        }
    }

    @Attribute
    @AttributeProperties(description = "Total number of archivers errors")
    public int getArchiversErrorTotal() {
        return listeners.getTotalErrors();
    }

    @Attribute
    @AttributeProperties(description = "List attributes where archiving not running")
    public String[][] getArchiversKoList() throws DevFailed {
        List<String[]> result = new ArrayList<>();
        result.add(new String[]{"Device name", "Attribute not running name"});

        GroupCmdReplyList groupResult = group.command_inout("GetKoAttributes", true);
        final Enumeration<?> tmpReplyEnumeration = groupResult.elements();
        while (tmpReplyEnumeration.hasMoreElements()) {
            final GroupCmdReply tmpReply = (GroupCmdReply) tmpReplyEnumeration.nextElement();
            try {
                final DeviceData tmpDeviceData = tmpReply.get_data();
                for (String error : tmpDeviceData.extractStringArray()) {
                    result.add(new String[]{tmpReply.dev_name(), error});
                }
            } catch (final DevFailed e) {
                result.add(new String[]{tmpReply.dev_name(), e.toString()});
            }
        }
        return result.toArray(new String[][]{});
    }

    @Attribute
    @AttributeProperties(description = "Total number of SCALAR attributes", unit = "attributes")
    public int getScalarTotalCharge() {
        return listeners.geTotalChargeScalar();
    }

    @Attribute
    @AttributeProperties(description = "Total number of SPECTRUM attributes", unit = "attributes")
    public int getSpectrumTotalCharge() {
        return listeners.geTotalChargeSpectrum();
    }

    @Attribute
    @AttributeProperties(description = "Total number of IMAGE attributes", unit = "attributes")
    public int getImageTotalCharge() {
        return listeners.getTotalChargeImage();
    }

    @Attribute
    @AttributeProperties(description = "Total number of SCALAR+SPECTRUM+IMAGE attributes", unit = "attributes")
    public int getArchiversTotalCharge() {
        return listeners.geTotalChargeScalar() + listeners.geTotalChargeSpectrum() + listeners.getTotalChargeImage();
    }

    @Attribute
    @AttributeProperties(description = "Total number of KO attributes")
    public int getArchiversKoTotal() {
        return listeners.getTotalKo();
    }

    @Attribute
    @AttributeProperties(description = "Total insertion rate", unit = "insertions/s", format = "%5.2f")
    public double getArchiversTotalInsertionRate() {
        return listeners.getTotalInsertionRate();
    }

    @Attribute
    @AttributeProperties(description = "Total error insertion rate", unit = "insertions/s", format = "%5.2f")
    public double getArchiversTotalErrorInsertionRate() {
        return listeners.getTotalErrorInsertionRate();
    }

    @Attribute
    @AttributeProperties(description = "Total memory", unit = "Mo", format = "%10.2f")
    public double getArchiversTotalMemory() {
        return listeners.getTotalMemory();
    }

    /**
     * Send command RetryForKOAttributes to all archivers
     *
     * @return
     * @throws DevFailed
     */
    @Command
    public String[] retryForKoAttributesAll() throws DevFailed {
        List<String> result = new ArrayList<>();
        GroupCmdReplyList groupResult = group.command_inout("RetryForKOAttributes", true);
        final Enumeration<?> tmpReplyEnumeration = groupResult.elements();
        while (tmpReplyEnumeration.hasMoreElements()) {
            final GroupCmdReply tmpReply = (GroupCmdReply) tmpReplyEnumeration.nextElement();
            try {
                final DeviceData tmpDeviceData = tmpReply.get_data();
                result.add(tmpReply.dev_name() + " respond : " + tmpDeviceData.extractShort());
            } catch (final DevFailed e) {
                result.add(tmpReply.dev_name() + " not respond : " + e.toString());
            }
        }
        return result.toArray(new String[]{});
    }

    @Command
    public void forceRefresh() {
        task.valueReader();
    }
}
