package org.tango.archiving.monitoring.database.infra;

import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.config.InsertionStatus;
import fr.soleil.tango.archiving.event.select.AttributeValue;

import java.util.Map;
import java.util.Optional;

public interface IDatabaseAccess {
    void connectDatabase(DatabaseConnectionConfig params);

    AttributeValue getLast(String attributeName);

    Map<InsertionModes, InsertionStatus> getAllRunningAttributes();

    Optional<InsertionModes> getCurrentInsertionModes(String attributeName);
}
