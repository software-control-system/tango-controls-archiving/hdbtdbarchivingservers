package org.tango.archiving.monitoring.database;

import fr.esrf.Tango.DevFailed;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.config.InsertionStatus;
import fr.soleil.tango.archiving.event.select.AttributeValue;
import fr.soleil.tango.clientapi.TangoAttribute;
import fr.soleil.tango.clientapi.TangoCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.monitoring.database.domain.AttributeInsertionReport;
import org.tango.archiving.monitoring.database.domain.AttributeInsertionStatus;
import org.tango.archiving.monitoring.database.infra.IDatabaseAccess;
import org.tango.utils.DevFailedUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class MonitoringAgent {

    public static final String TANGO_HOST_PATTERN = "tango://\\w+:\\d+/";
    private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringAgent.class);
    private final IDatabaseAccess databaseAccess;

    public MonitoringAgent(final IDatabaseAccess databaseAccess) {
        this.databaseAccess = databaseAccess;
    }

    public Map<InsertionModes, InsertionStatus> getAttributes(final List<String> exclusions, final List<String> selections) {
        // Get started in DB
        Map<InsertionModes, InsertionStatus> runningAttributes = databaseAccess.getAllRunningAttributes();
        Map<InsertionModes, InsertionStatus> runningAttributesResult = new HashMap<>(runningAttributes);
        if (!selections.isEmpty()) {
            // Remove not selected attributes
            for (InsertionModes modes : runningAttributes.keySet()) {
                boolean toKeep = false;
                for (String selection : selections) {
                    if (Pattern.compile(selection, Pattern.CASE_INSENSITIVE).matcher(modes.getAttributeName()).matches()) {
                        toKeep = true;
                        LOGGER.debug("monitoring {}", modes.getAttributeName());
                    }
                }
                if (!toKeep) {
                    runningAttributesResult.remove(modes);
                }
            }
        } else if (!exclusions.isEmpty()) {
            // Remove excluded list
            for (InsertionModes modes : runningAttributes.keySet()) {
                for (String exclusion : exclusions) {
                    if (Pattern.compile(exclusion, Pattern.CASE_INSENSITIVE).matcher(modes.getAttributeName()).matches()) {
                        runningAttributesResult.remove(modes);
                        LOGGER.info("not monitoring {}", modes.getAttributeName());
                    }
                }
            }
        }
        return runningAttributesResult;
    }

    public AttributeInsertionReport checkAttribute(final Map.Entry<InsertionModes, InsertionStatus> attribute, final int safetyPeriod) {
        String attributeName = attribute.getKey().getAttributeName();
        AttributeInsertionReport report = new AttributeInsertionReport();
        report.setInsertionModes(attribute.getKey());
        report.setInsertionConfig(attribute.getValue());
        AttributeInsertionStatus attributeStatus;
        try {
            final AttributeValue value = databaseAccess.getLast(attributeName);
            report.setLastValue(value);
            LOGGER.debug("{} last value is {}", attributeName, value);
            if (value.getDataTime() != null) {
                long archivingPeriod = attribute.getKey().getPeriodPeriodic();
                long delta = System.currentTimeMillis() - value.getDataTime().getTime();
                if (archivingPeriod > 0 && delta > archivingPeriod + safetyPeriod) {
                    // last insertion data is too old
                    attributeStatus = AttributeInsertionStatus.CONTROL_KO;
                    LOGGER.debug("{} KO, values has not been inserted since {}", attributeName, value.getDataTime());
                    // check if device/attribute still exists
                    checkAttribute(attributeName, report);
                } else if (value.getValueR() == null && value.getValueW() == null) {
                    // Null value
                    attributeStatus = AttributeInsertionStatus.CONTROL_NULL;
                    LOGGER.debug("{} values are NULL", attributeName);
                    checkAttribute(attributeName, report);
                } else {
                    attributeStatus = AttributeInsertionStatus.CONTROL_OK;
                    LOGGER.debug("{} OK, values has not been inserted since {}", attributeName, value.getDataTime());
                }
            } else {
                attributeStatus = AttributeInsertionStatus.CONTROL_KO;
                LOGGER.debug("{} KO, no data in DB", attributeName);
                // check if device/attribute still exists
                checkAttribute(attributeName, report);
            }
        } catch (Throwable e) {
            attributeStatus = AttributeInsertionStatus.CONTROL_FAILED;
            LOGGER.error("Monitoring failed for attribute " + attributeName, e);
            report.setReportingError(e.getMessage());
            report.setTangoMessage("Check on DB failed");
        }
        report.setInsertionStatus(attributeStatus);
        return report;
    }

    private static void checkAttribute(final String attributeName, final AttributeInsertionReport report) {
        try {
            TangoAttribute att = new TangoAttribute(attributeName);
            String tangoMessage = "Attribute connection OK";

            if (att.isScalar() && att.isNumber()) {
                final String readValue = att.extract(String.class);
                if (readValue.equalsIgnoreCase(Double.toString(Double.NaN))) {
                    tangoMessage = "Attribute value is NaN";
                }
            } else {
                att.extract();
            }
            final long lastTimeStamp = att.getTimestamp();
            tangoMessage += ", read timestamp is " + (System.currentTimeMillis() - lastTimeStamp) + " ms ago";
            report.setTangoMessage(tangoMessage);
            // TODO check if type of format has changed

            // Get current error message from archiver
            String archiverName = report.getInsertionConfig().getArchiver();
            if (archiverName.startsWith("tango://")) {
                // XXX TangoCommand does not support full name for a device
                archiverName = archiverName.replaceAll(TANGO_HOST_PATTERN, "");
            }
            TangoCommand command = new TangoCommand(archiverName + "/getErrorMessageForAttribute");
            String message = command.execute(String.class, attributeName);
            if (message != null && !message.isEmpty()) {
                report.setTangoMessage(report.getTangoMessage() + "- archiver message :" + message);
            }
        } catch (DevFailed e) {
            if (e.errors.length > 0) {
                if (e.errors[0].reason.equals("DB_DeviceNotDefined")) {
                    report.setTangoMessage("KO - Device not defined in Tango DB");
                } else {
                    report.setTangoMessage("ERROR - " + e.errors[0].desc);
                }
            } else {
                report.setTangoMessage(DevFailedUtils.toString(e));
            }
        }
    }
}
