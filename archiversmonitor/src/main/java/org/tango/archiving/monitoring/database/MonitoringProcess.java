package org.tango.archiving.monitoring.database;

import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.config.InsertionStatus;
import fr.soleil.tango.archiving.event.select.AttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.archiving.monitoring.database.domain.AttributeInsertionReport;
import org.tango.archiving.monitoring.database.domain.AttributeInsertionStatus;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MonitoringProcess implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(MonitoringProcess.class);

    private final MonitoringAgent monitoringAgent;
    private final List<String> excludedDevices;

    private final List<String> selectedDevices;

    private final int safetyPeriod;
    private final int pauseBetweenAttributes;
    private final Map<String, String[]> attributeReportList = Collections.synchronizedMap(new LinkedHashMap<>());
    private final Map<String, String[]> attributeKoReportList = Collections.synchronizedMap(new LinkedHashMap<>());

    private final Map<String, String[]> attributeNullReportList = Collections.synchronizedMap(new LinkedHashMap<>());
    private Map<InsertionModes, InsertionStatus> attributes;
    private final AtomicInteger checkedAttributesNr = new AtomicInteger();

    public MonitoringProcess(final MonitoringAgent monitoringAgent, final List<String> excludedDevices, final List<String> selectedDevices,
                             final int safetyPeriod, final int pauseBetweenAttributes) {
        this.monitoringAgent = monitoringAgent;
        this.excludedDevices = excludedDevices;
        this.selectedDevices = selectedDevices;
        this.safetyPeriod = safetyPeriod;
        this.pauseBetweenAttributes = pauseBetweenAttributes;
        attributeReportList.put("header",
                new String[]{"Attribute", "Control", "Last timestamp", "Period", "Archiver", "Database check pb"});
        attributeKoReportList.put("header",
                new String[]{"Attribute", "Last timestamp", "Period", "Archiver", "Last inserted error", "Attribute status check"});
        attributeNullReportList.put("header",
                new String[]{"Attribute", "Last timestamp", "Period", "Archiver", "Last inserted error", "Attribute status check"});

    }

    public String[][] getKoReport() {
        final LinkedList<String[]> tmp = new LinkedList<>(attributeKoReportList.values());
        String[][] result = new String[tmp.size()][5];
        for (int i = 0; i < result.length; i++) {
            result[i] = tmp.get(i);
        }
        return result;
    }

    public int getNullSize() {
        return attributeNullReportList.size() - 1;
    }

    public int getKoSize() {
        return attributeKoReportList.size() - 1;
    }

    public String[][] getNullReport() {
        final LinkedList<String[]> tmp = new LinkedList<>(attributeNullReportList.values());
        String[][] result = new String[tmp.size()][5];
        for (int i = 0; i < result.length; i++) {
            result[i] = tmp.get(i);
        }
        return result;
    }

    public String[][] getReport() {
        final LinkedList<String[]> tmp = new LinkedList<>(attributeReportList.values());
        String[][] result = new String[tmp.size()][5];
        for (int i = 0; i < result.length; i++) {
            result[i] = tmp.get(i);
        }
        return result;
    }

    public void init() {
        LOGGER.info("Initializing monitoring");
        attributes = monitoringAgent.getAttributes(excludedDevices, selectedDevices);
        LOGGER.info("Will monitor {} attributes", attributes.size());
    }

    public int getAttributesNr() {
        return attributes.size();
    }

    public int getCheckedAttributesNr() {
        return checkedAttributesNr.get();
    }

    @Override
    public void run() {
        try {
            long tick = System.currentTimeMillis();
            checkedAttributesNr.set(0);
            int attributesNr = attributes.size();
            for (Map.Entry<InsertionModes, InsertionStatus> attribute : attributes.entrySet()) {
                String attributeName = attribute.getKey().getAttributeName();
                LOGGER.info("Checking for {} nr {}/{}", attributeName, checkedAttributesNr.incrementAndGet(), attributesNr);
                try {
                    final AttributeInsertionReport status = monitoringAgent.checkAttribute(attribute, safetyPeriod);
                    addReport(status);
                    switch (status.getInsertionStatus()) {
                        case CONTROL_OK:
                            removeNullReport(attributeName);
                            removeKoReport(attributeName);
                            break;
                        case CONTROL_NULL:
                            addNullReport(status);
                            removeKoReport(attributeName);
                            break;
                        default:// case CONTROL_KO case CONTROL_FAILED:
                            addKoReport(status);
                            removeNullReport(attributeName);
                            break;
                    }
                    if (pauseBetweenAttributes > 0) {
                        Thread.sleep(pauseBetweenAttributes);
                    }
                } catch (Throwable e) {
                    final AttributeInsertionReport status = getDefaultInsertionReport(attributeName, e);
                    addReport(status);
                    addKoReport(status);
                    LOGGER.error("Monitoring failed for attribute " + attribute, e);
                }

            }
            long tock = System.currentTimeMillis();
            LOGGER.info("Diagnosis of {} attributes took {} s", attributes.size(), (tock - tick) / 1000.0);
        } catch (Throwable e) {
            LOGGER.error("Monitoring failed ", e);
        }
    }

    private void addReport(AttributeInsertionReport status) {
        String[] result = new String[6];
        result[0] = status.getInsertionModes().getAttributeName();
        result[1] = status.getInsertionStatus().name();
        if (status.getLastValue() != null && status.getLastValue().getDataTime() != null) {
            result[2] = status.getLastValue().getDataTime().toString();
        } else {
            result[2] = "N/A";
        }
        result[3] = Integer.toString(status.getInsertionModes().getPeriodPeriodic());
        result[4] = status.getInsertionConfig().getArchiver();
        if (status.getReportingError() != null) {
            result[5] = status.getReportingError();
        } else {
            result[5] = "N/A";
        }
        attributeReportList.put(result[0], result);
    }

    private void removeNullReport(String attributeName) {
        attributeNullReportList.remove(attributeName);
    }

    private void removeKoReport(String attributeName) {
        attributeKoReportList.remove(attributeName);
    }

    private void addNullReport(AttributeInsertionReport status) {
        String[] attributeReportResult = getErrorArray(status);
        attributeNullReportList.put(attributeReportResult[0], attributeReportResult);
    }

    private void addKoReport(AttributeInsertionReport status) {
        String[] attributeReportResult = getErrorArray(status);
        attributeKoReportList.put(attributeReportResult[0], attributeReportResult);
    }

    private AttributeInsertionReport getDefaultInsertionReport(String attributeName, Throwable e) {
        final AttributeInsertionReport status = new AttributeInsertionReport();
        InsertionModes insertionModes = new InsertionModes();
        insertionModes.setAttributeName(attributeName);
        status.setInsertionModes(insertionModes);
        status.setInsertionStatus(AttributeInsertionStatus.CONTROL_FAILED);
        status.setTangoMessage("Monitoring failed for attribute : " + e.getMessage());
        return status;
    }

    private String[] getErrorArray(AttributeInsertionReport status) {
        String[] attributeReportResult = new String[6];
        attributeReportResult[0] = status.getInsertionModes().getAttributeName();
        attributeReportResult[4] = "N/A";
        final AttributeValue attributeValue = status.getLastValue();
        if (attributeValue != null && attributeValue.getDataTime() != null) {
            attributeReportResult[1] = attributeValue.getDataTime().toString();
            if (attributeValue.getErrorMessage().isPresent()) {
                attributeReportResult[4] = attributeValue.getErrorMessage().get();
            }
        } else {
            attributeReportResult[1] = "N/A";
        }
        attributeReportResult[2] = Integer.toString(status.getInsertionModes().getPeriodPeriodic());
        attributeReportResult[3] = status.getInsertionConfig().getArchiver();
        if (status.getTangoMessage() != null) {
            attributeReportResult[5] = status.getTangoMessage();
        } else {
            attributeReportResult[5] = "N/A";
        }
        return attributeReportResult;
    }
}
