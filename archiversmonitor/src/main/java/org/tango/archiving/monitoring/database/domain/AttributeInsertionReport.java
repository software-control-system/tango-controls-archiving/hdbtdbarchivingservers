package org.tango.archiving.monitoring.database.domain;

import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.config.InsertionStatus;
import fr.soleil.tango.archiving.event.select.AttributeValue;

public class AttributeInsertionReport {

    /**
     * @see AttributeInsertionStatus
     */
    private AttributeInsertionStatus insertionStatus;
    private InsertionModes insertionModes;
    private InsertionStatus insertionConfig;

    /**
     * Last value from database
     */
    private AttributeValue lastValue;
    /**
     * Error when getting report from archiving database
     */
    private String reportingError;
    /**
     * Reporting message from tango attributes
     */
    private String tangoMessage;

    public String getTangoMessage() {
        return tangoMessage;
    }

    public void setTangoMessage(final String tangoMessage) {
        this.tangoMessage = tangoMessage;
    }

    public InsertionStatus getInsertionConfig() {
        return insertionConfig;
    }

    public void setInsertionConfig(final InsertionStatus insertionConfig) {
        this.insertionConfig = insertionConfig;
    }

    public String getReportingError() {
        return reportingError;
    }

    public void setReportingError(final String reportingError) {
        this.reportingError = reportingError;
    }

    public AttributeInsertionStatus getInsertionStatus() {
        return insertionStatus;
    }

    public void setInsertionStatus(final AttributeInsertionStatus insertionStatus) {
        this.insertionStatus = insertionStatus;
    }

    public InsertionModes getInsertionModes() {
        return insertionModes;
    }

    public void setInsertionModes(final InsertionModes insertionModes) {
        this.insertionModes = insertionModes;
    }

    public AttributeValue getLastValue() {
        return lastValue;
    }

    public void setLastValue(final AttributeValue lastValue) {
        this.lastValue = lastValue;
    }

}
