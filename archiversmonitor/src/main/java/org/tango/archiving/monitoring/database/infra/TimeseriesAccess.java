package org.tango.archiving.monitoring.database.infra;

import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.build.TangoArchivingServicesBuilder;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.config.InsertionStatus;
import fr.soleil.tango.archiving.event.select.AttributeValue;
import fr.soleil.tango.archiving.services.TangoArchivingConfigService;
import fr.soleil.tango.archiving.services.TangoArchivingFetcherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Optional;

public class TimeseriesAccess implements IDatabaseAccess {

    private static final Logger LOGGER = LoggerFactory.getLogger(TimeseriesAccess.class);
    private TangoArchivingFetcherService fetcherService;
    private TangoArchivingConfigService configService;

    @Override
    public void connectDatabase(final DatabaseConnectionConfig params) {
        fetcherService = new TangoArchivingServicesBuilder().buildFetcher(params);
        configService = new TangoArchivingServicesBuilder().buildConfigFetcher(params);
    }

    @Override
    public AttributeValue getLast(String attributeName) {
        return fetcherService.getLast(attributeName, true);
    }

    @Override
    public Map<InsertionModes, InsertionStatus> getAllRunningAttributes() {
        return configService.getStartedAttributes();
    }

    @Override
    public Optional<InsertionModes> getCurrentInsertionModes(String attributeName) {
        return configService.getCurrentInsertionModes(configService.getAttributeID(attributeName).getAsInt());
    }
}
