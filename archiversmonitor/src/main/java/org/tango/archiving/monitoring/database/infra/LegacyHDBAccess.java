package org.tango.archiving.monitoring.database.infra;

import fr.esrf.Tango.AttrWriteType;
import fr.soleil.archiving.common.api.ConnectionFactory;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.common.api.tools.DbData;
import fr.soleil.archiving.hdbtdb.api.DataBaseManager;
import fr.soleil.archiving.hdbtdb.api.DataBaseManagerFactory;
import fr.soleil.archiving.hdbtdb.api.tools.mode.Mode;
import fr.soleil.database.connection.AbstractDataBaseConnector;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.archiving.build.DatabaseConnectionConfig;
import fr.soleil.tango.archiving.config.InsertionModes;
import fr.soleil.tango.archiving.config.InsertionStatus;
import fr.soleil.tango.archiving.event.select.AttributeValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class LegacyHDBAccess implements IDatabaseAccess {

    private static final Logger LOGGER = LoggerFactory.getLogger(LegacyHDBAccess.class);
    private final boolean isHistoric;
    private DataBaseManager database;

    public LegacyHDBAccess(boolean isHistoric) {
        this.isHistoric = isHistoric;
    }

    @Override
    public void connectDatabase(final DatabaseConnectionConfig params) {
        LOGGER.info("init connection to archiving DB with params: {}", params);
        DataBaseParameters legacyParams = new DataBaseParameters();
        legacyParams.setDbType(DataBaseParameters.DataBaseType.parseDataBaseType(params.getDbType().name()));
        legacyParams.setHost(params.getHost());
        legacyParams.setName(params.getName());
        legacyParams.setSchema(params.getSchema());
        legacyParams.setUser(params.getUser());
        legacyParams.setPassword(params.getPassword());
        legacyParams.setInactivityTimeout(params.getIdleTimeout());
        legacyParams.setMaxPoolSize((short) params.getMaxPoolSize());
        if (params.getHealthRegistry() != null) {
            legacyParams.setHealthRegistry(params.getHealthRegistry());
        }
        if (params.getMetricRegistry() != null) {
            legacyParams.setMetricRegistry(params.getMetricRegistry());
        }
        AbstractDataBaseConnector connector;
        try {
            connector = ConnectionFactory.connect(legacyParams);
        } catch (ArchivingException e) {
            throw new RuntimeException(e);
        }
        // IArchivingManagerApiRef manager = ArchivingManagerApiRefFactory.getInstance(isHistoric, connector);
        database = DataBaseManagerFactory.getDataBaseManager(isHistoric, connector);
    }

    @Override
    public AttributeValue getLast(final String attributeName) {
        AttributeValue value = new AttributeValue();
        try {
            final DbData data = database.getExtractor().getDataGetters().getNewestValue(attributeName);
            Object readValue;
            if (data.getTimedData() != null && data.getTimedData()[0].getValue() != null) {
                readValue = getFirstArrayValue(data.getTimedData()[0].getValue());
                // Only one value is returned by getNewestValue
                switch (data.getWritable()) {
                    case AttrWriteType._READ:
                    case AttrWriteType._READ_WRITE:
                        value.setValueR(readValue);
                    case AttrWriteType._WRITE:
                        value.setValueW(readValue);
                        break;
                }
                value.setDataTime(Timestamp.from(Instant.ofEpochMilli(data.getTimedData()[0].getTime())));
            }
        } catch (ArchivingException e) {
            throw new RuntimeException(e);
        }
        return value;
    }

    private Object getFirstArrayValue(Object value) {
        Object result;
        if ((value != null) && value.getClass().isArray()) {
            result = Array.get(value, 0);
        } else {
            result = null;
        }
        return result;
    }

    @Override
    public Map<InsertionModes, InsertionStatus> getAllRunningAttributes() {
        Map<InsertionModes, InsertionStatus> map = new HashMap<>();
        try {
            String[] currentArchivedAtt = database.getMode().getCurrentArchivedAtt();
            for (int i = 0; i < currentArchivedAtt.length; i++) {
                Optional<InsertionModes> modesOptional = getCurrentInsertionModes(currentArchivedAtt[i]);
                if (modesOptional.isPresent()) {
                    InsertionModes modes = modesOptional.get();
                    // set dummy id to allow all insertions in map
                    modes.setAttributeID(i);
                    InsertionStatus insertionStatus = new InsertionStatus();
                    insertionStatus.setArchiver(database.getMode().getArchiverForAttribute(currentArchivedAtt[i]));
                    insertionStatus.setStartDate(Timestamp.from(Instant.ofEpochMilli(0)));
                    map.put(modes, insertionStatus);
                }
            }
        } catch (final ArchivingException e) {
            throw new RuntimeException(e);
        }
        return map;
    }

    @Override
    public Optional<InsertionModes> getCurrentInsertionModes(final String attributeName) {
        InsertionModes insertionModes = new InsertionModes();
        try {
            final Mode modes = database.getMode().getCurrentArchivingMode(attributeName);
            insertionModes.setAttributeName(attributeName);
            //     insertionModes.setAttributeID(attributeLightMode.getAttributeId());
            if (modes.getModeP() != null) {
                insertionModes.setPeriodic(true);
                insertionModes.setPeriodPeriodic(modes.getModeP().getPeriod());
            }
            if (modes.getModeA() != null) {
                insertionModes.setAbsolute(true);
                insertionModes.setPeriodAbsolute(modes.getModeA().getPeriod());
                insertionModes.setDecreaseDeltaAbsolute(modes.getModeA().getValInf());
                insertionModes.setIncreaseDeltaAbsolute(modes.getModeA().getValSup());
                insertionModes.setSlowDriftAbsolute(modes.getModeA().isSlow_drift());
            }
            if (modes.getModeR() != null) {
                insertionModes.setRelative(true);
                insertionModes.setPeriodRelative(modes.getModeR().getPeriod());
                insertionModes.setSlowDriftRelative(modes.getModeR().isSlow_drift());
                insertionModes.setDecreasePercentRelative(modes.getModeR().getPercentInf());
                insertionModes.setIncreaseDeltaAbsolute(modes.getModeR().getPercentSup());
            }
            if (modes.getModeD() != null) {
                insertionModes.setDifference(true);
                insertionModes.setPeriodDifference(modes.getModeD().getPeriod());
            }
            insertionModes.setEvent(modes.getEventMode() != null);
        } catch (ArchivingException e) {
            throw new RuntimeException(e);
        }
        return Optional.of(insertionModes);
    }
}
