package org.tango.archiving.monitoring.database.domain;

public enum AttributeInsertionStatus {
    /**
     * Everything is fine
     */
    CONTROL_OK,
    /**
     * Last insertion is too old
     */
    CONTROL_KO,
    /**
     * Last inserted value is NULL
     */
    CONTROL_NULL,
    /**
     * Check not yet done
     */
    CONTROL_NOT_DONE,
    /**
     * Check has failed
     */
    CONTROL_FAILED
}
